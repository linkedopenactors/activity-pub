package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.apache.commons.rdfrdf4j.RDF4JIRI;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.mockito.Mockito;

public class TestStoreSaveReadPagedCollection {

	private RDF4J rdf4j;
	private static StoreRepository storeRepository;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	public TestStoreSaveReadPagedCollection() {
		rdf4j = new RDF4J();
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(new VocabContainerDefault(rdf4j), rdf4j, datasetRepository);
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(rdf4j, prefixMapper);
	}
	
	@Test
	void test() {
		String storeOwnerName = "actor_" + UUID.randomUUID().toString();
		IRI owner1 = rdf4j.createIRI("https://example.org/" + storeOwnerName);
		IRI outbox = activityPubObjectIdBuilder.createFromUrl(owner1.toString() + "/outbox");
		
		Store store = getStore(rdf4j, storeRepository, owner1.toString(), storeOwnerName);		
		
		// Create Collection
		int initialSize = 300;
		createCollection(owner1, outbox, store, initialSize);
		assertEquals(initialSize, store.getCollection(owner1, outbox)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList())
				.size());

		// Read Sub Collection Start		
		int pageSize = 10;
		int offset = 0;
		List<IRI> subCollection = store.subCollection(owner1, "outbox", outbox, offset, pageSize)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());
		
		assertEquals(pageSize, subCollection.size());
		for (int i = 0; i < pageSize; i++) {			
			IRI item = createItemIri(owner1, offset + i);
			assertEquals(item, subCollection.get(i));
		}

		
		// Read Sub Collection Middle		
		pageSize = 10;
		offset = 150;
		subCollection = store.subCollection(owner1, "outbox", outbox, offset, pageSize)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());

		
		assertEquals(pageSize, subCollection.size());
		for (int i = 0; i < pageSize; i++) {			
			IRI item = createItemIri(owner1, offset + i);
			assertEquals(item, subCollection.get(i));
		}

		// Read Sub Collection End		
		pageSize = 10;
		offset = 280;
		subCollection = store.subCollection(owner1, "outbox", outbox, offset, pageSize)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());
		
		assertEquals(pageSize, subCollection.size());
		for (int i = 0; i < pageSize; i++) {			
			IRI item = createItemIri(owner1, offset + i);
			assertEquals(item, subCollection.get(i));
		}

	}

	private List<IRI> createCollection(IRI owner1, IRI outbox, Store store, int initialSize) {
		List<IRI> items = new ArrayList<>();
		for (int i = 0; i < initialSize; i++) {			
			items.add(createItemIri(owner1, i));
		}
		store.saveCollection(owner1, outbox, items, "", "Test saveCollection");
		return items;
	}

	private RDF4JIRI createItemIri(IRI owner1, int i) {
		return rdf4j.createIRI(owner1.toString() + "/activity_" + i);
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}

