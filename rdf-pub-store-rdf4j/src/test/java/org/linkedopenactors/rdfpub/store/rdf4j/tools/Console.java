package org.linkedopenactors.rdfpub.store.rdf4j.tools;

import java.io.File;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Console {

	public static void main(String[] args) {
		Console c = new Console();
		c.run();
	}

	private void run() {
		String actor = "http%3A%2F%2Fexample.org%2Factor_7fa42eaa-f84b-468c-b57e-292afcf87c31";
		String subjectString = "https://example.com/9b7d70bc-c2d4-40ce-bbb1-1d52a02b2e54";
		
		File dataDir = new File("/home/fredy/git/rdf-pub/rdf-pub-store-rdf4j/target/rdf4jstore/datadir/repositories/" + actor);
		Repository repo = new SailRepository( new MemoryStore(dataDir) );
		
		try(RepositoryConnection con = repo.getConnection()) {
			IRI subject = Values.iri(subjectString);
			RepositoryResult<Statement> result = con.getStatements(subject, null, null);
			QueryResults.asModel(result);
		}
	}
}
