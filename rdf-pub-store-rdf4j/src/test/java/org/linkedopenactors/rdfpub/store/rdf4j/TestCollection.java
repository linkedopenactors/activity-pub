package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;

public class TestCollection {

	private RDF4J rdf4j;
	private static StoreRepository storeRepository;
	private VocabContainer vocabContainer;
	
	public TestCollection() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer, rdf4j, datasetRepository);
	}
	
	@Test
	void testCollectionAlreadyExists() {
		String storeOwnerName = "actor_" + UUID.randomUUID().toString();
		IRI owner1 = rdf4j.createIRI("https://example.org/" + storeOwnerName);
		Store store = getStore(rdf4j, storeRepository, owner1.toString(), storeOwnerName);
		
		IRI outbox = rdf4j.createIRI(owner1.toString() + "/outbox");
		
		IRI item1 = rdf4j.createIRI(owner1.toString() + "/activity1");
		IRI item2 = rdf4j.createIRI(owner1.toString() + "/activity2");
		List<IRI> items = List.of(item1, item2);
		
		store.saveCollection(owner1, outbox, items, "", "Test saveCollection");
		
		IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
			store.saveCollection(owner1, outbox, Collections.emptyList(), "", "Test saveCollection");
        } );

	    assertTrue(exception.getMessage().startsWith("graph '"));
	    assertTrue(exception.getMessage().endsWith("' already exists!"));
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {		
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}

