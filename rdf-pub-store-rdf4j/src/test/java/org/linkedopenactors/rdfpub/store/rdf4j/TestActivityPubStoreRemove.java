package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdResource;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.mockito.Mockito;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
public class TestActivityPubStoreRemove {

	private ActivityPubStore activityPubStore;
	private RDF4J rdf4j;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private StringToGraphConverter stringToGraphConverter;
	private StoreRepository storeRepository;
	private RdfPubObjectId owner;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 */
	@BeforeEach
	public void setup() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);		
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer, rdf4j, datasetRepository);

		String external = "http://localhost:8081";
		String internal = "urn:rdfpub:";
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn(external);
		when(prefixMapper.getInternaleIriPrefix()).thenReturn(internal);
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(rdf4j, prefixMapper);

		owner = activityPubObjectIdBuilder.createActor();
		Store store = getStore(rdf4j, storeRepository, owner.toString(), owner.getIdentifier());
	

		activityPubObjectFactory = new ActivityPubObjectFactory(vocabContainer, rdf4j, new StringToGraphConverterDefault(rdf4j),
				new GraphToStringConverterDefault(prefixMapper), activityPubObjectIdBuilder, prefixMapper);
		stringToGraphConverter = new StringToGraphConverterDefault(rdf4j);
		activityPubStore = new ActivityPubStoreDefault(vocabContainer, owner, store, activityPubObjectFactory, activityPubObjectIdBuilder);
	}

	@Test
	void test() {
		Actor actor = getSampleActor(owner);
		ActorsStore actorsStore = getActorsStore(actor);
		assertTrue(actorsStore.getOutbox().isEmpty());		

		// Create Note1 & Note1Activity
		RdfPubObjectIdResource note1Id = activityPubObjectIdBuilder.createResource(owner);
		ActivityPubObject transientNote1 = activityPubObjectFactory.create(note1Id);
		transientNote1.setType(Set.of(vocabContainer.vocAs().Note()));
		transientNote1.setContent("This is content");
		
		RdfPubObjectIdActivity activity1Id = activityPubObjectIdBuilder.createActivity(owner);
		Activity transientNote1Activity = activityPubObjectFactory.createActivity(activity1Id);
		transientNote1Activity.setType(Set.of(vocabContainer.vocAs().Activity()));
		transientNote1Activity.addObject(transientNote1);
		log.debug("Note1Activity before saving: " + transientNote1Activity);
		activityPubStore.add(transientNote1Activity, "adding test activity");

		// Create Note2 & Note2Activity
		RdfPubObjectIdResource note2Id = activityPubObjectIdBuilder.createResource(owner);
		ActivityPubObject transientNote2 = activityPubObjectFactory.create(note2Id);
		transientNote2.setType(Set.of(vocabContainer.vocAs().Note()));
		transientNote2.setContent("This is also content");
		
		RdfPubObjectIdActivity activity2Id = activityPubObjectIdBuilder.createActivity(owner);
		Activity transientNote2Activity = activityPubObjectFactory.createActivity(activity2Id);
		transientNote2Activity.setType(Set.of(vocabContainer.vocAs().Activity()));
		transientNote2Activity.addObject(transientNote2);
		log.debug("Note2Activity before saving: " + transientNote2Activity);
		activityPubStore.add(transientNote2Activity, "adding test activity");

		Activity persistentNote1Activity = activityPubStore.findBySubject(activity1Id, 1).orElseThrow().asConvertable().asActivity();
		Activity persistentNote2Activity = activityPubStore.findBySubject(activity2Id, 1).orElseThrow().asConvertable().asActivity();
		
		log.debug("persistentNote1Activity (reread): " + persistentNote1Activity);
		log.debug("persistentNote2Activity (reread): " + persistentNote2Activity);
		
		ActivityPubObject persistentNote1 = persistentNote1Activity.resolveObject().stream().findFirst().orElseThrow();
		ActivityPubObject persistentNote2 = persistentNote2Activity.resolveObject().stream().findFirst().orElseThrow();
		
		persistentNote1.setContent("That is changed content");
		RdfPubObjectIdActivity updateActivity1Id = activityPubObjectIdBuilder.createActivity(owner);
		Activity transientUpdateNote1Activity = activityPubObjectFactory.createActivity(updateActivity1Id);
		transientUpdateNote1Activity.setType(Set.of(vocabContainer.vocAs().Activity()));
		transientUpdateNote1Activity.addObject(persistentNote1);
		activityPubStore.update(transientUpdateNote1Activity, "transientUpdateNote1Activity");
		
		persistentNote2.setContent("That is also changed content");
		RdfPubObjectIdActivity updateActivity2Id = activityPubObjectIdBuilder.createActivity(owner);
		Activity transientUpdateNote2Activity = activityPubObjectFactory.createActivity(updateActivity2Id);
		transientUpdateNote2Activity.setType(Set.of(vocabContainer.vocAs().Activity()));
		transientUpdateNote2Activity.addObject(persistentNote2);
		activityPubStore.update(transientUpdateNote2Activity, "transientUpdateNote2Activity");
		
//		activityPubStore.dump();

		// Delete Note 2
		Activity toDelete = activityPubStore.findBySubject(activity2Id, 1).orElseThrow().asConvertable().asActivity();
		RdfPubBlankNodeOrIRI objectToDelete = toDelete.getObject().stream().findFirst().orElseThrow();
		assertTrue(activityPubStore.findBySubject(objectToDelete).isPresent());		
		assertEquals(12, activityPubStore.countTriples());		
		activityPubStore.remove(objectToDelete, "delete note: " + objectToDelete.getIRIString());
		assertEquals(10, activityPubStore.countTriples());
		assertFalse(activityPubStore.findBySubject(objectToDelete).isPresent());

		// Delete Note 2
		assertTrue(activityPubStore.findBySubject(activity1Id).isPresent());
		activityPubStore.remove(activity2Id, "delete note: " + activity2Id.getIRIString());
		assertEquals(8, activityPubStore.countTriples());
		assertFalse(activityPubStore.findBySubject(objectToDelete).isPresent());

		// Delete Activity 1 & Note 1
		Activity toDelete1 = activityPubStore.findBySubject(activity1Id, 1).orElseThrow().asConvertable().asActivity();
		RdfPubBlankNodeOrIRI objectToDelete1 = toDelete1.getObject().stream().findFirst().orElseThrow();
		activityPubStore.remove(objectToDelete1, "delete");
		activityPubStore.remove(activity1Id, "delete");
		assertEquals(4, activityPubStore.countTriples());		
		activityPubStore.remove(updateActivity1Id, "delete");
		assertEquals(2, activityPubStore.countTriples());
		activityPubStore.remove(updateActivity2Id, "delete");
		assertEquals(0, activityPubStore.countTriples());
//		activityPubStore.dump();
		
		log.debug("sss\n" + activityPubStore.findBySubject(note1Id));
	}

	private Actor getSampleActor(RdfPubBlankNodeOrIRI actorSubject) {
		
//		String inbox = activityPubObjectIdBuilder.createCollection(actorSubject, "inbox").getIRIString();
		String actorString = """
			{
			  "@context": ["https://www.w3.org/ns/activitystreams",
			               {"@language": "ja"}],
			  "type": "Person",
			  "id": "%s",
			  "following": "http://example.org/following.json",
			  "followers": "http://example.org/followers.json",
			  "liked": "urn:rdfpub:liked.json",
			  "inbox": "%s/col/INBOX",
			  "outbox": "%s/col/OUTBOX",
			  "preferredUsername": "kenzoishii",
			  "name": "石井健蔵",
			  "summary": "この方はただの例です",
			  "icon": [
			    "urn:rdfpub:image/165987aklre4"
			  ]
			}				
				""".formatted(actorSubject.getIRIString(), actorSubject.getIRIString(), actorSubject.getIRIString());
		
		ActivityPubObject actor = activityPubObjectFactory.create(actorSubject, stringToGraphConverter.convert(RdfFormat.JSONLD, actorString));
		
		Actor asActor = actor.asConvertable().asActor();
		return asActor;
	}
	
	private ActorsStore getActorsStore(Actor owner) {
		return new ActorsStoreDefault(rdf4j, owner, activityPubStore, activityPubObjectFactory, new ServiceForSavingActivities(vocabContainer, rdf4j, activityPubObjectIdBuilder));
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}
