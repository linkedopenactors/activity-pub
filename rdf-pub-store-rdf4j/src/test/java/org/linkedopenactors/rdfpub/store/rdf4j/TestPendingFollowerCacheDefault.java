package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;

public class TestPendingFollowerCacheDefault extends AbstractTest{

	private PendingFollowerCache pendingFollowerCache;
	private RdfPubObjectIdActor actorsBaseSubject;
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 */
	@BeforeEach
	public void setup2() {
		vocabContainer = new VocabContainerDefault(new RDF4J());	
		RdfPubObjectIdActor actorId = getOwner().getSubject().asActor().orElseThrow();
		
		actorsBaseSubject = actorId.getBaseSubject().asActor().orElseThrow();
		
		RdfPubObjectId storeName = getActivityPubObjectIdBuilder().createPendingFollowerCacheId(actorsBaseSubject).asRdfPubObjectId().orElseThrow();
		
		ActivityPubStore cacheStore = new ActivityPubStoreDefault(vocabContainer, storeName, getStore(), getActivityPubObjectFactory(), getActivityPubObjectIdBuilder());
		
		pendingFollowerCache = new PendingFollowerCacheDefault(vocabContainer, cacheStore);
	}

	@Test
	public void testEmptyGraph() {
		assertTrue(pendingFollowerCache.getPendingFollower().isEmpty());
		RdfPubObjectIdActivity activityId = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity = getActivityPubObjectFactory().create(activityId).asConvertable().asActivity();

		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			pendingFollowerCache.addPendingFollowActivity(followerActivity);
	    });
		
		assertEquals("graph is empty.", exception.getMessage());
	}

	@Test
	public void testWrongActivityType() {
		assertTrue(pendingFollowerCache.getPendingFollower().isEmpty());
		RdfPubObjectIdActivity activityId = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity = getActivityPubObjectFactory().create(activityId).asConvertable().asActivity();
		followerActivity.setType(Set.of(vocabContainer.vocAs().Accept()));

		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			pendingFollowerCache.addPendingFollowActivity(followerActivity);
	    });
		
		assertEquals("not a follow activity.", exception.getMessage());
	}

	@Test
	public void testNoActor() {
		assertTrue(pendingFollowerCache.getPendingFollower().isEmpty());
		RdfPubObjectIdActivity activityId = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity = getActivityPubObjectFactory().create(activityId).asConvertable().asActivity();
		followerActivity.setType(Set.of(vocabContainer.vocAs().Follow()));

		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			pendingFollowerCache.addPendingFollowActivity(followerActivity);
	    });
		
		assertEquals("one actor expected, but is: 0", exception.getMessage());
	}

	@Test
	public void testTwoActors() {
		assertTrue(pendingFollowerCache.getPendingFollower().isEmpty());
		RdfPubObjectIdActivity activityId = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity = getActivityPubObjectFactory().create(activityId).asConvertable().asActivity();
		followerActivity.setType(Set.of(vocabContainer.vocAs().Follow()));
		
		followerActivity.setActor(Set.of(getActivityPubObjectIdBuilder().createActor(), getActivityPubObjectIdBuilder().createActor()));

		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			pendingFollowerCache.addPendingFollowActivity(followerActivity);
	    });
		
		assertEquals("one actor expected, but is: 2", exception.getMessage());
	}

	@Test
	public void testSuccessOneItem() {
		assertTrue(pendingFollowerCache.getPendingFollower().isEmpty());
		RdfPubObjectIdActivity activityId = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity = getActivityPubObjectFactory().create(activityId).asConvertable().asActivity();
		followerActivity.setType(Set.of(vocabContainer.vocAs().Follow()));
		
		RdfPubObjectIdActor actorWhoWantsToFollow = getActivityPubObjectIdBuilder().createActor();
		followerActivity.setActor(Set.of(actorWhoWantsToFollow));
		
		RdfPubObjectIdActor toBeFollowed = getActivityPubObjectIdBuilder().createActor();
		followerActivity.setObject(Set.of(toBeFollowed));

		pendingFollowerCache.addPendingFollowActivity(followerActivity);
		
		assertEquals(1, pendingFollowerCache.getPendingFollower().size());
		assertEquals(actorWhoWantsToFollow.getIRIString(), pendingFollowerCache.getPendingFollower().stream().findFirst().orElseThrow().getIRIString());
		
		pendingFollowerCache.removePendingFollowActivity(actorWhoWantsToFollow);
		assertEquals(0, pendingFollowerCache.getPendingFollower().size());
	}

	@Test
	@Disabled
	// TODO !!!
	public void testSuccessTwoItems() {
		assertTrue(pendingFollowerCache.getPendingFollower().isEmpty());

		RdfPubObjectIdActor actorWhoWantsToFollow1 = getActivityPubObjectIdBuilder().createActor();
		RdfPubObjectIdActor toBeFollowed1 = getActivityPubObjectIdBuilder().createActor();
		
		RdfPubObjectIdActivity activityId1 = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity1 = getActivityPubObjectFactory().create(activityId1).asConvertable().asActivity();
		followerActivity1.setType(Set.of(vocabContainer.vocAs().Follow()));	
		followerActivity1.setActor(Set.of(actorWhoWantsToFollow1));
		followerActivity1.setObject(Set.of(toBeFollowed1));
		
		RdfPubObjectIdActor actorWhoWantsToFollow2 = getActivityPubObjectIdBuilder().createActor();
		RdfPubObjectIdActor toBeFollowed2 = getActivityPubObjectIdBuilder().createActor();
		
		RdfPubObjectIdActivity activityId2 = getActivityPubObjectIdBuilder().createActivity(actorsBaseSubject);
		Activity followerActivity2 = getActivityPubObjectFactory().create(activityId2).asConvertable().asActivity();
		followerActivity2.setType(Set.of(vocabContainer.vocAs().Follow()));	
		followerActivity2.setActor(Set.of(actorWhoWantsToFollow2));
		followerActivity2.setObject(Set.of(toBeFollowed2));
		
		pendingFollowerCache.addPendingFollowActivity(followerActivity1);
		pendingFollowerCache.addPendingFollowActivity(followerActivity2);
		
		assertEquals(2, pendingFollowerCache.getPendingFollower().size());
		List<String> followers = pendingFollowerCache.getPendingFollower().stream()
				.map(RdfPubBlankNodeOrIRI::getIRIString)
				.collect(Collectors.toList());
		
		assertTrue(followers.contains(actorWhoWantsToFollow2.getIRIString()));
		assertTrue(followers.contains(actorWhoWantsToFollow1.getIRIString()));
		
		pendingFollowerCache.removePendingFollowActivity(actorWhoWantsToFollow1);		
		assertEquals(1, pendingFollowerCache.getPendingFollower().size());
		
		assertTrue(followers.contains(actorWhoWantsToFollow2.getIRIString()));
	}
}
