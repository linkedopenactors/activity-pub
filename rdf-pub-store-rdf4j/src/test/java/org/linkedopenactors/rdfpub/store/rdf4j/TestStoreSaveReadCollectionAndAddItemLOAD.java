package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Disabled
public class TestStoreSaveReadCollectionAndAddItemLOAD {

	private RDF4J rdf4j;
	private static StoreRepository storeRepository;

	public TestStoreSaveReadCollectionAndAddItemLOAD() {
		rdf4j = new RDF4J();
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(new VocabContainerDefault(rdf4j), rdf4j, datasetRepository);
	}
	
	@Test
	void test() {
		String storeOwnerName = "actor_" + UUID.randomUUID().toString();
		IRI owner1 = rdf4j.createIRI("https://example.org/" + storeOwnerName);
		Store store = getStore(rdf4j, storeRepository, owner1.toString(), storeOwnerName);
		
		IRI outbox = rdf4j.createIRI(owner1.toString() + "/outbox");
		int initialSize = 1000;
		List<IRI> items = new ArrayList<>();
		for (int i = 0; i < initialSize; i++) {			
			items.add(rdf4j.createIRI(owner1.toString() + "/activity_" + i));
		}

		long startInitialSave = System.currentTimeMillis();
		log.debug("start saving");
		store.saveCollection(owner1, outbox, items, "", "Test saveCollection");
		log.debug("saving initial collection with "+initialSize+" items: " + ((System.currentTimeMillis()-startInitialSave)/1000) + " sec");
		// Read created Collection		
		List<IRI> col = store.getCollection(owner1, outbox)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());

		
		assertEquals(initialSize, col.size());		
		for (int i = 0; i < initialSize; i++) {			
			assertEquals(items.get(i), col.get(i));
		}
		
		String addedBaseUrl = owner1.toString() + "/activity_added_";
		// Add Item
		int addedItemsSize = 100;

		List<IRI> addedItems = new ArrayList<>();
		for (int i = 0; i < addedItemsSize; i++) {
			addedItems.add(rdf4j.createIRI(addedBaseUrl + i));
		}
		addedItems.forEach(toAdd->store.addItem(owner1, outbox, "", toAdd, "Test addItem"));
		col = store.getCollection(owner1, outbox)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());
		assertEquals(initialSize + addedItemsSize, col.size());
		for (int i = 0; i < addedItemsSize; i++) {			
			IRI addedItem = rdf4j.createIRI(addedBaseUrl + i);
			assertEquals(addedItem, col.get(items.size()+i));
		}
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}

