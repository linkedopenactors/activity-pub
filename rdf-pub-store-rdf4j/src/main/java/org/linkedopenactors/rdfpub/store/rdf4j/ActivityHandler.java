package org.linkedopenactors.rdfpub.store.rdf4j;

import org.linkedopenactors.rdfpub.domain.AT;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

public interface ActivityHandler {
	enum Context {
		inbox,outbox;
	}
	void handle( Actor activityOwner, Activity activity, ActivityPubStore activityPubStore, Actor activityPubStoreOwner, RdfPubBlankNodeOrIRI collection);
	boolean isResponsibleFor(Context ctx, AT activityType);
}
