package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.rdf.api.Dataset;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

public class StoreDump {
	
	private VocabContainer vocabContainer;

	public StoreDump(VocabContainer vocabContainer) {
		this.vocabContainer = vocabContainer;
		
	}
	
	public void dump(Dataset dataset, String loggableStoreName) {
		System.out.println("######################################################");
		System.out.println("######################################################");
		System.out.println("### -> dump ("+loggableStoreName+") ###");
		System.out.println("######################################################");
		AtomicLong allTriples = new AtomicLong(0);
		System.out.println("Graphs: " + dataset.getGraphNames().count());
		dataset.getGraphNames().forEach(gn->{
			Graph g = dataset.getGraph(gn).orElseThrow();
			logSubjects(g);
			allTriples.addAndGet(g.stream().count());
			System.out.println("\t######################################################");
			System.out.println("\t# Graph: ("+g.stream().count()+") " + gn.toString());
			System.out.println("\t######################################################");
			System.out.println(toTurtle(g));
		});
		System.out.println("######################################################");
		System.out.println("### <- dump ("+allTriples.get()+")###");
		System.out.println("######################################################");
		System.out.println("######################################################");
	}
	
	
	private void logSubjects(Graph g) {
		Map<String, List<String>> d = new HashMap<>();
		g.stream(null, vocabContainer.vocRdf().type(), null)
				.forEach(t->{
					String objectAsString = t.getObject().toString();
					List<String> items = Optional.ofNullable(d.get(objectAsString)).orElse(new ArrayList<String>());
					items.add(t.getSubject().toString());
					d.put(objectAsString, items);
				});

		d.keySet().forEach(k->{
			System.out.println(convert(k, d.get(k)));
		});
		System.out.println();
	}
	
	private String convert(String type, List<String> subjects) {
		StringBuilder sb = new StringBuilder();
		sb.append("typ: " + type).append(":");
		subjects.forEach(s->sb.append("\n\t").append(s));		
		return sb.toString();
	}
	
	private String toTurtle(Graph graph) {
		RDF4J rdf4j = new RDF4J();
		Model model = new ModelBuilder().build();
		graph.stream().forEach(t->model.add(rdf4j.asStatement(t)));
		return convert(model);
	}

	private String convert(Model asModel) {
		StringWriter sw = new StringWriter();		
		Rio.write(asModel, sw, RDFFormat.TURTLE);
		return sw.toString();
	}
}
