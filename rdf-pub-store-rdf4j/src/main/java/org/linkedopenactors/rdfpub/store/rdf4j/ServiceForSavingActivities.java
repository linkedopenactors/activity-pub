package org.linkedopenactors.rdfpub.store.rdf4j;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class ServiceForSavingActivities {

	private RDF rdf;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private VocabContainer vocabContainer;
	
	public ServiceForSavingActivities(VocabContainer vocabContainer, RDF rdf, ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.vocabContainer = vocabContainer;
		this.rdf = rdf;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
	}

	private void saveActivity(RdfPubBlankNodeOrIRI actorId, Activity activity, RdfPubIRI collection, ActivityPubStore activityPubStore) {
		activity.addType(vocabContainer.vocProv().Activity());		
		Optional.ofNullable(actorId)
			.ifPresent(activity::setWasAssociatedWith);
		// changed 19.11.2024, not sure if baseSubject is correct !!
		// Save activity as object and as item in the collection
		RdfPubIRI activitySubject = (RdfPubIRI)activity.getSubject();
		activityPubStore.addCollectionItem(collection, activitySubject, "needed?");
		activityPubStore.add( activity.asConvertable().isolated(), "needed?");
	}
	
	/**
	 * Save the passed activity and all contained objects.
	 * @param activityOwner
	 * @param activity
	 * @param collection
	 * @param activityPubStore
	 */
	public void saveActivityIncludingObjects(RdfPubBlankNodeOrIRI activityOwnerId, Activity activity, RdfPubIRI collection,
			ActivityPubStore activityPubStore) {
		saveActivity(activityOwnerId, activity, collection, activityPubStore);
		
		// Creates all objects, contained in the activity
		Set<ActivityPubObject> resolveObject = activity.resolveAllOfGraph();
		resolveObject
			.forEach(object->{
				if(object.getSubject().isRdfPubObjectId()) {
					// We use this method also for inbox, therefore we only save internal objects
					createObjectAndAddItToRevisionCollection(activityOwnerId, object, activity.getSubject(), activityPubStore);
				}
			});
	}
	
	/**
	 * Save the passed objects with an internal subject in a collection that managed the revisions of the object.
	 * @param activityOwner The actor who is the initiator of the activity.
	 * @param object The object to save.
	 * @param activitySubject The subject of the activity, that creates the passed object.
	 */
	private void createObjectAndAddItToRevisionCollection( RdfPubBlankNodeOrIRI activityOwnerSubject, ActivityPubObject object, RdfPubBlankNodeOrIRI activitySubject, ActivityPubStore activityPubStore) {
		
		object.addType(vocabContainer.vocProv().Entity());		
		object.setWasAttributedTo(activityOwnerSubject);
		object.setWasGeneratedBy(activitySubject);

		RdfPubObjectId pointerSubject = object.getSubject().asRdfPubObjectId().orElseThrow(()->new IllegalArgumentException("object: " + object.getSubject()));
		log.trace("createObjectAndAddItToRevisionCollection: " + object.getSubject());
		log.trace("pointerSubject: " + pointerSubject);
		
		// Set revision of the object to save
		Optional<ActivityPubObject> prevRevOpt = activityPubStore.findLatestRevision(pointerSubject);
		prevRevOpt
			.ifPresent(prevRev->{
				RdfPubBlankNodeOrIRI subject = prevRev.getSubject();
				object.setWasRevisionOf(subject);
				object.setWasGeneratedBy(activitySubject);
				object.setUpdated(Instant.now());
				object.setSameAs(null);
			});
			
		//----------
		// Was, wenn es das pointer object noch gar nicht gibt? Legen wir dann eins an? bei new actor anscheinend nicht !!??
		//----------
		
		log.trace("prevRevOpt: " + prevRevOpt); 
				
		
		RdfPubObjectId revisionSubject;
		if(pointerSubject.isResource()) {
			revisionSubject = activityPubObjectIdBuilder.createResource(pointerSubject.asResource().orElseThrow());
		} else if(pointerSubject.isActor()) {
			revisionSubject = activityPubObjectIdBuilder.createActor(pointerSubject.asActor().orElseThrow());
		} else if(pointerSubject.isActivity()) {
			revisionSubject = activityPubObjectIdBuilder.createActivity(pointerSubject.asActivity().orElseThrow());
		} else {
			throw new IllegalStateException("did i forgot anything ?? pointerSubject: " + pointerSubject);
		}
//		RdfPubIRI revisionSubject = activityPubObjectIdBuilder.createResource(activityOwner.getSubject().asActor().orElseThrow());
		
//		RdfPubIRI revisionSubject = activityPubObjectIdBuilder.createActor(activityOwner.getSubject()); // TODO macht es sinn hie rdas baseSubject zu nehmen ??
		RdfPubIRI pointerSubjectInternal = pointerSubject; 
		
		// Save the object!
		log.debug("save object with revisionSubject: " + revisionSubject);
//		log.debug("pointerSubject of the saved object is: " + pointerSubject);
		log.debug("pointerSubjectInternal of the saved object is: " + pointerSubjectInternal);
		Set<Triple> objectTriples = replaceSubject(object, revisionSubject);
		
		log.debug(null);
		activityPubStore.add(objectTriples, "needed ??");
		
		// Add's the revisionSubject to the pointer Collection.
		activityPubStore.addCollectionItem(pointerSubjectInternal, revisionSubject, "needed??");
	}

	private Set<Triple> replaceSubject(ActivityPubObject object, IRI newSubject) {
		Set<Triple> newTriples = object.asConvertable().isolated().asGraph().stream()
				.map(t->rdf.createTriple(newSubject, t.getPredicate(), t.getObject())).collect(Collectors.toSet());
		return newTriples;
	}

}
