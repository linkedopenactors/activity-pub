package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdfrdf4j.RDF4J;

class GraphToGraphs {
		
	private RDF4J rdf;
	private Map<BlankNodeOrIRI, Graph> graphs;
	
	public GraphToGraphs(RDF4J rdf) {
		this.rdf = rdf;
		this.graphs = new HashMap<>();
	}
	
	public Map<BlankNodeOrIRI, Graph> convert(Graph g) {
		g.stream().forEach(triple->{
			Graph gr = getGraphForSubject(triple.getSubject());
			gr.add(triple);
		});
		return this.graphs;
	}
	
	private Graph getGraphForSubject(BlankNodeOrIRI subject) {
		if(!this.graphs.containsKey(subject)) {
			this.graphs.put(subject, rdf.createGraph());
		}
		return this.graphs.get(subject);
	}
}
