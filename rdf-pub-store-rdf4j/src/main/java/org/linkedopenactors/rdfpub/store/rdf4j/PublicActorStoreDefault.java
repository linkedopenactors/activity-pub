package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.RDFTerm;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PublicActorStoreDefault implements PublicActorStore {

	private ActivityPubStore activityPubStore;
	private RdfPubObjectIdActor instanceId;
	private RdfPubIRI followers;
	private RdfPubIRI followings;
	private RdfPubIRI inbox;
	private ServiceForSavingActivities serviceForSavingActivities;
	private VocabContainer vocabContainer;
	
	public PublicActorStoreDefault(VocabContainer vocabContainer, RDF rdf, ActivityPubStore activityPubStore, RdfPubObjectIdActor instanceId,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder,
			ServiceForSavingActivities serviceForSavingActivities) {
		this.vocabContainer = vocabContainer;
		this.activityPubStore = activityPubStore;
		this.instanceId = instanceId;
		this.serviceForSavingActivities = serviceForSavingActivities;
		followers = activityPubObjectIdBuilder.createFromUrl(vocabContainer.vocAs().Public().getIRIString()+"/follower");
		followings = activityPubObjectIdBuilder.createFromUrl(vocabContainer.vocAs().Public().getIRIString()+"/following");
		inbox = activityPubObjectIdBuilder.createFromUrl(vocabContainer.vocAs().Public().getIRIString()+"/inbox");
	}

	@Override
	public void addToPersons(RdfPubIRI actorId) {
		activityPubStore.addCollectionItem(vocabContainer.vocRdfPub().personsCollection(), actorId, "n.a.");
	}
	
	@Override
	public void addToApplications(RdfPubIRI actorId) {
		activityPubStore.addCollectionItem(vocabContainer.vocRdfPub().applicationsCollection(), actorId, "n.a.");
	}

	@Override
	public Set<RdfPubIRI> getPersonsCollection() {
		return getCollection(vocabContainer.vocRdfPub().personsCollection()); 
	}

	@Override
	public Set<RdfPubIRI> getApplicationsCollection() {
		return getCollection(vocabContainer.vocRdfPub().applicationsCollection()); 
	}

	@Override
	public Set<RdfPubIRI> getGroupsCollection() {
		return getCollection(vocabContainer.vocRdfPub().groupsCollection()); 
	}

	@Override
	public Set<RdfPubIRI> getOrganizationsCollection() {
		return getCollection(vocabContainer.vocRdfPub().organizationsCollection()); 
	}

	@Override
	public Set<RdfPubIRI> getServicesCollection() {
		return getCollection(vocabContainer.vocRdfPub().servicesCollection()); 
	}

	@Override
	public Set<RdfPubIRI> getActorsCollection() {
		Set<RdfPubIRI> allActors = new HashSet<RdfPubIRI>();
		allActors.addAll(getPersonsCollection());
		allActors.addAll(getApplicationsCollection());
		allActors.addAll(getGroupsCollection());
		allActors.addAll(getOrganizationsCollection());
		allActors.addAll(getServicesCollection());
		return allActors;
	}

	@Override
	public Set<Actor> getActors() {
		return getActorsCollection().stream().map(this::findBySubject)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor)
				.collect(Collectors.toSet());
	}

	@Override
	public OrderedCollectionPage getPersons(Integer pageSize, Integer startIndex) {
		Set<RdfPubIRI> personSubjects = getPersonsCollection();
		// ^^Achtung das wird nicht funktionieren, wenn es änderungen / revisions gibt!		
		return activityPubStore.getCollection(personSubjects, vocabContainer.vocAs().Public(), pageSize, startIndex, 0);
	}

	@Override
	public OrderedCollectionPage getPublicCollection(Integer startIndex, Integer pageSize) {
		List<RdfPubIRI> itemIris = activityPubStore.getCollection(inbox);
		log.debug("getPublicCollection items: " + itemIris.size());
		return activityPubStore.getCollection(itemIris, vocabContainer.vocAs().Public(), pageSize, startIndex, 1);
	}

	@Override
	public OrderedCollectionPage getEmptyCollection(Integer startIndex, Integer pageSize) {
		return activityPubStore.getCollection(Collections.emptyList(), vocabContainer.vocAs().Public(), pageSize, startIndex, 1);
	}

	@Override
	public Instance getInstanceActor() {
		return findBySubject(instanceId).orElseThrow().asConvertable().asInstance();
	}

	@Override
	public Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject) {
		return activityPubStore.findBySubject(subject);
	}

	@Override
	public Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject, int deep) {
		return activityPubStore.findBySubject(subject, deep);
	}

	@Override
	public Set<ActivityPubObject> findByPredicateAndObject(RdfPubIRI predicate, RDFTerm object) {
		return activityPubStore.findByPredicateAndObject(predicate, object);
	}

	@Override
	public Optional<ActivityPubObject> findByTriple(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object) {
		return activityPubStore.findByTriple(subject, predicate, object);
	}
	
	@Override
	public RdfPubBlankNodeOrIRI addToInbox(Activity activity, String logMsg) {
		
		log.debug("->addToInbox (owner: as:Public , activity:" +activity.getSubject()+")");
		serviceForSavingActivities.saveActivityIncludingObjects(vocabContainer.vocAs().Public(), activity, inbox, activityPubStore);
		// TODO replace subject
		log.debug("<-addToInbox ("+activity.getSubject()+")");
		return activity.getSubject();
	}

	@Override
	public void addFollower(RdfPubIRI follower) {
		Optional.ofNullable(followers)
			.ifPresent(f->activityPubStore.addCollectionItem(f, follower, "addFollower"));
	}

	@Override
	public void removeFollower(RdfPubIRI follower) {
		activityPubStore.remove(follower, "removeFollower");		
	}

	@Override
	public List<RdfPubIRI> getFollowers() {
		return Optional.ofNullable(followers)
				.map(activityPubStore::getCollection)
				.orElse(Collections.emptyList());
	}

	@Override
	public List<RdfPubIRI> getFollowings() {
		return Optional.ofNullable(followings)
				.map(activityPubStore::getCollection)
				.orElse(Collections.emptyList());
	}

	private Set<RdfPubIRI> getCollection(RdfPubIRI collection) {
		return new HashSet<>(activityPubStore.getCollection(collection));
	}

	@Override
	public void dump() {
		activityPubStore.dump();
	}
}
