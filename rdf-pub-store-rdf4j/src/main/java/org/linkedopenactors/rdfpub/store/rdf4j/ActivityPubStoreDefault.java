package org.linkedopenactors.rdfpub.store.rdf4j;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.store.Store;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActivityPubStoreDefault implements ActivityPubStore {

	private Store store;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private RdfPubIRI storeOwnerSubject;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private VocabContainer vocabContainer;
	
	public ActivityPubStoreDefault(VocabContainer vocabContainer, RdfPubIRI storeOwnerSubject, Store store,
			ActivityPubObjectFactory activityPubObjectFactory, ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.vocabContainer = vocabContainer;
		this.storeOwnerSubject = storeOwnerSubject;
		this.store = store;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
	}
	
	@Override
	public void add(ActivityPubObject ao1, String logMsg) {
		store.add(storeOwnerSubject, ao1.asGraph(), logMsg);
		log.debug("add(owner:"+getStoreOwner4Logging()+", ActivityPubObject)");
	}
	
	@Override
	public void add(Set<Triple> newTriples, String logMsg) {
		store.add(storeOwnerSubject, newTriples, logMsg);
		log.debug("add(owner:"+getStoreOwner4Logging()+", someTriples)");
	}

	@Override
	public void update(ActivityPubObject ao1, String logMsg) {
		Set<BlankNodeOrIRI> subjects = ao1.asGraph().stream().map(Triple::getSubject).collect(Collectors.toSet());
		store.remove(storeOwnerSubject, subjects, logMsg);
		store.add(storeOwnerSubject, ao1.asGraph(), logMsg);
	}

	@Override
	public void addCollectionItem(RdfPubIRI collection, RdfPubIRI item, String logMsg) {
		log.debug("addCollectionItem(["+logMsg+"] - owner:"+getStoreOwner4Logging()+", "+collection+", item: "+ item + ")");
		store.addItem(storeOwnerSubject, collection, "", item, logMsg);
	}

	@Override
	public void removeCollectionItem(RdfPubIRI collection, RdfPubIRI item, String logMsg) {
		log.debug("removeCollectionItem(["+logMsg+"] - owner:"+getStoreOwner4Logging()+", "+collection+", item: "+ item + ")");
		store.removeItem(storeOwnerSubject, collection, "", item, logMsg);
	}

	@Override
	public void remove(RdfPubBlankNodeOrIRI objectToDelete, String logMsg) {
		store.remove(storeOwnerSubject, Set.of(objectToDelete), logMsg);
	}

	@Override
	public void remove(Set<RdfPubIRI> objectsToDelete, String logMsg) {
		Set<BlankNodeOrIRI> converted = objectsToDelete.stream().map(IRI.class::cast).collect(Collectors.toSet());
		store.remove(storeOwnerSubject, converted, logMsg);
	}

	@Override
	public String queryTuple(String query) {
		return store.queryTuple(query);
	}

	@Override
	public List<RdfPubIRI> getCollection(RdfPubIRI collection) {
		return store.getCollection(storeOwnerSubject, collection)
			.map(PersistableRdfPubCollection::getItems)			
			.orElse(Collections.emptyList())
			.stream()
				.map(i->activityPubObjectIdBuilder.createFromUrl(i.getIRIString()))
				.collect(Collectors.toList());
	}
	
	@Override
	public boolean existsCollection(RdfPubIRI collection) {
		return store.getCollection(storeOwnerSubject, collection).isPresent();
	}

	@Override
	public void createCollection(RdfPubIRI collection, String humanReadableName) {
		store.saveCollection(storeOwnerSubject, collection, Collections.emptyList(), humanReadableName, "create collection " + humanReadableName);
	}
	
	@Deprecated // move to UcReadCollection !
	@Override
	public OrderedCollectionPage getCollection(Collection<RdfPubIRI> irisToResolve, RdfPubIRI namedGraphToUse,
			Integer pageSize, Integer startIndex, Integer deep) {
		Set<RdfPubIRI> irisToResolvePaged = new HashSet<>();
		
		int max = startIndex+pageSize > irisToResolve.size() ? irisToResolve.size() : startIndex+pageSize;
		
		List<RdfPubIRI> irisToResolveList = new ArrayList<>(irisToResolve);
		for (int i = startIndex; i < max; i++) {
			irisToResolvePaged.add(irisToResolveList.get(i));
		}
		
		Set<ActivityPubObject> activities = findAll(irisToResolvePaged.stream()
					.map(RdfPubIRI.class::cast)
					.collect(Collectors.toSet()), deep).stream()
				.sorted((o1, o2) -> compare(o1,o2))
				.collect(Collectors.toSet());

		String subjectOfOrderedCollection = String.format("%s?pageSize=%s&startIndex=%s", namedGraphToUse.toString(), pageSize, startIndex);
		ActivityPubObject activityPubObject = activityPubObjectFactory.create(activityPubObjectIdBuilder.createFromUrl(subjectOfOrderedCollection));
		OrderedCollectionPage orderedCollectionPage = activityPubObject.asConvertable().asOrderedCollectionPage();
		
		orderedCollectionPage.setSummary("collection: " +namedGraphToUse.toString() + "; pageSize: " + pageSize + "; startIndex: " + startIndex);
		orderedCollectionPage.setPartOf(namedGraphToUse);
		orderedCollectionPage.setTotalItems(activities.size());
		orderedCollectionPage.setType(Set.of(vocabContainer.vocAs().OrderedCollectionPage()));
		activities.forEach(orderedCollectionPage::addObject);
		
		return orderedCollectionPage;
	}

	private int compare(ActivityPubObject o1, ActivityPubObject o2) {
		Instant _1970 = Instant.ofEpochMilli(0);
		Instant o1p = o1.getPublished().orElse(_1970);
		Instant o2p = o2.getPublished().orElse(_1970);
		return o1p.compareTo(o2p);
	}

	@Override
	public Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject) {
		return findBySubject(subject, 0);
	}
		
	@Override
	public Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject, int deep) {
		Optional<ActivityPubObject> result = store.find(storeOwnerSubject, subject, deep)
				.map(graph->activityPubObjectFactory.create(subject, graph));
		log.trace("findBySubject(owner:"+getStoreOwner4Logging()+", subject:"+subject+") -> found: " + result.isPresent() + getName(result));
		return result;
	}

	private String getName(Optional<ActivityPubObject> result) {
		return " (" + result.flatMap(ActivityPubObject::getName).orElse("unnamed") + ")";
	}

	@Override
	public Optional<ActivityPubObject> findByTriple(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate,RDFTerm object) {
		Optional<Graph> find = store.find(storeOwnerSubject, subject, predicate, object);
		if(find.isEmpty()) {
			log.debug("findByTriple(owner:"+getStoreOwner4Logging()+", subject:"+subject+", predicate:"+predicate+",RDFTerm:"+object+") -> found: false");
			return Optional.empty();
		} else {			
			Optional<ActivityPubObject> ao = find
				.map(graph->activityPubObjectFactory.create(subject, graph));
			log.debug("findByTriple(owner:"+getStoreOwner4Logging()+", subject:"+subject+", predicate:"+predicate+",RDFTerm:"+object+") -> found: true" + getName(ao));
			return ao;
		}
	}

	@Override
	public Set<RdfPubIRI> findIris(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object) {
		Optional<Graph> find= store.find(storeOwnerSubject, subject, predicate, object);
		if(find.isPresent()) {
			return find.orElseThrow().stream()
				.map(Triple::getSubject)
				.filter(boi-> boi instanceof IRI)
				.map(IRI.class::cast)
				.map(IRI::getIRIString)
				.map(activityPubObjectIdBuilder::createFromUrl)
				.collect(Collectors.toSet());
		} else {
			return Collections.emptySet();
		}
	}

	@Override
	public Set<ActivityPubObject> findByPredicateAndObject(RdfPubIRI predicate, RDFTerm object) {
		Map<IRI, Graph> graphs = store.find(storeOwnerSubject, predicate, object);
		Map<RdfPubBlankNodeOrIRI, Graph> convertedGraphs = convert(graphs);
		Set<ActivityPubObject> result;
		if(convertedGraphs.isEmpty()) {
			result = Collections.emptySet();
		} else {			
			Set<ActivityPubObject> aoSet = convertedGraphs.entrySet().stream()
					.map(entry->activityPubObjectFactory.create(entry.getKey(), entry.getValue()))
					.collect(Collectors.toSet());
			result = aoSet;
		}
		log.debug("findBySubjectAndObject(predicate:"+predicate+",RDFTerm:"+object+") -> found: " + result.size());
		return result;
	}

	private Map<RdfPubBlankNodeOrIRI, Graph> convert(Map<IRI, Graph> in) {
        Map<RdfPubBlankNodeOrIRI, Graph> y = in.entrySet().stream()
                .map(entry -> Map.entry(toRdpPubIri(entry.getKey()), entry.getValue()))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        return y;    
	}

	private RdfPubBlankNodeOrIRI toRdpPubIri(IRI key) {
		return activityPubObjectIdBuilder.createFromUrl(key.getIRIString());
	}
	
	
	
	@Override
	public Set<ActivityPubObject> findAll(Set<RdfPubIRI> subjectsParam) {
		return findAll(subjectsParam, 0);
	}
	
	@Override
	public Set<ActivityPubObject> findAll(Set<RdfPubIRI> subjectsParam, int deep) {
		List<BlankNodeOrIRI> subjects = subjectsParam.stream()
				.map(IRI.class::cast)				
				.collect(Collectors.toList());		
		
		Set<ActivityPubObject> collection = store.findAll(storeOwnerSubject, subjects, deep).entrySet().stream()
			.filter(e->e.getKey() instanceof IRI)
			.map(e->activityPubObjectFactory.create(activityPubObjectIdBuilder.createFromUrl(e.getKey().toString()), e.getValue()))
			.collect(Collectors.toSet());
		log.debug("findAll(owner:"+getStoreOwner4Logging()+", subjectsParam:"+subjectsParam+", deep:"+deep+") -> found: " + collection.size());
		return collection;
	}

	@Override
	public Optional<ActivityPubObject> findLatestRevision(RdfPubIRI pointerSubject) {
		Optional<ActivityPubObject> result = store.findLatestRevisionSubject(storeOwnerSubject, pointerSubject)
				.map(latestSubjectIri->activityPubObjectIdBuilder.createFromUrl(latestSubjectIri.getIRIString()))
				.flatMap(this::findBySubject);
		log.debug("findLatestRevision(pointerSubject:"+pointerSubject+") -> found: " + getName(result) + result.map(f->f.getSubject()));
		return result;
	}

	@Override
	public Optional<ActivityPubObject> findWholeGraph(RdfPubIRI subject) {
		return store.find(storeOwnerSubject)
				.map(graph->activityPubObjectFactory.create(subject, graph));
	}

	@Override
	public RdfPubIRI getOwner() {
		return storeOwnerSubject;
	}

	@Override
	public Graph dump(RdfPubIRI graphName) {
		Graph graph = store.find(graphName).orElseThrow();
		return graph;
	}
	
	@Override
	public void dump() {
		store.dump();
	}

	/**
	 * @return {@link Set} of all known graphNames.
	 */
	@Override
	public Set<BlankNodeOrIRI> getGraphNames() {
		return store.getGraphNames();
	}
	
	/**
	 * @return number of all triples in the store.
	 */
	public long countTriples() {
		return store.countTriples();
	}
	
	/**
	 * @param graphName
	 * @return number of all triples in the passed graph.
	 */
	public long countTriples(BlankNodeOrIRI graphName) {
		return store.countTriples(graphName);
	}

	
	private String getStoreOwner4Logging() {
			return storeOwnerSubject.getIRIString();
	}
}
