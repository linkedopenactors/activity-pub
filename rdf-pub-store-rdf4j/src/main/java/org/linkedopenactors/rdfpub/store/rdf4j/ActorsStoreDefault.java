package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.RDFTerm;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;

import io.sentry.spring.jakarta.tracing.SentrySpan;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ActorsStoreDefault implements ActorsStore {

	protected ActivityPubStore activityPubStore;
	private ServiceForSavingActivities serviceForSavingActivities;

	private RdfPubObjectIdActor storeOwnerId;	
	private String storeOwnerPreferredUsername;
	private RdfPubObjectIdCollection outbox;
	private RdfPubObjectIdCollection inbox;
	private RdfPubObjectIdCollection followers;
	private RdfPubObjectIdCollection following;
	
	public ActorsStoreDefault(RDF rdf, Actor storeOwner, ActivityPubStore activityPubStore,
			ActivityPubObjectFactory activityPubObjectFactory, ServiceForSavingActivities activityHandler) {
		this.serviceForSavingActivities = activityHandler;
		
		storeOwnerId = storeOwner.getSubject().asActor().orElseThrow();
		storeOwnerPreferredUsername = storeOwner.getPreferredUsername().orElse(storeOwner.getSubject().getIRIString());
		outbox = storeOwner.getOutbox().asCollection().orElseThrow();
		inbox = storeOwner.getInbox().asCollection().orElseThrow();
		followers = storeOwner.getFollowers().asCollection().orElse(null);
		following = storeOwner.getFollowing().asCollection().orElse(null);
		
		this.activityPubStore = activityPubStore;
	}
	
	protected Set<RdfPubBlankNodeOrIRI> getCollection(RdfPubObjectIdCollection collection) {
		return new HashSet<>(activityPubStore.getCollection(collection));
	}
	
	@SentrySpan
	@Override
	public RdfPubObjectIdActivity addToOutbox(Activity activity, String logMsg) {
		return addToOutbox(storeOwnerId, activity, logMsg);
	}

	private RdfPubObjectIdActivity addToOutbox(RdfPubObjectIdActor activityOwnerId, Activity activity, String logMsg) {
		RdfPubObjectIdActivity activitySubject = activity.getSubject().asActivity().orElseThrow();
//		RdfPubObjectIdActor activityOwnerId = activityOwner.getSubject().asActor().orElseThrow();
		serviceForSavingActivities.saveActivityIncludingObjects(activityOwnerId.getExternalSubject(), activity, outbox,
				activityPubStore);
		// TODO add complete activity to auditlog ??? which should be seperate from the business object repo eg. graph !!
		// maybe actor.getsubject() + "/auditlog" or a completely seperated db, which can be used as eventStream ?!
		
		// TODO replace subject
		return activitySubject;
	}

	@Override
	public RdfPubBlankNodeOrIRI addToInbox(Activity activity, String logMsg) {
		return addToInbox(storeOwnerId, activity, logMsg);
	}

	private RdfPubBlankNodeOrIRI addToInbox(RdfPubObjectIdActor activityOwnerId, Activity activity, String logMsg) {
		log.debug("->addToInbox (owner: " + storeOwnerPreferredUsername + ", activity:" +activity.getSubject()+")");
		serviceForSavingActivities.saveActivityIncludingObjects(activityOwnerId.getExternalSubject(), activity, inbox,
				activityPubStore);
		// TODO replace subject
		log.debug("<-addToInbox ("+activity.getSubject()+")");
		return activity.getSubject();
	}

	@Override
	public void addFollower(RdfPubIRI activityIri) {
		Optional.ofNullable(followers)
			.ifPresent(f->activityPubStore.addCollectionItem(f, activityIri, "addFollower"));
	}

	@Override
	public void removeFollower(RdfPubIRI follower) {
		Optional.ofNullable(followers)
			.ifPresent(f->activityPubStore.removeCollectionItem(f, follower, "removeFollower"));
	}

	@Override
	public void addFollowing(RdfPubIRI activityIri) {
		Optional.ofNullable(following)
			.ifPresent(f->activityPubStore.addCollectionItem(f, activityIri, "addFollowing"));
	}

	@Override
	public void removeFollowing(RdfPubIRI following) {
		Optional.ofNullable(following)
			.ifPresent(f->activityPubStore.removeCollectionItem(f, following, "removeFollowing"));
	}

	public OrderedCollectionPage getInbox(Integer startIndex, Integer pageSize) {
		List<RdfPubIRI> col = activityPubStore.getCollection(inbox);
		return getCollection(col, inbox, pageSize, startIndex);
	}

	@Override
	public List<RdfPubIRI> getOutbox() {
		return activityPubStore.getCollection(outbox);
	}

	public OrderedCollectionPage getOutbox(Integer startIndex, Integer pageSize) {
		List<RdfPubIRI> col = activityPubStore.getCollection(outbox);
		return getCollection(col, outbox, pageSize, startIndex);
	}

	@Override
	public List<RdfPubIRI> getCollection(RdfPubObjectIdCollection ci, Integer startIndex, Integer pageSize) {
		return activityPubStore.getCollection(ci);
	}

	@Override
	public List<RdfPubIRI> getFollowers() {
		return Optional.ofNullable(followers)
				.map(activityPubStore::getCollection)
				.orElse(Collections.emptyList());
	}

	@Override
	public List<RdfPubIRI> getFollowings() {
		return Optional.ofNullable(following)
				.map(activityPubStore::getCollection)
				.orElse(Collections.emptyList());
	}

	@Override
	public Graph dump() {
		Graph graph = activityPubStore.dump(storeOwnerId.getBaseSubject());
		GraphDump gd = new GraphDump();
		gd.dumpToLog(graph);
		return graph;
	}

	@Override
	public Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject) {
		return activityPubStore.findBySubject(subject);
	}

	@Override
	public Optional<ActivityPubObject> find(RdfPubBlankNodeOrIRI subject, int deep) {
		return activityPubStore.findBySubject(subject, deep);
	}

	@Override
	public Optional<ActivityPubObject> find(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object) {
		return activityPubStore.findByTriple(subject, predicate, object);
	}

	@Override
	public Set<ActivityPubObject> find(RdfPubIRI predicate, RDFTerm object) {
		return activityPubStore.findByPredicateAndObject(predicate, object);
	}

	@Override
	public Set<ActivityPubObject> findAll(Set<RdfPubIRI> subject) {
		return activityPubStore.findAll(subject);
	}

	@Override
	public Set<ActivityPubObject> findAll(Set<RdfPubIRI> subject, int deep) {
		return activityPubStore.findAll(subject, deep);
	}

	@Override
	public String toString() {
		return "ActorsStoreDefault [storeOwner=" + storeOwnerId.getExternalSubject() + "]";
	}

	@Override
	public String queryTuple(String query) {
		return activityPubStore.queryTuple(query);
	}

	/**
	 * Resolves the objects to the passed {@link IRI}'s. If there are referenced objets they are also resolved!
	 * @param irisToResolve The subjects of the objects, that should be resolved.
	 * @param namedGraphToUse The graph that should be used to search for objects
	 * @param pageSize 
	 * @param startIndex
	 * @return {@link OrderedCollectionPage}
	 */
	
	protected OrderedCollectionPage getCollection(Collection<RdfPubIRI> irisToResolve,
			RdfPubIRI namedGraphToUse, Integer pageSize, Integer startIndex) {
		return getCollection(irisToResolve, namedGraphToUse, pageSize, startIndex, 1);
	}
	
	/**
	 * Resolves the objects to the passed {@link IRI}'s. If there are referenced objets they are also resolved!
	 * @param irisToResolve The subjects of the objects, that should be resolved.
	 * @param namedGraphToUse The graph that should be used to search for objects
	 * @param pageSize 
	 * @param startIndex
	 * @param deep up to which hierarchy level should object references be resolved ?
	 * @return {@link OrderedCollectionPage}
	 */
	protected OrderedCollectionPage getCollection(Collection<RdfPubIRI> irisToResolve,
			RdfPubIRI namedGraphToUse, Integer pageSize, Integer startIndex, Integer deep) {
		return activityPubStore.getCollection(irisToResolve, namedGraphToUse, pageSize, startIndex, deep);
	}

	@Override
	public Set<BlankNodeOrIRI> getGraphNames() {
		return activityPubStore.getGraphNames();
	}
}
