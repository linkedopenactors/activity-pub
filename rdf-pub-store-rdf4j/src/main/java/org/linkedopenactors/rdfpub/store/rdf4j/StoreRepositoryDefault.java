package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.apache.commons.rdfrdf4j.RDF4JDataset;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class StoreRepositoryDefault implements StoreRepository {

	private DatasetRepository datasetRepository;
	private RDF4J rdf;
	private VocabContainer vocabContainer;
	
	public StoreRepositoryDefault(VocabContainer vocabContainer, RDF4J rdf, DatasetRepository datasetRepository) {
		this.vocabContainer = vocabContainer;
		this.rdf = rdf;
		this.datasetRepository = datasetRepository;
	}

	@Override
	public Optional<org.linkedopenactors.rdfpub.store.Store> findStore(IRI datasetId, String loggableStoreName) {
		return datasetRepository.findDataset(encode(datasetId))
				.map(ds->new StoreDefault(vocabContainer, rdf, (RDF4JDataset)ds, loggableStoreName));
	}

	@Override
	public org.linkedopenactors.rdfpub.store.Store createStore(IRI datasetId, String loggableStoreName) {
		return new StoreDefault(vocabContainer, rdf, (RDF4JDataset)datasetRepository.findDataset(encode(datasetId))
			.orElse((RDF4JDataset)datasetRepository.createDataset(encode(datasetId))), loggableStoreName);
	}

	private String encode(IRI storeName) {
		try {
			String encoded = URLEncoder.encode(storeName.toString(), StandardCharsets.UTF_8.toString());
			log.trace("encode(storeName: "+storeName+") -> " + encoded);
			return encoded;
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("Unable to encode storeName: " + storeName, e);
		}
	}
	
	@Override
	public void shutdown() {
		datasetRepository.shutdown();		
	}	
}
