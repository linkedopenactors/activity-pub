package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GraphDump {

	private Map<BlankNodeOrIRI, List<String>> subjects;
	
	public void dumpToLog(Graph graph) {
		subjects = new HashMap<>();
		
		graph.stream().forEach(t-> {
			String prop = t.getPredicate() + " - " + t.getObject();
			addProperty(t.getSubject(), prop.toString());
		});		
		
		subjects.keySet().forEach(subject -> {
			log.debug("---------------------------");
			log.debug(subject.toString());
			subjects.get(subject).forEach(prop -> log.debug("\t" + prop));
		});
	}
	
	private void addProperty(BlankNodeOrIRI subject, String prop) {
		List<String> props = subjects.get(subject);
		props = props == null ? new ArrayList<>() : props;
		props.add(prop);
		subjects.put(subject, props);
	}
}
