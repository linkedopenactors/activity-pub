package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.Graph;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Scope("rdfpubRequest")
@Slf4j
public class PendingFollowerCacheDefault implements PendingFollowerCache {
	
	private ActivityPubStore store;
	private VocabContainer vocabContainer;

	public PendingFollowerCacheDefault(VocabContainer vocabContainer, ActivityPubStore store) {
		this.vocabContainer = vocabContainer;
		this.store = store;
	}

	@Override
	public void addPendingFollowActivity(Activity activity) {
		log.trace("->addPendingFollowActivity()");
		log.trace("activity: \n" + activity);
		Graph asGraph = activity.asGraph();
		if(asGraph.size()==0) {
			throw new IllegalArgumentException("graph is empty.");
		}
		if(!activity.getTypes().contains(vocabContainer.vocAs().Follow())) {
			throw new IllegalArgumentException("not a follow activity.");
		}
		if(activity.getActor().size()!=1) {
			throw new IllegalArgumentException("one actor expected, but is: " + activity.getActor().size());
		}
		store.add(activity, "adding PendingFollowActivity");
		log.trace("<-addPendingFollowActivity()");
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getPendingFollower() {
		log.trace("->getPendingFollower()");
		Set<RdfPubIRI> graphBySubject = store.findIris(null, vocabContainer.vocRdf().type(), vocabContainer.vocAs().Follow());
		
		Set<Optional<ActivityPubObject>> map = graphBySubject.stream()
				.map(id->store.findBySubject(id, 1))
				.collect(Collectors.toSet());
		
		Set<RdfPubBlankNodeOrIRI> result = map.stream()
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActivity)
				.map(act->act.getActor().stream().findFirst())
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toSet());
		log.trace("<-getPendingFollower()");
		return result;
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getPendingFollowerActivityIris() {
		log.trace("->getPendingFollower()");
		Set<RdfPubBlankNodeOrIRI> result = getPendingFollowerActivities().stream()
				.map(act->act.getSubject())
				.collect(Collectors.toSet());
		
		log.trace("<-getPendingFollower()");
		return result;
	}

	@Override
	public Set<Activity> getPendingFollowerActivities() {
		log.trace("->getPendingFollower()");
		Set<RdfPubIRI> followActivitySubjects = store.findIris(null, vocabContainer.vocRdf().type(), vocabContainer.vocAs().Follow());
		Set<Activity> result = getPendingFollowerActivities(followActivitySubjects).stream()
				.collect(Collectors.toSet());		
		log.trace("<-getPendingFollower()");
		return result;
	}

	@Override
	public Set<Activity> getPendingFollowerActivities(Set<RdfPubIRI> iris) {
		log.trace("->getPendingFollowerActivities()");
		Set<Activity> result = iris.stream()
				.map(id->store.findBySubject(id, 1))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActivity)
				.collect(Collectors.toSet());
		log.trace("<-getPendingFollowerActivities()");
		return result;
	}

	@Override
	public void removePendingFollowActivity(RdfPubBlankNodeOrIRI actorId) {
		Set<RdfPubIRI> graphBySubject = store.findIris(null, vocabContainer.vocRdf().type(), vocabContainer.vocAs().Follow());
		Set<Optional<ActivityPubObject>> map = graphBySubject.stream()
				.map(id->store.findBySubject(id, 1))
				.collect(Collectors.toSet());
		Set<Activity> activity = map.stream()
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActivity)
//				.filter(act->act.getActor().stream().map(RdfPubIRI::getIRIString).collect(Collectors.toSet()).contains(actorId.getIRIString()))
				.collect(Collectors.toSet());
		
		activity.forEach(activityToBeDeleted->{
			store.remove(activityToBeDeleted.getObject().stream().findFirst().orElseThrow(), "delete object of " + activityToBeDeleted);
			store.remove(activityToBeDeleted.getSubject(), "delete activity: " + activityToBeDeleted.getSubject());
		});
	}

	@Override
	public boolean contains(Activity activity) {
		return contains(activity.getSubject().getIRIString());
	}

	@Override
	public boolean contains(String activitySubject) {
		Set<RdfPubBlankNodeOrIRI> pendingFollowerActivityIris = getPendingFollowerActivityIris();
		return pendingFollowerActivityIris.stream()
				.map(RdfPubBlankNodeOrIRI::getIRIString)
				.filter(id -> id.equals(activitySubject))
				.count() > 0;
	}
}
