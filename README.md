# rdf-pub - actvity-pub server implementation

The documentation is available as gitlab page. The url pattern is  

https://linkedopenactors.gitlab.io/rdf-pub

*NOTE:*
Currently there is no solution for different branches.    
The documentation of the last built branch is always displayed! Please pay attention to the 'version' under the heading !  
If someone needs a documentation of a certain version, please contact me!

## API
* http://localhost:8081/swagger-ui/index.html
* http://localhost:8081/v3/api-docs/

## Contributing

Thank you for considering contributing to rdf-pub.

You can contribute in the following ways:

- Finding and reporting bugs
- Contributing code to rdf-pub by fixing bugs or implementing features
- Improving the documentation

### Bug reports

Bug reports and feature suggestions must use descriptive and concise titles and be submitted to Gitlab Issues. Please use the search function to make sure that you are not submitting duplicates, and that a similar report or request has not already been resolved or rejected.

### Documentation

Is located in a seperate [asciidoc project](https://gitlab.com/linkedopenactors/rdf-pub/-/tree/develop/rdf-pub-documentation) contributions follow the same process as code contribution.

### Code

We are currently follow the [gitflow branching model](https://nvie.com/posts/a-successful-git-branching-model/). 
To make life easy for us, we use [gitflow-maven-plugin](https://github.com/aleksandr-m/gitflow-maven-plugin) to support the gitflow process.

#### develop-branch
The main development branch is develop. Each commit triggers a the ci pipeline

1. **maven-all:** compile the code, run the unit-tests, generate the maven site including javadoc etc., 
2. **maven-sonatype-deploy:** pushes the libraries into maven-central
3. **build-push-image:** build the docker image and pushes it into the gitlab registry.
    - The development environment checks for new docker images with the tag 'latest' at regular intervals. If there is a newer one, the deployment process of the newest image is started. You can check this on the [info page](https://rdfpub.test.opensourceecology.de/actuator/info).
4. **integrationtest:** run the integrationtest against the latest docker image.

#### feature-branch
Due to the process described above, development should not be done against the *develop* branch. To develop bug fixes or features, a feature branch is created. After a merge-request is reviewed and accepted, it is merged into the develop branch. It's a good idea to do a rebase before.

#### main-branch
Each time we 'release' the develop branch, a tag is created and the develop branch is merged into the master branch.
In the future this should trigger a deployment to the production environment!

#### release-branch
Currently we do not use release branches, but it's possible to use them, if necessary.

#### hotfix-branches
Currently we do not use hotfix branches, but it's possible to use them, if necessary.
