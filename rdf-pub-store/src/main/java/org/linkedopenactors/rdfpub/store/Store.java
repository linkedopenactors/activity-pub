package org.linkedopenactors.rdfpub.store;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;

/**
 * An rdf object store.
 */
public interface Store {

	/**
	 * Adds all Triples of the passed graph to the graph with the passed graphName.
	 * @param graph
	 * @param graphName
	 * @param logMsg
	 */
	void add(BlankNodeOrIRI graphName, Graph graph, String logMsg);

	/**
	 * Adds all Triples to the graph with the passed graphName.
	 * @param graphName
	 * @param newTriples
	 * @param logMsg
	 */
	void add(BlankNodeOrIRI graphName, Set<Triple> newTriples, String logMsg);
	
	/**
	 * Removes all triples with the passed subjects.
	 * @param graphName
	 * @param subjects
	 * @param logMsg
	 */
	void remove(BlankNodeOrIRI graphName, Set<BlankNodeOrIRI> subjects, String logMsg);
	
	/**
	 * Try to find a {@link Graph} with the passed subject/id and extracts the Triples matching the passed subject. 
	 * @param graphName The name of the namedGraph.
	 * @param subject The id of the namedGraph.
	 * @return Triples matching the passed subject contained in the {@link Graph} for the passed graphName.
	 */
	Optional<Graph> find(BlankNodeOrIRI graphName, BlankNodeOrIRI subject);
	
	/**
	 * Try to find a {@link Graph} with the passed subject/id and extracts the Triples matching the passed subject. 
	 * @param graphName The name of the namedGraph.
	 * @param subject The id of the namedGraph.
	 * @param deep up to which hierarchy level should object references be resolved ? 
	 * @return Triples matching the passed subject contained in the {@link Graph} for the passed graphName.
	 */
	Optional<Graph> find(BlankNodeOrIRI graphName, BlankNodeOrIRI subject, int deep);
	
	/**
	 * Try to find a {@link Graph} with the passed subject/predicate.
	 * @param graphName The name of the namedGraph.
	 * @param subject The subject to search for.
	 * @param predicate The predicate to search for.
	 * @param object The object to search for.
	 * @return The {@link Graph}
	 */
	Optional<Graph> find(BlankNodeOrIRI graphName, BlankNodeOrIRI subject, IRI predicate, RDFTerm object);
	
	/** 
	 * Try to find {@link Graph}s with the passed predicate. 
	 * @param graphName The name of the namedGraph.
	 * @param predicate The predicate to search for.
	 * @param object The object to search for.
	 * @return Graphs matching the passed predicate/object ordered by subject.
	 */
	Map<IRI, Graph> find(BlankNodeOrIRI graphName, IRI predicate, RDFTerm object);
	
	/**
	 * Find object with the passed subjects in the passed graph.
	 * @param graphName The name of the namedGraph.
	 * @param subject The subjects to search
	 * @return Map With Graphs by Subject.
	 */
	Map<IRI, Graph> findAll(BlankNodeOrIRI graphName, List<BlankNodeOrIRI> subject);
	
	/**
	 * Find object with the passed subjects in the passed graph and resolve dependencies up to the passed depth.
	 * @param graphName The name of the namedGraph.
	 * @param subject The subjects to search
	 * @param deep up to which hierarchy level should object references be resolved ?
	 * @return Map With Graphs by Subject.
	 */
	Map<IRI, Graph> findAll(BlankNodeOrIRI graphName, List<BlankNodeOrIRI> subject, int deep);
	
	/**
	 * Gets the subject of the latest revision.
	 * @param graphName
	 * @param collectionSubject
	 * @return The subject of the latest revision.
	 */
	Optional<IRI> findLatestRevisionSubject(BlankNodeOrIRI graphName, IRI collectionSubject);
	
	/**
	 * Try to find a {@link Graph} with the passed graphName. 
	 * @param graphName The name of the namedGraph.
	 * @return {@link Graph} for the passed graphName.
	 */
	Optional<Graph> find(BlankNodeOrIRI graphName);

	/**
	 * @return {@link Set} of all known graphNames.
	 */
	Set<BlankNodeOrIRI> getGraphNames();
	
	/**
	 * @return number of all triples in the store.
	 */
	long countTriples();
	
	/**
	 * @param graphName
	 * @return number of all triples in the passed graph.
	 */
	long countTriples(BlankNodeOrIRI graphName);

		
	/**
	 * Saves the {@link List} of {@link IRI} int the collection with the passed name
	 * in the graph with the passed graphName.
	 * 
	 * @param graphName
	 * @param subject
	 * @param items
	 * @param humanReadableName
	 * @param logMsg
	 * @throws IllegalStateException if the collection already exists.
	 */
	void saveCollection(BlankNodeOrIRI graphName, IRI subject, List<IRI> items, String humanReadableName, String logMsg);
	
	/**
	 * Add the passed item to the collection with the passed name in the graph with
	 * the passed graphName.
	 * 
	 * @param graphName
	 * @param collection
	 * @param humanReadableName
	 * @param item
	 * @param logMsg
	 */
	void addItem(BlankNodeOrIRI graphName, IRI collection, String humanReadableName, IRI item, String logMsg);
	
	void removeItem(BlankNodeOrIRI graphName, IRI collection, String humanReadableName, IRI item, String logMsg);
	
	/**
	 * 
	 * @param graphName
	 * @param collectionSubject
	 * @return The collection with the passed name in the graph with the passed
	 *         graphName.
	 */
	Optional<PersistableRdfPubCollection> getCollection(BlankNodeOrIRI graphName, IRI collectionSubject);
	
	/**
	 * 
	 * @param graphName
	 * @param humanReadableName
	 * @param collection
	 * @param offset
	 * @param limit
	 * @return The collection page with the passed name in the graph with the passed
	 *         graphName, starting at position offset ending on position offset + limit.
	 */
	Optional<PersistableRdfPubCollection> subCollection(BlankNodeOrIRI graphName, String humanReadableName,
			IRI collection, int offset, int limit);

	/**
	 * Searches for Collections, where the passed member is a member of. 
	 * @param graphName
	 * @param member
	 * @return List of Collections, where the passed member is a member of.
	 */
	Set<PersistableRdfPubCollection> findCollection(BlankNodeOrIRI graphName, IRI member);
	
	void dump();

	String queryTuple(String query);
}
