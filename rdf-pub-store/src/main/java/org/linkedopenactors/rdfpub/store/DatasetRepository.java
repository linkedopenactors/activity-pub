package org.linkedopenactors.rdfpub.store;

import org.apache.commons.rdf.api.Dataset;

/**
 * Abstraction from the underlaying rdf store, that is used behing commons-rdf.
 */
public interface DatasetRepository {
	/**
	 * Exception possibly depending on the implementation this throws an
	 *                   Exception, if there is already a dataset with that name.
	 * @param datasetName
	 * @return A newly created {@link Dataset}
	 */
	Dataset createDataset(String datasetName);
	
	/**
	 * @param datasetName
	 * @return The {@link Dataset} found for the passed datasetName;
	 */
	java.util.Optional<Dataset> findDataset(String datasetName);
	
	/**
	 * Free resources used by the {@link DatasetRepository}
	 */
	void shutdown();
}
