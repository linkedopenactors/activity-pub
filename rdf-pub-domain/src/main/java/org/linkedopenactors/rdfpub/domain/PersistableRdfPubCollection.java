package org.linkedopenactors.rdfpub.domain;

import java.util.List;

import org.apache.commons.rdf.api.IRI;

public interface PersistableRdfPubCollection {

	String getHumanReadableName();

	IRI getSubject();

	List<IRI> getItems();

	IRI getCollectionSubject();

}