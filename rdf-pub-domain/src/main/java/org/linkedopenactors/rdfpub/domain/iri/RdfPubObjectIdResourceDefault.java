package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.RDF;

class RdfPubObjectIdResourceDefault extends RdfPubObjectIdBase implements RdfPubObjectIdResource {
	
	private UUID resourceIdentifier;
	public void setResourceIdentifier(UUID resourceIdentifier) {
		this.resourceIdentifier = resourceIdentifier;
	}

	private UUID resourceRevision;

	public RdfPubObjectIdResourceDefault(RdfPubIRIFactory vocabContainerParam, RDF rdf, String baseIriString, String instanceDomainString, String instanceActorIdentifier, String actorIdentifier) {
		super(vocabContainerParam, rdf, baseIriString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
		this.resourceIdentifier = UUID.randomUUID();
	}

	public RdfPubObjectIdResourceDefault(RdfPubIRIFactory vocabContainerParam, RDF rdf, String baseIriString, String instanceDomainString, String instanceActorIdentifier, String actorIdentifier, UUID resourceIdentifier) {
		super(vocabContainerParam, rdf, baseIriString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
		this.resourceIdentifier = resourceIdentifier;
	}

	public UUID getResourceIdentifier() {
		return resourceIdentifier;
	}

	public void setRevision(UUID resourceRevision) {
		this.resourceRevision = resourceRevision;
	}

	@Override
	public Optional<UUID> getRevision() {
		return Optional.ofNullable(resourceRevision);
	}

	@Override
	public RdfPubBlankNodeOrIRI getSubject() {
		String withoutRevision = getBaseSubject().getIRIString();
		withoutRevision = isRevision() ? withoutRevision + "/" + resourceRevision : withoutRevision;
		return rdfPubIRIFactory.createRdfPubIriIri(withoutRevision);
	}

	@Override
	public RdfPubIRI getBaseSubject() {
		return rdfPubIRIFactory.createRdfPubIriIri(super.getBaseSubjectString() + "/" + RESOURCE_TOKEN + "/" + resourceIdentifier);
	}

	@Override
	public boolean isRevision() {
		return Optional.ofNullable(resourceRevision).isPresent();
	}

	@Override
	public String toString() {
		return getSubject().getIRIString();
	}
}
