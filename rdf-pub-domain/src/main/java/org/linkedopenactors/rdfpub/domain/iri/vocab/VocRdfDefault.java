package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;

public class VocRdfDefault implements VocRdf {
	
	private RDF rdf;

	public VocRdfDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI type() {
		return new RdfPubIRIDefault(rdf.createIRI(type));
	}

	public RdfPubIRI Bag() {
		return new RdfPubIRIDefault(rdf.createIRI(Bag));
	}

	public RdfPubIRI Property() {
		return new RdfPubIRIDefault(rdf.createIRI(Property));
	}
}
