package org.linkedopenactors.rdfpub.domain.cache;

import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

/**
 * A Cache for follow Activities.
 * If Actor A wants to follow Actor B, then Actor A sends a follow activity. This is cached in this cache, until Actor B accept or reject the follow request. 
 */
public interface PendingFollowerCache {

	/**
	 * Adds the passed activity and object to this cache.
	 * @param activity
	 */
	void addPendingFollowActivity(Activity activity);
	
	/**
	 * Extracts the Actors out of the follow activities.
	 * @return The extracted Actors out of the follow activities.
	 */
	Set<RdfPubBlankNodeOrIRI> getPendingFollower();
	
	/**
	 * Returns a list of Follower Activity Iris. 
	 * @return A list of Follower Activity Iris.
	 */
	Set<RdfPubBlankNodeOrIRI> getPendingFollowerActivityIris();
	
	/**
	 * Returns a list of Follower Activities.
	 * @return A list of Follower Activities.
	 */
	Set<Activity> getPendingFollowerActivities();
	
	/**
	 * Searches for the passed subjects in the PendingFollowerActivities. 
	 * @param iris
	 * @return The activities found for the passed iris.
	 */
	Set<Activity> getPendingFollowerActivities(Set<RdfPubIRI> iris);
	
	/**
	 * Removes the follow activity for the passed requesting actor. This has to be
	 * done, if the actor that is followed reject/accept the follow request.
	 * 
	 * @param actorId
	 */
	void removePendingFollowActivity(RdfPubBlankNodeOrIRI actorId);

	/**
	 * Checks. if the passed activity subject is already in the cache.
	 * @param activitySubject
	 * @return True, if the passed activity subject is already in the cache.
	 */
	boolean contains(String activitySubject);
	
	/**
	 * Checks. if the passed activity is already in the cache.
	 * @param activity
	 * @return True, if the passed activity is already in the cache.
	 */
	boolean contains(Activity activity);
}
