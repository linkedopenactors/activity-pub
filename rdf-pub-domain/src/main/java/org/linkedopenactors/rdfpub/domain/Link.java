package org.linkedopenactors.rdfpub.domain;

import java.net.URI;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

import com.neovisionaries.i18n.LanguageCode;

public interface Link extends LinkOrObject {

 	/**
 	 * The target resource pointed to by a Link. 
 	 * @return The target resource pointed to by a Link.
 	 */
	 URI getHref();
	 
	 void setHref(URI href);
	 
 	/**
	 * A link relation associated with a Link. The value MUST conform to both the
	 * [HTML5] and [RFC5988] "link relation" definitions. In the [HTML5], any string
	 * not containing the "space" U+0020, "tab" (U+0009), "LF" (U+000A), "FF"
	 * (U+000C), "CR" (U+000D) or "," (U+002C) characters can be used as a valid
	 * link relation.
	 * @return link relation associated with a Link
	 */
	 Set<String> getRel();
	 
	 /**
	  * Hints as to the language used by the target resource. Value MUST be a [BCP47] Language-Tag. 
	  * @return The LanguageCode.
	  */
	 Optional<LanguageCode> getHreflang();
	 
	 	/**
		 * On a Link, specifies a hint as to the rendering height in device-independent
		 * pixels of the linked resource.
		 * 
		 * @return The height
		 */
	 Optional<Long> getHeight();
	 
	 /**
	  * On a Link, specifies a hint as to the rendering width in device-independent pixels of the linked resource. 
	  * @return The width
	  */
	 Optional<Long> getWidth();
	 
	 /**
	  * Identifies an entity that provides a preview of this object.
	  * @return Identifierd of the entities that provides a preview of this object.
	  */
	 Set<RdfPubBlankNodeOrIRI> getPreview();
	 
}