package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.IRI;

public class RdfPubIRIDefault implements RdfPubIRI {
	
    private final IRI iri;

    public RdfPubIRIDefault(final IRI iriParam) {
    	this.iri = iriParam;
    }
    
	public boolean isBlankNode() {
		return false;
	}

    @Override
    public boolean equals(final Object obj) {
    	return iri.equals(obj);
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null || !(obj instanceof IRI)) {
//            return false;
//        }
//        final IRI other = (IRI) obj;
//        boolean equals = getIRIString().equals(other.getIRIString());
//		return equals;
    }

    @Override
    public String getIRIString() {    	
    	return iri.getIRIString();
    }

    @Override
    public int hashCode() {
        return iri.hashCode();
    }

    @Override
    public String ntriplesString() {
        return iri.ntriplesString();
    }

    @Override
    public String toString() {
        return iri.toString();
    }
}

