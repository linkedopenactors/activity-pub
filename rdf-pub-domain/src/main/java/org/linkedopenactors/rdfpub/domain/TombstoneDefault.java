package org.linkedopenactors.rdfpub.domain;

import java.time.Instant;

import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

public class TombstoneDefault extends ActivityPubObjectDefault implements Tombstone {

	public TombstoneDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}

	@Override
	public void setDeleted(Instant instant) {
		set(vocAs().deleted(), fromInstant(instant));		
	}
}
