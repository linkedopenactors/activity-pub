package org.linkedopenactors.rdfpub.domain;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.RDFTerm;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;

/**
 * The store from an actors perspective.
 */
public interface ActorsStore extends ReceiverStore {
	RdfPubObjectIdActivity addToOutbox(Activity activity, String logMsg);	
	RdfPubBlankNodeOrIRI addToInbox(Activity activity, String logMsg);
	void addFollower(RdfPubIRI follower);
	void removeFollower(RdfPubIRI follower);
	void addFollowing(RdfPubIRI follower);
	void removeFollowing(RdfPubIRI follower);
	
	List<RdfPubIRI> getCollection(RdfPubObjectIdCollection collection, Integer startIndex, Integer pageSize);
	OrderedCollectionPage getInbox(Integer startIndex, Integer pageSize);
	OrderedCollectionPage getOutbox(Integer startIndex, Integer pageSize);
	
	List<RdfPubIRI> getOutbox();
	List<RdfPubIRI> getFollowers();
	
	Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject);
	Optional<ActivityPubObject> find(RdfPubBlankNodeOrIRI subject, int deep);
	Optional<ActivityPubObject> find(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object);
	Set<ActivityPubObject> find(RdfPubIRI predicate, RDFTerm object);

	/**
	 * Find all objects with the passed subjects in the actors store and resolve dependencies up to the passed depth.
	 * @param subject Subjects to search for.
	 * @return Found {@link ActivityPubObject}s 
	 */
	Set<ActivityPubObject> findAll(Set<RdfPubIRI> subject);
	
	/**
	 * Find all objects with the passed subjects in the actors store and resolve dependencies up to the passed depth.
	 * @param subject Subjects to search for.
	 * @param deep up to which hierarchy level should object references be resolved ?
	 * @return Found {@link ActivityPubObject}s 
	 */
	Set<ActivityPubObject> findAll(Set<RdfPubIRI> subject, int deep);
	Graph dump();
	
	Set<BlankNodeOrIRI> getGraphNames();
	String queryTuple(String query);
}
