package org.linkedopenactors.rdfpub.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
/**
 * Factory for creating non persistant {@link ActivityPubObject}s. 
 */
public class ActivityPubObjectFactory {

	private StringToGraphConverter stringToGraphConverter;
	private GraphToStringConverter graphToStringConverter;
	private RDF rdf;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private PrefixMapper prefixMapper;
	private VocabContainer vocabContainer;

	/**
	 * @param rdf
	 * @param stringToGraphConverter
	 * @param graphToStringConverter
	 * @param activityPubObjectIdBuilder
	 * @param prefixMapper
	 */
	public ActivityPubObjectFactory(VocabContainer vocabContainer, RDF rdf, StringToGraphConverter stringToGraphConverter,
			GraphToStringConverter graphToStringConverter, ActivityPubObjectIdBuilder activityPubObjectIdBuilder, PrefixMapper prefixMapper) {
		this.vocabContainer = vocabContainer;
		this.rdf = rdf;
		this.stringToGraphConverter = stringToGraphConverter;
		this.graphToStringConverter = graphToStringConverter;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
		this.prefixMapper = prefixMapper;
	}
	
	/**
	 * @param subject
	 * @param graph
	 * @return The created ActivityPubObject
	 */
	public ActivityPubObject create(RdfPubBlankNodeOrIRI subject, Graph graph) {
		if(subject == null) {
			Set<BlankNodeOrIRI> subjects = graph.stream().map(Triple::getSubject).collect(Collectors.toSet());
			if(subjects.size()!=1) {
				throw new IllegalStateException("passed subject is null, but the graph has not exact one subject: " + subjects);
			} else {
				subject = subjects.stream()
						.findFirst()
						.filter(IRI.class::isInstance)
						.map(IRI.class::cast)
						.map(IRI::getIRIString)
						.map(activityPubObjectIdBuilder::createFromUrl)
						.orElseThrow();
			}
		}	
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, subject, internalize(graph),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new ActivityPubObjectDefault(vocabContainer, data);
	}
		
	
	private Graph internalize(Graph graph) {
		String graphAsString = graphToStringConverter.internalize(RdfFormat.TURTLE, graph);
		Graph internalizedGraph = stringToGraphConverter.convert(RdfFormat.TURTLE, graphAsString);		
		return internalizedGraph;
	}
	
	/**
	 * 
	 * @param subject
	 * @param modelAsTurtleString
	 * @return The created ActivityPubObject.
	 */
	public ActivityPubObject create(String subject, String modelAsTurtleString) {
		return create(RdfFormat.TURTLE, modelAsTurtleString, subject);
	}
	
	/**
	 * 
	 * @param modelAsTurtleString
	 * @return The created ActivityPubObject.
	 */
	public ActivityPubObject create(String modelAsTurtleString) {
		return create(RdfFormat.TURTLE, modelAsTurtleString);
	}
	
	/**
	 * 
	 * @param format
	 * @param modelAsTurtleString
	 * @return created {@link List} of {@link Activity}s.
	 */
	public List<Activity> createActivityList(RdfFormat format, String modelAsTurtleString) {
		Graph graph = stringToGraphConverter.convert(format, modelAsTurtleString);		
		
		RdfPubBlankNodeOrIRI ident = vocabContainer.vocAs().OrderedCollectionPage();
		
		Set<Triple> orderedCollectionPages =  graph.stream(null, null, ident).collect(Collectors.toSet());
		if(orderedCollectionPages.size() == 1) {
			Triple orderedCollectionPageTriple = orderedCollectionPages.stream().findFirst().get();
			RdfPubBlankNodeOrIRI orderedCollectionPageSubject = activityPubObjectIdBuilder.createFromUrl(orderedCollectionPageTriple.getSubject().toString());
			ActivityPubObject orderedCollectionPage = create(graph, orderedCollectionPageSubject);
			OrderedCollectionPage ocp = orderedCollectionPage.asConvertable().asOrderedCollectionPage();
			List<Activity> itemsAsActivities = ocp.getItems().stream().map(d->{
				Activity a = create(graph, d).asConvertable().asActivity();
				return a;
			}).collect(Collectors.toList());			
			System.out.println();
			return itemsAsActivities;
		} else {
			log.warn("no activities!");
			log.trace("no activities -> " + modelAsTurtleString);
			return Collections.emptyList();
		}
	}

	public List<Actor> createActorList(RdfFormat format, String modelAsTurtleString) {
		Graph graph = stringToGraphConverter.convert(format, modelAsTurtleString);		
		
		
		graph.stream().forEach(r->log.debug("\tcreateActorList graph item: " + r));
		
		Set<BlankNodeOrIRI> subjects = graph.stream(null, vocabContainer.vocRdf().type(), null)
				.filter(this::isActor)
				.map(t->t.getSubject())
				.collect(Collectors.toSet());
		
		List<Actor> actors =  subjects.stream()
				.map(s->filterBySubject(graph, s))
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor)
				.collect(Collectors.toList());
		
		return actors;
	}

//	private boolean isActivity(Triple t) {
//		Boolean res = toRdfPubIRI(t.getObject()).map(vocabContainer::isActivity).orElse(false);
//		return res;
//	}
	
	private boolean isActor(Triple t) {
		Boolean res =  toRdfPubIRI(t.getObject()).map(this::isActor).orElse(false);
		return res;
	}
	
	private boolean isActor(RdfPubBlankNodeOrIRI iri) {
		return iri.equals(vocabContainer.vocAs().Person()) 
			|| iri.equals(vocabContainer.vocAs().Group()) 
			|| iri.equals(vocabContainer.vocAs().Organization())
			|| iri.equals(vocabContainer.vocAs().Service())
			|| iri.equals(vocabContainer.vocAs().Application());
	}

	private Optional<RdfPubIRI> toRdfPubIRI(RDFTerm term) {
		Optional<RdfPubIRI> result = Optional.empty();
		if(term instanceof IRI) {
			result = Optional.ofNullable(activityPubObjectIdBuilder.createFromUrl(term.toString()));
		}
		return result;
	}
	
	private ActivityPubObject filterBySubject(Graph graphParam, BlankNodeOrIRI s) {
		Graph graph = rdf.createGraph();
		ActivityPubObject ao = create(graphParam, activityPubObjectIdBuilder.createFromUrl(s.toString()));
		Set<RdfPubBlankNodeOrIRI> references = ao.getReferences(false, true);
		
		////////////////
		// TODO hier gibt es das Problem mit BlankNodes!
		////////////////
		
		references.add(activityPubObjectIdBuilder.createFromUrl(s.toString()));
		
		references.stream()
			.map(subj -> graphParam.stream(subj, null, null).collect(Collectors.toSet()))
			.flatMap(Collection::stream)
			.forEach(t->graph.add(t));
			
		ActivityPubObject activityPubObject = create(graph, activityPubObjectIdBuilder.createFromUrl(s.toString()));
		return activityPubObject;
	}

	/**
	 * 
	 * @param format
	 * @param modelAsTurtleString
	 * @param subject
	 * @return the created {@link ActivityPubObject}
	 */
	public ActivityPubObject create(RdfFormat format, String modelAsTurtleString, String subject) {
		if(!StringUtils.hasText(modelAsTurtleString)) {
			throw new IllegalStateException("modelAsTurtleString is empty! subject: " + subject);
		}
		Graph graph = stringToGraphConverter.convert(format, modelAsTurtleString);
		return create(graph, activityPubObjectIdBuilder.createFromUrl(subject));
	}
	
	/**
	 * 
	 * @param format
	 * @param modelAsTurtleString
	 * @return the created {@link ActivityPubObject}
	 */
	public ActivityPubObject create(RdfFormat format, String modelAsTurtleString) {
		Graph graph = stringToGraphConverter.convert(format, modelAsTurtleString);	
		return create(graph, findRoot(graph));
	}

	private ActivityPubObject create(Graph graph, RdfPubBlankNodeOrIRI root) {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, root, internalize(graph), graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		ActivityPubObjectDefault ao = new ActivityPubObjectDefault(vocabContainer, data);
		return ao;
	}

	private RdfPubBlankNodeOrIRI findRoot(Graph graph) {
		if(graph == null) {
			throw new IllegalStateException("graph is mandatory!");
		}
		Set<IRI> subjects = graph.stream()
				.map(Triple::getSubject)
				.filter(sub->sub instanceof IRI)
				.map(IRI.class::cast)
				.collect(Collectors.toSet());
		
		if(subjects.size()==1) {
			return activityPubObjectIdBuilder.createFromUrl(subjects.stream().findFirst().orElseThrow().getIRIString());
		} else if(subjects.isEmpty()) {
			String graphAsString = graphToStringConverter.convert(RdfFormat.TURTLE, graph);
			log.debug("graph: " + graphAsString);
			log.warn("No subjects! Object ID is mandatory.");
			graph.stream().forEach(t->log.debug("Triple: " + t));
			throw new UnsupportedActivityPubObject("No subjects! Object ID is mandatory. \n" + graphAsString);
		} else {		
			Set<IRI> referencedSubjects2 = graph.stream()
				.map(t->t.getObject())
				.filter(o -> o instanceof IRI)
				.map(IRI.class::cast)
				.collect(Collectors.toSet());
			
			Set<IRI> unreferenced2 = subjects.stream()
					.filter(s->!referencedSubjects2.contains(s))
					.collect(Collectors.toSet());
			
			if(unreferenced2.size()!=1) {
				log.warn("found ["+unreferenced2.size()+"]) subjects: " + unreferenced2);
				throw new UnsupportedActivityPubObject("Only one Subject expected, but is: " + unreferenced2);
			}
			return activityPubObjectIdBuilder.createFromUrl(unreferenced2.stream().findFirst().orElseThrow().getIRIString());
		}
	}

	/**
	 * 
	 * @param subject
	 * @return the created {@link ActivityPubObject}
	 */
	public ActivityPubObject create(RdfPubBlankNodeOrIRI subject) {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, subject, null, graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new ActivityPubObjectDefault(vocabContainer, data);
	}
	
	/**
	 * @param subject
	 * @return the created {@link ActivityPubObject}
	 */
	public Activity createActivity(RdfPubBlankNodeOrIRI subject) {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, subject, null, graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new ActivityPubObjectDefault(vocabContainer, data).asConvertable().asActivity();
	}
}
