package org.linkedopenactors.rdfpub.domain.iri;

import java.net.URL;

import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;

public interface RdfPubObjectId extends RdfPubIRI {

	public static final String RESOURCE_TOKEN = "res";
	public static final String ACTIVITY_TOKEN = "act";
	public static final String COLLECTION_TOKEN = "col";
	public static final String PREFIX_TOKEN = "resource";
	public static final String CACHE_TOKEN = "cache";
	
	/**
	 * @return The UUID which uniquely identifies an {@link Actor}.
	 */
	String getIdentifier();

	/**
	 * The id of the {@link ActivityPubObject}. Because an {@link ActivityPubObject}
	 * is a RDF Object, the id is called subject. See
	 * https://www.w3.org/TR/rdf12-concepts/#data-model
	 * 
	 * @return The id/subject of the {@link ActivityPubObject}. 
	 */
	RdfPubBlankNodeOrIRI getSubject();

	/**
	 * @return Same as {@link #getSubject()} but as external url, that starts with 'http(s)' instead of 'urn:'
	 */
	RdfPubIRI getExternalSubject();
	
	/**
	 * An {@link ActivityPubObject} has Versions, so the {@link #getSubject()} Method normally
	 * returns the subject of the current version. But sometimes you want the base
	 * url without the version 'extension'. Then you can use this method.
	 * 
	 * @return The id/subject of the {@link ActivityPubObject} without observing the exact revision.
	 */
	RdfPubIRI getBaseSubject();
	
	/**
	 * @return Same as {@link #getBaseSubject()} but as external url, that starts with 'http(s)' instead of 'urn:'
	 */
	RdfPubBlankNodeOrIRI getExternalBaseSubject();
	
	/**
	 * @return The IRIString of the subject without the scheme. A bit like {@link URL#getPath()}.
	 */
	String getPath();
}
