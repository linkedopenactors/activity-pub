package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.rdf.api.BlankNode;
import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Literal;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocLDP;
import org.linkedopenactors.rdfpub.domain.iri.VocOwl;
import org.linkedopenactors.rdfpub.domain.iri.VocPending;
import org.linkedopenactors.rdfpub.domain.iri.VocProv;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;
import org.linkedopenactors.rdfpub.domain.iri.VocRdfPub;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.linkedopenactors.rdfpub.domain.iri.VocSecurity;
import org.linkedopenactors.rdfpub.domain.iri.VocXsdDatatypes;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import com.neovisionaries.i18n.LanguageCode;

public class LinkOrObjectDefault implements LinkOrObject {

	private RdfPubIRI datatypeLong;
	private Graph graph;
	protected RDF rdf;
	private RdfPubBlankNodeOrIRI subject;
	private GraphToStringConverter graphToStringConverter;
	protected ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private boolean externalGraph;
	private PrefixMapper prefixMapper;
	private VocabContainer vocabContainer;
	
	public LinkOrObjectDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		this.vocabContainer = vocabContainer;
		this.rdf = data.getRdf();
		this.subject = data.getSubject();
		this.activityPubObjectIdBuilder = data.getActivityPubObjectIdBuilder();
		this.prefixMapper = data.getPrefixMapper();
		this.graph = Optional.ofNullable(data.getGraph()).orElse(rdf.createGraph()); // TODO ACHTUNG, ist das wieder ein parallelstreamiger ???
		this.graphToStringConverter = data.getGraphToStringConverter();
		this.datatypeLong = vocXsdDatatypes().long_();
		setExternalGraph();
	}

	@Override
	public Optional<String> getName() {
		// TODO contentMap
		return graph.stream(getSubjectToSearchGraph(), vocAs().name(), null)
				.findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(Literal::getLexicalForm);
	}

	@Override
	public void setName(LanguageCode languageCode, String name) {
		// TODO contentMap
		set(vocAs().name(), rdf.createLiteral(name));
	}

	@Override
	public void setName(String name) {
		set(vocAs().name(), rdf.createLiteral(name));
	}

	@Override
	public void addType(RdfPubBlankNodeOrIRI type) {
		add(vocRdf().type(), type);
	}
	
	@Override
	public Set<RdfPubIRI> getTypes() {
		return getIriIris(vocRdf().type());
	}

	@Override
	public Optional<String> getName(LanguageCode languageCode) {
		return graph.stream(getSubjectToSearchGraph(), vocAs().name(), null).findFirst().map(Triple::getObject).map(RDFTerm::toString);
	}

	@Override
	public RdfPubBlankNodeOrIRI getSubject() {
		return subject;
	}

	/**
	 * 
	 */
	@Override
	public void setType(Set<RdfPubIRI> type) {
		RdfPubIRI predicate = vocRdf().type();
		graph.remove(subject, predicate, null);
		type.forEach(val->add(predicate, val));
	}
	
	@Override
	public Optional<MimeType> getMediaType() {
		return getString(vocAs().mediaType())
				.map(mt->MimeTypeUtils.parseMimeType(mt));
	}

	@Override
	public Graph asGraph() {
		return graph;
	}
	
	@Override
	public Graph asExternalGraph() {
		Graph extGraph = rdf.createGraph();
		graph.stream().forEach(t->{				
			extGraph.add(t.getSubject(), t.getPredicate(), mapExternal(t.getObject()));				
		});
		return extGraph;
	}
	
	private RDFTerm mapExternal(RDFTerm rdfTerm) {
		if(!(rdfTerm instanceof IRI)) {
			return rdfTerm;
		} else {
			String iriAsString = rdfTerm.toString().replace(getInternalPrefix(), getExternalPrefix());
			return rdf.createIRI(iriAsString);
		}
	}
	
	private String getInternalPrefix() {
		String internaleIriPrefix = prefixMapper.getInternaleIriPrefix();
		internaleIriPrefix = internaleIriPrefix.endsWith("/") ? internaleIriPrefix.substring(0, internaleIriPrefix.length()-1) : internaleIriPrefix;
		return internaleIriPrefix;
	}

	private String getExternalPrefix() {
		String externalIriPrefix = prefixMapper.getExternalIriPrefix();
		externalIriPrefix = externalIriPrefix.endsWith("/") ? externalIriPrefix : externalIriPrefix + "/";
		return externalIriPrefix;
	}

	protected void setExternalGraph() {
		this.externalGraph = this.graph.stream().findFirst()
				.map(x->x.getSubject().toString().startsWith("http"))
				.orElse(false);
	}
	
	/**
	 * The {@link #subject} is a {@link RdfPubBlankNodeOrIRI} which can also be a
	 * {@link RdfPubObjectId}. The default representation of a
	 * {@link RdfPubObjectId} is internal without http(s)! So if the graph has
	 * {@link Triple}s with http(s) subject, you will not find the predicates.
	 * Therefore this method provide the correct subject format.
	 * 
	 * @return subject representation to use for searching the graph.
	 */
	protected RdfPubBlankNodeOrIRI getSubjectToSearchGraph() {
		RdfPubBlankNodeOrIRI subj = subject;
		if(isExternalGraph()) {
			if(subject.isRdfPubObjectId()) {
				subj = subject.asRdfPubObjectId().orElseThrow().getExternalSubject();		
			} 
		}
		return subj;
	}

	protected boolean isExternalGraph() {
		return externalGraph;
	}

	protected void setString(RdfPubIRI predicate, String value) {
		set(predicate, rdf.createLiteral(value));
		// TODO language map !!
	}

	protected void set(RdfPubIRI predicate, RDFTerm term) {
		graph.remove(subject, predicate, null);
		if(term != null) {
			add(predicate, term);
		}
	}

	protected void add(RdfPubIRI predicate, RDFTerm term) {
		try {
			graph.add(subject, predicate, term);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(predicate + " -" + term);
			e.printStackTrace();
		}	
	}	

	protected Set<RdfPubBlankNodeOrIRI> getIris(RdfPubIRI predicate) {
		RdfPubBlankNodeOrIRI subjectToSearchGraph = getSubjectToSearchGraph();
		Set<? extends Triple> found = graph.stream(subjectToSearchGraph, predicate, null)
				.collect(Collectors.toSet());
		Set<RdfPubBlankNodeOrIRI> collect = found.stream()
				.map(Triple::getObject)
				.map(BlankNodeOrIRI.class::cast)
				.map(BlankNodeOrIRI::toString)
				.map(activityPubObjectIdBuilder::createFromUrl)
				.collect(Collectors.toSet());
		return collect;
	}

	protected Set<RdfPubIRI> getIriIris(RdfPubIRI predicate) { // TODO rename
		RdfPubBlankNodeOrIRI subjectToSearchGraph = getSubjectToSearchGraph();
		Stream<? extends Triple> stream = graph.stream(subjectToSearchGraph, predicate, null);
		Set<RdfPubIRI> collect = stream
				.map(Triple::getObject)
				.map(IRI.class::cast)
				.map(IRI::getIRIString)
				.map(activityPubObjectIdBuilder::createFromUrl)
				.map(RdfPubIRI.class::cast)
				.collect(Collectors.toSet());
		return collect;
	}

	protected void set(RdfPubIRI predicate, Set<RdfPubBlankNodeOrIRI> terms) {
		graph.remove(subject, predicate, null);
		terms.forEach(val->add(predicate, val));
	}

	protected Optional<RdfPubBlankNodeOrIRI> getIri(RdfPubIRI predicate) {
		Stream<? extends Triple> stream = graph.stream(getSubjectToSearchGraph(), predicate, null);
		Set<IRI> collect = stream
				.map(Triple::getObject)
				.filter(IRI.class::isInstance)
				.map(IRI.class::cast)
				.collect(Collectors.toSet());
		if(collect.size()>1) {
			throw new IllegalStateException("Exact one hit for '"+predicate+"' expected, but was: " + collect.size());
		}
		return collect.stream().findFirst()
				.map(IRI::getIRIString)
				.map(activityPubObjectIdBuilder::createFromUrl);
		// TODO use also getRdfTerm(IRI predicate) and do filtering on the result
	}

	protected Set<String> getStrings(RdfPubIRI predicate) {
		return getLiterals(predicate).stream()
				.map(Literal::getLexicalForm)
				.collect(Collectors.toSet());
		// TODO language map !!
	}

	protected Optional<String> getString(RdfPubIRI predicate) {
		return getLiteral(predicate)
				.map(Literal::getLexicalForm);
		// TODO language map !!
	}

	protected Optional<Literal> getLiteral(RdfPubIRI predicate) {
		return graph.stream(getSubjectToSearchGraph(), predicate, null)
			.findFirst()
			.map(Triple::getObject)
			.map(Literal.class::cast);
	}		

	protected Set<Literal> getLiterals(RdfPubIRI predicate) {
		return graph.stream(getSubjectToSearchGraph(), predicate, null)
			.map(Triple::getObject)
			.map(Literal.class::cast)
			.collect(Collectors.toSet());
	}		

	protected RdfPubIRI getDatatypeLong() {
		return datatypeLong;
	}
	
	protected GraphToStringConverter getGraphToStringConverter() {
		return graphToStringConverter;
	}
	
	protected void setSubject(RdfPubBlankNodeOrIRI subjectParam) {
		this.subject = subjectParam;
	}

	protected Optional<Long> getLong(RdfPubIRI predicate) {
		return getLiteral(predicate)
				.filter(h->h.getDatatype().equals(getDatatypeLong()))
				.map(Literal::getLexicalForm)
				.map(Long::valueOf);
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getReferences(boolean isolated) {
		return getReferences(isolated, false);
	}
	
	@Override
	public Set<RdfPubBlankNodeOrIRI> getReferences(boolean isolated, boolean externalized) {
		Stream<? extends Triple> streamToUse;
		if(isolated) {
			streamToUse = this.graph.stream(getSubject(), null, null);
		} else {
			streamToUse = this.graph.stream();
		}		
				
		Set<String> result1 = streamToUse
				.filter(this::isReference)
				.map(Triple::getObject)
				.map(RDFTerm::toString)
				.collect(Collectors.toSet());

		Set<RdfPubBlankNodeOrIRI> result = result1.stream()
			.map(activityPubObjectIdBuilder::createFromUrl)
			.map(this::internExternFix)
			.collect(Collectors.toSet());
		
		return result;		
	}

	private RdfPubBlankNodeOrIRI internExternFix(RdfPubBlankNodeOrIRI iri) {
		if(iri instanceof RdfPubObjectId) {
			return ((RdfPubObjectId)iri).getExternalSubject();
		} else {
			return iri;
		}
	}
	
	private boolean isReference(Triple t) {
		RDFTerm object =  t.getObject();
		if (object instanceof IRI || object instanceof BlankNode || object instanceof BlankNodeOrIRI) {
			return true;			
		}
		return false;
	}
	
	protected VocabContainer vocabContainer() {
		return vocabContainer;
	}
	
	protected VocAs vocAs() {
		return vocabContainer.vocAs();
	}

	protected VocLDP vocLDP() {
		return vocabContainer.vocLDP();
	}
	
	protected VocOwl vocOwl() {
		return vocabContainer.vocOwl();
	}
	
	protected VocPending vocPending() {
		return vocabContainer.vocPending();
	}
	
	protected VocProv vocProv() {
		return vocabContainer.vocProv();
	}
	
	protected VocRdf vocRdf() {
		return vocabContainer.vocRdf();
	}
		
	protected VocRdfPub vocRdfPub() {
		return vocabContainer.vocRdfPub();
	}
	
	protected VocSchema vocSchema() {
		return vocabContainer.vocSchema();
	}
	
	protected VocSecurity vocSecurity() {
		return vocabContainer.vocSecurity();
	}
	
	protected VocXsdDatatypes vocXsdDatatypes() {
		return vocabContainer.vocXsdDatatypes();
	}
}
