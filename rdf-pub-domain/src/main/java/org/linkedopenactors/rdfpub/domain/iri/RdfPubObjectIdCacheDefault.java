package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.RDF;

class RdfPubObjectIdCacheDefault extends RdfPubObjectIdBase implements RdfPubObjectIdCache {

	private String cacheId;
	public RdfPubObjectIdCacheDefault(RdfPubIRIFactory vocabContainerParam, RDF rdf, String internalUrnString, String instanceDomainString,
			String instanceActorIdentifier, String actorIdentifier, String cacheId) {
		super(vocabContainerParam, rdf, internalUrnString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
		this.cacheId = cacheId;
	}

	@Override
	public RdfPubIRI getSubject() {
		String val = super.getBaseSubjectString() + "/" + RdfPubObjectId.CACHE_TOKEN + "/" + cacheId;
		return rdfPubIRIFactory.createRdfPubIriIri(val);
	}


	@Override
	public RdfPubIRI getBaseSubject() {
		return getSubject();
	}

	@Override
	public String getCacheId() {
		return cacheId;
	}
}
