package org.linkedopenactors.rdfpub.domain;

import java.net.URL;

import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;

/**
 * Some static properties of an instance.
 */
public interface InstanceProperties extends PrefixMapper {

	/**
	 * @return The preffered username of the instance.
	 */
	String getPreferedUsername();
	
	/**
	 * @see <a href="https://maven.apache.org/repositories/artifacts.html">Maven Artifacts</a>
	 * @return The maven version, which represents the version of the currently running software. 
	 */
	String getMavenVersion();
	
	/**
	 * Git Commit ID is the unique SHA-hash value generated automatically and
	 * assigned to commits whenever a new commit is made to the repository. Commit
	 * id is used while merging commits or checking out files from different
	 * commits.
	 * @see <a href="https://git-scm.com/book/en/v2">git book</a>
	 * @return The unique SHA-hash value of the currently running software.
	 */
	String getCommitId();	
	
	/**
	 * @see <a href="https://schema.org/identifier">schema.org</a> 
	 * @return Unique identifier of this instance.
	 */
	String getIdentifier();
	
	/**
	 * @return The <a href="https://de.wikipedia.org/wiki/Uniform_Resource_Locator">URL</a> which routes to this instance.
	 */
	URL getInstanceDomain();
}
