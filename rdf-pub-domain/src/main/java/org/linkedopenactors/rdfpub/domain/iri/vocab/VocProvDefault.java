package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocProv;

public class VocProvDefault implements VocProv {
	
	private RDF rdf;

	public VocProvDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI Entity() {
		return new RdfPubIRIDefault(rdf.createIRI(Entity));
	}

	public RdfPubIRI Activity() {
		return new RdfPubIRIDefault(rdf.createIRI(Activity));
	}
	
	public RdfPubIRI wasAssociatedWith() {
		return new RdfPubIRIDefault(rdf.createIRI(wasAssociatedWith));
	}
	
	public RdfPubIRI wasAttributedTo() {
		return new RdfPubIRIDefault(rdf.createIRI(wasAttributedTo));
	}
	
	public RdfPubIRI wasGeneratedBy() {
		return new RdfPubIRIDefault(rdf.createIRI(wasGeneratedBy));
	}
	
	public RdfPubIRI wasRevisionOf() {
		return new RdfPubIRIDefault(rdf.createIRI(wasRevisionOf));
	}
}
