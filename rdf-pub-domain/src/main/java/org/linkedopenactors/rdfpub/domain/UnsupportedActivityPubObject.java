package org.linkedopenactors.rdfpub.domain;

public class UnsupportedActivityPubObject extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnsupportedActivityPubObject(String message, Throwable cause) {
		super(message, cause);
	}

	public UnsupportedActivityPubObject(String message) {
		super(message);
	}
}
