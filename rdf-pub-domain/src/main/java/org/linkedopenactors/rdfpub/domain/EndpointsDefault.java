package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

public class EndpointsDefault extends ActivityPubObjectDefault implements Endpoints {

	public EndpointsDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getOauthAuthorizationEndpoint() {
		RdfPubIRI oauthAuthorizationEndpoint = vocAs().oauthAuthorizationEndpoint();
		return getIris(oauthAuthorizationEndpoint).stream().findFirst();
	}
	
	@Override
	public Optional<RdfPubBlankNodeOrIRI> getOauthTokenEndpoint() {
		return getIris(vocAs().oauthTokenEndpoint()).stream().findFirst();
	}

	@Override
	public void setOauthAuthorizationEndpoint(RdfPubBlankNodeOrIRI oauthAuthorizationEndpoint) {
		set(vocAs().oauthAuthorizationEndpoint(), oauthAuthorizationEndpoint);		
	}

	@Override
	public void setOauthTokenEndpoint(RdfPubBlankNodeOrIRI oauthTokenEndpoint) {
		set(vocAs().oauthTokenEndpoint(), oauthTokenEndpoint);		
	}
}
