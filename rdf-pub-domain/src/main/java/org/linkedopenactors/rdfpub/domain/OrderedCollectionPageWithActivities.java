package org.linkedopenactors.rdfpub.domain;

import java.util.List;

public interface OrderedCollectionPageWithActivities {
	List<Activity> getActivities();
	OrderedCollectionPage getOrderedCollectionPage();
}
