package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;

public class VocSchemaDefault implements VocSchema {
	
	private RDF rdf;

	public VocSchemaDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI version() {
		return new RdfPubIRIDefault(rdf.createIRI(version));
	}
}
