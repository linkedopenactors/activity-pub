package org.linkedopenactors.rdfpub.domain.iri;

public interface VocRdfPub {
	public static final String NS = "https://schema.rdf-pub.org/";

	public final static String commitId = NS + "commitId";
	
	public final static String oauth2IssuerUserId = NS + "oauth2IssuerUserId";
	
	public final static String oauth2Issuer = NS + "oauth2Issuer";
	
	public final static String version = NS + "version";
	
	public final static String activityPubObjectVersion = NS + "activityPubObjectVersion";
	
	public final static String personsCollection = NS + "personsCollection";

	public final static String applicationsCollection = NS + "applicationsCollection";
	
	public final static String groupsCollection = NS + "groupsCollection";
	
	public final static String organizationsCollection = NS + "organizationsCollection";
	
	public final static String servicesCollection = NS + "servicesCollection";

	/**
	 * rdf-pub commitId
	 * <p>
	 * {@code http://rdf-pub.org#commitId}
	 * <p>
	 * The git commitId from which this instance was built.
	 * @see <a href="http://rdf-pub.org#commitId">commitId</a>
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #commitId} property.
	 */
	RdfPubIRI commitId();

	/**
	 * The unique userId, that a user has in it's {@link #oauth2Issuer}.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #oauth2Issuer} property.
	 */
	RdfPubIRI oauth2IssuerUserId();
	
	/**
	 * The issue host e.g. 'login.m4h.network'.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #oauth2Issuer} property.
	 */
	RdfPubIRI oauth2Issuer();

	/**
	 * The software version of rdf-pub. Normaly matches to the maven version.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #version} property.
	 */
	RdfPubIRI version();

	/**
	 * The version of an object. Assuming an actor has version 5, the FEP-4ccd is
	 * now implemented in rdf-pub, which means that an actor also receives a
	 * pendingFollowers and pendingFollowing collection. Then the actor must be
	 * updated to version 6, which is extended by the 2 collections.
	 * Actor is just a sample, applies to all types of objects.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #activityPubObjectVersion} property.
	 */
	RdfPubIRI activityPubObjectVersion();
	
	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the personsCollection.
	 */
	RdfPubIRI personsCollection();

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the applicationsCollection.
	 */
	RdfPubIRI applicationsCollection();

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the groupsCollection.
	 */
	RdfPubIRI groupsCollection();

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the organisationsCollection.
	 */
	RdfPubIRI organizationsCollection();

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the servicesCollection.
	 */
	RdfPubIRI servicesCollection();
}
