package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocSecurity;

public class VocSecurityDefault implements VocSecurity {
	
	private RDF rdf;

	public VocSecurityDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI owner() {
		return new RdfPubIRIDefault(rdf.createIRI(owner));
	}

	public RdfPubIRI publicKeyPem() {
		return new RdfPubIRIDefault(rdf.createIRI(publicKeyPem));
	}

	public RdfPubIRI publicKey() {
		return new RdfPubIRIDefault(rdf.createIRI(publicKey));
	}
}
