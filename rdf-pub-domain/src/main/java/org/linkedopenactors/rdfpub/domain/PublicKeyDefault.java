package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Literal;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocSecurity;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

public class PublicKeyDefault extends ActivityPubObjectDefault implements PublicKey {

	private RDF rdf;

	public PublicKeyDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
		this.rdf = data.getRdf();
	}

	@Override
	public void setOwner(RdfPubBlankNodeOrIRI owner) {
		set(vocSecurity().owner(), owner);		
	}

//	@Override
//	public void setPublicKeyPem(String publicKey) {
//		set(security.publicKeyPem(), rdf.createLiteral(publicKey));	
//	}
	
	@Override
	public void setPublicKeyPem(String publicKey) {
		set(vocSecurity().publicKeyPem(), rdf.createLiteral(publicKey));	
	}

	@Override
	public Optional<java.security.PublicKey> getPublicKeyPem() {		
		BlankNodeOrIRI subjectToSearchGraph = getSubjectToSearchGraph();
		IRI publicKeyPem = vocSecurity().publicKeyPem();
		return asGraph().stream(subjectToSearchGraph, publicKeyPem, null).findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(Literal::getLexicalForm)
				.map(NativePublicKey::readX509PublicKey);
	}	
}
