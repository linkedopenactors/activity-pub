package org.linkedopenactors.rdfpub.domain;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Literal;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import com.neovisionaries.i18n.LanguageCode;

class ActivityPubObjectDefault extends LinkOrObjectDefault implements ActivityPubObject {

	private DomainObjectInitialazationData data;

	public ActivityPubObjectDefault(VocabContainer vocabContainer, DomainObjectInitialazationData dataParam) {		
		super(vocabContainer, dataParam);
		data = dataParam;
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getAttributedtTo() {
		return getIris(vocAs().attributedTo());
	}

	protected void setLong(RdfPubIRI predicate, long value) {
		set(predicate, rdf.createLiteral(Long.toString(value), getDatatypeLong()));
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getTags() {
		return getIris(vocAs().tag());
	}

	@Override
	public void addTag(RdfPubBlankNodeOrIRI tag) {
		Set<RdfPubBlankNodeOrIRI> tags = getTags();
		tags.add(tag);
		set(vocAs().tag(), tags);
	}

	@Override
	public void addTags(Set<Link> tags) {
		tags.forEach(this::addTag);
	}

	@Override
	public void addTag(Link tag) {
		addTag(tag.getSubject());
		this.add(tag.asGraph());
	}

	protected Optional<RDFTerm> getRdfTerm(RdfPubIRI predicate) {
		Stream<? extends Triple> stream = asGraph().stream(getSubjectToSearchGraph(), predicate, null);
		Set<RDFTerm> collect = stream
				.map(Triple::getObject)
				.collect(Collectors.toSet());
		if(collect.size()>1) {
			throw new IllegalStateException("Exact one hit for '"+predicate+"' expected, but was: " + collect.size());
		}
		return collect.stream().findFirst();
	}

	@Override
	public void setAttributedTo(Set<RdfPubBlankNodeOrIRI> attributedtTo) {
		set(vocAs().attributedTo(), attributedtTo);		
	}
	
	@Override
	public void addAttributedTo(RdfPubBlankNodeOrIRI attributedtTo) {
		add(vocAs().attributedTo(), attributedtTo);		
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getAudience() {
		return getIris(vocAs().audience());
	}

	@Override
	public void setAudience(Set<RdfPubBlankNodeOrIRI> audience) {
		set(vocAs().audience(), audience);
	}

	@Override
	public void addAudience(RdfPubBlankNodeOrIRI audience) {
		add(vocAs().audience(), audience);		
	}

	@Override
	public Optional<String> getContent() {
		// TODO contentMap
		RdfPubBlankNodeOrIRI subjectToSearchGraph = getSubjectToSearchGraph();
		return asGraph().stream(subjectToSearchGraph, vocAs().content(), null)
				.findFirst()
				.map(Triple::getObject)
				.map(RDFTerm::toString);
	}

	@Override
	public void setContent(String content) {
		set(vocAs().content(), rdf.createLiteral(content));
	}

	@Override
	public Optional<Instant> getPublished() {
		return asGraph().stream(getSubjectToSearchGraph(), vocAs().published(), null)
				.findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(this::toInstant);
	}

	@Override
	public void setPublished(Instant instant) {
		set(vocAs().published(), fromInstant(instant));
	}

	@Override
	public void setUpdated(Instant instant) {
		set(vocAs().updated(), fromInstant(instant));		
	}

	@Override
	public Optional<String> getSummary() {
		return asGraph().stream(getSubjectToSearchGraph(), vocAs().summary(), null).findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(Literal::getLexicalForm);
	}

	@Override
	public Optional<String> getSummary(LanguageCode languageCode) {
		// TODO name map !!
		return asGraph().stream(getSubjectToSearchGraph(), vocAs().summary(), null).findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(Literal::getLexicalForm);
	}

	@Override
	public void setSummary(LanguageCode languageCode, String summary) {
		// TODO name map !!
		set(vocAs().summary(), rdf.createLiteral(summary));
	}

	@Override
	public void setSummary(String summary) {
		set(vocAs().summary(), rdf.createLiteral(summary));
	}
	

//	@Override
//	public void addUpdated(Instant instant) {
//		add(vocAs().updated), fromInstant(instant));		
//	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getTo() {
		return getIris(vocAs().to());
	}

	@Override
	public void setTo(Set<RdfPubBlankNodeOrIRI> to) {
		set(vocAs().to(), to);
	}

	@Override
	public void addTo(RdfPubBlankNodeOrIRI to) {
		add(vocAs().to(), to);		
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getBto() {
		return getIris(vocAs().bto());
	}

	@Override
	public void setBto(Set<RdfPubBlankNodeOrIRI> bto) {
		set(vocAs().bto(), bto);
	}

	@Override
	public void addBto(RdfPubBlankNodeOrIRI bto) {
		add(vocAs().bto(), bto);		
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getCc() {
		return getIris(vocAs().cc());
	}

	@Override
	public void setCc(Set<RdfPubBlankNodeOrIRI> cc) {
		set(vocAs().cc(), cc);
	}

	@Override
	public void addCc(RdfPubBlankNodeOrIRI cc) {
		add(vocAs().cc(), cc);
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getBcc() {
		return getIris(vocAs().bcc());
	}

	@Override
	public void setBcc(Set<RdfPubBlankNodeOrIRI> bcc) {
		set(vocAs().bcc(), bcc);
	}

	@Override
	public void addBcc(RdfPubBlankNodeOrIRI bcc) {
		add(vocAs().bcc(), bcc);
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getReceivers() {
		HashSet<RdfPubBlankNodeOrIRI> receivers = new HashSet<>();
		receivers.addAll(getAudience());
		receivers.addAll(getTo());
		receivers.addAll(getBto());
		receivers.addAll(getCc());
		receivers.addAll(getBcc());
		return receivers;
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getExternalReceivers() {
		return getReceivers().stream()
			.filter(this::isExternal)
			.collect(Collectors.toSet());
	}

	@Override
	public Set<RdfPubObjectIdCollection> getInternalReceiverCollections() {
		Set<RdfPubBlankNodeOrIRI> internal = getReceivers().stream()
			.filter(RdfPubBlankNodeOrIRI::isCollection)
			.collect(Collectors.toSet());
		return internal.stream()
			.map(RdfPubBlankNodeOrIRI::asCollection)
			.map(Optional::orElseThrow)
			.collect(Collectors.toSet());
	}
	
	@Override
	public Set<RdfPubObjectIdActor> getInternalReceivers() {
		Set<RdfPubBlankNodeOrIRI> internal = getReceivers().stream()
			.filter(RdfPubBlankNodeOrIRI::isActor)
			.collect(Collectors.toSet());
		return internal.stream()
			.map(RdfPubBlankNodeOrIRI::asActor)
			.map(Optional::orElseThrow)
			.collect(Collectors.toSet());
	}

	private boolean isExternal(RdfPubBlankNodeOrIRI receiver) {
		return !isInternal(receiver);
	}
	
	private boolean isInternal(RdfPubBlankNodeOrIRI receiver) {
		boolean isIntern = receiver.isRdfPubObjectId();
		boolean isPublic = receiver.getIRIString().equals(vocAs().Public().getIRIString());
		return isIntern || isPublic;
	}
	
	@Override
	public void hideBlindReceivers() {
		setBcc(Collections.emptySet());
		setBto(Collections.emptySet());
	}

	@Override
	public boolean isAttributedTo(RdfPubBlankNodeOrIRI requestedActorId) {
		return getIris(vocAs().attributedTo()).contains(requestedActorId);
	}

	@Override
	public boolean isReceiver(RdfPubBlankNodeOrIRI requestedActorId) {
		return getReceivers().contains(requestedActorId);
	}

//	@Override
//	public void unify(ActivityPubObject activity) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void setNamespaces(Map<String, IRI> namespaces) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public ActivityPubObject addNamespaces(Map<String, IRI> namespaces) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public ActivityPubObject addNamespace(String prefix, IRI namespace) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Optional<Long> getObjectVersion() {
		return getLong(vocRdfPub().activityPubObjectVersion());
	}

	@Override
	public void setObjectVersion(Long objectVersion) {
		setLong(vocRdfPub().activityPubObjectVersion(), objectVersion);
	}
	
	@Override
	public void setWasAttributedTo(RdfPubBlankNodeOrIRI actor) {
		set(vocProv().wasAttributedTo(), actor);
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getWasAttributedTo() {
		return getIri(vocProv().wasAttributedTo());
	}

	@Override
	public void removeWasAttributedTo() {
		remove(getSubject(), vocProv().wasAttributedTo());
	}

	@Override
	public void setWasGeneratedBy(RdfPubBlankNodeOrIRI subject) {
		set(vocProv().wasGeneratedBy(), subject);		
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getWasGeneratedBy() {
		return getIri(vocProv().wasGeneratedBy());
	}

	@Override
	public void removeWasGeneratedBy() {
		remove(getSubject(), vocProv().wasGeneratedBy());
	}

	@Override
	public void setWasRevisionOf(RdfPubBlankNodeOrIRI subject) {
		set(vocProv().wasRevisionOf(), subject);		
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getWasRevisionOf() {
		return getIri(vocProv().wasRevisionOf());
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getSameAs() {
		return getIri(vocOwl().sameAs());
	}

	@Override
	public void setSameAs(RdfPubBlankNodeOrIRI subject) {
		set(vocOwl().sameAs(), subject);
	}

	@Override
	public void removeSameAs() {
		Set<RdfPubBlankNodeOrIRI> sameAsReferencesToRemove = asGraph().stream(getSubject(), vocOwl().sameAs(), null)
			.map(Triple::getObject)
			.filter(sub->sub instanceof IRI)
			.map(RDFTerm::toString)
			.map(activityPubObjectIdBuilder::createFromUrl)
			.collect(Collectors.toSet());

		// Remove all statements of the triples referenced by sameAs
		sameAsReferencesToRemove.forEach(sameAs->{	
			remove(sameAs);
		});
		
		// remove the sameAs property
		remove(getSubject(), vocOwl().sameAs());
	}

//	@Override
//	public Optional<ActivityPubObject> getResource(IRI property, boolean removeItself) {
//		// TODO Auto-generated method stub
//		return Optional.empty();
//	}
//
//	@Override
//	public Set<ActivityPubObject> getResources(IRI property, boolean removeItself) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	protected Literal fromInstant(Instant instant) {
		ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
		String isiIOnstantString = DateTimeFormatter.ISO_INSTANT.format(zdt);		
        return rdf.createLiteral(isiIOnstantString, vocXsdDatatypes().dateTime());
	}
	
	protected Instant toInstant(Literal dateTimeLiteral) {
		if(!vocXsdDatatypes().dateTime().equals(dateTimeLiteral.getDatatype())) {
			throw new IllegalStateException("unsupported Datatype: " + dateTimeLiteral.getDatatype());
		}
		String dateString = dateTimeLiteral.getLexicalForm();
		return Instant.parse(dateString);
	}

	protected void remove(RdfPubBlankNodeOrIRI subject) {
		asGraph().remove(subject, null, null);
	}

	protected void remove(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate) {
		asGraph().remove(subject, predicate, null);
	}

	protected void removeAll(Collection<RdfPubBlankNodeOrIRI> subjects) {
		subjects.forEach(this::remove);
	}

	@Override
	public String toString() {
		return getGraphToStringConverter().convert(RdfFormat.TURTLE, asGraph());
	}
	
	@Override
	public String toString(RdfFormat format) {
		return getGraphToStringConverter().convert(format, asGraph());
	}

	@Override
	public String toString(RdfFormat format, boolean resolveIris) {
		return getGraphToStringConverter().convert(format, asGraph(), resolveIris);
	}

	protected void add(Graph graphParam) {
		Graph currentGraph = asGraph();
		graphParam.stream()
			.forEach(currentGraph::add);
	}

	@Override
	public ActivityPubObjectConvertable asConvertable() {
		return new ActivityPubObjectConvertableDefault(vocabContainer(), data, this);
	}
	
	protected Optional<String> getStringLiteral(String predicate) {
		return asGraph().stream(getSubjectToSearchGraph(), rdf.createIRI(predicate), null).findFirst().map(Triple::getObject).map(RDFTerm::toString);
	}

	@Override
	public boolean isActivity() {
		return vocabContainer().isActivity(getTypes());
//		List<RdfPubIRI> knownTypes = Arrays.asList(ActivityType.values()).stream()
//				.map(t->t.getIri())
//				.collect(Collectors.toList());
//		boolean isActivity = false;
//		for (RdfPubIRI type : getTypes()) {
//			isActivity = knownTypes.contains(type);
//			if(isActivity) {
//				break;
//			}
//		}
//		return isActivity;
	}
	
	protected void replaceSubject(RdfPubBlankNodeOrIRI subject, RdfPubObjectId newSubject) {		
		Set<Triple> withSubject = asGraph().stream(subject, null, null)
				.collect(Collectors.toSet());
		Set<Triple> withReferences = asGraph().stream(null, null, subject)
				.collect(Collectors.toSet());

		Graph currentGraph = asGraph();
		withSubject.forEach(currentGraph::remove);
		withReferences.forEach(currentGraph::remove);
		
		withSubject.forEach(t->asGraph().add(newSubject, t.getPredicate(), t.getObject()));
		withReferences.forEach(t->asGraph().add(t.getSubject(), t.getPredicate(), newSubject));		
	}

	protected Set<RdfPubBlankNodeOrIRI> subjectsOfInternalGraph() {
		Set<RdfPubBlankNodeOrIRI> subjects = asGraph().stream(null, vocabContainer().vocRdf().type(), null)
			.map(Triple::getSubject)
			.map(BlankNodeOrIRI::toString)
			.map(activityPubObjectIdBuilder::createFromUrl)
			.collect(Collectors.toSet());
		return subjects;
	}
	
	public void partialUpdate(ActivityPubObject newOne) {
		Map<RdfPubIRI, Set<RDFTerm>> props = new HashMap<>();
		graphAsStream(newOne).forEach(triple->{
			if(!props.containsKey(triple.getPredicate())) {
				RdfPubIRI predicate = activityPubObjectIdBuilder.createFromUrl(triple.getPredicate().getIRIString());
				props.put(predicate, new HashSet<>());
			}
			Set<RDFTerm> terms = props.get(triple.getPredicate());			
			terms.add(triple.getObject());
		});
		props.keySet().forEach(predicate->{
			this.remove(newOne.getSubject(), predicate);
		});
		
		props.forEach((k,v)->{
			v.stream().forEach(term->this.add(k, term));
		});
	}
	
	private Stream<? extends Triple> graphAsStream(ActivityPubObject newOne) {
		return  newOne.asGraph()
				.stream(newOne.getSubject(), null, null)
				.collect(Collectors.toSet()) // because internal the use parallelStream !
				.stream();
	}
	
	@Override
	public Set<RdfPubBlankNodeOrIRI> getUrls() {
		return getIris(vocAs().url());
	}

	@Override
	public Optional<ActivityPubObject> getFirstIcon() {
		Optional<RdfPubBlankNodeOrIRI> iconUrlOpt = getIris(vocAs().icon()).stream().findFirst();
		return iconUrlOpt.flatMap(i->this.asConvertable().resolve(i));		
	}
}
