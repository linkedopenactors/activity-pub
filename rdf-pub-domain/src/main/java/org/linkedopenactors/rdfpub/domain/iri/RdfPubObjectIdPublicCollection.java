package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.RDF;

class RdfPubObjectIdPublicCollection extends RdfPubObjectIdBase implements RdfPubObjectIdCollection {
	
	private RdfPubIRI asPublic;

	public RdfPubObjectIdPublicCollection(RdfPubIRIFactory vocabContainer, RdfPubIRI asPublic, RDF rdf, String baseIriString, String instanceDomainString,
			String instanceActorIdentifier, String actorIdentifier) {
		super(vocabContainer, rdf, baseIriString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
		this.asPublic = asPublic;
	}

	@Override
	public RdfPubBlankNodeOrIRI getSubject() {
		return asPublic;
	}

	@Override
	public RdfPubIRI getBaseSubject() {
		return asPublic;
	}

	@Override
	public String getCollectionIdentifier() {
		return VocAs.Public.replace(VocAs.NS, "");
	}
}
