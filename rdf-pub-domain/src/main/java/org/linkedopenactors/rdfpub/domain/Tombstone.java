package org.linkedopenactors.rdfpub.domain;

import java.time.Instant;

public interface Tombstone extends ActivityPubObject {

	/**
	 * On a Tombstone object, the deleted property is a timestamp for when the object was deleted. 
	 * @param instant
	 */
	void setDeleted(Instant instant);
}
