package org.linkedopenactors.rdfpub.domain.iri;

public interface RdfPubObjectIdCache extends RdfPubObjectId {
	String getCacheId();
}
