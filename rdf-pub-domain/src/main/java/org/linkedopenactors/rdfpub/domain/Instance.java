package org.linkedopenactors.rdfpub.domain;

import java.net.URL;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;


public interface Instance extends Actor {
	
	static final String INSTANCE_ACTOR_NAME = "instanceActor";
	
	/**
	 * Gives you the Domain behind the rdf-pub server is running. That is also the base namespace of all rdf objets.
	 * @return The Domain.
	 * @deprecated will be removed!
	 */
	@Deprecated
	URL getDomain();
	
	/**
	 * @return The {@link RdfPubObjectIdCollection} of the instance's inbox collection. 
	 */
	RdfPubObjectIdCollection getCollectionInbox();
	
	/**
	 * @return The {@link RdfPubObjectIdCollection} of the instance's outbox collection. 
	 */
	RdfPubObjectIdCollection getCollectionOutbox();
	
	/**
	 * @return The {@link RdfPubObjectIdCollection} of the instance's public collection. 
	 */
	RdfPubObjectIdCollection getCollectionPublic();
	
	/**
	 * @return The application Version a.k.a Release Version. It's the maven version of the rdf-pub project.
	 */
	String getAppVersion();
	
	/**
	 * @return The Version of the profile. e.g. 1, but it's possible, that the profile changes in newer rdf-pub versions.
	 */
	Long getProfileVersion();
	
	/**
	 * @return SPARQL Endpoint to query the public collection.
	 */
	RdfPubBlankNodeOrIRI getPublicSparqlEndpoint();
	
	void setType(RdfPubBlankNodeOrIRI id);

	void setInboxCollection(RdfPubBlankNodeOrIRI inboxCollection);

	void setOutboxCollection(RdfPubBlankNodeOrIRI outboxCollection);

	void setPublicCollection(RdfPubBlankNodeOrIRI publicCollection);

	void setInstanceVersion(String mavenVersion);

	void setCommitId(String commitId);

	void setPublicSparqlEndpoint(RdfPubBlankNodeOrIRI sparqlPublic);
}
