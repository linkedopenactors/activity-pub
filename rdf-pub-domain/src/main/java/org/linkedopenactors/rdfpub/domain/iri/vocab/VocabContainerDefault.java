package org.linkedopenactors.rdfpub.domain.iri.vocab;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocLDP;
import org.linkedopenactors.rdfpub.domain.iri.VocOwl;
import org.linkedopenactors.rdfpub.domain.iri.VocPending;
import org.linkedopenactors.rdfpub.domain.iri.VocProv;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;
import org.linkedopenactors.rdfpub.domain.iri.VocRdfPub;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.linkedopenactors.rdfpub.domain.iri.VocSecurity;
import org.linkedopenactors.rdfpub.domain.iri.VocXsdDatatypes;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;

@Component
public class VocabContainerDefault implements VocabContainer {

	private List<RdfPubBlankNodeOrIRI> knownActivityTypes;
	private RDF rdf;
	
	public VocabContainerDefault(RDF rdf) {
		this.rdf = rdf;
		knownActivityTypes = Arrays.asList(vocAs().Accept(), vocAs().Create(), vocAs().Update(),
				vocAs().Delete(), vocAs().Undo(), vocAs().Follow(), vocAs().Ignore(), vocAs().Reject(), vocAs().Like(),
				vocAs().Remove());
	}
	
	
	@Override
	public RdfPubIRI asPublic() {
		return vocAs().Public();
	}


	@Override
	public VocAs vocAs() {
		return new VocAsDefault(rdf);
	}

	@Override
	public VocLDP vocLDP() {
		return new VocLDPDefault(rdf);
	}

	@Override
	public VocOwl vocOwl() {
		return new VocOwlDefault(rdf);
	}

	@Override
	public VocPending vocPending() {
		return new VocPendingDefault(rdf);
	}

	@Override
	public VocProv vocProv() {
		return new VocProvDefault(rdf);
	}

	@Override
	public VocRdf vocRdf() {
		return new VocRdfDefault(rdf);
	}

	@Override
	public VocRdfPub vocRdfPub() {
		return new VocRdfPubDefault(rdf);
	}

	@Override
	public VocSchema vocSchema() {
		return new VocSchemaDefault(rdf);
	}

	@Override
	public VocSecurity vocSecurity() {
		return new VocSecurityDefault(rdf);
	}

	@Override
	public VocXsdDatatypes vocXsdDatatypes() {
		return new VocXsdDatatypesDefault(rdf);
	}

	@Override
	public boolean isActivity(Set<RdfPubIRI> rdfPubIRIs) {
		return rdfPubIRIs.stream()
				.filter(this::isActivity)
				.count() > 0;
	}
	
	@Override
	public boolean isActivity(RdfPubIRI rdfPubIRI) {
		return knownActivityTypes.contains(rdfPubIRI);
	}

//	@Override
//	public RdfPubIRI createRdfPubIRI(String iriAsString) {
//		return new RdfPubIRIDefault(rdf.createIRI(iriAsString));
//	}

	@Override
	public RdfPubIRI createRdfPubIriIri(String iriAsString) {
		return new RdfPubIRIDefault(rdf.createIRI(iriAsString));
	}
}
