package org.linkedopenactors.rdfpub.domain.iri;

public interface VocOwl {
	public static final String NS = "http://www.w3.org/2002/07/owl#";
	public final static String sameAs = NS + "sameAs";

	RdfPubIRI sameAs();
}
