package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Set;

public interface VocabContainer extends RdfPubIRIFactory { // todo rename e.g. iriMagic

	VocAs vocAs();
	VocLDP vocLDP();
	VocOwl vocOwl();
	VocPending vocPending();
	VocProv vocProv();
	VocRdf vocRdf();
	VocRdfPub vocRdfPub();
	VocSchema vocSchema();
	VocSecurity vocSecurity();
	VocXsdDatatypes vocXsdDatatypes();
	
	boolean isActivity(RdfPubIRI rdfPubIRI);
	boolean isActivity(Set<RdfPubIRI> rdfPubIRIs);

}