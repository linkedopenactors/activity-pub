package org.linkedopenactors.rdfpub.domain;

import java.util.Set;

import org.apache.commons.rdf.api.Literal;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

public class OrderedCollectionPageDefault extends ActivityPubObjectDefault implements OrderedCollectionPage {

	public OrderedCollectionPageDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}
	
	@Override
	public RdfPubBlankNodeOrIRI partOf() {
		return getIri(vocAs().partOf()).orElseThrow();
	}

	@Override
	public long totalItems() {
		return  asGraph().stream(getSubjectToSearchGraph(), vocAs().totalItems(), null)
				.findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(l -> l.getLexicalForm())
				.map(Long::valueOf)
				.orElse(0L);
	}

	@Override
	public void setPartOf(RdfPubBlankNodeOrIRI partOf) {
		set(vocAs().partOf(), partOf);
	}

	@Override
	public void setTotalItems(int size) {
		set(vocAs().totalItems(), rdf.createLiteral(Integer.toString(size), rdf.createIRI("http://www.w3.org/2001/XMLSchema#nonNegativeInteger")));
		
	}

	@Override
	public void addObject(ActivityPubObject object) {
		add(vocAs().items(), object.getSubject());
		this.add(object.asGraph());
	}

	@Override
	public void setItems(Set<RdfPubBlankNodeOrIRI> items) {
		set(vocAs().items(), items);
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getItems() {
		return getIris(vocAs().items());
	}
}
