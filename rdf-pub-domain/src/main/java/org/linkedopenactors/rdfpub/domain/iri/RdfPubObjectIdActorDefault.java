package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.RDF;

class RdfPubObjectIdActorDefault extends RdfPubObjectIdBase implements RdfPubObjectIdActor {
	
	private UUID actorRevision;
	private String preferredUsername;
	
	public RdfPubObjectIdActorDefault(RdfPubObjectIdActorDefault id) {
		super(id.rdfPubIRIFactory, id.rdf, id.internalUrnString, id.instanceDomainString, id.instanceActorIdentifier, id.actorIdentifier);
		id.getRevision().ifPresent(this::setRevision);
	}
	
	/**
	 * @param rdfPubIRIFactory
	 * @param rdf
	 * @param internalUrnString The internal instance independent urn prefix.
	 * @param instanceDomainString The URL which routes to this instance.
	 * @param instanceActorIdentifier
	 * @param actorIdentifier The unique id of the actor. That is not the subject, but a part of the subject!
	 */
	public RdfPubObjectIdActorDefault(RdfPubIRIFactory rdfPubIRIFactory, RDF rdf, String internalUrnString, String instanceDomainString, String instanceActorIdentifier, String actorIdentifier) {
		super(rdfPubIRIFactory, rdf, internalUrnString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
	}

	public Optional<String> getPreferredUsername() {
		return Optional.ofNullable(preferredUsername);
	}

	public void setPreferredUsername(String preferredUsername) {
		this.preferredUsername = preferredUsername;
	}
	
	public void setRevision(UUID actorRevision) {
		this.actorRevision = actorRevision;
	}

	@Override
	public Optional<UUID> getRevision() {
		return Optional.ofNullable(actorRevision);
	}

	@Override
	public RdfPubBlankNodeOrIRI getSubject() {
		String val = isRevision() ? super.getBaseSubjectString() + "/" + actorRevision : super.getBaseSubjectString();
		return new RdfPubIRIDefault(rdfPubIRIFactory.createRdfPubIriIri(val));
	}

	@Override
	public RdfPubIRI getBaseSubject() {
		return super.getBaseActor();
	}

	@Override
	public boolean isRevision() {
		return Optional.ofNullable(actorRevision).isPresent();
	}

	@Override
	public String toString() {
		return getSubject().getIRIString();
	}
}
