package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.RDF;

class RdfPubObjectIdCollectionDefault extends RdfPubObjectIdBase implements RdfPubObjectIdCollection {

	private String collectionIdentifier;

	public RdfPubObjectIdCollectionDefault(RdfPubIRIFactory vocabContainerParam, RdfPubObjectIdCollectionDefault id) {
		super(id.rdfPubIRIFactory, id.rdf, id.internalUrnString, id.instanceDomainString, id.instanceActorIdentifier, id.actorIdentifier);
		this.collectionIdentifier = id.getCollectionIdentifier();
	}
	
	public RdfPubObjectIdCollectionDefault(RdfPubIRIFactory vocabContainerParam, RDF rdf, String baseIriString, String instanceDomainString, String instanceActorIdentifier, String actorIdentifier, String collectionIdentifier) {
		super(vocabContainerParam, rdf, baseIriString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
		this.collectionIdentifier = collectionIdentifier;
	}

	@Override
	public RdfPubBlankNodeOrIRI getSubject() {
		return getBaseSubject();
	}

	@Override
	public RdfPubIRI getBaseSubject() {
		return rdfPubIRIFactory.createRdfPubIriIri(super.getBaseSubjectString() + "/" + COLLECTION_TOKEN + "/" + collectionIdentifier);
	}

	@Override
	public String getCollectionIdentifier() {
		return collectionIdentifier;
	}
}
