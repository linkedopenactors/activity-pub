package org.linkedopenactors.rdfpub.domain;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

import lombok.Data;

@Data
class DomainObjectInitialazationData {
	private final RDF rdf; 
	private final RdfPubBlankNodeOrIRI subject; 
	private final Graph graph;
	private final GraphToStringConverter graphToStringConverter;
	private final ActivityPubObjectIdBuilder activityPubObjectIdBuilder; 
	private final PrefixMapper prefixMapper;
}
