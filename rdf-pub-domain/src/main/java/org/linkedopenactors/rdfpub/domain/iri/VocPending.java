package org.linkedopenactors.rdfpub.domain.iri;

public interface VocPending {
	public static final String NS = "https://purl.archive.org/socialweb/pending#";
	
	public static final String pendingFollowers = NS + "pendingFollowers";
	public static final String pendingFollowing = NS + "pendingFollowing";
	
	RdfPubIRI pendingFollowing();

	RdfPubIRI pendingFollowers();
}
