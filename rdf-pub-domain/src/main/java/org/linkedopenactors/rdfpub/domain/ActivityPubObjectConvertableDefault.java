package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdResource;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

class ActivityPubObjectConvertableDefault implements ActivityPubObjectConvertable {

	private RDF rdf;
	private GraphToStringConverter graphToStringConverter;
	private ActivityPubObject activityPubObject;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private PrefixMapper prefixMapper;
	private VocabContainer vocabContainer;
	
	public ActivityPubObjectConvertableDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data,
			ActivityPubObjectDefault activityPubObject) {
		this.vocabContainer = vocabContainer;
		this.rdf = data.getRdf();
		this.graphToStringConverter = data.getGraphToStringConverter();
		this.activityPubObject = activityPubObject;
		this.activityPubObjectIdBuilder = data.getActivityPubObjectIdBuilder();
		this.prefixMapper = data.getPrefixMapper();
	}

	@Override
	public Activity asActivity() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new ActivityDefault(vocabContainer, data);
	}

	@Override
	public Actor asActor() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new ActorDefault(vocabContainer,data);
	}

	@Override
	public Tombstone asTombstone() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new TombstoneDefault(vocabContainer,data);
	}

	@Override
	public Optional<ActivityPubObject> resolve(RdfPubBlankNodeOrIRI subject) {
		Set<? extends Triple> triples = activityPubObject.asGraph().stream(subject, null, null).collect(Collectors.toSet());
		Optional<? extends Triple> firstTriple = triples.stream().findFirst();
		if(firstTriple.isPresent()) {
			DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, subject, activityPubObject.asGraph(),
					graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
			ActivityPubObjectDefault value = new ActivityPubObjectDefault(vocabContainer, data);
			return Optional.of(value);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public ActivityPubObject isolated() {
		RdfPubBlankNodeOrIRI subject = activityPubObject.getSubject();
		Graph isolated = rdf.createGraph();
		Graph g = activityPubObject.asGraph();
		g.stream()
			.filter(s->s.getSubject().equals(subject))
			.forEach(isolated::add);
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, subject, isolated,
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new ActivityPubObjectDefault(vocabContainer, data);
	}

	@Override
	public ActivityPubObject asRevison() {
		RdfPubObjectIdResource revisionSubject = activityPubObjectIdBuilder.createResource(activityPubObject.getSubject());
		Graph revision = rdf.createGraph();
		activityPubObject.asGraph().stream()
			.filter(triple->triple.getSubject().equals(activityPubObject.getSubject()))
			.forEach(t->{
				revision.add(revisionSubject, t.getPredicate(), t.getObject());
			});

		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, revisionSubject, revision,
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);

		ActivityPubObjectDefault revisionAo = new ActivityPubObjectDefault(vocabContainer, data);
		revisionAo.setWasRevisionOf(activityPubObject.getSubject());
		return revisionAo;
	}

	@Override
	public PublicKey asPublicKey() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new PublicKeyDefault(vocabContainer, data);
	}

	@Override
	public Endpoints asEndpoints() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new EndpointsDefault(vocabContainer, data);
	}

	@Override
	public Instance asInstance() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new InstanceDefault(vocabContainer,data);
	}

	@Override
	public OrderedCollectionPage asOrderedCollectionPage() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new OrderedCollectionPageDefault(vocabContainer, data);
	}

	@Override
	public Link asLink() {
		DomainObjectInitialazationData data = new DomainObjectInitialazationData(rdf, activityPubObject.getSubject(), activityPubObject.asGraph(),
				graphToStringConverter, activityPubObjectIdBuilder, prefixMapper);
		return new LinkDefault(vocabContainer,data);
	}

	@Override
	public String toString() {
		return activityPubObject.toString();
	}
}
