package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocProv;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActivityDefault extends ActivityPubObjectDefault implements Activity {

	public ActivityDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getObject() {
		return getIris(vocAs().object());
	}

	@Override
	public void setObject(Set<RdfPubBlankNodeOrIRI> object) {
		set(vocAs().object(), object);		
	}

	@Override
	public void addObject(ActivityPubObject activityPubObject) {
		add(vocAs().object(), activityPubObject.getSubject());
		this.add(activityPubObject.asGraph());
	}

	@Override
	public void setObjects(Set<ActivityPubObject> activityPubObjects) {
		set(vocAs().object(), activityPubObjects.stream()
				.map(ActivityPubObject::getSubject)
				.collect(Collectors.toSet()));		
		removeAll(getObject());
		activityPubObjects.stream()
			.map(ActivityPubObject::asGraph)
			.forEach(this::add);
	}

	@Override
	public void addObject(RdfPubBlankNodeOrIRI object) {
		add(vocAs().object(), object);		
	}

	@Override
	public Set<ActivityPubObject> resolveObject() {
		return getObject().stream()
				.map(asConvertable()::resolve)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toSet());
	}

	public Set<ActivityPubObject> resolveAllOfGraph() {
		Set<RdfPubBlankNodeOrIRI> subjects = subjectsOfInternalGraph();
		subjects.remove(getSubject());
		Set<ActivityPubObject> others = subjects.stream()
			.map(s->asConvertable().resolve(s))
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toSet());
		return others;
	}
	
	@Override
	public Set<RdfPubIRI> getActor() {
		return getIriIris(vocAs().actor());
	}

//	@Override
//	public AT activityType() {		
//		Set<RdfPubIRI> types = getIris(vocRdf().type());
//		
//		Set<ActivityType> activityTypes = types.stream()
//			.map(ActivityType::valueOf)
//			.filter(Optional::isPresent)
//			.map(Optional::get)
//			.collect(Collectors.toSet());
//		
//		
//		if(activityTypes.stream().count()>1) {
//			throw new IllegalStateException("activity has more than one type!");
//		}
//		
//		Optional<ActivityType> type = activityTypes.stream().findFirst();
//		if(type.isEmpty()) {
//			log.error("no activityType!");
//		}
//		return type.orElseThrow(()->new IllegalStateException("no activityType! " + toString()));
//	}

//	@Override
//	public boolean is(AT activityType) {
//		return activityType().equals(activityType);
//	}

	@Override
	public void addActor(RdfPubBlankNodeOrIRI actor) {
		add(vocAs().actor(), actor);		
	}

	@Override
	public void setActor(Set<RdfPubBlankNodeOrIRI> actor) {
		set(vocAs().actor(), actor);		
	}

	@Override
	public void setTarget(RdfPubBlankNodeOrIRI target) {
		set(vocAs().target(), target);		
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getTarget() {
		return getIris(vocAs().target()).stream().findFirst();
	}

	@Override
	public boolean isActivity() {
		return true;
	}

	@Override
	public void setWasAssociatedWith(RdfPubBlankNodeOrIRI actor) {
		set(vocProv().wasAssociatedWith(), actor);		
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getWasAssociatedWith() {
		return getIris(vocProv().wasAssociatedWith()).stream().findFirst();
	}

	@Override
	public void removeWasAssociatedWith() {
		remove(getSubject(), vocProv().wasAssociatedWith());		
	}

	@Override
	public void unify() {
		Set<RdfPubBlankNodeOrIRI> audience = getAudience();
		Set<RdfPubBlankNodeOrIRI> cc = getCc();
		Set<RdfPubBlankNodeOrIRI> bcc = getBcc();
		Set<RdfPubBlankNodeOrIRI> to = getTo();
		Set<RdfPubBlankNodeOrIRI> bto = getBto();
		
		// collect all receivers
		Set<ActivityPubObject> objects = resolveObject();
		objects.forEach(o->{
			audience.addAll(o.getAudience());
			cc.addAll(o.getCc());
			bcc.addAll(o.getBcc());
			to.addAll(o.getTo());
			bto.addAll(o.getBto());
		});
		
		// apply collected receivers to all objects
		objects.forEach(o->{
			o.setAudience(audience);
			o.setCc(cc);
			o.setBcc(bcc);
			o.setTo(to);
			o.setBto(bto);
		});
		
		// apply collected receivers to activity
		setAudience(audience);
		setCc(cc);
		setBcc(bcc);
		setTo(to);
		setBto(bto);
	}


	@Override
	public void replaceSubjects(RdfPubObjectIdActor actorsBaseSubject) {
		replaceSubjects(actorsBaseSubject, true); 
	}
	
	@Override
	public void replaceSubjects(RdfPubObjectIdActor actorsBaseSubject, boolean includingObjects) {
		replaceActivitySubject(actorsBaseSubject);
		if(includingObjects) {
			// get all subjects of the objects without the activity subject
			Set<RdfPubBlankNodeOrIRI> subjects = asGraph().stream()
					.map(Triple::getSubject)
					.filter(IRI.class::isInstance)
					.map(IRI.class::cast)
					.filter(s->!s.getIRIString().equals(getSubject().getIRIString()))
					.map(IRI::getIRIString)
					.map(activityPubObjectIdBuilder::createFromUrl)
					.filter(this::isEmbedded)
					.collect(Collectors.toSet());
			subjects
				.forEach(s->replaceSubject(s, activityPubObjectIdBuilder.createResource(actorsBaseSubject)));
		}
		setExternalGraph();		
	}

	private boolean isEmbedded(RdfPubBlankNodeOrIRI iri) {
		long triples = asGraph().stream(iri, vocRdf().type(), null).count();
		return triples > 0;
	}
	
	private RdfPubObjectIdActivity replaceActivitySubject(RdfPubObjectIdActor actorsBaseSubject) {
		RdfPubObjectIdActivity newSubject = activityPubObjectIdBuilder.createActivity(actorsBaseSubject);
		replaceSubject(getSubject(), newSubject);
		setSubject(newSubject);
		
		// TODO passt das auch für Actors "RdfPubObjectIdResource" ??
		
		return newSubject; 
	}

	@Override
	public void replaceSubjectsWithInternals() {
		Set<RdfPubBlankNodeOrIRI> objectSubjects = asGraph().stream()
				.map(Triple::getSubject)
				.filter(IRI.class::isInstance)
				.map(IRI.class::cast)
				.map(IRI::getIRIString)
				.map(activityPubObjectIdBuilder::createFromUrl)
				.collect(Collectors.toSet());

		objectSubjects
			.forEach(s->{
				RdfPubObjectId newIri = activityPubObjectIdBuilder.createFromUrl(s.getIRIString()).asRdfPubObjectId().orElseThrow();
//				RdfPubIRI resolveExternal = RdfPubIRIDefault.valueOf(subjectProvider.resolveInternal(s));
				replaceSubject(s, newIri);
			});
		setExternalGraph();
	}

	@Override
	public void replaceObject(ActivityPubObject existing) {
		this.asGraph().remove(existing.getSubject(), null, null);
		this.setObjects(Set.of(existing));		
	}

	@Override
	public void hideBlindReceivers() {
		super.hideBlindReceivers();
		resolveObject().forEach(ActivityPubObject::hideBlindReceivers);
	}
}

