package org.linkedopenactors.rdfpub.domain;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;

import com.neovisionaries.i18n.LanguageCode;


public interface ActivityPubObject extends LinkOrObject {

//	Set<IRI> getAttachment();
//	void addAttachment(IRI attachment);
	
	/**
	 * Identifies one or more entities to which this object is attributed. The
	 * attributed entities might not be Actors. For instance, an object might be
	 * attributed to the completion of another activity.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attributedto
	 * 
	 * @return attributedtTo
	 */
	Set<RdfPubBlankNodeOrIRI> getAttributedtTo();
	
	/**
	 * Identifies one or more entities to which this object is attributed. The
	 * attributed entities might not be Actors. For instance, an object might be
	 * attributed to the completion of another activity.
	 * See Also:  https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attributedto
	 * 
	 * @param attributedtTo
	 */
	void setAttributedTo(Set<RdfPubBlankNodeOrIRI> attributedtTo);
	
	/**
	 * Identifies one or more entities to which this object is attributed. The
	 * attributed entities might not be Actors. For instance, an object might be
	 * attributed to the completion of another activity.
	 * See Also:  https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attributedto
	 * 
	 * @param attributedtTo
	 */
	void addAttributedTo(RdfPubBlankNodeOrIRI attributedtTo);

	/**
	 * Identifies one or more entities that represent the total population of
	 * entities for which the object can considered to be relevant.
	 * See Also:  https://www.w3.org/TR/activitystreams-vocabulary/#dfn-audience
	 * 
	 * @return audience
	 */
	Set<RdfPubBlankNodeOrIRI> getAudience();
	
	/**
 	 * Identifies one or more entities that represent the total population of
 	 * entities for which the object can considered to be relevant.
	 * See Also:  https://www.w3.org/TR/activitystreams-vocabulary/#dfn-audience
	 * 
	 * @param audience
	 */
	void addAudience(RdfPubBlankNodeOrIRI audience);
	
	/**
 	 * Identifies one or more entities that represent the total population of
 	 * entities for which the object can considered to be relevant.
	 * See Also:  https://www.w3.org/TR/activitystreams-vocabulary/#dfn-audience
	 * 
	 * @param audience
	 */
	void setAudience(Set<RdfPubBlankNodeOrIRI> audience);

	Optional<String> getContent();
	void setContent(String content);
//	void addContent(String content);
//	void addContent(String language, String content); // TODO language as enum, ist there already an iso thing ?	
	
//	context  
	
//	endTime | generator | icon | image | inReplyTo | location | preview | 
	
	/**
	 * The date and time describing the actual or expected starting time of the
	 * object. When used with an Activity object, for instance, the startTime
	 * property specifies the moment the activity began or is scheduled to begin.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-published
	 * 
	 * @return publishing date
	 */
	Optional<Instant> getPublished();
	
	/**
	 * The date and time describing the actual or expected starting time of the
	 * object. When used with an Activity object, for instance, the startTime
	 * property specifies the moment the activity began or is scheduled to begin.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-published
	 * 
	 * @param instant
	 */
	void setPublished(Instant instant);
	
//	replies | startTime  

	/**
	 * A natural language summarization of the object encoded as HTML. Multiple
	 * language tagged summaries MAY be provided.
	 * 
	 * @param languageCode
	 * @return The summary
	 */
	Optional<String> getSummary(LanguageCode languageCode);
	
	/**
	 * Same as {@link ActivityPubObject#getSubject()} but without language or with default language.
	 * @return summary
	 */
	Optional<String> getSummary();
	
	/**
	 * A natural language summarization of the object encoded as HTML. Multiple
	 * language tagged summaries MAY be provided.
	 * 
	 * @param languageCode
	 * @param summary
	 */
	void setSummary(LanguageCode languageCode, String summary);

	/**
	 * Same as {@link ActivityPubObject#setSummary(String)} but without language or with default language.
	 * 
	 * @param summary
	 */
	void setSummary(String summary);
	
	Set<RdfPubBlankNodeOrIRI> getTags();
	void addTag(RdfPubBlankNodeOrIRI tag);
	void addTag(Link tag);
	void addTags(Set<Link> tags);
	
	void setUpdated(Instant instant);

//	url | 

	/**
	 * Identifies an entity considered to be part of the public primary audience of an Object 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-to
	 * 
	 * @return to
	 */
	Set<RdfPubBlankNodeOrIRI> getTo();
	
	/**
	 * Identifies an entity considered to be part of the public primary audience of an Object 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-to
	 * 
	 * @param to
	 */
	void addTo(RdfPubBlankNodeOrIRI to);
	
	/**
	 * Identifies an entity considered to be part of the public primary audience of an Object 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-to
	 * 
	 * @param to
	 */
	void setTo(Set<RdfPubBlankNodeOrIRI> to);

	/**
	 * Identifies an Object that is part of the private primary audience of this Object.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bto 
	 * 
	 * @return bto
	 */
	Set<RdfPubBlankNodeOrIRI> getBto();
	
	/**
	 * Identifies an Object that is part of the private primary audience of this Object.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bto 
	 * 
	 * @param bto
	 */
	void addBto(RdfPubBlankNodeOrIRI bto);

	/**
	 * Identifies an Object that is part of the private primary audience of this Object.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bto 
	 * 
	 * @param bto
	 */
	void setBto(Set<RdfPubBlankNodeOrIRI> bto);

	/**
	 * Identifies an Object that is part of the public secondary audience of this Object. 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-cc
	 * 
	 * @return cc
	 */
	Set<RdfPubBlankNodeOrIRI> getCc();	

	/**
	 * Identifies an Object that is part of the public secondary audience of this Object. 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-cc
	 * 
	 * @param cc
	 */
	void addCc(RdfPubBlankNodeOrIRI cc);

	/**
	 * Identifies an Object that is part of the public secondary audience of this Object. 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-cc
	 * 
	 * @param cc
	 */
	void setCc(Set<RdfPubBlankNodeOrIRI> cc);
	
	/**
	 * Identifies one or more Objects that are part of the private secondary audience of this Object. 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bcc
	 * 
	 * @return bcc
	 */
	Set<RdfPubBlankNodeOrIRI> getBcc();
	
	/**
	 * Identifies one or more Objects that are part of the private secondary audience of this Object. 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bcc
	 * 
	 * @param bcc
	 */
	void addBcc(RdfPubBlankNodeOrIRI bcc);
	
	/**
	 * Identifies one or more Objects that are part of the private secondary audience of this Object. 
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bcc
	 * 
	 * @param bcc
	 */
	void setBcc(Set<RdfPubBlankNodeOrIRI> bcc);

//	mediaType | duration
	
	/**
	 * Collection of {@link ActivityPubObject#getTo()},
	 * {@link ActivityPubObject#getBto()}, {@link ActivityPubObject#getCc()},
	 * {@link ActivityPubObject#getBcc()}, {@link ActivityPubObject#getAudience()}
	 * 
	 * @return {@link Set} of all receivers.
	 */
	Set<RdfPubBlankNodeOrIRI> getReceivers();

	/**
	 * Same as {@link #getReceivers()}, but only the internal ones.
	 * @return {@link Set} of all internal receivers.
	 */
	Set<RdfPubObjectIdActor> getInternalReceivers();
	
	Set<RdfPubObjectIdCollection> getInternalReceiverCollections();
	
	/**
	 * Same as {@link #getReceivers()}, but only the external ones.
	 * @return {@link Set} of all external receivers.
	 */
	Set<RdfPubBlankNodeOrIRI> getExternalReceivers();
	
	/**
	 * Removes {@link VocAs#bcc} AND {@link VocAs#bto()}
	 */
	void hideBlindReceivers();

	/**
	 * Checks, if the passed ActorId is in the {@link VocAs#attributedTo()} of the {@link ActivityPubObject}.
	 * @param requestedActorId
	 * @return True, if the passed ActorId is in the {@link VocAs#attributedTo()} of the {@link ActivityPubObject}.
	 */
	boolean isAttributedTo(RdfPubBlankNodeOrIRI requestedActorId);

	/**
	 * Checks, if the passed ActorId is in {@link ActivityPubObject#getReceivers()}
	 * 
	 * @param requestedActorId
	 * @return True, if the passed ActorId is in {@link ActivityPubObject#getReceivers()}
	 */
	boolean isReceiver(RdfPubBlankNodeOrIRI requestedActorId);
	
	
	
//	ActivityPubObject clone();
	
//	void setIdentifier(String identifier);
//	Optional<Resource> getSuccessorOf();
//	void setContext(Set<Resource> contexts);
//	void setNamespaces(Map<String, IRI> namespaces);
//	ActivityPubObject addNamespaces(Map<String, IRI> namespaces);
//	ActivityPubObject addNamespace(String prefix, IRI namespace);
	
//	Set<Resource> contexts();
//	Map<String, IRI> namespaces();
//	ActivityPubObject addContext(Set<Resource> contexts);
//	ActivityPubObject addContext(Resource context);
//	Long getVersion();
	Optional<Long> getObjectVersion();
	void setObjectVersion(Long objectVersion);
//	void setVersion(Long version);
//	String getIdentifier();
	
//	Optional<Resource> getSameAs();
//	Optional<Resource> getWasGeneratedBy();
//	Optional<Resource> getWasRevisionOf();
//	Map<String,String> replaceSubjects(ResourceSubjectProvider subjectProvider);
//	String replaceSubject(ResourceSubjectProvider subjectProvider);
//	String replaceSubject(String newSubject);
//	void setSameAs(Resource subject);
//	void init(ActivityPubObject source);
	
//	/**
//	 * Overwrites all {@link PredicateDefault}s of this {@link ActivityPubObject} with the
//	 * {@link PredicateDefault}s of the passed {@link ActivityPubObject}. All {@link PredicateDefault}s in
//	 * the passed parameter 'expect' are not overwritten!
//	 */
//	void overwrite(ActivityPubObject object, Set<IRI> expect);
	
//	@Deprecated
//	void setSuccessorOf(Resource successorOf);
//	void replaceName(String value);
//	void visit(PredicateVisitor visitor);
////	void visit(RdfObjectVisitor visitor);
//	void visitReferences(PredicateVisitor visitor);
//	void removeSameAs();
	
	String toString(RdfFormat format);
	
	/**
	 * Converts the passed graph/model iinto a string.
	 * @param format The format that the string should have, Turtle, Json-LD, ...
	 * @param resolveIris True, if the internal iris should be replaced with external https:// urls.
	 * @return The graph/model as string in the passed format.
	 */
	String toString(RdfFormat format, boolean resolveIris);
//	String toPrettyString();
	
//	ActivityPubObjectConvertable asConvertable();

//	/**
//	 * Attention! If removeItself is true, the underlaying graph is cloned, so changes did not affect the original object.
//	 * @param predicate The predicate, that points to the resource to be resolved.
//	 * @param removeItself If true, the statements of this Object are removde. makes maybe sense, if this is an Activity.
//	 * @return The resolved Resource as {@link ActivityPubObject}
//	 */
//	Optional<ActivityPubObject> getResource(IRI property, boolean removeItself);
//
//	/**
//	 * Attention! If removeItself is true, the underlaying graph is cloned, so changes did not affect the original object.
//	 * @param predicate The predicate, that points to the resource to be resolved.
//	 * @param removeItself If true, the statements of this Object are removde. makes maybe sense, if this is an Activity.
//	 * @return The resolved Resources as {@link ActivityPubObject}s
//	 */
//	Set<ActivityPubObject> getResources(IRI property, boolean removeItself);
	
	//	Optional<ActivityPubObject> getResource(RpProperty predicate, boolean clone);
//	Set<ActivityPubObject> getResources(RpProperty property);

//	boolean is(RpResource activity);

//	void setWasRevisionOf(Resource subject);
//
//	void setWasGeneratedBy(Resource subject);

//	/**
//	 * copies all statements of the passed {@link ActivityPubObject} owned by the passed resource to itself. 
//	 * @param activity
//	 * @param subject
//	 */
//	void copyStatementsOf(ActivityPubObject activity, Resource subject);
//
//	void removeWasRevisionOf();

	ActivityPubObjectConvertable asConvertable();

	boolean isActivity();

	void setWasAttributedTo(RdfPubBlankNodeOrIRI subject);
	Optional<RdfPubBlankNodeOrIRI> getWasAttributedTo();
	void removeWasAttributedTo();

	void setWasGeneratedBy(RdfPubBlankNodeOrIRI subject);
	Optional<RdfPubBlankNodeOrIRI> getWasGeneratedBy();
	void removeWasGeneratedBy();

	void setWasRevisionOf(RdfPubBlankNodeOrIRI subject);
	Optional<RdfPubBlankNodeOrIRI> getWasRevisionOf();
	
	void setSameAs(RdfPubBlankNodeOrIRI subject);
	Optional<RdfPubBlankNodeOrIRI> getSameAs();
	void removeSameAs();
	
	Set<RdfPubBlankNodeOrIRI> getUrls(); 
	
	void partialUpdate(ActivityPubObject newOne);
	
	Optional<ActivityPubObject> getFirstIcon();
}
