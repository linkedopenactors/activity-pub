package org.linkedopenactors.rdfpub.domain.iri;

import java.util.UUID;


public interface RdfPubObjectIdActivity extends RdfPubObjectId {
	UUID getActivityIdentifier();
}
