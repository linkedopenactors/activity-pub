package org.linkedopenactors.rdfpub.domain.iri;

public interface RdfPubObjectIdCollection extends RdfPubObjectId {
	String getCollectionIdentifier();
}
