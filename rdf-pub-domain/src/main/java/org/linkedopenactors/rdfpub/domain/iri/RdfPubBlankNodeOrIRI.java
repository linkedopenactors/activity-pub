package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Optional;

import org.apache.commons.rdf.api.BlankNodeOrIRI;

public interface RdfPubBlankNodeOrIRI extends BlankNodeOrIRI {

	/**
	 * @return True, if this {@link RdfPubBlankNodeOrIRI} represents a rdf-pub internal {@link RdfPubObjectIdResource}.
	 */
	default boolean isResource() {
		return this instanceof RdfPubObjectIdResource;
	}

	/**
	 * @return True, if the ir represents a blankNode
	 */
	boolean isBlankNode();

	/**
	 * @return this {@link RdfPubBlankNodeOrIRI} as a rdf-pub internal {@link RdfPubObjectIdResource}, if {@link #isResource()} is true.
	 */
	default Optional<RdfPubObjectIdResource> asResource() {
		if(isResource()) {
			return Optional.of((RdfPubObjectIdResource)this);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * @return True, if this {@link RdfPubBlankNodeOrIRI} represents an rdf-pub internal {@link RdfPubObjectIdActor}.
	 */
	default boolean isActor() {
		return this instanceof RdfPubObjectIdActor;
	}

	/**
	 * @return this {@link RdfPubBlankNodeOrIRI} as a rdf-pub internal {@link RdfPubObjectIdActor}, if {@link #isActor()} is true.
	 */
	default Optional<RdfPubObjectIdActor> asActor() {
		if(isActor()) {
			return Optional.of((RdfPubObjectIdActor)this);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * @return True, if this {@link RdfPubBlankNodeOrIRI} represents an rdf-pub internal {@link RdfPubObjectIdCollection}.
	 */
	default boolean isCollection() {
		return this instanceof RdfPubObjectIdCollection;
	}

	/**
	 * @return this {@link RdfPubBlankNodeOrIRI} as a rdf-pub internal {@link RdfPubObjectIdCollection}, if {@link #isCollection()} is true.
	 */
	default Optional<RdfPubObjectIdCollection> asCollection() {
		if(isCollection()) {
			return Optional.of((RdfPubObjectIdCollection)this);
		} else {
			return Optional.empty();
		}
	}
	
	/**
	 * @return this {@link RdfPubBlankNodeOrIRI} as a rdf-pub internal {@link RdfPubObjectIdPublicCollection}, if {@link #isPublicCollection()} is true.
	 */
	default Optional<RdfPubObjectIdPublicCollection> asPublicCollection() {
		if(isPublicCollection()) {
			return Optional.of((RdfPubObjectIdPublicCollection)this);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * @return True, if this {@link RdfPubBlankNodeOrIRI} represents a rdf-pub internal {@link RdfPubObjectIdActivity}.
	 */
	default boolean isActivity() {
		return this instanceof RdfPubObjectIdActivity;
	}

	/**
	 * @return this {@link RdfPubBlankNodeOrIRI} as a rdf-pub internal {@link RdfPubObjectIdActivity}, if {@link #isActivity()} is true.
	 */
	default Optional<RdfPubObjectIdActivity> asActivity() {
		if(isActivity()) {
			return Optional.of((RdfPubObjectIdActivity)this);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * @return True, if this {@link RdfPubBlankNodeOrIRI} represents a rdf-pub internal {@link RdfPubObjectId}.
	 */
	default boolean isRdfPubObjectId() {
		return this instanceof RdfPubObjectId;
	}
	
	/**
	 * @return True, if this {@link RdfPubBlankNodeOrIRI} represents the public collection.
	 */
	default boolean isPublicCollection() {
		return this instanceof RdfPubObjectIdPublicCollection;
	}
	
	default Optional<RdfPubObjectId> asRdfPubObjectId() {
		if(isRdfPubObjectId()) {
			return Optional.of((RdfPubObjectId)this);
		} else {
			return Optional.empty();
		}
	}
	
	/**
     * Wrapper while convertin from IRI only to IRI & blankNodes.
     */
    String getIRIString();	
}