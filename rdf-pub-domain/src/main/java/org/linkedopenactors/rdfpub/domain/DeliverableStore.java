package org.linkedopenactors.rdfpub.domain;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

/**
 * Store, that is able to receive activities. Thats a common interface for
 * standard Actor Stores and the public collection. 11/2024 i removed the public
 * actor and so there was no actorsRepository any more for the public actor. but
 * the public collection must be able to receive activities!
 */
@Deprecated // we have to eliminate that !!!
public interface DeliverableStore {
	RdfPubBlankNodeOrIRI deliver(Activity activity);
}
