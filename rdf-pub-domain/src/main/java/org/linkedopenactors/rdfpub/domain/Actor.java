package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;


public interface Actor extends ActivityPubObject, ActorId {
	
	/**
	 * Gives you the OUTBOX_SPARQL endpoint of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 * @return The OUTBOX_SPARQL endpoint of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 */
//	IRI getOutboxSparqlEndpoint();
	
	/**
	 * Gives you the INBOX_SPARQL endpoint of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 * @return The INBOX_SPARQL endpoint of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 */
//	IRI getInboxSparqlEndpoint();

	/**
	 * Gives you the <a href="https://www.w3.org/TR/activitypub/#outbox">outbox</a> of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 * @return The <a href="https://www.w3.org/TR/activitypub/#outbox">outbox</a> of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 */
	RdfPubBlankNodeOrIRI getOutbox();
	void setOutbox(RdfPubBlankNodeOrIRI outboxIri);
	
	/**
	 * Gives you the <a href="https://www.w3.org/TR/activitypub/#inbox">inbox</a> of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 * @return The <a href="https://www.w3.org/TR/activitypub/#inbox">inbox</a> of the <a href="https://www.w3.org/TR/activitypub/#actor-objects">actor</a>
	 */
	RdfPubBlankNodeOrIRI getInbox();
	void setInbox(RdfPubBlankNodeOrIRI inboxIri);
	
	RdfPubBlankNodeOrIRI getPendingFollowers();
	void setPendingFollowers(RdfPubBlankNodeOrIRI pendingFollowersIri);
	
	RdfPubBlankNodeOrIRI getFollowers();
	void setFollowers(RdfPubBlankNodeOrIRI followersIri);
	
	Optional<RdfPubBlankNodeOrIRI> getPendingFollowing();
	void setPendingFollowing(RdfPubBlankNodeOrIRI pendingFollowing);

	RdfPubBlankNodeOrIRI getFollowing();
	void setFollowing(RdfPubBlankNodeOrIRI following);

	RdfPubBlankNodeOrIRI getLiked();
	void setLiked(RdfPubBlankNodeOrIRI liked);
	
	/**
	 * 
	 * @return The unique id of the user.
	 */
	Optional<String> getOauth2IssuerUserId();
	void setOauth2IssuerUserId(String userId);
	
	Optional<String> getOauth2Issuer();
	void setOauth2Issuer(String issuer);
	
	void setPreferredUsername(String string);
	Optional<String> getPreferredUsername();
	
	/**
	 * Gets the {@link RdfPubBlankNodeOrIRI} of the publicKey.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the publicKey.
	 */
	Optional<RdfPubBlankNodeOrIRI> getPublicKey();

	/**
	 * Gets the {@link RdfPubBlankNodeOrIRI} of the publicKey. 
	 * @param resolveIris True, if the internal iris should be replaced with external https:// urls.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the publicKey.
	 */
	Optional<RdfPubBlankNodeOrIRI> getPublicKey(boolean resolveIris);
	Optional<PublicKey> resolvePublicKey();
	void setPublicKey(PublicKey pk);
	
	Optional<RdfPubBlankNodeOrIRI> getEndpoints();
	void setEndpoints(Endpoints endpoints);
	Optional<Endpoints> resolveEndpoints();
}
