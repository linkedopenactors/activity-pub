package org.linkedopenactors.rdfpub.domain;

import java.util.UUID;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdResource;



public interface ActivityPubObjectIdBuilder {
	
	RdfPubObjectIdCollection createRdfPubObjectIdPublicCollection();
	
	RdfPubObjectIdActor createActor();
	RdfPubObjectIdActor createActor(String actorIdentifier);
	RdfPubObjectIdActor createActor(RdfPubObjectIdActor actor);
	RdfPubObjectIdActor createActorRevision(String actorIdentifier, UUID actorRevision);
	RdfPubObjectIdActor createActor(RdfPubBlankNodeOrIRI iri);

	RdfPubObjectIdResource createResource(RdfPubObjectIdActor actorIdentifier);
	RdfPubObjectIdResource createResource(RdfPubObjectIdResource resource);
	RdfPubObjectIdResource createResource(RdfPubObjectIdActor actorIdentifier, UUID resourceIdentifier);
	RdfPubObjectIdResource createResource(RdfPubObjectIdActor actorIdentifier, UUID resourceIdentifier, UUID resourceRevision);
	RdfPubObjectIdResource createResource(RdfPubBlankNodeOrIRI iri);
	
//	RdfPubIriIri createOauthAuthorizationEndpoint(RdfPubIRI actorId);
//	RdfPubIriIri createOauthTokenEndpoint(RdfPubIRI actorId);
	
	RdfPubObjectIdActivity createActivity(RdfPubObjectId actorId);
	RdfPubObjectIdActivity createActivity(RdfPubBlankNodeOrIRI actorId);
	RdfPubObjectIdActivity createActivity(RdfPubObjectIdActivity activity);

	RdfPubIRI createFromUrl(String subject);
	RdfPubBlankNodeOrIRI createFromUrl(RdfPubBlankNodeOrIRI subject);
	RdfPubBlankNodeOrIRI create(RdfPubBlankNodeOrIRI subject);
	
	/**
	 * Just create an Endpoint for this instance like
	 * https://dev.rdf-pub.org/oauth/oauthAuthorizationEndpoint.
	 * 
	 * @param path
	 * @return the passed iri as {@link RdfPubBlankNodeOrIRI}.
	 */
	RdfPubIRI create(String path);
	
	RdfPubObjectId instanceActorSubject();
	RdfPubObjectIdCollection createPublicCollection();
	RdfPubObjectIdCollection createCollection(RdfPubObjectId actorId, String collectionId);
	RdfPubObjectIdCollection createCollection(RdfPubBlankNodeOrIRI actorId, String collectionId);

	RdfPubBlankNodeOrIRI createDummy();

	RdfPubIRI createPendingFollowerCacheId(RdfPubIRI recipientActorId);
	RdfPubIRI createPendingFollowingCacheId(RdfPubIRI recipientActorId);
}
