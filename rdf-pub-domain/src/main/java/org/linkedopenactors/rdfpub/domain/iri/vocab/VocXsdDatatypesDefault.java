package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocXsdDatatypes;

public class VocXsdDatatypesDefault implements VocXsdDatatypes {
	
	private RDF rdf;

	public VocXsdDatatypesDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI nonNegativeInteger() {
		return new RdfPubIRIDefault(rdf.createIRI(nonNegativeInteger));
	}

	public RdfPubIRI string() {
		return new RdfPubIRIDefault(rdf.createIRI(string));
	}

	public RdfPubIRI decimal() {
		return new RdfPubIRIDefault(rdf.createIRI(decimal));
	}

	public RdfPubIRI integer() {
		return new RdfPubIRIDefault(rdf.createIRI(integer));
	}

	public RdfPubIRI long_() {
		return new RdfPubIRIDefault(rdf.createIRI(long_));
	}

	public RdfPubIRI floatType() {
		return new RdfPubIRIDefault(rdf.createIRI(float_type));
	}

	public RdfPubIRI booleanType() {
		return new RdfPubIRIDefault(rdf.createIRI(boolean_type));
	}

	public RdfPubIRI date() {
		return new RdfPubIRIDefault(rdf.createIRI(date));
	}
	
	public RdfPubIRI dateTime() {
		return new RdfPubIRIDefault(rdf.createIRI(dateTime));
	}
		
	public RdfPubIRI time() {
		return new RdfPubIRIDefault(rdf.createIRI(time));
	}
	
	public RdfPubIRI duration() {
		return new RdfPubIRIDefault(rdf.createIRI(duration));
	}
}
