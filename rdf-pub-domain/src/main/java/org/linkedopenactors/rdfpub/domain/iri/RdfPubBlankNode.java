package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.BlankNode;

public interface RdfPubBlankNode extends RdfPubBlankNodeOrIRI, BlankNode  {

}
