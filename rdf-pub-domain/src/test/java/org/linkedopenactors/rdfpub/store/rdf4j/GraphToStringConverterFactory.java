package org.linkedopenactors.rdfpub.store.rdf4j;

import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;

public class GraphToStringConverterFactory {

	public static GraphToStringConverter createGraphToStringConverter(PrefixMapper prefixMapper) {
		return new GraphToStringConverterDefault(prefixMapper);
	}
}
