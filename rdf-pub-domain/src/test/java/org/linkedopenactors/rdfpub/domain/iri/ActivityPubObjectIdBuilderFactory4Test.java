package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;

public class ActivityPubObjectIdBuilderFactory4Test {

	public static ActivityPubObjectIdBuilder create(RdfPubIRIFactory vocabContainer, RDF rdf, PrefixMapper prefixMapper) {
		return new ActivityPubObjectIdBuilderDefault(vocabContainer, rdf, prefixMapper);
	}
}
