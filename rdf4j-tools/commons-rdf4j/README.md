This is a adjusted copy of https://github.com/apache/commons-rdf/tree/master/commons-rdf-rdf4j

I have no idea how we can provide a customized implementation in context of apache.commons-rdf. They may be in the process of switching from jdk 8 to jdk 11. 
The versions of RDF4J are not really compatible and it makes little sense to keep the interfaces and all implementations together. apache-commons-jena maintains its own implementation of apache-commons-rdf. See https://github.com/apache/commons-rdf/pull/196#issuecomment-1951378506

Sure this part of the libs is licensed also with Apache License 2.0.
If there are any problems regarding the license, let me know!