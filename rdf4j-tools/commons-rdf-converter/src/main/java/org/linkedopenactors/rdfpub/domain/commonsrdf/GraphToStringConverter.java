package org.linkedopenactors.rdfpub.domain.commonsrdf;

import org.apache.commons.rdf.api.Graph;

public interface GraphToStringConverter {
	String convert(RdfFormat rdfFormat, Graph graph);
	
	/**
	 * @param rdfFormat
	 * @param graph
	 * @param resolveIris True, if the internal iris should be replaced with external https:// urls.
	 * @return The grap as string.
	 */
	String convert(RdfFormat rdfFormat, Graph graph, boolean resolveIris);
	
	/**
	 * Takes the graph, converts it to a String in the passed {@link RdfFormat} and
	 * replaces all external IRI's with internal ones. E.g. 'http://localhost:8080/' -> 'urn:rdfpub:'
	 * 
	 * @param rdfFormat
	 * @param graph
	 * @return The passed graph as String with internal IRI's.
	 */
	String internalize(RdfFormat rdfFormat, Graph graph);
	
	/**
	 * Takes the graph, converts it to a String in the passed {@link RdfFormat} and
	 * replaces all internal IRI's with external ones. E.g. 'urn:rdfpub:' -> 'http://localhost:8080/' 
	 * 
	 * @param rdfFormat
	 * @param graph
	 * @return The passed graph as String with external IRI's.
	 */
	String externalize(RdfFormat rdfFormat, Graph graph);
}
