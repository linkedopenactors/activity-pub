package org.rdfpub.vocabulary;

//import org.apache.commons.rdf.api.IRI;
//import org.linkedopenactors.rdfpub.domain.iri.RdfPubIriIri;
//
//public class RdfPubIRIIriDefault implements RdfPubIriIri {
//	
//    private final IRI iri;
//
//    public RdfPubIRIIriDefault(final IRI iriParam) {
//    	this.iri = iriParam;
//    }
//    
//	public boolean isBlankNode() {
//		return false;
//	}
//
//    @Override
//    public boolean equals(final Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null || !(obj instanceof IRI)) {
//            return false;
//        }
//        final IRI other = (IRI) obj;
//        boolean equals = getIRIString().equals(other.getIRIString());
//		return equals;
//    }
//
//    @Override
//    public String getIRIString() {    	
//    	if(iri instanceof IRI) {
//    		return ((IRI)iri).getIRIString();
//    	} else {
//    		return iri.toString();
//    	}
//    }
//
//    @Override
//    public int hashCode() {
//        return iri.hashCode();
//    }
//
//    @Override
//    public String ntriplesString() {
//        return "<" + getIRIString() + ">";
//    }
//
//    @Override
//    public String toString() {
//        return getIRIString();
//    }
//}
//
