package org.linkedopenactors.rdfpub.store.rdf4j;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Values;

class XsdDateTime implements CoreDatatype {

	@Override
	public IRI getIri() {
		return Values.iri("https://www.w3.org/TR/xmlschema11-2/#dateTime");
	}

}
