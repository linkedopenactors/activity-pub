package org.linkedopenactors.rdfpub.store.rdf4j;

import org.eclipse.rdf4j.rio.RDFFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;

class RdfFormatConverter {

	public static RDFFormat toRdf4j(RdfFormat rdfFormat) {
		switch (rdfFormat) {
		case JSONLD: 
			return RDFFormat.JSONLD;

		case RDFXML: 
			return RDFFormat.RDFXML;

		case TRIG: 
			return RDFFormat.TRIG;

		default:
			return RDFFormat.TURTLE;
		}
	}
}
