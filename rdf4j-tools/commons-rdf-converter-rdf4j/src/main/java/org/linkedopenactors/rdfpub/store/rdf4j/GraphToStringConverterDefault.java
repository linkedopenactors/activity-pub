package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.StringWriter;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class GraphToStringConverterDefault implements GraphToStringConverter {

	private RDF4J rdf4j;
	private PrefixMapper prefixMapper;

	public GraphToStringConverterDefault(PrefixMapper prefixMapper) {
		this.prefixMapper = prefixMapper;
		rdf4j = new RDF4J();
	}
	
	@Override
	public String convert(RdfFormat rdfFormat, Graph graph) {
		return convert(rdfFormat, graph, false);
	}

	@Override
	public String convert(RdfFormat rdfFormat, Graph graph, boolean resolveIris) {
		Model asModel = asModel(graph);
		return convert(rdfFormat, asModel, resolveIris);
	}

	public String internalize(RdfFormat rdfFormat, Graph graph) {
		Model asModel = asModel(graph);
		String modelAsString = modelToString(rdfFormat, asModel);
		return internalize(modelAsString);
	}

	public String externalize(RdfFormat rdfFormat, Graph graph) {
		Model asModel = asModel(graph);
		String modelAsString = modelToString(rdfFormat, asModel);
		return externalize(modelAsString);
	}

	/**
	 * Converts the passed graph/model into a stinrg.
	 * @param rdfFormat The format that the string should have, Turtle, Json-LD, ...
	 * @param asModel The graph/model to convert into a string
	 * @param resolveIris true, if all urls should be externalized. E.g. urn:rdfpub: -> https://rdf-pub.org
	 * @return The graph/model as string in the passed format. 
	 */
	private String convert(RdfFormat rdfFormat, Model asModel, boolean resolveIris) {
		
		String modelAsString = modelToString(rdfFormat, asModel);
		
		if(resolveIris) {
			if(prefixMapper==null) {
				log.warn("subjectProvider in null! Unable to resolve iris !!");
			} else {
				String subject = asModel.filter(null, null, null).stream().findAny().orElseThrow().getSubject().toString();
				if( subject.startsWith("http") ) {
					modelAsString = internalize(modelAsString);
				} else {
					modelAsString = externalize(modelAsString);
				}
			}
		}				
		if(RdfFormat.JSONLD.equals(rdfFormat)) {
			modelAsString = new CompacterDefault().compact(asModel, modelAsString);
		}
		return modelAsString;
	}

	private String modelToString(RdfFormat rdfFormat, Model asModel) {
		StringWriter sw = new StringWriter();		
		Rio.write(asModel, sw, RdfFormatConverter.toRdf4j(rdfFormat));
		String asString = sw.toString();
		return asString;
	}

	private String internalize(String graphAsString) {
		graphAsString = graphAsString.replaceAll(getExternalPrefix(), getInternalPrefix());
		return graphAsString;
	}

	private String getInternalPrefix() {
		String internaleIriPrefix = prefixMapper.getInternaleIriPrefix();
		internaleIriPrefix = internaleIriPrefix.endsWith("/") ? internaleIriPrefix.substring(0, internaleIriPrefix.length()-1) : internaleIriPrefix;
		return internaleIriPrefix;
	}

	private String getExternalPrefix() {
		String externalIriPrefix = prefixMapper.getExternalIriPrefix();
		externalIriPrefix = externalIriPrefix.endsWith("/") ? externalIriPrefix : externalIriPrefix + "/";
		return externalIriPrefix;
	}
	
	private String externalize(String graphAsString) {
		graphAsString = graphAsString.replaceAll(getInternalPrefix(), getExternalPrefix());
		return graphAsString;
	}

	private Model asModel(Graph graph) {
		Model model = new ModelBuilder().build();
		graph.stream().forEach(t->model.add(rdf4j.asStatement(t)));
		return model;
	}
}
