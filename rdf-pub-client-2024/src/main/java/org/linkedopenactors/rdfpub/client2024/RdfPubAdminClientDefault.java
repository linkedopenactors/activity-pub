package org.linkedopenactors.rdfpub.client2024;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
class RdfPubAdminClientDefault implements RdfPubAdminClient {

	private String serverBaseUrl;
	private HttpAdapter httpAdapter;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private ObjectReader setOfStringReader;
	private ObjectMapper objectMapper;

	public RdfPubAdminClientDefault(HttpAdapter httpAdapter, ActivityPubObjectFactory activityPubObjectFactory,
			@Value("${rdf-pub-server.url}") String serverBaseUrlParam) {
		this.httpAdapter = httpAdapter;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.serverBaseUrl = serverBaseUrlParam.endsWith("/") ? serverBaseUrlParam : serverBaseUrlParam + "/";
		objectMapper = new ObjectMapper();
		setOfStringReader = objectMapper.readerFor(new TypeReference<Set<String>>() {});
	}

	@Override
	public List<Actor> discoverActors() {
		Response response = httpAdapter.get(HttpAdapter.MEDIA_TYPE_TURTLE, serverBaseUrl + "/admin/actors/persons");
		log.debug("response: " + response);
		HttpStatus status = HttpStatus.valueOf(response.getStatus());

		List<Actor> actors = Collections.emptyList();
		if (status.is2xxSuccessful()) {
			actors = response.getBody().map(body -> activityPubObjectFactory.createActorList(RdfFormat.TURTLE, body))
					.orElseThrow().stream().map(activityPubObject -> activityPubObject.asConvertable())
					.map(convertable -> convertable.asActor()).collect(Collectors.toList());

		} else {
			log.warn("discoverActors failed: " + response);
		}
		return actors;
	}

	@Override
	public Set<String> getGraphNames(String actorId) {
		Response response = httpAdapter.get(MediaType.APPLICATION_JSON,
				serverBaseUrl + "/admin/" + actorId + "/graphNames");
		return response.getBody()
				.map(this::toSet)
				.orElse(Collections.emptySet());
	}

	private Set<String> toSet(String body) {
		try {
			JsonNode arrayNode = objectMapper.readTree(body);
			return setOfStringReader.readValue(arrayNode);
		} catch (Exception e) {
			throw new IllegalStateException("unable to parse response", e);
		}
	}
}
