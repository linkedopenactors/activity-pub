package org.linkedopenactors.rdfpub.client2024;

import java.util.Map;
import java.util.Optional;

import lombok.Data;

@Data
public class Response {
	private Map<String, Object> headers;
	private String body;
	private int status;
	
	public Response(String body, int status, Map<String, Object> headers) {
		this.body = body;
		this.status = status;
		this.headers = headers;		
	}
	
	public Optional<String> getBody() {
		return Optional.ofNullable(body);
	}
}
