package org.linkedopenactors.rdfpub.client2024;

import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;

public interface RdfPubClient {

	Optional<String> sparql(Actor actor, String query);
	
	Optional<Actor> discoverActor(String oauth2Name);
	
	Optional<Actor> readActor(String subject);
	Optional<Actor> readActor(RdfPubObjectIdActor actorId);
		
	/**
	 * Reads the object with the passed Domain only on this instance.
	 * @param subject
	 * @return Found Object.
	 */
	Optional<ActivityPubObject> readObject(String subject);	
	
	Optional<String> readPlain(String subject);
	
	Optional<ActivityPubObject> readObject(RdfPubObjectId subject);
	
	/**
	 * Reads the object with the passed Domain, independent, if it is on this instance or on any other instance!
	 * @param subject
	 * @return Found Object.
	 */
	Optional<ActivityPubObject> resolveObject(String subject);
	
	String update(Actor actor, String resource);
	
	// macht das überhaupt sinn? aus C2S perspektive wird immer in die outbox gepostet. nur S2S wäre das eine Option !
	@Deprecated
	RdfPubBlankNodeOrIRI sendToInbox(Actor actor, Activity activity);
	
	Optional<Activity> readActivity(String locationOfInitialActivity);
	
	Optional<Activity> readActivity(RdfPubObjectIdActivity locationOfInitialActivity);
		
	
	Set<Activity> readPublicActivityCollection();
	Set<Activity> readActivityCollection(RdfPubObjectIdCollection collection);
	Set<Activity> readActivityCollection(RdfPubObjectIdCollection collection, int pageSize, int startIndex);

	Set<Actor> readActorCollection(RdfPubObjectIdCollection collection);
	
	/**
	 * Creates a new, empty ActivityPubObject, that can be updated and send to the outbox.  
	 * @return a new, empty ActivityPubObject, that can be updated and send to the outbox.
	 */
	ActivityPubObject createNewEmpty();
	
	RdfPubObjectIdActivity update(Actor actor, ActivityPubObject ao);
	
	/**
	 * Sends the passed {@link ActivityPubObject} to the actors outbox. 
	 * @param ao The Object to create.
	 * @param actor The actor who creates the new object
	 * @return The location of the newly created CREATE activity.
	 */
	RdfPubObjectIdActivity sendToOutbox(Actor actor, ActivityPubObject ao);

	@Deprecated // Use sendToOutbox(Actor actor, ActivityPubObject ao); 
	String sendToOutbox(Actor actor, String resource);

	/**
	 * Creates an {@link RdfPubBlankNodeOrIRI} from the passed String.
	 * @param iriString
	 * @return {@link RdfPubBlankNodeOrIRI} created from the passed String.
	 */
	RdfPubBlankNodeOrIRI toIri(String iriString);
}
