package org.linkedopenactors.rdfpub.client2024;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.jayway.jsonpath.JsonPath;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RdfPubClientDefault implements RdfPubClient {

	private String serverBaseUrl;
	private HttpAdapter httpAdapter;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	public RdfPubClientDefault(HttpAdapter httpAdapter, ActivityPubObjectFactory activityPubObjectFactory,
			@Value("${rdf-pub-server.url}") String serverBaseUrlParam,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.httpAdapter = httpAdapter;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
		this.serverBaseUrl = serverBaseUrlParam.endsWith("/") ? serverBaseUrlParam : serverBaseUrlParam + "/";
	}

	@Override
	public Optional<String> sparql(Actor actor, String query) {
		String actorBaseUrl = actor.getSubject().asActor().orElseThrow().getExternalBaseSubject().getIRIString();
		String url = actorBaseUrl + "/todoCollection/sparql";
		Response response = httpAdapter.post(url, query, MediaType.APPLICATION_JSON);		
		return response.getBody();
	}

	@Override
	public Optional<Actor> readActor(String subject) {
		return readObject(subject)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor);
	}

	@Override
	public Optional<Actor> readActor(RdfPubObjectIdActor actorId) {
		return readActor(actorId.getExternalSubject().getIRIString());
	}

	@Override
	public Optional<Activity> readActivity(RdfPubObjectIdActivity locationOfInitialActivity) {
		return readActivity(locationOfInitialActivity.getExternalSubject().getIRIString());
	}

	@Override
	public Optional<Activity> readActivity(String subject) {
		return readObject(subject)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActivity);
	}

	@Override
	public Optional<ActivityPubObject> readObject(RdfPubObjectId subject) {
		return readObject(subject.getExternalSubject().getIRIString());
	}

	@Override
	public Optional<ActivityPubObject> readObject(String subject) {
		return httpAdapter.get(subject).getBody()
			.map(this::toTurtle)
			.map(s->activityPubObjectFactory.create(subject, s));
	}

	@Override
	public Optional<String> readPlain(String subject) {
		return httpAdapter.get(subject).getBody();
	}

	@Override
	public Optional<ActivityPubObject> resolveObject(String subject) {
		return httpAdapter.get(serverBaseUrl + "/resolve?url=" + subject).getBody()
				.map(this::toTurtle)
				.map(s->activityPubObjectFactory.create(subject, s));
	}

	private String toTurtle(String jsonLd) {
		StringWriter sw = new StringWriter();
		Model m;
		try {			
			m = Rio.parse(new StringReader(jsonLd), RDFFormat.JSONLD);
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			log.error("text to parse: " + jsonLd);
			throw new IllegalStateException("unable to read object", e);
		}
		Rio.write(m, sw, RDFFormat.TURTLE);
		return sw.toString();
	}

	@Override
	public RdfPubBlankNodeOrIRI sendToInbox(Actor actor, Activity activity) {
		String url = actor.getInbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		Response response = httpAdapter.post(url, activity.toString(RdfFormat.TURTLE), RdfFormat.TURTLE);
		return activityPubObjectIdBuilder.createFromUrl(extractLocation(response));
	}

	@Override
	public String sendToOutbox(Actor actor, String resource) {
		String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		Response response = httpAdapter.post(url, resource);
		return extractLocation(response);
	}

	@Override
	public RdfPubObjectIdActivity sendToOutbox(Actor actor, ActivityPubObject ao) {
		String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		Response response = httpAdapter.post(url, ao.toString(RdfFormat.TURTLE), RdfFormat.TURTLE);
		String location = extractLocation(response);
		return activityPubObjectIdBuilder.createFromUrl(location).asActivity().orElseThrow();
	}

	@Override
	public RdfPubObjectIdActivity update(Actor actor, ActivityPubObject ao) {
		RdfPubBlankNodeOrIRI rdfPubIRI = actor.getOutbox();
		if(actor.getOutbox().isRdfPubObjectId()) {
			rdfPubIRI = actor.getOutbox().asRdfPubObjectId().orElseThrow().getExternalSubject();
		}
		String asTurtleString = ao.toString(RdfFormat.TURTLE);
		Response response = httpAdapter.post(rdfPubIRI.getIRIString(), asTurtleString, RdfFormat.TURTLE);
		String location = extractLocation(response);
		return activityPubObjectIdBuilder.createFromUrl(location).asActivity().orElseThrow();
	}

	@Override
	public String update(Actor actor, String resource) {
		Response response = httpAdapter.post(actor.getOutbox().toString(), resource);
		return extractLocation(response);
	}
	
	@Override
	public Optional<Actor> discoverActor(String oauth2Name) {
		String url = serverBaseUrl + "/.well-known/webfinger?resource=acct:" + oauth2Name;
		return httpAdapter.get(url).getBody()
				.map(this::extractHref)
				.flatMap(this::readActor);
	}

	@Override
	public Set<Activity> readPublicActivityCollection() {
		RdfPubObjectIdCollection col = activityPubObjectIdBuilder.createCollection(activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME), VocAs.Public.replace(VocAs.NS, ""));
		return readActivityCollectionInternal(col);
	}

	@Override
	public Set<Activity> readActivityCollection(RdfPubObjectIdCollection collection) {
		return readActivityCollectionInternal(collection);
	}

	@Override
	public Set<Actor> readActorCollection(RdfPubObjectIdCollection collection) {
		return readActorCollectionInternal(collection);
	}
	
	@Override
	public Set<Activity> readActivityCollection(RdfPubObjectIdCollection collection, int pageSize, int startIndex) {
		HttpHeaderAttribute pageSizeAttr = new HttpHeaderAttribute("pageSize", Integer.toString(pageSize));		
		HttpHeaderAttribute startIndexAttr = new HttpHeaderAttribute("startIndex", Integer.toString(startIndex));
		return readActivityCollectionInternal(collection, pageSizeAttr, startIndexAttr);
	}
	
	@Override
	public RdfPubBlankNodeOrIRI toIri(String item) {
		return activityPubObjectIdBuilder.createFromUrl(item);
	}

	@Override
	public ActivityPubObject createNewEmpty() {
		RdfPubBlankNodeOrIRI tmpSubject = activityPubObjectIdBuilder.createDummy();
		return activityPubObjectFactory.create(tmpSubject);
	}

	private String extractLocation(Response response) {
		String location;
		
		Object loc = response.getHeaders().get(HttpHeaders.LOCATION);
		if(loc instanceof String) {
			location = (String)loc;
		} else {
			@SuppressWarnings("unchecked")
			Collection<String> locations = (Collection<String>)response.getHeaders().get(HttpHeaders.LOCATION);
			Boolean hasExactOneElement = Optional.ofNullable(locations)
				.map(collection->collection.size())
				.map(size-> size==1)
				.orElse(false);
			if(!hasExactOneElement) {
				throw new IllegalStateException("Location does not exactly have one value! -> " + locations);
			} else {
				location = locations.stream().findFirst().orElseThrow(()->new IllegalStateException("Location does not have first element!"));
			}
		}
		return location;
	}
	
	private Set<Activity> readActivityCollectionInternal(RdfPubObjectIdCollection collection, HttpHeaderAttribute...headerAttributes) {
		
		String url = collection.getExternalSubject().getIRIString();
		
		log.debug("->readActivityCollectionInternal("+collection+")");	
		Response activitiesAsString = httpAdapter.get(HttpAdapter.MEDIA_TYPE_TURTLE, url, headerAttributes);
		List<Activity> activities = activitiesAsString.getBody()
			.map(body->activityPubObjectFactory.createActivityList(RdfFormat.TURTLE, body))
			.orElse(Collections.emptyList());
		log.debug("<-readActivityCollectionInternal("+collection+") found: " + activities.size());
		return new HashSet<Activity>(activities);
	}

	private Set<Actor> readActorCollectionInternal(RdfPubObjectIdCollection collection, HttpHeaderAttribute...headerAttributes) {
		
		String url = collection.getExternalSubject().getIRIString();
		
		log.debug("->readActorCollectionInternal("+collection+")");	
		Response actorsAsString = httpAdapter.get(HttpAdapter.MEDIA_TYPE_TURTLE, url, headerAttributes);
		List<Actor> actors = actorsAsString.getBody()
			.map(body->activityPubObjectFactory.createActorList(RdfFormat.TURTLE, body))
			.orElse(Collections.emptyList());
		log.debug("<-readActorCollectionInternal("+collection+") found: " + actors.size());
		return new HashSet<Actor>(actors);
	}
	
	private String extractHref(String webfingerJson) {
		return JsonPath.parse(webfingerJson).read("$['links'][0]['href']");
	}
}
