package org.linkedopenactors.rdfpub.client2024;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpHeaderAttribute {
	private String name;
	private String value;
}
