package org.linkedopenactors.rdfpub.client2024;

import java.util.List;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Actor;

public interface RdfPubAdminClient {
	List<Actor> discoverActors();
	Set<String> getGraphNames(String actorId);
}
