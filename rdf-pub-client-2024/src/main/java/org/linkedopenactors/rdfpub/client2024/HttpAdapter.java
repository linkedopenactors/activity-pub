package org.linkedopenactors.rdfpub.client2024;

import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.springframework.http.MediaType;

public interface HttpAdapter {
	
	public static final MediaType MEDIA_TYPE_JSON_LD = new MediaType("application", "ld+json");
	public static final MediaType MEDIA_TYPE_TURTLE = new MediaType("text", "turtle");
	
	Response get(MediaType mediaType ,String url, HttpHeaderAttribute... headerAttributes);
	Response get(String url, HttpHeaderAttribute... headerAttributes);
	Response post(String url, String body);
	Response post(String url, String body, RdfFormat rdfFormat);
	Response post(String url, String body, MediaType mediaType);
	Response post(RdfPubObjectId url, String body, RdfFormat rdfFormat);
}
