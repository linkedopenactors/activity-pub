package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.BlankNode;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.ap.rdfpubtestapp.model.UiTriple;
import org.linkedopenactors.rdfpub.client2024.RdfPubAdminClient;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
public abstract class BaseController {

	@Autowired
	private RdfPubClient rdfPubClient;

	@Autowired
	private RdfPubAdminClient rdfPubAdminClient;

	@Autowired
	private VocabContainer vocabContainer;
	
	protected RdfPubClient getRdfPubClient() {
		return rdfPubClient;
	}

	protected RdfPubAdminClient getRdfPubAdminClient() {
		return rdfPubAdminClient;
	}

	protected Optional<Actor> getCurrentActor(Authentication authentication, HttpSession httpSession) {
		Actor actor = (Actor)httpSession.getAttribute("currentActor");
		Optional<Actor> actorOpt = Optional.ofNullable(actor);		
		if(actorOpt.isEmpty()) {
			Actor actorForSession = 
					Optional.ofNullable(authentication)
			.map(Authentication::getName)
			.flatMap(rdfPubClient::discoverActor)
			.orElseThrow(()->new IllegalStateException("unable to dicoverActor! authentication: " +authentication ));
			
			httpSession.setAttribute("currentActor", actorForSession);
			actorOpt = Optional.ofNullable((Actor)httpSession.getAttribute("currentActor"));
			log.debug("using new loaded actor: " + actorForSession.getSubject());
		} else  {
			log.debug("using actor from session: " + actorOpt.get().getSubject());
		}
    	return actorOpt;
	}
	
	protected List<UiTriple> toPropertyList(ActivityPubObject object) {
    	return object.asGraph().stream()
    		.map(this::toProperty)
    		.collect(Collectors.toList());
    }

	protected PropertyCollection toPropertyCollection(String title, ActivityPubObject object) {
		return new PropertyCollection(object.getSubject().getIRIString(), object.getName().orElse("n.a."), toPropertyList(object));
	}
	
	private UiTriple toProperty(Triple t) {
		Object obj = t.getObject(); 
		return new UiTriple(vocabContainer, "n/a", t.getPredicate().getIRIString(), "todo description", obj, obj instanceof IRI, obj instanceof BlankNode);
	}
}

	