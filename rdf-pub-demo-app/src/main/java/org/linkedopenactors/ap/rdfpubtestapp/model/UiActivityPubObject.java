package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.util.List;

/**
 * 
 */
@Deprecated
public class UiActivityPubObject {	
	private Property id;
	private Property published;
	private List<Property> types;
	private Property name;
	private Property summary;

	/**
	 * 
	 * @param id
	 * @param published
	 * @param types
	 * @param name
	 * @param summary
	 */
	public UiActivityPubObject(Property id, Property published, List<Property> types, Property name, Property summary) {
		super();
		this.id = id;
		this.published = published;
		this.types = types;
		this.name = name;
		this.summary = summary;
	}

	/**
	 * 
	 * @return Published as {@link Property}
	 */
	public Property getPublished() {
		return published;
	}

	/**
	 * 
	 * @param published
	 */
	public void setPublished(Property published) {
		this.published = published;
	}

	/**
	 * 
	 * @return {@link List} of Types as {@link Property}
	 */
	public List<Property> getTypes() {
		return types;
	}

	/**
	 * 
	 * @param types
	 */
	public void setType(List<Property> types) {
		this.types = types;
	}

	/**
	 * 
	 * @return Name as {@link Property}
	 */
	public Property getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(Property name) {
		this.name = name;
	}

	/**
	 * 
	 * @return Summary as {@link Property}
	 */
	public Property getSummary() {
		return summary;
	}

	/**
	 * 
	 * @param summary
	 */
	public void setSummary(Property summary) {
		this.summary = summary;
	}

	/**
	 * 
	 * @return Id as {@link Property}
	 */
	public Property getId() {
		return id;
	}
}
