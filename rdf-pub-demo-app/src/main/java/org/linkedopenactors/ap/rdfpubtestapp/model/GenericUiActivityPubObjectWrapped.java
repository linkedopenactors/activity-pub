package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.BlankNode;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

/**
 * 
 */
public class GenericUiActivityPubObjectWrapped implements GenericUiActivityPubObject {

	private ActivityPubObject activityPubObject;
	
	private PredicateDescritionRepository predicateDescritionRepository;
	
	private List<Property> properties;

	private VocabContainer vocabContainer;
	
	/**
	 * 
	 * @param predicateDescritionRepository
	 * @param activityPubObject
	 */
	public GenericUiActivityPubObjectWrapped(VocabContainer vocabContainer, PredicateDescritionRepository predicateDescritionRepository, ActivityPubObject activityPubObject) {
		this.vocabContainer = vocabContainer;
		this.predicateDescritionRepository = predicateDescritionRepository;
		this.activityPubObject = activityPubObject;
		
		properties = activityPubObject.asExternalGraph().stream()
			.map(this::createProperty)
			.collect(Collectors.toList());

		properties.add(new Property(vocabContainer, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "id",
				"A unique id of the object", activityPubObject.getSubject(), true, false));
	}

	@Override
	public List<Property> getProperties() {
		return properties;
	}

	@Override
	public String getPropertyAsString(String propertyName) {
		Optional<String> map = this.properties.stream()
				.filter(p->p.getPredicate().equals(propertyName))
				.findFirst()
				.map(Property::getValue);
		String orElse = map
				.orElse("n.a.");
		return orElse;
	}

	@Override
	public Instant getPubished() {
		Optional<Instant> i = activityPubObject.getPublished();
		DateTimeFormatter df = DateTimeFormatter.ofPattern("dd LLL uuuu HH:mm.ss");
		df = df.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());
		if(i.isEmpty()) {
			return Instant.EPOCH;
		} else {
			return i.get();
		}
	}

	@Override
	public String getSubject() {
		return activityPubObject.getSubject().getIRIString();
	}

	@Override
	public String getSubjectEncoded() {
		return URLEncoder.encode(getSubject(), StandardCharsets.UTF_8);
	}

	@Override
	public String getName() {
		return activityPubObject.getName().orElse("unnamed");
	}

	
	@Override
	public boolean isFollowable() {
		Set<RdfPubIRI> types = activityPubObject.getTypes();
		return types.contains(vocabContainer.vocAs().Person())
				|| types.contains(vocabContainer.vocAs().Group())
				|| types.contains(vocabContainer.vocAs().Organization())
				|| types.contains(vocabContainer.vocAs().Service())
				|| types.contains(vocabContainer.vocAs().Application());
		
		// TODO add other types somehow !
	}

	@Override
	public String asJsonLd() {
		return activityPubObject.toString(RdfFormat.JSONLD);
	}

	@Override
	public String asTurtle() {
		return activityPubObject.toString(RdfFormat.TURTLE);
	}

	public Optional<String> getIconUrl() {
		Optional<String> fi = activityPubObject.getFirstIcon()
				.flatMap(ao -> ao.getUrls().stream().findFirst())
				.map(iri->iri.getIRIString());
		return fi;
	}

	@Override
	public String getSummary() {
		return activityPubObject.getSummary().orElse("");
	}

	private Property createProperty(Triple triple) {
		Optional<PredicateDescriptor> desc = predicateDescritionRepository.find(triple.getPredicate().getIRIString());
		String predicate = desc.map(PredicateDescriptor::getId).orElse(triple.getPredicate().getIRIString());
		
		String humanReadableName = desc.map(PredicateDescriptor::getName).orElse(triple.getPredicate().getIRIString());
		String description = desc.map(PredicateDescriptor::getDescription).orElse("not available!");
		RDFTerm value = triple.getObject();
		return new Property(vocabContainer, predicate, humanReadableName, description, value, triple.getObject() instanceof IRI, triple.getObject() instanceof BlankNode);
	}
}
