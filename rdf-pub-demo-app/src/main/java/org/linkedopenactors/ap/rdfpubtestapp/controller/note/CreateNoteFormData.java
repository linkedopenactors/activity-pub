package org.linkedopenactors.ap.rdfpubtestapp.controller.note;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 
 */
@Data
public class CreateNoteFormData {
	@NotBlank
	private String name;
	
	@NotBlank
	private String content;
	
	private String summary;

	@NotBlank
	private String to;
	
	private String bto;
	
	private String cc;
	
	private String bcc; 
}
