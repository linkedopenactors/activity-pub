 package org.linkedopenactors.ap.rdfpubtestapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 */
@SpringBootApplication(scanBasePackages = {
		"org.linkedopenactors.ap.rdfpubtestapp.config",
		"org.linkedopenactors.ap.rdfpubtestapp.model",
		"org.linkedopenactors.ap.rdfpubtestapp.controller",
		"org.linkedopenactors.ap.rdfpubtestapp.controller.note",
		"org.linkedopenactors.ap.rdfpubtestapp.controller.admin",
		"org.linkedopenactors.rdfpub.client2024",
		"org.linkedopenactors.rdfpub.domain",
		"org.linkedopenactors.rdfpub.store.rdf4j"
		})		
public class RdfPubTestAppApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(RdfPubTestAppApplication.class, args);
	}
}
