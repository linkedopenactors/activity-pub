package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.util.Optional;

import org.linkedopenactors.ap.rdfpubtestapp.model.Converter;
import org.linkedopenactors.ap.rdfpubtestapp.model.GenericUiActivityPubObject;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
@Controller
public class ReadObjectController extends BaseController {

	@Autowired
	private Converter converter; 

	@Autowired
	private VocabContainer vocabContainer;

	/**
	 * 
	 * @param model
	 * @param authentication
	 * @param httpSession
	 * @param subject
	 * @return The name of the fragment to show
	 */
    @GetMapping("/read")
	public String readObject(Model model, Authentication authentication, HttpSession httpSession,
			@RequestParam(name = "subject") String subject) {
    	return readObject(model, subject);
    }
    
    /**
     * 
     * @param model
     * @param subject
     * @return The name of the fragment to show
     */
    public String readObject(Model model, String subject) {
    	log.debug("subject: " + subject);

    	RdfPubClient client =  getRdfPubClient();
    	Optional<ActivityPubObject> objectOpt = client.resolveObject(subject);    	
    	model.addAttribute("found", objectOpt.isPresent());
    	
    	GenericUiActivityPubObject l = objectOpt.map(o->{
    		return converter.convertUltimate(o);
    	}).orElseThrow();
    	model.addAttribute("genericUiActivityPubObject", l);
    	model.addAttribute("index", 0);
    	Boolean isActor = objectOpt.map(ActivityPubObject::getTypes).map(iris->iris.contains(vocabContainer.vocAs().Person())).orElse(false); // TODO other actor types
//		if(isActor) {
//    		return "fragments/showActor";
//    	}
    	return "fragments/showSingleObject";
    }
}

	