package org.linkedopenactors.ap.rdfpubtestapp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 */
@Controller
@RequestMapping("/")
public class HomeController {
   
	/**
	 * 
	 * @param model
	 *  @return The name of the fragment to show
	 */
	@GetMapping
    public String index(Model model) {
        return "index";
    }
}
