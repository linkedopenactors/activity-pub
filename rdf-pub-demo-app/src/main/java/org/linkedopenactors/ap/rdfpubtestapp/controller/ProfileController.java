package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.linkedopenactors.ap.rdfpubtestapp.model.Converter;
import org.linkedopenactors.ap.rdfpubtestapp.model.GenericUiActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Controller
@Slf4j
public class ProfileController extends BaseController {

	@Autowired
	private Converter converter; 

	/**
	 * 
	 * @param model
	 * @param authentication
	 * @param httpSession
	 * @return The name of the fragment to show
	 */
    @GetMapping("/profile")
    public String profile(Model model, Authentication authentication, HttpSession httpSession) {
    	log.debug("->profile");
    	Optional<Actor> currentActor = getCurrentActor(authentication, httpSession);
    	
    	GenericUiActivityPubObject ui = currentActor
    			.map(converter::convertUltimate)
    			.map(genericUiActivityPubObject -> genericUiActivityPubObject)
    			.orElse(null);
    	
    	model.addAttribute("index", 0);
    	model.addAttribute("genericUiActivityPubObject", ui);
    	
    	log.debug("<-profile");
    	return "fragments/showSingleObject";
    }
}
