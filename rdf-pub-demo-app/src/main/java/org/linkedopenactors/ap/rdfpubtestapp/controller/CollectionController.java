package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.ap.rdfpubtestapp.model.Converter;
import org.linkedopenactors.ap.rdfpubtestapp.model.GenericUiActivityPubObject;
import org.linkedopenactors.ap.rdfpubtestapp.model.UiActor;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
@Controller
public class CollectionController extends BaseController {

	@Autowired
	private Converter converter; 
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	/**
	 * 
	 * @param model
	 * @return The name of the fragment to show
	 */
	@GetMapping("/collection/public")
    public String collectionPublic(Model model) {
		Actor instanceActor = getRdfPubClient().readActor(activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME)).orElseThrow();
		Instance instance = instanceActor.asConvertable().asInstance();		
		RdfPubObjectIdCollection c = instance.getCollectionPublic();		
		String s = readActivityCollection(model, "Public Collection", "showUltimateObjects", c);
    	return s;
    }
    
	/**
	 * 
	 * @param model
	 * @param authentication
	 * @param httpSession
	 * @return The name of the fragment to show
	 */
    @GetMapping("/collection/inbox")
    public String collectionInbox(Model model, Authentication authentication, HttpSession httpSession) {
    	Optional<Actor> actorOpt = getCurrentActor(authentication, httpSession);
		return readActivityCollection(model, "Inbox Collection", "showUltimateObjects", actorOpt.map(Actor::getInbox).flatMap(RdfPubBlankNodeOrIRI::asCollection).orElse(null));
    }
    
    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @return The name of the fragment to show
     */
    @GetMapping("/collection/outbox")
    public String collectionOutbox(Model model, Authentication authentication, HttpSession httpSession) {
    	
    	logTokenExpirationTime(authentication);
    	
    	Optional<Actor> actorOpt = getCurrentActor(authentication, httpSession);
    	return readActivityCollection(model, "Outbox Collection", "showUltimateObjects", actorOpt.map(Actor::getOutbox).flatMap(RdfPubBlankNodeOrIRI::asCollection).orElse(null));
    }

	private void logTokenExpirationTime(Authentication authentication) {
		Object principal = authentication.getPrincipal();
    	if(principal instanceof OidcUser) {
    		Object claim = ((OidcUser)principal).getClaim("exp");
    		if(claim instanceof Instant) {
    			Duration duration = Duration.between(Instant.now(), ((Instant)claim));
    			log.debug("Oauth2 token valid for the next " + duration.getSeconds() + " seconds!");
    		}
    	}
	}

    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @return The name of the fragment to show
     */
    @GetMapping("/collection/pendingFollowers")
    public String collectionPendingFollowers(Model model, Authentication authentication, HttpSession httpSession) {
    	Optional<Actor> actorOpt = getCurrentActor(authentication, httpSession);
    	return readActivityCollection(model, "PendingFollowers", "showPendingFollowers", actorOpt.map(Actor::getPendingFollowers).flatMap(RdfPubBlankNodeOrIRI::asCollection).orElse(null));
    }

    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @return The name of the fragment to show
     */
    @GetMapping("/collection/followers")
    public String collectionFollowers(Model model, Authentication authentication, HttpSession httpSession) {
    	Optional<Actor> actorOpt = getCurrentActor(authentication, httpSession);
    	Set<UiActor> uiActors = Optional.ofNullable(actorOpt.map(Actor::getFollowers).flatMap(RdfPubBlankNodeOrIRI::asCollection).orElse(null))
    			.map(url-> getRdfPubClient().readActorCollection(url))    			
    			.orElse(Collections.emptySet())
    			.stream().map(UiActor::new)
    			.collect(Collectors.toSet());
    	model.addAttribute("genericUiActivityPubObjects", uiActors);
    	model.addAttribute("title", "Followers");
        return "fragments/showFollower2";
    	
    }

    /**
     * @param model
     * @param authentication
     * @param httpSession
     * @return The name of the fragment to show
     */
    @GetMapping("/collection/following")
    public String collectionFollowing(Model model, Authentication authentication, HttpSession httpSession) {
    	Optional<Actor> actorOpt = getCurrentActor(authentication, httpSession);
    	RdfPubObjectIdCollection collectionIri = actorOpt.map(Actor::getFollowing).flatMap(RdfPubBlankNodeOrIRI::asCollection).orElse(null);
    	Set<Actor> all = Optional.ofNullable(collectionIri)
    			.map(url-> getRdfPubClient().readActorCollection(url))
    			.orElse(Collections.emptySet());
    	List<GenericUiActivityPubObject> uis = convertToUiObjects(all);
    	model.addAttribute("genericUiActivityPubObjects", uis);
    	model.addAttribute("title", "Following");
        return "fragments/showUltimateObjects";

    }

    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @return The name of the fragment to show
     */
    @GetMapping("/collection/pendingFollowing")
    public String collectionPendingFollowing(Model model, Authentication authentication, HttpSession httpSession) {
    	Optional<Actor> actorOpt = getCurrentActor(authentication, httpSession);
    	RdfPubObjectIdCollection collectionId = actorOpt
    			.flatMap(Actor::getPendingFollowing)
    			.flatMap(RdfPubBlankNodeOrIRI::asCollection)
    			.orElse(null);
		return readActivityCollection(model, "Pending Following", "showPendingFollowing", collectionId);
    }

    private String readActivityCollection(Model model, String title, String site, RdfPubObjectIdCollection collectionIri) {
    	Set<Activity> all = Optional.ofNullable(collectionIri)
    			.map(url-> getRdfPubClient().readActivityCollection(url))
    			.orElse(Collections.emptySet());
    	log.debug("readActivityCollection(" + collectionIri + ") -> " + all.size());
    	List<GenericUiActivityPubObject> uis = convertToUiObjects(all);
    	model.addAttribute("genericUiActivityPubObjects", uis);
    	model.addAttribute("title", title);
        return "fragments/" + site;
    }

	private List<GenericUiActivityPubObject> convertToUiObjects(Set<? extends ActivityPubObject> all) {
		all = all.stream()
			.filter(object->!object.getSubject().isBlankNode())
			.collect(Collectors.toSet());
		
    	List<GenericUiActivityPubObject> uis = all.stream()
    			.map(converter::convertUltimate)
    			.collect(Collectors.toList());
    	Collections.sort(uis, (one, two) -> two.getPubished().compareTo(one.getPubished()));
		return uis;
	}
}

