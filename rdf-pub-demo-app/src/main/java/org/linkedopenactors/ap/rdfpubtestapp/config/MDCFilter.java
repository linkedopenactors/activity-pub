package org.linkedopenactors.ap.rdfpubtestapp.config;

import java.io.IOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;

/**
 * A servlet filter to log request and response
 * The logging implementation is pretty native and for demonstration only
 * @author hemant
 *
 */
@Component
@Order(2)
public class MDCFilter implements jakarta.servlet.Filter {

	private final static Logger LOG = LoggerFactory.getLogger(MDCFilter.class);

	/**
	 * 
	 */
	public MDCFilter() {
		LOG.info("Initializing MDCFilter");
	}
	
	/**
	 * 
	 */
	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		LOG.info("Initializing filter :{}", this);
	}

	/**
	 * 
	 */
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String xRequestId = req.getHeader("X-Request-ID");
		if(!StringUtils.hasText(xRequestId)) {
			xRequestId = UUID.randomUUID().toString().substring(0,8);
		}
		String xCorrelationId = req.getHeader("X-Correlation-ID");
		MDC.put("X-Request-ID", xRequestId);
		MDC.put("X-Correlation-ID", xCorrelationId);
		chain.doFilter(request, response);
		MDC.remove("X-Correlation-ID");
		MDC.remove("X-Request-ID");
	}

	/**
	 * 
	 */
	@Override
	public void destroy() {		
		LOG.warn("Destructing filter :{}", this);
	}
}