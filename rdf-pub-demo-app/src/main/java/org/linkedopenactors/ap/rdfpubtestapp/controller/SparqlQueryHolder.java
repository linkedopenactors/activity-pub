package org.linkedopenactors.ap.rdfpubtestapp.controller;

public class SparqlQueryHolder {
	private String query;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
