package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.time.Instant;
import java.util.List;

public interface GenericUiActivityPubObject {

	/**
	 * 
	 * @return The properties of this object.
	 */
	List<Property> getProperties();

	/**
	 * 
	 * @param propertyName
	 * @return The property value of the propery with the passed name
	 */
	String getPropertyAsString(String propertyName);

	/**
	 * 
	 * @return published date
	 */
	Instant getPubished();

	String getSubject();

	String getSubjectEncoded();

	String getName();
	
	String asJsonLd();
	
	String asTurtle();
	
	String getSummary();
	
	boolean isFollowable();

}