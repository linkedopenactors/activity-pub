package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
@Controller
public class SearchController extends BaseController {

	@Autowired
	private ReadObjectController readObjectController;
	
	/**
	 * 
	 * @param model
	 * @param authentication
	 * @param httpSession
	 * @param subject
	 * @return The name of the fragment to show
	 */
    @PostMapping("/search")
    public String search(Model model, Authentication authentication, HttpSession httpSession,
    		@RequestParam String subject) {
    	model.asMap().forEach((k,v)->System.out.println(k + " -> " + v ));
    	log.debug("search: " + subject);
		return readObjectController.readObject(model, authentication, httpSession, subject.trim());
    }

    @GetMapping("/sparqlSearch")
	public String sparqlSearch(Model model, Authentication authentication, HttpSession httpSession) {
    	SparqlQueryHolder sqh = new SparqlQueryHolder();
    	sqh.setQuery("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
    			+ "PREFIX as: <https://www.w3.org/ns/activitystreams#>\n"
    			+ "\n"
    			+ "SELECT *\n"
    			+ "WHERE {\n"
    			+ " ?s rdf:type <https://www.w3.org/ns/activitystreams#Note> .\n"
    			+ " ?s as:content ?content .\n"
    			+ "}");
    	model.addAttribute("formData", sqh);
    	model.addAttribute("title", "SPARQL Search");
    	return "fragments/sparqlSearch";
    	
    }
        	
    @PostMapping("/sparql")
    public String sparql(Model model, Authentication authentication, HttpSession httpSession,
    		@RequestParam String query) {
    	model.asMap().forEach((k,v)->System.out.println(k + " -> " + v ));
    	log.debug("sparql: " + query);
    	
    	String result = "error";
    	Optional<Actor> actor = getCurrentActor(authentication, httpSession);
    	if(actor.isPresent()) {
    		result = actor
    				.flatMap(a -> getRdfPubClient().sparql(a, query))
    				.orElse("not found");
    	} else {
    		result = "not authenticated!";
    	}
    	model.addAttribute("title", "search result:");
    	model.addAttribute("result", result);
		return "fragments/sparqlSearchResult";
    }
    
}

	