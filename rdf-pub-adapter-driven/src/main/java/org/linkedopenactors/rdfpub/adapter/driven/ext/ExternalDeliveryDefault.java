package org.linkedopenactors.rdfpub.adapter.driven.ext;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.adapter.http.ext.SoftwareDeterminator;
import org.linkedopenactors.rdfpub.adapter.http.ext.SupportedSoftware;
import org.linkedopenactors.rdfpub.app.external.DiscoverExternalActor;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.sentry.spring.jakarta.tracing.SentrySpan;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExternalDeliveryDefault implements org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s.ExternalDelivery
{
	@Autowired
	private ExternalDeliveryAdatperMastodon externalDeliveryAdatperMastodon;

	@Autowired
	private ExternalDeliveryAdatperManyfold externalDeliveryAdatperManyfold;
	
	@Autowired
	private ExternalDeliveryAdatperStandard externalDeliveryAdatperStandard;

	@Autowired
	private DiscoverExternalActor discoverExternalActor; 
	
	@Autowired
	private SoftwareDeterminator softwareDeterminator;
	
	@Value("${instance.domain}") 
	private String instanceDomain;
	
	@SentrySpan
	public void deliver(ActorsStore authenticatedActorStore, Actor authenticatedActor, RdfPubBlankNodeOrIRI activitySubject) {
		Activity activity = authenticatedActorStore.find(activitySubject, 0).orElseThrow().asConvertable().asActivity();
		
		Set<ActivityPubObject> objects = activity.getObject().stream().map(o->{
			return authenticatedActorStore.find(o, 1).orElse(null);			
		})
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
		if(!objects.isEmpty()) {
			activity.setObjects(objects);
		}
		
		activity.getExternalReceivers().stream()
			.map(receiver->new CmdExternalDelivery(authenticatedActor, getReceiver(receiver).orElse(null), activity))
			.forEach(this::deliverNew);
	}
		
	private Optional<Actor> getReceiver(RdfPubBlankNodeOrIRI receiverIri) {
		Optional<Actor> receiverOpt = discoverExternalActor.discover(receiverIri.getIRIString());
		if(receiverOpt.isEmpty()) {
			log.warn("ignoring undiscoverable actor: " + receiverIri.getIRIString());
		} 
		return receiverOpt;
	}
	
	private void deliverNew(CmdExternalDelivery cmd) {
		Optional<Actor> actorOpt = Optional.ofNullable(cmd.getReceiver());
		if(actorOpt.isEmpty()) {
			log.warn("ignoring delivery. no receiver in command!");
			return;
		}
		Actor receiver = actorOpt.orElseThrow(); 
		ExternalDeliveryAdatper eda = getExternalDeliveryAdatper(softwareDeterminator.findSoftware(receiver));
		eda.deliver(cmd);
	}
	
	private ExternalDeliveryAdatper getExternalDeliveryAdatper(SupportedSoftware sv) {
		switch (sv) {
		case mastodon: {
			return externalDeliveryAdatperMastodon;
		}
		case pixelfed: {
			return externalDeliveryAdatperMastodon;
		}
		case friendica: {
			return externalDeliveryAdatperMastodon;
		}
		case manyfold: {
			return externalDeliveryAdatperManyfold;
		}
		default:
			return externalDeliveryAdatperStandard;
		}
	}
}
