package org.linkedopenactors.rdfpub.adapter.driven;

import java.util.List;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPageWithActivities;

import lombok.Data;

@Data
public class OrderedCollectionPageWithActivitiesDefault implements OrderedCollectionPageWithActivities {
	private final List<Activity> activities;
	private final OrderedCollectionPage orderedCollectionPage;
}
