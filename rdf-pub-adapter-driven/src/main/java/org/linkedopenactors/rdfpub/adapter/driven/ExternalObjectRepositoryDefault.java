package org.linkedopenactors.rdfpub.adapter.driven;

import java.net.URI;
import java.net.URL;
import java.security.PrivateKey;
import java.time.Duration;
import java.util.Optional;

import org.linkedopenactors.rdfpub.adapter.http.FredysSignature;
import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;
import reactor.util.retry.Retry;

@Service
@Slf4j
public class ExternalObjectRepositoryDefault implements ExternalObjectRepository {

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory; 
	
	@Autowired
	private WebClient webClient; 
	
	@Autowired
	private FredysSignature fredysSignature;
	
	@Autowired
	private org.linkedopenactors.rdfpub.app.service.actor.KeyPairGenerator generatorKeyPairGenerator;

	@Autowired
	private PublicActorStore publicActorStore;
	
	@Override
	public Optional<org.linkedopenactors.rdfpub.domain.ActivityPubObject> read(RdfPubBlankNodeOrIRI subject) {
        var request =
                webClient
                        .get()
                        .uri(subject.toString())
                        .accept(new MediaType("application", "activity+json"));
        String responseString;
		try {
			responseString = request
			        .retrieve()
			        .onStatus(org.springframework.http.HttpStatusCode::isError, ClientResponse::createException)
			        .bodyToMono(String.class)
			        .retryWhen(Retry.backoff(3, Duration.ofSeconds(2))
			                .filter(throwable -> throwable instanceof ResponseStatusException))
			        .block();
			
			return Optional.ofNullable(activityPubObjectFactory.create(RdfFormat.JSONLD, responseString));
		} catch (WebClientResponseException e) {
			if(!e.getStatusCode().isSameCodeAs(HttpStatus.GONE)) {
				log.error("error reading '"+subject+"'", e);
			}
			return Optional.empty();
		}        
	}

	@Override
	public Optional<ActivityPubObject> readWithInstanceSignature(String url) {
		return readWithSignature(url, publicActorStore.getInstanceActor());
	}
	
	@Override
	public Optional<ActivityPubObject> readWithSignature(String url, Actor currentAuthenticatedActor) {
		RdfPubObjectIdActor receiverId = currentAuthenticatedActor.getSubject().asActor().orElseThrow();
		String actorsPublicKeyId = receiverId.getExternalBaseSubject().getIRIString();
				
		PrivateKey privateKey = generatorKeyPairGenerator.getPrivateKey(receiverId.getIdentifier());
		URL urlObject;
		try {
			urlObject = URI.create(url).toURL();
		} catch (Exception e) {
			log.error(url + "  is no valid url!", e);
			return Optional.empty();
		}
		var signatureHeaders = fredysSignature.getSignatureHeadersGet(urlObject, actorsPublicKeyId, privateKey);
        var request =
                webClient
                        .get()
                        .uri(url)
                        .accept(new MediaType("application", "activity+json"));
        
        signatureHeaders.asMap()
        	.forEach((k,v) -> request.header(k, v));

        String responseString;
		try {
			ResponseEntity<String> res = request
			        .retrieve()
			        .onStatus(org.springframework.http.HttpStatusCode::isError, ClientResponse::createException)
			        .toEntity(String.class)
			        .retryWhen(Retry.backoff(3, Duration.ofSeconds(2))
			                .filter(throwable -> throwable instanceof ResponseStatusException))
			        .block();
			if(log.isTraceEnabled()) {
				res.getHeaders().forEach((k,v)->log.trace("header: " + k + " - " +v));
				log.trace("StatusCode: " + res.getStatusCode());
			}
			responseString = res.getBody();
			Optional<ActivityPubObject> found = Optional.ofNullable(activityPubObjectFactory.create(RdfFormat.JSONLD, responseString, url));
			log.debug("readWithSignature("+url+") - found: " + found.isPresent());
			return found;
		} catch (WebClientResponseException e) {
			if(!e.getStatusCode().isSameCodeAs(HttpStatus.GONE)) {
				log.error("error readWithSignature '"+url+"':" + e.getMessage() + " - " + e.getStatusCode() + " - " + e.getResponseBodyAsString());
			}
			return Optional.empty();
		} catch (Exception e) {
			log.debug("error reading '"+url+"'" + e.getMessage());
			log.trace("error reading '"+url+"'", e);
			return Optional.empty();
		}        
	}
	
//	private String toPublicKeyString(PublicKey public_key) {
//		byte[] byte_pubkey = public_key.getEncoded();
//		String str_key = Base64.getEncoder().encodeToString(byte_pubkey);
//		str_key = "-----BEGIN PUBLIC KEY-----\n" + str_key;
//		str_key = str_key + "\n-----END PUBLIC KEY-----";
//		str_key = Base64.getEncoder().encodeToString(str_key.getBytes());
//		return str_key;
//	}
//	
//	private String toPrivateKeyString(PrivateKey key) {
//		byte[] byte_key = key.getEncoded();
//		String str_key = Base64.getEncoder().encodeToString(byte_key);
//		str_key = "-----BEGIN PRIVATE KEY-----\n" + str_key;
//		str_key = str_key + "\n-----END PRIVATE KEY-----";
//		str_key = Base64.getEncoder().encodeToString(str_key.getBytes());
//		return str_key;
//	}
}
