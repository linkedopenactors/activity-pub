package org.linkedopenactors.rdfpub.adapter.driven.ext;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.adapter.ObjectAdapter;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.springframework.stereotype.Component;

@Component
class ExternalDeliveryAdatperMastodon extends ExternalDeliveryAdatperAbstract {
	
	private ObjectAdapter objectAdapter;

	public ExternalDeliveryAdatperMastodon(ObjectAdapter objectAdapter) {
		this.objectAdapter = objectAdapter;
	}
	
	protected String determineKeyId(Actor outboxOwner) {
		RdfPubObjectIdActor outbowOwnerId = outboxOwner.getSubject().asActor().orElseThrow();
		return outbowOwnerId.getExternalBaseSubject().getIRIString();
	}
	
	protected String activityToJsonBody(Actor outboxOwner, Activity activity) {
		// delegate to objectAdapter which is also used for HTTP Get from mastodon !
		String adjusted = objectAdapter.adjust(activity, RdfFormat.JSONLD, new UserAgent("mastodon", "1", ""));
//		log.debug("adjusted: " + adjusted);
		return adjusted;
	}
}
