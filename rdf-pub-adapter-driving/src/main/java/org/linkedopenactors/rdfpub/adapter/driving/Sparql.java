package org.linkedopenactors.rdfpub.adapter.driving;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.service.sparql.QuerySparqlTuple;
import org.linkedopenactors.rdfpub.app.service.sparql.UcSparql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Sparql extends AbstractController {

	@Autowired
	private UcSparql ucSparql;
	
	@PostMapping(value = "/resource/{userId}/{collection}/sparql")
	public ResponseEntity<String> tupleQuery(@org.springframework.web.bind.annotation.RequestBody String query,
			@PathVariable String userId, @PathVariable String collection, HttpServletRequest request,
			final JwtAuthenticationToken authentication) {
		log.debug("->tupleQuery");
		Optional<String> response;
		try {
			QuerySparqlTuple querySparqlTuple = new QuerySparqlTuple(query, getAuthenticatedActorHolder(), extractUserAgent(request).orElse(null));
			response = ucSparql.perform(querySparqlTuple);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error executing Sparql", e);
		}
		return response.map(body->ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
				.body(body))
		.orElse(ResponseEntity.notFound().build());
	}
}
