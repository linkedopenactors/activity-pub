package org.linkedopenactors.rdfpub.adapter.driving;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.adapter.ObjectAdapter;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.actor.QueryByUrl;
import org.linkedopenactors.rdfpub.app.service.actor.UcQueryActor;
import org.linkedopenactors.rdfpub.app.service.readobject.QueryReadObject;
import org.linkedopenactors.rdfpub.app.service.readobject.UcReadObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ContentTypeHeaderMappings;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.github.jsonldjava.shaded.com.google.common.net.HttpHeaders;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
class ControllerReadObject extends AbstractController {

	@Autowired
	private ObjectAdapter objectAdapter;
	
	@Autowired
	private ContentTypeHeaderMappings contentTypeHeaderMappings;
	
	@Autowired
	private UcReadObject ucReadObject;

	@Autowired
	private UcQueryActor ucQueryActor;

	@Autowired
	private AuthenticatedActorHolder authenticatedActorHolder; 

	@GetMapping(
			value = "/resource/{objectId}/")
	@ResponseBody
	public String getObjectGenericWithSlash(@PathVariable String objectId, @RequestParam(name="deep", defaultValue = "1") int deep, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		return getObjectGeneric(objectId, deep, request, authentication);
	}
	
	@GetMapping(
			value = "/resource/{objectId}")
	@ResponseBody
	public String getObjectGeneric(@PathVariable String objectId, @RequestParam(name="deep", defaultValue = "1") int deep, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		String result = ucQueryActor.perform(new QueryByUrl(authenticatedActorHolder, request.getHeader(HttpHeaders.ACCEPT), request.getRequestURI(), extractUserAgent(request).orElse(null)))
				.orElseThrow(()->new ResponseStatusException(HttpStatus.GONE, "unable to read: " + request.getRequestURI()));
		return result;
	}

	@GetMapping(value = "/resource/{userId}/res/{objectId}")
	@ResponseBody
	public ResponseEntity<String> getObjectGeneric(@PathVariable String userId, @RequestParam(name="deep", defaultValue = "0") int deep, @PathVariable String objectId, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		log.debug("->getObjectgeneric(userId: "+userId+", objectId: "+objectId+") RemoteHost: " + request.getRemoteHost());
		RdfFormat rdfFormat = contentTypeHeaderMappings.determineRdfFormat(request.getHeader(HttpHeaders.ACCEPT));
		Optional<String> objectAsString = readObject(deep, authentication, rdfFormat, request.getRequestURI(), extractRemoteServer(request), extractUserAgent(request).orElse(null));
		ResponseEntity<String> resultResponseEntity = objectAsString
				.map( body -> ResponseEntity.ok().body(body) ) 
	            .orElseGet( () -> ResponseEntity.notFound().build() );

		log.debug("<- getObjectgeneric(userId: "+userId+", objectId: "+objectId+"): " + objectAsString);
		return resultResponseEntity;
	}

	@GetMapping(
			value = "/resource/{userId}/act/{objectId}")
	@ResponseBody
	public ResponseEntity<String> getActivityGeneric(@PathVariable String userId, @RequestParam(name="deep", defaultValue = "0") int deep, @PathVariable String objectId, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		return getObjectGeneric(userId, deep, objectId, request, authentication);	
	}
	
	@GetMapping(value = "/resource/{userId}/res/{objectId}/{revisionId}")
	@ResponseBody
	public String getObjectWithRevisionGeneric(@PathVariable String userId, @PathVariable String objectId, @PathVariable String revisionId, @RequestParam(name="deep", defaultValue = "0") int deep, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		log.debug("-> getObjectWithRevisionGeneric(userId: "+userId+", objectId: "+objectId+", revisionId: " + revisionId +") RemoteHost: " + request.getRemoteHost());
		RdfFormat rdfFormat = contentTypeHeaderMappings.determineRdfFormat(request.getHeader(HttpHeaders.ACCEPT));
		Optional<String> objectAsString = readObject(deep, authentication, rdfFormat, request.getRequestURI(), extractRemoteServer(request), extractUserAgent(request).orElse(null));
		String res = objectAsString
	            .orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
		log.debug("<- getObjectWithRevisionGeneric(userId: "+userId+", objectId: "+objectId+", revisionId: " + revisionId +")");
		return res;
	}
	
	@GetMapping(value = "/resource/{userId}/outbox/{objectId}")
	public String getActivityOutboxGeneric(@PathVariable String objectId, @RequestParam(name="deep", defaultValue = "0") int deep, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		log.debug("->getActivityOutboxGeneric(objectId: "+objectId+") RemoteHost: " + request.getRemoteHost());
		RdfFormat rdfFormat = contentTypeHeaderMappings.determineRdfFormat(request.getHeader(HttpHeaders.ACCEPT));
		return readObject(deep, authentication, rdfFormat, request.getRequestURI(), extractRemoteServer(request), extractUserAgent(request).orElse(null))
	            .orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@GetMapping(value = "/resolve")
	public ResponseEntity<String> resolve(@RequestParam("url") String url, @RequestParam(name="deep", defaultValue = "0") int deep, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		log.debug("-> resolve("+url+")");
		boolean authenticated = Optional.ofNullable(authentication)
			.map(a->a.isAuthenticated())
			.orElse(false);
		if(!authenticated ) {
			throw new IllegalStateException("not authenticated! -> " + url);
		}
		return readObject(deep, authentication, RdfFormat.JSONLD, url, extractRemoteServer(request), extractUserAgent(request).orElse(null))
				.map( body -> ResponseEntity.ok().body(body) ) 
	            .orElseGet( () -> ResponseEntity.notFound().build() );
	}

	private Optional<String> readObject(int deep, final JwtAuthenticationToken authentication, RdfFormat rdfFormat,
			String requestURL, String remoteServer, UserAgent userAgent) {
		QueryReadObject queryReadObject = new QueryReadObject(requestURL, deep, authenticatedActorHolder, userAgent);
		Optional<ActivityPubObject> activityPubObjectOpt = ucReadObject.perform(queryReadObject);
		Optional<String> objectAsStringOpt = activityPubObjectOpt.map(ao->objectAdapter.adjust(ao, rdfFormat, userAgent));
		return objectAsStringOpt;
	}
}
