
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

/**
 * statistics about the users of this server.
 * 
 */
public class Users {

    /**
     * The total amount of on this server registered users.
     * 
     */
    private Integer total;
    /**
     * The amount of users that signed in at least once in the last 180 days.
     * 
     */
    private Integer activeHalfyear;
    /**
     * The amount of users that signed in at least once in the last 30 days.
     * 
     */
    private Integer activeMonth;

    /**
     * The total amount of on this server registered users.
     * 
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * The total amount of on this server registered users.
     * 
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    public Users withTotal(Integer total) {
        this.total = total;
        return this;
    }

    /**
     * The amount of users that signed in at least once in the last 180 days.
     * 
     */
    public Integer getActiveHalfyear() {
        return activeHalfyear;
    }

    /**
     * The amount of users that signed in at least once in the last 180 days.
     * 
     */
    public void setActiveHalfyear(Integer activeHalfyear) {
        this.activeHalfyear = activeHalfyear;
    }

    public Users withActiveHalfyear(Integer activeHalfyear) {
        this.activeHalfyear = activeHalfyear;
        return this;
    }

    /**
     * The amount of users that signed in at least once in the last 30 days.
     * 
     */
    public Integer getActiveMonth() {
        return activeMonth;
    }

    /**
     * The amount of users that signed in at least once in the last 30 days.
     * 
     */
    public void setActiveMonth(Integer activeMonth) {
        this.activeMonth = activeMonth;
    }

    public Users withActiveMonth(Integer activeMonth) {
        this.activeMonth = activeMonth;
        return this;
    }

}
