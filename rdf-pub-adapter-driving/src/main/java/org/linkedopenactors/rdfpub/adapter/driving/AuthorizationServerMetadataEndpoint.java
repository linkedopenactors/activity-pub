package org.linkedopenactors.rdfpub.adapter.driving;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;

import lombok.Data;
import reactor.util.retry.Retry;

/**
 * See Also: https://datatracker.ietf.org/doc/html/rfc8414
 * Chapter: 3. Obtaining Authorization Server Metadata
 */
@RestController
class AuthorizationServerMetadataEndpoint {

	@Value("${keycloak.oidcDiscoveryEndpoint}")	
	private String oidcDiscoveryEndpoint;
	
	@Autowired
	private WebClient webClient;
	
    @Data
    class ServerMetadata {
        private String publicSparql;
    }

    @GetMapping("/.well-known/oauth-authorization-server")
    public ResponseEntity<String> oauthAuthorizationServer() {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8")
                .body(getOpenidConfiguration());
    }

    private String getOpenidConfiguration() {
    	String url = oidcDiscoveryEndpoint;
        var request =
                webClient
                        .get()
                        .uri(url)
                        .accept(MediaType.APPLICATION_JSON);
        return request
                .retrieve()
                .onStatus(HttpStatusCode::isError, ClientResponse::createException)
                .bodyToMono(String.class)
                .retryWhen(Retry.backoff(3, Duration.ofSeconds(2))
                        .filter(throwable -> throwable instanceof ResponseStatusException))
                .block();
    }
}
