
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.HashMap;
import java.util.Map;

public enum Inbound {

    ATOM_1_0("atom1.0"),
    GNUSOCIAL("gnusocial"),
    IMAP("imap"),
    PNUT("pnut"),
    POP_3("pop3"),
    PUMPIO("pumpio"),
    RSS_2_0("rss2.0"),
    TWITTER("twitter");
    private final String value;
    private final static Map<String, Inbound> CONSTANTS = new HashMap<String, Inbound>();

    static {
        for (Inbound c: values()) {
            CONSTANTS.put(c.value, c);
        }
    }

    Inbound(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String value() {
        return this.value;
    }

    public static Inbound fromValue(String value) {
        Inbound constant = CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }

}
