/**
 * Driving (primary) port, specifies how the domain can be used. Driving actors are e.g. TestCases, HumanUsers, Message Queues, Web-Apps, ... 
 */
package org.linkedopenactors.rdfpub.adapter.driving;
