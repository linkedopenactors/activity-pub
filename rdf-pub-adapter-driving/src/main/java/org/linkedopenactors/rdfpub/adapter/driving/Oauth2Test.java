package org.linkedopenactors.rdfpub.adapter.driving;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Controller
/**
 * Rest Controller for finding Actors.
 */
@Slf4j
class Oauth2Test {
	
	@GetMapping(value = "/oauth/oauthAuthorizationEndpoint")
	@ResponseBody	
	public void oauthAuthorizationEndpoint(HttpServletRequest request,
			HttpServletResponse httpServletResponse,
			@RequestParam("response_type") String responseType,
			@RequestParam("client_id") String clientActorId,
			@RequestParam("redirect_uri") String redirectUri,
			@RequestParam("scope") String scope,
			@RequestParam("state") String state,
			@RequestParam("code_challenge") String codeChallenge,
			@RequestParam("code_challenge_method") String codeChallengeMethod,
			final JwtAuthenticationToken authentication) {
		log.debug("response_type: " + responseType);
		log.debug("client_id: " + clientActorId);
		log.debug("redirect_uri: " + redirectUri);
		log.debug("scope: " + scope);
		log.debug("state: " + state);
		log.debug("code_challenge: " + codeChallenge);
		log.debug("code_challenge_method: " + codeChallengeMethod);

		// TODO get Profile at clientActorId and compare the redirect uris
		log(request);
		String forewardUrl = "https://login.m4h.network/auth/realms/LOA/protocol/openid-connect/auth";
			forewardUrl += "?client_id=dev.rdf-pub.org-postman";
			forewardUrl += "&response_type=" + responseType;
			forewardUrl += "&state=" + state;
			forewardUrl += "&redirect_uri=" + redirectUri;
			forewardUrl += "&scope=" + scope;
			
			httpServletResponse.setHeader("Location", forewardUrl);
		    httpServletResponse.setStatus(302);
//		return "forward:" + forewardUrl;
	}
	
	@PostMapping(value = "/oauth/oauthTokenEndpoint")
	@ResponseBody	
	public void oauthTokenEndpoint(HttpServletRequest request, 
			HttpServletResponse httpServletResponse,
			@RequestParam("code") String code,
			@RequestParam("client_id") String clientActorId,
			@RequestParam("redirect_uri") String redirectUri,
			@RequestParam("code_verifier") String codeVerifier,
			final JwtAuthenticationToken authentication) {
		log.debug("code: " + code);
		log.debug("client_id: " + clientActorId);
		log.debug("redirect_uri: " + redirectUri);
		log.debug("codeVerifier: " + codeVerifier);

		String forewardUrl = "https://login.m4h.network/auth/realms/LOA/protocol/openid-connect/token";
		forewardUrl += "?grant_type=authorization_code";
		forewardUrl += "&code=" + code;
		forewardUrl += "&client_id=dev.rdf-pub.org-postman";
		forewardUrl += "&redirect_uri=" + redirectUri;
		forewardUrl += "&code_verifier=" + codeVerifier;
		
		log.debug("oauthTokenEndpoint fw: " + forewardUrl);
		httpServletResponse.setHeader("Location", forewardUrl);
	    httpServletResponse.setStatus(302);
	}

	private String log(HttpServletRequest request) {
		log.debug("Logging Request  {} : {}", request.getMethod(), request.getRequestURI());
		log.debug("query: " + decode2(request.getQueryString()));
		for (Iterator<String> iterator = request.getHeaderNames().asIterator(); iterator.hasNext();) {
			String headerName = iterator.next();
			log.debug("request header: " + headerName + " - " + request.getHeader(headerName));
		}
		log.debug("body: " + getBody(request));
		return "failed";
	}
	
	private String decode2(String query) {

//		    URI uri = new URI(testUrl);
//
//		    String scheme = uri.getScheme();
//		    String host = uri.getHost();
//		    String query = uri.getRawQuery();
//
		    return Arrays.stream(query.split("&"))
		      .map(param -> param.split("=")[0] + "=" + decode(param.split("=")[1]))
		      .collect(Collectors.joining("&"));

		}
	private String decode(String value) {
	    try {
			return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("oje", e);
		}
	}
	private String getBody(HttpServletRequest request) {
		try {
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = request.getReader();
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
