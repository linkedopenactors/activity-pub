
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.HashMap;
import java.util.Map;

public enum Outbound {

    ATOM_1_0("atom1.0"),
    BLOGGER("blogger"),
    BUDDYCLOUD("buddycloud"),
    DIASPORA("diaspora"),
    DREAMWIDTH("dreamwidth"),
    DRUPAL("drupal"),
    FACEBOOK("facebook"),
    FRIENDICA("friendica"),
    GNUSOCIAL("gnusocial"),
    GOOGLE("google"),
    INSANEJOURNAL("insanejournal"),
    LIBERTREE("libertree"),
    LINKEDIN("linkedin"),
    LIVEJOURNAL("livejournal"),
    MEDIAGOBLIN("mediagoblin"),
    MYSPACE("myspace"),
    PINTEREST("pinterest"),
    PNUT("pnut"),
    POSTEROUS("posterous"),
    PUMPIO("pumpio"),
    REDMATRIX("redmatrix"),
    RSS_2_0("rss2.0"),
    SMTP("smtp"),
    TENT("tent"),
    TUMBLR("tumblr"),
    TWITTER("twitter"),
    WORDPRESS("wordpress"),
    XMPP("xmpp");
    private final String value;
    private final static Map<String, Outbound> CONSTANTS = new HashMap<String, Outbound>();

    static {
        for (Outbound c: values()) {
            CONSTANTS.put(c.value, c);
        }
    }

    Outbound(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String value() {
        return this.value;
    }

    public static Outbound fromValue(String value) {
        Outbound constant = CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }

}
