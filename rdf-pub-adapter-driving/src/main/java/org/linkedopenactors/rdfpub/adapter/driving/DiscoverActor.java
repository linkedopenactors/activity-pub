package org.linkedopenactors.rdfpub.adapter.driving;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.webfinger.QueryByWebfingerResource;
import org.linkedopenactors.rdfpub.app.service.webfinger.UcWebfinger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * HTTP Controller that provides webfinger profiles.
 */
@RestController
@Slf4j
class DiscoverActor extends AbstractController{
	
	@Autowired
	private UcWebfinger ucWebfinger; 
	
	@Autowired
	private AuthenticatedActorHolder authenticatedActorHolder; 

	/**
	 * Provides a webfinger profile.
	 * @param request
	 * @param resource
	 * @param authentication
	 * @return webfinger profile
	 */
	@GetMapping("/.well-known/webfinger")
	public ResponseEntity<String> webfingerEndpoint(HttpServletRequest request,
			@RequestParam(value = "resource", required = false) String resource,
			final JwtAuthenticationToken authentication) {
		try {
			ResponseEntity<String> responseEntity;
			if(!isUri(resource)) {
				responseEntity = ResponseEntity.badRequest().body("passed resource is no Uri.");
			} else if(!hasSchema(resource)) {
				responseEntity = ResponseEntity.badRequest().body("passed resource has no schema.");
			}
			else {
				responseEntity = webfinger(resource, authentication, extractUserAgent(request).orElse(null));
			}
			return responseEntity;
		} catch (Exception e) {
			log.error("error", e);
			throw e;
		}
	}

	private ResponseEntity<String> webfinger(String resource, final JwtAuthenticationToken jwtAuthentication, UserAgent userAgent) {
		
		QueryByWebfingerResource query = new QueryByWebfingerResource(authenticatedActorHolder, resource, userAgent);
		Optional<String> profile = ucWebfinger.perform(query);

		ResponseEntity<String> responseEntity;
		if(profile.isEmpty()) {
			log.debug("no actor found for resource: '"+resource+"' !");
			responseEntity = ResponseEntity.notFound().build();
		} else {
			String body = profile.orElseThrow();
			log.trace("success: " + body);				
			responseEntity = ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_TYPE, "application/jrd+json; charset=utf-8")
					.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
					.body(body);
		}
		return responseEntity;
	}

	private boolean isUri(String resource) {
		Optional<String> resourceOpt = Optional.ofNullable(resource);
		if(resourceOpt.isEmpty()) {
			return false;
		}
		try {
			new URI(resource);
			return true;
		} catch (URISyntaxException e) {
			return false;
		}
	}

	private boolean hasSchema(String resource) {
		URI uri;
		try {
			uri = new URI(resource);
		} catch (URISyntaxException e) {
			throw new RuntimeException("there is a problem in the order of validation the resource! At this point it has to be a valid URI !");
		}
		return Optional.ofNullable(uri.getScheme()).isPresent();
	}
}

