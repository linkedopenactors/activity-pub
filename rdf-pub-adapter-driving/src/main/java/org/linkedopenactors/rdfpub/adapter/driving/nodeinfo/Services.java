
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.List;


/**
 * The third party sites this server can connect to via their application API.
 * 
 */
public class Services {

    /**
     * The third party sites this server can retrieve messages from for combined display with regular traffic.
     * (Required)
     * 
     */
    private List<Inbound> inbound;
    /**
     * The third party sites this server can publish messages to on the behalf of a user.
     * (Required)
     * 
     */
    private List<Outbound> outbound;

    /**
     * The third party sites this server can retrieve messages from for combined display with regular traffic.
     * (Required)
     * 
     */
    public List<Inbound> getInbound() {
        return inbound;
    }

    /**
     * The third party sites this server can retrieve messages from for combined display with regular traffic.
     * (Required)
     * 
     */
    public void setInbound(List<Inbound> inbound) {
        this.inbound = inbound;
    }

    public Services withInbound(List<Inbound> inbound) {
        this.inbound = inbound;
        return this;
    }

    /**
     * The third party sites this server can publish messages to on the behalf of a user.
     * (Required)
     * 
     */
    public List<Outbound> getOutbound() {
        return outbound;
    }

    /**
     * The third party sites this server can publish messages to on the behalf of a user.
     * (Required)
     * 
     */
    public void setOutbound(List<Outbound> outbound) {
        this.outbound = outbound;
    }

    public Services withOutbound(List<Outbound> outbound) {
        this.outbound = outbound;
        return this;
    }

}
