package org.linkedopenactors.rdfpub.adapter.driving;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.readcollection.QueryReadCollection;
import org.linkedopenactors.rdfpub.app.service.readcollection.QueryReadCollection4Actor;
import org.linkedopenactors.rdfpub.app.service.readcollection.UcReadCollection;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ContentTypeHeaderMappings;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestScope
class ControllerReadCollection extends AbstractController {

	private ContentTypeHeaderMappings contentTypeHeaderMappings;

	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	private UcReadCollection ucReadCollection;

	private AuthenticatedActorHolder authenticatedActorHolder; 

	public ControllerReadCollection(ContentTypeHeaderMappings contentTypeHeaderMappings,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder, UcReadCollection ucReadCollection,
			AuthenticatedActorHolder authenticatedActorHolder) {
		this.contentTypeHeaderMappings = contentTypeHeaderMappings;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
		this.ucReadCollection = ucReadCollection;
		this.authenticatedActorHolder = authenticatedActorHolder;
	}

	@PostMapping(value = "/resource/{actorId}/{collectionId}")
	public ResponseEntity<String> shutUp(HttpServletRequest request, final JwtAuthenticationToken authentication) {
		log.warn("Irgend jemand: " + request.getRemoteHost() +" hört nicht auf Nachriochten an diese url zu senden !!! " + request.getRequestURI());
		return ResponseEntity.status(410).build();
	}
	
	@GetMapping(value = "/resource/{actorId}/col/{collectionId}")
	public ResponseEntity<String> getCollection(HttpServletRequest request,
			@PathVariable(value = "actorId") String actorIdParam, @PathVariable(value = "collectionId") String collectionId) {
		
		int pageSize = extractPageSize(request);
		int startIndex = extractStartIndex(request);
		try {
			OrderedCollectionPage orderedCollectionPage = read(actorIdParam, collectionId, pageSize, startIndex, extractUserAgent(request).orElse(null));
			ResponseEntity<String> result = convert(request, orderedCollectionPage);
			return result;
		} catch (Exception e) {
			log.error("<-getCollection(RemoteHost: " + request.getRemoteHost() + ") -> " + e.getMessage() + " returning empty collection!");
			log.error("",e);
			// TODO: the code is a bit confusing. since the collectionId is not public, an
			// empty collection is returned!
			OrderedCollectionPage orderedCollectionPage = readPublicOrEmptyCollection(actorIdParam, collectionId,
					pageSize, startIndex);
			return convert(request, orderedCollectionPage);
		}
	}

	@GetMapping(value = "/resource/instanceActor/col/{collectionId}")
	public ResponseEntity<String> getOfInstanceActor(@PathVariable(value = "collectionId") String collectionId,
			HttpServletRequest request) {
		log.debug("RemoteHost: " + request.getRemoteHost());
		QueryReadCollection query = createOrderedCollectionPage(extractPageSize(request),
				extractStartIndex(request), activityPubObjectIdBuilder.createPublicCollection());
		OrderedCollectionPage orderedCollectionPage = ucReadCollection.perform(query);			
		return convert(request, orderedCollectionPage);
	}

	private OrderedCollectionPage readPublicOrEmptyCollection(String actorId, String collectionId, int pageSize, int startIndex) {
		RdfPubObjectIdActor collectionOwner = activityPubObjectIdBuilder.createActor(actorId);
		RdfPubObjectIdCollection collection = activityPubObjectIdBuilder.createCollection(collectionOwner, collectionId);
		QueryReadCollection query = createOrderedCollectionPage(pageSize, startIndex, collection);
		OrderedCollectionPage orderedCollectionPage = ucReadCollection.perform(query);
		return orderedCollectionPage;
	}
	
	private OrderedCollectionPage read(String actorId, String collectionId, int pageSize, int startIndex, UserAgent userAgent) {
		RdfPubObjectIdActor collectionOwner = activityPubObjectIdBuilder.createActor(actorId);
		RdfPubObjectIdCollection collection = activityPubObjectIdBuilder.createCollection(collectionOwner, collectionId);
		QueryReadCollection4Actor query = createOrderedCollectionPage(pageSize, startIndex, actorId, collection, userAgent);
		query.setCollectionId(collection.getCollectionIdentifier());
		OrderedCollectionPage orderedCollectionPage = ucReadCollection.perform(query);
		return orderedCollectionPage;
	}
	
	private QueryReadCollection createOrderedCollectionPage(int pageSize, int startIndex, RdfPubObjectIdCollection collection) {
		QueryReadCollection query = new QueryReadCollection();
		query.setCollectionInfo(collection);		
		query.setStartIndex(startIndex);		
		query.setPageSize(pageSize);
		return query;
	}
	
	private QueryReadCollection4Actor createOrderedCollectionPage(int pageSize, int startIndex, String actorId,
			RdfPubObjectIdCollection collection, UserAgent userAgent) {
		QueryReadCollection4Actor query = new QueryReadCollection4Actor(authenticatedActorHolder, userAgent);
		query.setActorId(actorId);		
		query.setCollectionInfo(collection);		
		query.setStartIndex(startIndex);		
		query.setPageSize(pageSize);
		query.setCollectionId(collection.getCollectionIdentifier());
		return query;
	}

	private ResponseEntity<String> convert(HttpServletRequest request, OrderedCollectionPage orderedCollectionPage) {
		RdfFormat rdfFormat = contentTypeHeaderMappings.determineRdfFormat(request.getHeader(HttpHeaders.ACCEPT));
		String result = orderedCollectionPage.toString(rdfFormat, true);		
		return ResponseEntity.ok(result);
	}

	private int extractStartIndex(HttpServletRequest request) {
		return Integer.parseInt(Optional.ofNullable(request.getHeader("startIndex")).orElse("0").toString());
	}

	private int extractPageSize(HttpServletRequest request) {
		return Integer.parseInt(Optional.ofNullable(request.getHeader("pageSize")).orElse("100").toString());
	}
}
