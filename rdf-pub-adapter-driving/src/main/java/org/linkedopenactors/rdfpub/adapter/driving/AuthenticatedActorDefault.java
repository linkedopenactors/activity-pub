package org.linkedopenactors.rdfpub.adapter.driving;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.actor.QueryByAuthentication;
import org.linkedopenactors.rdfpub.app.service.actor.UcQueryActor;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

class AuthenticatedActorDefault implements AuthenticatedActor {
	
	private UcQueryActor ucQueryActor;
	private ActorsStoreRepository actorsStoreRepository;
	private AuthenticatedActorHolder authenticationHolder;
	private Actor cachedActor;

	public AuthenticatedActorDefault(UcQueryActor ucQueryActor, ActorsStoreRepository actorsStoreRepository,
			AuthenticatedActorHolder authentication) {
		if(!authentication.getAuthentication().isAuthenticated()) {
			throw new IllegalArgumentException("not authenticated!");
		}
		this.ucQueryActor = ucQueryActor;
		this.actorsStoreRepository = actorsStoreRepository;
		this.authenticationHolder = authentication;
	}
	
	@Override
	public String getActorsLoggableName() {
		String actorname = getActor().getPreferredUsername()								
				.orElse(getActorId().getBaseSubject().getIRIString());
		return actorname;
	}
	
	@Override
	public RdfPubObjectIdActor getActorId() {
		return getActor().getSubject().asActor().orElseThrow();
	}

	@Override
	public Actor getActor() {
		if(cachedActor==null) {			
			Actor actor = ucQueryActor.perform(new QueryByAuthentication(authenticationHolder, null));
			if(!actor.getOauth2IssuerUserId().orElse(null).equals(authenticationHolder.getAuthentication().getIssuerUserId())) {
				throw new IllegalStateException("IssuerUserId mismatch! " + actor.getOauth2IssuerUserId().orElse(null) + " != " + authenticationHolder.getAuthentication().getIssuerUserId()) ;
			}
			cachedActor = actor;
		}
		return cachedActor;
	}
	
	@Override
	public ActorsStore getActorsStore() {
		return actorsStoreRepository.find(getActor()).orElseThrow();
	}

	@Override
	public PendingFollowerCache getPendingFollowerCache() {
		return actorsStoreRepository.getPendingFollowerCache(getActorId().getBaseSubject(), getActorsLoggableName());
	}

	@Override
	public PendingFollowerCache getPendingFollowingCache() {
		return actorsStoreRepository.getPendingFollowingCache(getActorId().getBaseSubject(), getActorsLoggableName());
	}
}
