package org.linkedopenactors.rdfpub.adapter.driving;

import java.net.MalformedURLException;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.CmdReceiveActivityOutbox;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * HTTP Controller that receives outbox activities.
 */
@RestController
@Slf4j
class ReceiveActivityOutbox extends AbstractController {
	
	@Value( "${instance.domain}")
	private String instanceDomain;
	
	@Autowired
	private org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxReceiveActivityService receiveActivityOutboxService;
	
	@Autowired
	private AuthenticatedActorHolder authenticatedActorHolder;

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path="/resource/{actorId}/col/outbox")
	public ResponseEntity<Void> receiveOutboxActivity(@org.springframework.web.bind.annotation.RequestBody String body,
			HttpServletRequest request, @PathVariable(value = "actorId") String actorIdParam,
			final JwtAuthenticationToken authentication) throws MalformedURLException {
				log.debug("outbox body: " + body);
		if(!StringUtils.hasText(body)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "body is mandatory!");
		}
		// TODO who is allowed to call this method ??
		CmdReceiveActivityOutbox cmd = new CmdReceiveActivityOutbox(authenticatedActorHolder, actorIdParam, body, extractRdfFormat(request), extractUserAgent(request).orElse(null));
		RdfPubObjectIdActivity location = receiveActivityOutboxService.perform(cmd);
		
		org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
	    headers.add("Location", location.getExternalSubject().getIRIString());
	    
	    return new ResponseEntity<>(null, headers, HttpStatus.CREATED);
	}
}