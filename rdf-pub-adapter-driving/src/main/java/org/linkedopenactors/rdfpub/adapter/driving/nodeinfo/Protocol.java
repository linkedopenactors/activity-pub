
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.HashMap;
import java.util.Map;

public enum Protocol {

    ACTIVITYPUB("activitypub"),
    BUDDYCLOUD("buddycloud"),
    DFRN("dfrn"),
    DIASPORA("diaspora"),
    LIBERTREE("libertree"),
    OSTATUS("ostatus"),
    PUMPIO("pumpio"),
    TENT("tent"),
    XMPP("xmpp"),
    ZOT("zot");
    private final String value;
    private final static Map<String, Protocol> CONSTANTS = new HashMap<String, Protocol>();

    static {
        for (Protocol c: values()) {
            CONSTANTS.put(c.value, c);
        }
    }

    Protocol(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String value() {
        return this.value;
    }

    public static Protocol fromValue(String value) {
        Protocol constant = CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }

}
