
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

/**
 * Usage statistics for this server.
 * 
 */
public class Usage {

    /**
     * statistics about the users of this server.
     * (Required)
     * 
     */
    private Users users;
    /**
     * The amount of posts that were made by users that are registered on this server.
     * 
     */
    private Integer localPosts;
    /**
     * The amount of comments that were made by users that are registered on this server.
     * 
     */
    private Integer localComments;

    /**
     * statistics about the users of this server.
     * (Required)
     * 
     */
    public Users getUsers() {
        return users;
    }

    /**
     * statistics about the users of this server.
     * (Required)
     * 
     */
    public void setUsers(Users users) {
        this.users = users;
    }

    public Usage withUsers(Users users) {
        this.users = users;
        return this;
    }

    /**
     * The amount of posts that were made by users that are registered on this server.
     * 
     */
    public Integer getLocalPosts() {
        return localPosts;
    }

    /**
     * The amount of posts that were made by users that are registered on this server.
     * 
     */
    public void setLocalPosts(Integer localPosts) {
        this.localPosts = localPosts;
    }

    public Usage withLocalPosts(Integer localPosts) {
        this.localPosts = localPosts;
        return this;
    }

    /**
     * The amount of comments that were made by users that are registered on this server.
     * 
     */
    public Integer getLocalComments() {
        return localComments;
    }

    /**
     * The amount of comments that were made by users that are registered on this server.
     * 
     */
    public void setLocalComments(Integer localComments) {
        this.localComments = localComments;
    }

    public Usage withLocalComments(Integer localComments) {
        this.localComments = localComments;
        return this;
    }

}
