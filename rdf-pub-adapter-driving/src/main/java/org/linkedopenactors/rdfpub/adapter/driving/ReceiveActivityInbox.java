package org.linkedopenactors.rdfpub.adapter.driving;

import java.net.MalformedURLException;

import org.linkedopenactors.rdfpub.adapter.http.HttpHeaders;
import org.linkedopenactors.rdfpub.adapter.http.HttpMethod;
import org.linkedopenactors.rdfpub.adapter.http.SignatureVerifierFactory;
import org.linkedopenactors.rdfpub.adapter.http.UnauthorizedException;
import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.GoneException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.CmdReceiveActivityInbox;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * HTTP Controller that receives inbox activities.
 */
@RestController
@Slf4j
class ReceiveActivityInbox extends AbstractController {
	
	@Value( "${instance.domain}")
	private String instanceDomain;
	
	@Autowired
	private org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxReceiveActivityService receiveActivityInboxService;

	@Autowired
	private SignatureVerifierFactory signatureVerifierFactory;
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path="/resource/{actorId}/col/inbox")
	public ResponseEntity<String> receiveInboxActivity(@org.springframework.web.bind.annotation.RequestBody String body,
			HttpServletRequest request, @PathVariable(value = "actorId") String actorId) throws MalformedURLException {
		
		if(!StringUtils.hasText(body)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "body is mandatory!");
		}
		
		try {
			SignatureVerifier fredysVerifier = signatureVerifierFactory.build(body, extractHeaders(request));
			log.debug("body: " + body);
			CmdReceiveActivityInbox cmd = new CmdReceiveActivityInbox(request.getRemoteHost(), actorId, body, extractRdfFormat(request), fredysVerifier);
			receiveActivityInboxService.perform(cmd);
		} catch (UnauthorizedException e) {
			log.warn("server answer with HttpStatus.UNAUTHORIZED");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
		} catch (BadRequestException e) {
			log.warn("server answer with HttpStatus.BAD_REQUEST");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		} catch (GoneException e) {
//			log.debug("server answer with HttpStatus.GONE");
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	    return new ResponseEntity<>(HttpStatus.CREATED);
	}

	private HttpHeaders extractHeaders(HttpServletRequest request) {
    	HttpHeaders httpSignatureHeaders = new HttpHeaders();
    	httpSignatureHeaders.setHost(request.getHeader(HttpHeaders.HOST));
    	httpSignatureHeaders.setDate(request.getHeader(HttpHeaders.DATE));
    	httpSignatureHeaders.setDigest(request.getHeader(HttpHeaders.DIGEST));
    	httpSignatureHeaders.setSignature(request.getHeader(HttpHeaders.SIGNATURE));
    	httpSignatureHeaders.setContentType(request.getHeader(HttpHeaders.CONTENT_TYPE));
    	httpSignatureHeaders.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
    	httpSignatureHeaders.setContentLength(request.getHeader("content-length"));
		httpSignatureHeaders.setRequestTarget(HttpMethod.valueOf(request.getMethod()), request.getRequestURI());
    	
		return httpSignatureHeaders;
	}
}