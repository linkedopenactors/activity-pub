package org.linkedopenactors.rdfpub;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * A servlet filter to log request and response
 * The logging implementation is pretty native and for demonstration only
 * @author hemant
 *
 */
@Component
@Order(2)
public class RequestResponseLoggingFilter implements Filter {

	private final static Logger LOG = LoggerFactory.getLogger(RequestResponseLoggingFilter.class);

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		LOG.info("Initializing filter :{}", this);
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		LOG.debug("Logging Request  {} : {}", req.getMethod(), req.getRequestURI());
		for (Iterator<String> iterator = req.getHeaderNames().asIterator(); iterator.hasNext();) {
			String headerName = iterator.next();
			LOG.debug("request header: " + headerName + " - " + req.getHeader(headerName));
		}
		Instant start = Instant.now();
		chain.doFilter(request, response);
		long durationInSeconds = Duration.between(start, Instant.now()).toSeconds();
		String status = HttpStatus.valueOf(res.getStatus()).toString();
		LOG.trace("Logging Response : {} : {} ->  status: {}, contentType: {}, durationInSeconds: {}", req.getMethod(), req.getRequestURI(), status, res.getContentType(), durationInSeconds);
		for (String headerName : res.getHeaderNames()) {
			LOG.trace("response header: " + headerName + " - " + res.getHeader(headerName));
		}		
	}

	@Override
	public void destroy() {
		LOG.warn("Destructing filter :{}", this);
	}
}