package org.linkedopenactors.rdfpub.config;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.linkedopenactors.rdfpub.domain.UnsupportedActivityPubObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

import io.sentry.Sentry;
import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
class ErrorControllerAdvice {
    @ExceptionHandler(UnsupportedActivityPubObject.class) // exception handled
    public ResponseEntity<ErrorResponse> handleUnsupportedActivityPubObject(
        Exception e
    ) {
        // ... potential custom logic
    	Sentry.captureException(e);
    	
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED; // 501

        ErrorResponse errorResponse = new ErrorResponse(
		  status, 
		  e.getMessage()
		);
//        Log.getLogger(ErrorControllerAdvice.class).warn(errorResponse.toString());
        log.error(errorResponse.toString(), e);
        
		return new ResponseEntity<>(
            errorResponse,
            status
        );
    }

    @ExceptionHandler(ResponseStatusException.class) // exception handled
    public ResponseEntity<ErrorResponse> handleResponseStatusException(ResponseStatusException e) {

    	if(e.getStatusCode().equals(HttpStatus.GONE)) {
	    	log.warn("status: " + e.getStatusCode());
	        log.warn("status: " + e.getReason());
    	} else {
	    	Sentry.captureException(e);	    	
	    	log.error("status: " + e.getStatusCode());
	        log.error("status: " + e.getReason());
	        log.error("handleResponseStatusException: ", e);
    	}	
        HttpStatus status = HttpStatus.valueOf(e.getStatusCode().value());
        ErrorResponse errorResponse = new ErrorResponse(status, e.getMessage());        
		return new ResponseEntity<>(
            errorResponse,
            status
        );
    }

    
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class) // exception handled
    public ResponseEntity<ErrorResponse> handleResponseStatusException(HttpRequestMethodNotSupportedException incommingException) {

    	HttpStatus status = HttpStatus.valueOf(incommingException.getStatusCode().value());
    	log.warn("message: ("+status+") " + incommingException.getMessage());

        ErrorResponse errorResponse = new ErrorResponse(status, incommingException.getMessage());

        return new ResponseEntity<>(
            errorResponse,
            status
        );
    }
    
    @ExceptionHandler(org.springframework.web.servlet.resource.NoResourceFoundException.class) // exception handled
    public ResponseEntity<ErrorResponse> handleResponseStatusException(org.springframework.web.servlet.resource.NoResourceFoundException incommingException) {

    	log.warn("message: " + incommingException.getMessage());

        HttpStatus status = HttpStatus.valueOf(incommingException.getStatusCode().value());
        ErrorResponse errorResponse = new ErrorResponse(status, incommingException.getMessage());

        return new ResponseEntity<>(
            errorResponse,
            status
        );
    }

    // fallback method
    @ExceptionHandler(Exception.class) // exception handled
    public ResponseEntity<ErrorResponse> handleExceptions(
        Exception e
    ) {
        // ... potential custom logic

    	Sentry.captureException(e);

    	
    	log.error("exception: " + e.getClass().getName());
    	log.error("message: " + e.getMessage());
    	log.error("stacktrace: " + ExceptionUtils.getStackTrace(e));
    	
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR; // 500

    // converting the stack trace to String
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);
    e.printStackTrace(printWriter);
    String stackTrace = stringWriter.toString();

        return new ResponseEntity<>(
            new ErrorResponse(
              status, 
              e.getMessage(), 
              stackTrace // specifying the stack trace in case of 500s
            ),
            status
        );
    }
}