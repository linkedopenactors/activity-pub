package org.linkedopenactors.rdfpub.config;

import java.io.File;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring internal {@link Configuration}
 * @author naturzukunft@mastodon.social
 */
@Configuration
public class RepositoryConfig {
	
	@Value( "${app.rdfRepositoryHome}" )
	private String rdfRepositoryHome;

	/**
	 * Gives you the Spring managed Bean of type {@link Repository}.
	 * @return The Spring managed Bean of type {@link Repository}.
	 */
	@Bean
	public Repository getRepository() {
		File dataDir = new File(rdfRepositoryHome);
		return new SailRepository(new NativeStore(dataDir));	
	}
}
