package org.linkedopenactors.rdfpub.config.security;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

final class CustomJwtGrantedAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<GrantedAuthority> convert(@NonNull Jwt jwt) {
		var realmAccess = (Map<String, List<String>>) jwt.getClaim("realm_access");
		List<GrantedAuthority> collect = realmAccess.get("roles").stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role))
				.collect(Collectors.toList());
		return collect;
	}

	@Override
	public @NonNull <U> Converter<Jwt, U> andThen(@NonNull Converter<? super Collection<GrantedAuthority>, ? extends U> after) {
		return Converter.super.andThen(after);
	}
}
