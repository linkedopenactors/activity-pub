package org.linkedopenactors.rdfpub.config;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AopConfiguration {

    @Pointcut(
    		"within(org.linkedopenactors.rdfpub..*)"
    )
    public void monitor() { }
    
    @Bean
    public org.linkedopenactors.rdfpub.aop.PerformanceMonitorInterceptor performanceMonitorInterceptor() {
        return new org.linkedopenactors.rdfpub.aop.PerformanceMonitorInterceptor(false);
    }

    @Bean
    public Advisor performanceMonitorAdvisor() {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("org.linkedopenactors.rdfpub.config.AopConfiguration.monitor()");
        return new DefaultPointcutAdvisor(pointcut, performanceMonitorInterceptor());
    }
}