package org.linkedopenactors.rdfpub.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;

@Profile("!test")
@Configuration
public class JWTSecurityConfig {
		
	@Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
//		JwtIssuerAuthenticationManagerResolver authenticationManagerResolver = JwtIssuerAuthenticationManagerResolver
//				.fromTrustedIssuers("https://login.m4h.network/auth/realms/LOA", "https://idp.example.org/issuerTwo");
		
        http.csrf(AbstractHttpConfigurer::disable)// TODO csrf !!
        	.authorizeHttpRequests(authz -> authz
        				.requestMatchers("/asPublic/dump", "/actuator/env").hasRole("admin") // TODO validate that this is only accessible by admins
        				.requestMatchers("/admin/**").hasRole("superadmin")
        				.requestMatchers(        			
							"/",
							"/favicon.ico",
							"/content/**",
							"/actuator/**",
							"/api-doc",
							"/v3/api-docs/**",
							"/swagger-resources/**",
							"/content/swagger-ui/**",
							"/content/swagger-ui/index.html**",
							"/webjars/**",
							"/asPublic/sparql",
							"/.well-known/webfinger/",
							"/.well-known/webfinger*",
							"/.well-known/rdfpub",
							"/.well-known/nodeinfo",
							"/nodeinfo/2.1",
							"/error",
							"/actors/persons"
							,"/*/outbox"
							,"/*/outbox/*"
							,"/*/inbox"
							,"/resource/**"
							,"/resource/instanceActor/*"
		                    ,"/.well-known/oauth-authorization-server"
		                    ,"/oauth/*"
							).permitAll()
            .anyRequest()
            .authenticated())
        .oauth2ResourceServer(oauth2 -> oauth2
                .jwt(jwt -> jwt
                    .jwtAuthenticationConverter(customJwtAuthenticationConverter())
                )
            );
        return http.build();
    }
	
	@Bean
	public JwtDecoder jwtDecoder() {
	    return JwtDecoders.fromIssuerLocation("https://login.m4h.network/auth/realms/LOA");
	}
	
	@Bean
	public JwtAuthenticationConverter customJwtAuthenticationConverter() {
	    JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
	    converter.setJwtGrantedAuthoritiesConverter(new CustomJwtGrantedAuthoritiesConverter());
	    return converter;
	}
}
