package org.linkedopenactors.rdfpub.config.upgrade;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.auth.Authentication;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.CmdReceiveActivityOutboxTyped;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxReceiveActivityService;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocPending;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UpgradeActors {
    
    private ActivityPubObjectFactory activityPubObjectFactory;
    private OutboxReceiveActivityService receiveActivityOutboxService;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private PublicActorStore publicActorStore;
	private VocabContainer vocabContainer;
	
	public UpgradeActors (
			VocabContainer vocabContainer,
			ActivityPubObjectFactory activityPubObjectFactory,
			OutboxReceiveActivityService receiveActivityOutboxService,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder,
			PublicActorStore publicActorStore) {
				this.vocabContainer = vocabContainer;
				this.activityPubObjectFactory = activityPubObjectFactory;
				this.receiveActivityOutboxService = receiveActivityOutboxService;
				this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
				this.publicActorStore = publicActorStore;
	}
	
	@PostConstruct
    public void start() {
		try {
			boolean upgrade = Optional.ofNullable(System.getProperty("upgrade"))
					.map(Boolean::valueOf)
					.orElse(false);
			if(upgrade) {
				Set<Actor> actors = publicActorStore.getActors();
				handleVersionlessActors(actors);
				handleVersion1Actors(actors);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	private void handleVersionlessActors(Set<Actor> actors) {
		log.debug("-> handleVersionlessActors(size: " + + actors.size() +")");
		Set<Actor> actorsWithoutObjectVersion = actors.stream()
				.filter(d->d.getObjectVersion().isEmpty())
				.collect(Collectors.toSet());

		Set<Actor> actorsToUpdate = actorsWithoutObjectVersion.stream()
				.map(actor->{
					String baseSubjectString = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
					baseSubjectString = baseSubjectString.endsWith("/") ? baseSubjectString : baseSubjectString + "/"; 
					actor.setObjectVersion(1L);
					RdfPubObjectIdActor subject = actor.getSubject().asActor().orElseThrow();
					actor.setPendingFollowers(activityPubObjectIdBuilder.createCollection(subject, VocPending.pendingFollowers.replace(VocPending.NS, "")));
					// TODO ^^das geht so nicht. muss überschrieben werden, sonst ist das property doppelt! 
					actor.setPendingFollowing(activityPubObjectIdBuilder.createCollection(subject, VocPending.pendingFollowing.replace(VocPending.NS, "")));
					log.debug("actor: " + actor);
					return actor;
					}
				)
				.collect(Collectors.toSet());
		
		performReceiveActivityOutbox(actorsToUpdate, null);
		log.debug("<- handleVersionlessActors(size: " + + actors.size() +")");
	}

	private void handleVersion1Actors(Set<Actor> actors) {
		log.debug("-> handleVersion1Actors(size: " + + actors.size() +")");
		performReceiveActivityOutbox(actors.stream()
				.filter(a ->objectVersionIs(a, 1L))
				.map(this::upgradeV1Actor)
				.collect(Collectors.toSet()), null);
		log.debug("<- handleVersion1Actors(size: " + + actors.size() +")");
	}

	public Actor upgradeV1Actor(Actor actor) {
		log.debug("-> upgradeV1Actor("+actor.getPreferredUsername()+")");
		String baseSubjectString = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		baseSubjectString = baseSubjectString.endsWith("/") ? baseSubjectString : baseSubjectString + "/"; 
		actor.setObjectVersion(2L);
		
		RdfPubObjectIdActor subject = actor.getSubject().asActor().orElseThrow();
		actor.setPendingFollowers(activityPubObjectIdBuilder.createCollection(subject, VocPending.pendingFollowers.replace(VocPending.NS, "")));
		actor.setPendingFollowing(activityPubObjectIdBuilder.createCollection(subject, VocPending.pendingFollowing.replace(VocPending.NS, "")));

		actor.setInbox(activityPubObjectIdBuilder.createCollection(subject, VocAs.inbox.replace(VocAs.NS, "")));
		actor.setOutbox(activityPubObjectIdBuilder.createCollection(subject, VocAs.outbox.replace(VocAs.NS, "")));
		actor.setLiked(activityPubObjectIdBuilder.createCollection(subject, VocAs.liked.replace(VocAs.NS, "")));
		
		log.debug("actor: " + actor);
		log.debug("<- upgradeV1Actor("+actor.getPreferredUsername()+")");
		return actor;
	}
	
	private boolean objectVersionIs(Actor actor, Long expectedVersion) {
		return actor.getObjectVersion()
				.filter(version->version.equals(expectedVersion))
				.isPresent();
	}

	private void performReceiveActivityOutbox(Set<Actor> actorsToUpdate, UserAgent userAgent) {
		RdfPubObjectIdActor instanceActorId = activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME);
		
		actorsToUpdate.forEach(actor->{
			RdfPubObjectId subject4Activity = activityPubObjectIdBuilder.createActivity(instanceActorId);
			Activity updateActivity = activityPubObjectFactory.create(subject4Activity).asConvertable().asActivity();
			updateActivity.setType(Set.of(vocabContainer.vocAs().Update()));
			updateActivity.setTo(Set.of(vocabContainer.vocAs().Public()));
			updateActivity.setObjects(Set.of(actor));			
			CmdReceiveActivityOutboxTyped cmd = new CmdReceiveActivityOutboxTyped(instanceActorId, updateActivity, getAuthenticatedActorHolder(), userAgent);
			receiveActivityOutboxService.perform(cmd);
		});
	}

	private AuthenticatedActorHolder getAuthenticatedActorHolder() {
		return new AuthenticatedActorHolder() {
			
			@Override
			public Optional<AuthenticatedActor> getAuthenticatedActor() {
				return Optional.empty();
			}

			@Override
			public Authentication getAuthentication() {
				return null;
			}
		};
	}
}
