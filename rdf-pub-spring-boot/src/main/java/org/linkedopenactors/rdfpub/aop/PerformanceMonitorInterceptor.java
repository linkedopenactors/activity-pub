package org.linkedopenactors.rdfpub.aop;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.AbstractMonitoringInterceptor;

import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.monitor.EtmPoint;

@SuppressWarnings("serial")
public class PerformanceMonitorInterceptor extends AbstractMonitoringInterceptor {

	private static final EtmMonitor etmMonitor = EtmManager.getEtmMonitor();
	/**
	 * Create a new PerformanceMonitorInterceptor with a static logger.
	 */
	public PerformanceMonitorInterceptor() {
	}

	/**
	 * Create a new PerformanceMonitorInterceptor with a dynamic or static logger,
	 * according to the given flag.
	 * @param useDynamicLogger whether to use a dynamic logger or a static logger
	 * @see #setUseDynamicLogger
	 */
	public PerformanceMonitorInterceptor(boolean useDynamicLogger) {
		setUseDynamicLogger(useDynamicLogger);
	}


	@Override
	protected Object invokeUnderTrace(MethodInvocation invocation, Log logger) throws Throwable {
		String name = createInvocationTraceName(invocation);
		EtmPoint point = etmMonitor.createPoint(name);
		try {
			return invocation.proceed();
		}
		finally {
			point.collect();
		}
	}
}