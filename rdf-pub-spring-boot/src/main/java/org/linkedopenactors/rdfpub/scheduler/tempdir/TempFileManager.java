package org.linkedopenactors.rdfpub.scheduler.tempdir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TempFileManager {

	private TempFileManagerProperties tempFileManagerProperties;

	public TempFileManagerProperties getTempFileManagerProperties() {
		return tempFileManagerProperties;
	}

	public TempFileManager(TempFileManagerProperties tempFileManagerProperties) {
		this.tempFileManagerProperties = tempFileManagerProperties;
	}
	
	public TempFileManager(String customTempDirName, String prefix, String postix) {
		tempFileManagerProperties = new TempFileManagerProperties(customTempDirName, prefix, postix, 60 * 10);
	}
	
	public TempFileManager(String customTempDirName, String prefix, String postix, long maxAgeInSeconds) {
		tempFileManagerProperties = new TempFileManagerProperties(customTempDirName, prefix, postix, maxAgeInSeconds);
	}
	
	public void cleanup() throws IOException {
		for (File file : getTempFiles()) {
			long ageInSeconds = getAgeInSeconds(file);	
			if(ageInSeconds>=tempFileManagerProperties.getMaxAgeInSeconds()) {
				log.trace("file ("+file.getCanonicalPath()+") is older than "+tempFileManagerProperties.getMaxAgeInSeconds()+" seconds and will be deleted!");
				file.delete();
			} else {
				log.trace("file ("+file.getCanonicalPath()+") to young ("+ageInSeconds +" of "+ tempFileManagerProperties.getMaxAgeInSeconds()+" seconds) to delete.");	
			}
		}
	}

	public Collection<File> getTempFiles() {
		try {
			return FileUtils.listFiles(getTempDirFile(), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		} catch (IOException e) {
			throw new RuntimeException("error gettikng temp files", e);
		}
	}

	private long getAgeInSeconds(File file) throws IOException {
		long ageInSeconds;
		BasicFileAttributes attr = Files.readAttributes(Path.of(file.getAbsolutePath()), BasicFileAttributes.class);			
		Instant lastModified = attr.lastModifiedTime().toInstant();			
		ageInSeconds = Duration.between(lastModified, Instant.now()).toSeconds();			
		return ageInSeconds;
	}
	
	public File createTempFile() throws IOException {
			File tmpFile = File.createTempFile("/"+tempFileManagerProperties.getPrefix(), tempFileManagerProperties.getPostix(), getTempDirFile());
			return tmpFile;
	}

	private File getTempDirFile() throws IOException {
		return getTempDir().toFile();
	}
	
	private Path getTempDir() throws IOException {
		String systemTmpDirStr = System.getProperty("java.io.tmpdir");
		Path customTempDir = Path.of(systemTmpDirStr,IOUtils.DIR_SEPARATOR+"" , tempFileManagerProperties.getCustomTempDirName());
		if(!Files.exists(customTempDir)) {
			Files.createDirectory(customTempDir);
		}
		return customTempDir;
	}
}
