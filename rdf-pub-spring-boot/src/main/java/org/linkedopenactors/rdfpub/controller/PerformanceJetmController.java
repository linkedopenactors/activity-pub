package org.linkedopenactors.rdfpub.controller;

import java.io.StringWriter;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.renderer.SimpleTextRenderer;

@RestController
public class PerformanceJetmController {

	private static final EtmMonitor etmMonitor = EtmManager.getEtmMonitor();
	
	public PerformanceJetmController() {
		BasicEtmConfigurator.configure();
		etmMonitor.start();
	}

	@GetMapping(
		    value = "/jetm/print",
			headers = {"Accept=text/html"})
	@ResponseBody
	public String print(Model model) {
		return "<pre>" + getTableAsString() + "</pre>";
	}

	@GetMapping(
		    value = "/jetm/reset",
			headers = {"Accept=text/html"})
	@ResponseBody
	public void reset(Model model) {
		etmMonitor.reset();
	}

	private String getTableAsString() {
		StringWriter s = new StringWriter();
		EtmManager.getEtmMonitor().render(new SimpleTextRenderer(s));
		return s.toString();
	}
}