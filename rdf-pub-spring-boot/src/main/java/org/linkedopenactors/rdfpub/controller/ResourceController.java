package org.linkedopenactors.rdfpub.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
/**
 * Ich wollte den Root Pfad für rdf-pub verwenden, was leider mit sich bringt, dass es beim zugriff auf ressourcen Probleme gibt..
 * Daher gibt es ein abenteuerliches Mapping im ReadObject Controller und diesen Controller, der einzelne Ressourcen "weiterleitet".
 */
public class ResourceController {
	
	@GetMapping(
		    value = "/favicon.ico",
			headers = {"Accept=text/html"})
	public String favicon(Model model) {
		return "forward:/content/favicon.ico";
	}
}