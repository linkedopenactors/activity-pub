package org.linkedopenactors.rdfpub.filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.linkedopenactors.rdfpub.adapter.driving.UserAgentExtractor;
import org.linkedopenactors.rdfpub.app.UserAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * There are some noisy stuff in the fedi, that needs to be handled. At least temporarily
 */
@Component
@Order(2)
@Slf4j
public class RequestFilter implements jakarta.servlet.Filter {

	private final static Logger LOG = LoggerFactory.getLogger(RequestFilter.class);
	private UserAgentExtractor userAgentExtractor;

	public RequestFilter() {
		userAgentExtractor = new UserAgentExtractor();
	}
	
	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		CachedBodyHttpServletRequest cachedBodyHttpServletRequest =
				  new CachedBodyHttpServletRequest((HttpServletRequest)request);
		
		if(!isMastodonDeleteRequest(cachedBodyHttpServletRequest) 
				&& !isBlacklisted(cachedBodyHttpServletRequest)) {
			chain.doFilter(cachedBodyHttpServletRequest, response);
		} else {
			log.debug("ignoring!");
		}
	}
	
	private boolean isBlacklisted(HttpServletRequest request) {
		String path = request.getRequestURI();
		boolean blacklisted = blacklist().contains(path);
		log.debug(path + " blacklisted: " + blacklisted);
		return blacklisted;
	}
	
	private boolean isMastodonDeleteRequest(CachedBodyHttpServletRequest req) {		
		if(isMastodon(req)) {
			try {
				String body = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8.name());
				boolean containsDelete = body.contains("\"type\":\"Delete\"");
				boolean containssignature = body.contains("\"signatureValue\":");
				return containsDelete && containssignature;
			} catch (IOException e) {
				return false;
			}
		} else {
			return false;
		}
	}

	private boolean isMastodon(CachedBodyHttpServletRequest req) {
		Optional<UserAgent> ua = userAgentExtractor.extractUserAgent(req);
		return ua.map(UserAgent::getProduct)
				.map(String::toLowerCase)
				.map(product->"mastodon".equals(product))
				.orElse(false);
	}
	
	@Override
	public void destroy() {		
		LOG.warn("Destructing filter :{}", this);
	}
	
	
	private List<String> blacklist()  {
		return List.of("/resource/acto0b7cf49c-5b74-413b-b9d1-960e76a4a7e3/inbox");
	}
}
