package org.linkedopenactors.rdfpub.uc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.time.Instant;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestActorProfile")
@Slf4j
class TestWhoAmI {

	@Autowired
	private PrefixMapper prefixMapper;
	
	@Autowired
	private VocabContainer vocabContainer; 
	
	@Value("${app.rdfRepositoryHome}")
	private String rdfRepositoryHome;

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private TestHelperV2 testHelperV2;

	@BeforeEach
	public void setup() {			
	}

	@Test
	void testReadActorsProfileJsonLd() throws Exception {
		
		String userId = UUID.randomUUID().toString();
		String name = RandomStringUtils.random(10, true, false);
		URI issuer = URI.create("http://example.com");
		testHelperV2.createActor(userId, name, issuer.toString());
		
		MvcResult mvcResult; 
		mvcResult = testHelperV2.getMockMvc().perform(get("/api/whoami")
				.with(testHelperV2.jwt(userId, name, issuer.toString()))
//				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.header(HttpHeaders.ACCEPT, "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		
		String actorAsJson = mvcResult.getResponse().getContentAsString();
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(actorAsJson);
		
		String asText = null;
		try {
			asText = jsonNode.get("endpoints").get("oauthAuthorizationEndpoint").asText();
		} catch (Exception e) {
			log.error("actorAsJson: " + actorAsJson);
			e.printStackTrace();
		}		
		assertEquals(prefixMapper.getExternalIriPrefix() + "/oauth/oauthAuthorizationEndpoint", asText);
		assertEquals(prefixMapper.getExternalIriPrefix() + "/oauth/oauthTokenEndpoint", jsonNode.get("endpoints").get("oauthTokenEndpoint").asText());
		
		
		String root = mvcResult.getResponse().getHeader("root");
		ActivityPubObject ao = activityPubObjectFactory.create(RdfFormat.JSONLD, actorAsJson, root);
//		ActivityPubObject ao = stringToActivityPubObject.convert("application/ld+json", actorAsJson);
		Actor createActor = ao.asConvertable().asActor();
		
		assertTrue(createActor.getTypes().contains(vocabContainer.vocAs().Person()));
		assertEquals(userId, createActor.getOauth2IssuerUserId().orElseThrow());
		assertEquals(issuer.getRawAuthority(), createActor.getOauth2Issuer().orElseThrow());
		
		assertEquals(name, createActor.getName().orElseThrow());
//		assertEquals(BigInteger.valueOf(1), createActor.getVersion());
		assertTrue(createActor.getPublished().isPresent());
		assertEquals(Instant.class, createActor.getPublished().get().getClass());
		assertTrue(createActor.getInbox().asCollection().orElseThrow().getExternalSubject().getIRIString().startsWith("http://localhost:8081/"));
		assertTrue(createActor.getInbox().asCollection().orElseThrow().getIRIString().endsWith("/inbox"));

//		assertTrue(createActor.getInboxSparqlEndpoint().toString().startsWith("http://localhost:8081/"));		
//		assertTrue(createActor.getInboxSparqlEndpoint().toString().endsWith("/inbox/sparql"));
		
		String oauthAuthorizationEndpoint = createActor.resolveEndpoints()
				.orElseThrow()
				.getOauthAuthorizationEndpoint()
				.map(Object::toString)
				.orElse(null);
		assertEquals(prefixMapper.getExternalIriPrefix() + "/oauth/oauthAuthorizationEndpoint", oauthAuthorizationEndpoint);
	}
}
