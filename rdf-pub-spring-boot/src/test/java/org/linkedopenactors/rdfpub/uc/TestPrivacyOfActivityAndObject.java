package org.linkedopenactors.rdfpub.uc;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.client2024.HttpAdapter;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestPrivacyOfActivityAndObject")
class TestPrivacyOfActivityAndObject {

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private TestHelperV2 testHelperV2;

//	@Autowired
//	private ToActivityPubObject toActivityPubObject;
	
	@Value("${instance.domain}")
	private String instanceDomain; // TODO centralise ?

	private Actor actorMax;
	private Actor actorForeign;
	private Actor actorSomeUser;
	private String userIdMax;
	private JwtRequestPostProcessor maxJwt;
	private JwtRequestPostProcessor foreingJwt;
	private String preferredUsernameMax;

	private String userIdForeign;

	private String preferredUsernameForeign;

	private org.linkedopenactors.rdfpub.client2024.RdfPubClient pubClientMax;

	private org.linkedopenactors.rdfpub.client2024.RdfPubClient pubClientForeign;

	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	@BeforeEach
	public void setup() throws Exception {
		long start = System.currentTimeMillis();
		userIdMax = UUID.randomUUID().toString();
		preferredUsernameMax = "max_" + RandomStringUtils.random(10, true, false);
		actorMax = testHelperV2.createActor(userIdMax, preferredUsernameMax, instanceDomain);
		maxJwt = testHelperV2.jwt(userIdMax, preferredUsernameMax, instanceDomain);
		
		userIdForeign = UUID.randomUUID().toString();
		preferredUsernameForeign = "foreign_" + RandomStringUtils.random(10, true, false);
		actorForeign = testHelperV2.createActor(userIdForeign, preferredUsernameForeign, instanceDomain);
		foreingJwt = testHelperV2.jwt(userIdForeign, preferredUsernameForeign, instanceDomain);
		
		String serverBaseUrlParam = "http://example.org/";
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");

		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);

		org.linkedopenactors.rdfpub.client2024.HttpAdapter httpAdapter = testHelperV2.getHttpAdapter2024(userIdMax, preferredUsernameMax);		
		pubClientMax = new org.linkedopenactors.rdfpub.client2024.RdfPubClientDefault(httpAdapter, activityPubObjectFactory, serverBaseUrlParam, activityPubObjectIdBuilder);
		
		HttpAdapter httpAdapter2024 = testHelperV2.getHttpAdapter2024(userIdForeign, preferredUsernameForeign);
		pubClientForeign = new org.linkedopenactors.rdfpub.client2024.RdfPubClientDefault(httpAdapter2024, activityPubObjectFactory, serverBaseUrlParam, activityPubObjectIdBuilder);
		
		String userIdSomeUser= UUID.randomUUID().toString();
		String preferredUsernameSomeUser = "foreign_" + RandomStringUtils.random(10, true, false);
		actorSomeUser = testHelperV2.createActor(userIdSomeUser, preferredUsernameSomeUser, instanceDomain);
		
		long duration = System.currentTimeMillis()-start;
	}

	@Test
	void testUnallowedReadObjectOfOtherUser() throws Exception {
		IRI asObjectIri = Values.iri(activityPubObjectIdBuilder.createResource(actorMax.getSubject()).getExternalSubject().getIRIString());
		IRI asActivityId = Values.iri(activityPubObjectIdBuilder.createActivity(actorMax.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());

		
		Literal nameLiteral = literal("Max Mustermann");
		Model activityModel = new ModelBuilder()
				.subject(asActivityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), asObjectIri)
					.add(Values.iri(VocAs.name), nameLiteral)					
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(asObjectIri)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();
		
		// Send activity as POST
		StringWriter sw = new StringWriter();
		Rio.write(activityModel, sw, RDFFormat.TURTLE);

		String url = actorMax.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(url)
				.with(maxJwt)
				.content(sw.toString())
			    .contentType("text/turtle")
				.header(HttpHeaders.ACCEPT, "text/turtle"))				
				.andExpect(status().isCreated())
				.andReturn();
		
		// ReRead the Activity
		String activityLocation = Optional.ofNullable(mvcResult.getResponse().getHeader(HttpHeaders.LOCATION)).orElseThrow();		
		mvcResult = testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();

		Activity activity = testHelperV2.toActivity(activityLocation, mvcResult).orElseThrow();
		
		// Extract the object ID of the activity
		assertEquals(nameLiteral.stringValue(), activity.getName().orElse(null));
		String objectIRI = activity.getObject().stream().findFirst().orElseThrow().asResource().orElseThrow().getExternalSubject().getIRIString();
		
		// Read the object to be sure, it is saved.
		mvcResult = testHelperV2.getMockMvc().perform(get(objectIRI)
						.with(maxJwt)
						.header(HttpHeaders.ACCEPT, "application/ld+json"))
						.andExpect(status().isOk())
						.andReturn();
		
		ActivityPubObject activityPubObject = testHelperV2.toActivityPubObject(objectIRI, mvcResult).orElseThrow();		
		assertEquals(nameLiteral.stringValue(), activityPubObject.getName().orElse(null));
		
		// ReRead the Activity as foreign user
		mvcResult = testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(foreingJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isNotFound())
				.andReturn();
		
		// Read the object as foreign user
		mvcResult = testHelperV2.getMockMvc().perform(get(objectIRI)
				.with(foreingJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isNotFound())
				.andReturn();

	}

	@Test
	void testAllowedReadActivityOfOtherUser() throws Exception {
		Literal nameLiteral = literal("Max Mustermann");
		IRI asObjectId = Values.iri(activityPubObjectIdBuilder.createResource(actorMax.getSubject()).getExternalSubject().getIRIString());
		IRI asActivityId = Values.iri(activityPubObjectIdBuilder.createActivity(actorMax.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());
		
		Model activityModel = new ModelBuilder()
				.subject(asActivityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), asObjectId)
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(asObjectId)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocAs.bto), iri("http://example.org/testUser1"))
					.add(Values.iri(VocAs.bcc), iri("http://example.org/testUser2"))
					// Set "foreign" as receiver, therefore foreing is allowed to read the activity !
					.add(Values.iri(VocAs.to), iri(actorForeign.getSubject().toString()))
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();
				
		// Create Activity & Object
		StringWriter sw = new StringWriter();
		Rio.write(activityModel, sw, RDFFormat.TURTLE);
		
		String string = actorMax.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(string)
				.with(maxJwt)
				.content(sw.toString())
			    .contentType("text/turtle")
				.header(HttpHeaders.ACCEPT, "text/turtle"))				
				.andExpect(status().isCreated())
				.andReturn();

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);

		// Read as max
		mvcResult = testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();
		Activity activity = testHelperV2.toActivity(activityLocation, mvcResult).orElseThrow();
		assertEquals(nameLiteral.stringValue(), activity.getName().orElse(null));

		// TODO not only findFirst !!
		String uri = activity.getObject().stream().findFirst()
				.flatMap(RdfPubBlankNodeOrIRI::asResource)
				.orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(uri)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();
		ActivityPubObject activityPubObject = testHelperV2.toActivityPubObject(uri, mvcResult).orElseThrow();
		
		// read as foreign
		mvcResult = testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(foreingJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();

		mvcResult = testHelperV2.getMockMvc().perform(get(uri)
				.with(foreingJwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();
		
		activityPubObject = testHelperV2.toActivityPubObject(uri, mvcResult).orElseThrow();
		
		// validate reRead object
		assertEquals(nameLiteral.stringValue(), activityPubObject.getName().orElse(null));
		assertTrue(activityPubObject.getBto().isEmpty());
		assertTrue(activityPubObject.getBcc().isEmpty());
	}

	@Test
	void testAnonymousReadPublicActivityOfOtherUser() throws Exception {
		IRI asObjectId = Values.iri(activityPubObjectIdBuilder.createResource(actorMax.getSubject()).getExternalSubject().getIRIString());
		IRI asActivityId = Values.iri(activityPubObjectIdBuilder.createActivity(actorMax.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());

//		IRI asObjectId = iri(actorMax.getSubject().toString() + "/object");
		Literal nameLiteral = literal("Max Mustermann");		
		Model activityModel = new ModelBuilder()
				.subject(asActivityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), iri(asObjectId.toString()))
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(asObjectId.toString())
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteral)
					// If the object is public everybody can read it!
					.add(Values.iri(VocAs.to), Values.iri(VocAs.Public))					
					.add(Values.iri(VocAs.bto), iri("http://example.org/testUser1"))
					.add(Values.iri(VocAs.bcc), iri(actorSomeUser.getSubject().toString()))
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();
				
		//Send activity
		StringWriter sw = new StringWriter();
		Rio.write(activityModel, sw, RDFFormat.JSONLD);
		String activityLocation = pubClientMax.sendToOutbox(actorMax, sw.toString());
		assertNotNull(activityLocation);

		// read initial created activity & object
		Activity activity = pubClientForeign.readActivity(activityLocation).orElseThrow();
		assertEquals(nameLiteral.stringValue(), activity.getName().orElse(null));
		
		assertEquals(1, activity.getObject().size());
//		activity.getObject()
		String url = activity.getObject().stream().findFirst().orElseThrow().asResource().orElseThrow().getExternalSubject().getIRIString();
		ActivityPubObject reRead = pubClientForeign.readObject(url).orElseThrow();
		
//		ActivityPubObject reRead = activity.resolveObject().stream().findFirst().orElseThrow();
		
		Optional<String> name = reRead.getName();
		assertEquals(nameLiteral.stringValue(), name.orElse(null));
		assertTrue(reRead.getBto().isEmpty());
		assertTrue(reRead.getBcc().isEmpty());
	}
	
	@Test
	void testReadPublicActivityAsAnonymous() throws Exception {
		IRI asObjectId = Values.iri(activityPubObjectIdBuilder.createResource(actorMax.getSubject()).getExternalSubject().getIRIString());
		IRI asActivityId = Values.iri(activityPubObjectIdBuilder.createActivity(actorMax.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());
		Literal nameLiteral = literal("Max Mustermann");
		Model activityModel = new ModelBuilder()
				.subject(asActivityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), iri(asObjectId.toString()))
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(asObjectId.toString())
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteral)
					// If the object is public everybody can read is!
					.add(Values.iri(VocAs.to), Values.iri(VocAs.Public))					
					.add(Values.iri(VocAs.bto), iri("http://example.org/testUser1"))
					.add(Values.iri(VocAs.bcc), iri("http://example.org/testUser2"))
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();
				
		//Send activity		
		StringWriter sw = new StringWriter();
		Rio.write(activityModel, sw, RDFFormat.TURTLE);
		
		String url = actorMax.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(url)
				.with(maxJwt)
				.content(sw.toString())
			    .contentType("text/turtle")
				.header(HttpHeaders.ACCEPT, "text/turtle"))				
				.andExpect(status().isCreated())
				.andReturn();

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);	
		
		mvcResult = testHelperV2.getMockMvc().perform(get(activityLocation)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();
		Activity activity = testHelperV2.toActivity(activityLocation, mvcResult).orElseThrow();		
		assertEquals(nameLiteral.stringValue(), activity.getName().orElse(null));
				
		// read object
		String uri = activity.getObject().stream().findFirst().orElseThrow().asRdfPubObjectId().orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(uri)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();
		
		ActivityPubObject reRead = testHelperV2.toActivityPubObject(uri, mvcResult).orElseThrow();
		assertEquals(nameLiteral.stringValue(), reRead.getName().orElse(null));
	}
}
