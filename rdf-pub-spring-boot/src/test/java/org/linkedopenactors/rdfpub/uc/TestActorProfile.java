package org.linkedopenactors.rdfpub.uc;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.net.URI;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.client2024.HttpAdapter;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.client2024.RdfPubClientDefault;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocLDP;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import com.jayway.jsonpath.JsonPath;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestActorProfile")
class TestActorProfile {

	@Value("${app.rdfRepositoryHome}")
	private String rdfRepositoryHome;

	private String maxUserId;
	private String maxPreferredUsername;
	private JwtRequestPostProcessor maxJwt;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private VocabContainer vocabContainer; 
	
	@Autowired
	private TestHelperV2 testHelperV2;

	private Actor actorMax;

	private RdfPubClient rdfPubClientMax;

	private URI issuer;
	
	@BeforeEach
	public void setup() throws Exception {
		vocabContainer = new VocabContainerDefault(new RDF4J());
		issuer = URI.create("http://example.com");
		maxUserId =  UUID.randomUUID().toString();// "0816_" + RandomStringUtils.random(10, true, false);
		maxPreferredUsername = "max_" + RandomStringUtils.random(10, true, false);
		
		actorMax = testHelperV2.createActor(maxUserId, maxPreferredUsername, issuer.toString());
		maxJwt = testHelperV2.jwt(maxUserId, maxPreferredUsername, issuer.toString());
		HttpAdapter httpAdapter = testHelperV2.getHttpAdapter2024(maxUserId, maxPreferredUsername);
		String serverBaseUrlParam = "http://example.org/";
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");
		
		ActivityPubObjectIdBuilder activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);

		rdfPubClientMax = new RdfPubClientDefault(httpAdapter, activityPubObjectFactory, serverBaseUrlParam, activityPubObjectIdBuilder);
	}

	@Test
	void testReadActorsProfile() throws Exception {
		
		String userId = UUID.randomUUID().toString();
		String name = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, name, issuer.toString());
		
		String actorIdAsString = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		Actor actor2 = rdfPubClientMax.readActor(actorIdAsString).orElseThrow();
		
		Set<RdfPubIRI> types = actor2.getTypes();
		assertTrue(types.stream()
				.map(Object::toString)
				.anyMatch(w->vocabContainer.vocAs().Person().getIRIString().equals(w)));

		RdfPubBlankNodeOrIRI inbox = actor2.getInbox();
		String inboxString = inbox.asCollection().orElseThrow().getExternalBaseSubject().getIRIString();
		String inboxExpected = actorIdAsString + "/col/inbox";
		assertEquals(inboxExpected, inboxString);
		
		String outbox = actor2.getOutbox().asCollection().orElseThrow().getExternalBaseSubject().getIRIString();
		String outboxExpected = actorIdAsString + "/col/outbox";
		assertEquals(outboxExpected, outbox);
		
		assertEquals(name, actor.getName().orElseThrow().toString());
	}

	@Test
	void testReadActorsProfileJsonLd() throws Exception {
		String url = actorMax.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		Actor createActor = rdfPubClientMax.readActor(url).orElseThrow();

		Set<RdfPubIRI> types = createActor.getTypes();
		assertTrue(types.contains(vocabContainer.vocAs().Person()));
//		assertEquals(maxUserId + "@" + issuer.getRawAuthority(), createActor.getOauth2IssuerUserId().orElseThrow());
		assertEquals(maxPreferredUsername, createActor.getName().orElseThrow());
//		assertEquals(1L, createActor.getVersion());http://localhost:8081/0b5e1089-f814-44d5-a8d6-6a0fb5bede26
		assertTrue(createActor.getPublished().isPresent());
		assertEquals(Instant.class, createActor.getPublished().get().getClass());
		String inbox = createActor.getInbox().asCollection().orElseThrow().getExternalBaseSubject().getIRIString();
		assertTrue(inbox.startsWith("http://localhost:8081/"));
		assertTrue(createActor.getInbox().toString().endsWith("/inbox"));

//		assertTrue(createActor.getInboxSparqlEndpoint().toString().startsWith("http://localhost:8081/"));
//		assertTrue(createActor.getInboxSparqlEndpoint().toString().endsWith("/inbox/sparql"));
	}

	@Test
	void testReadActorsProfilePlainJsonLd() throws Exception {
		String url = actorMax.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		String createActor = rdfPubClientMax.readPlain(url).orElseThrow();
		System.out.println(createActor);
	}
	/**
	 * Profiles are public, so anybody should be able to read profiles of other actors.
	 * @throws Exception
	 */
	@Test	
	void testReadActorsProfileOfForeignActor() throws Exception {
		
		String userId = UUID.randomUUID().toString();
		String name = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, name, issuer.toString());
		
		String actorIdAsString = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		IRI actorId = Values.iri(actorIdAsString);

		// read as another 'foreign' actor
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(get(actorIdAsString)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		
		String contentAsString = mvcResult.getResponse().getContentAsString();
		StringReader sr = new StringReader(contentAsString);
		Model m = Rio.parse(sr, RDFFormat.TURTLE);
		
		Set<IRI> propertyIRIs = Models.getPropertyIRIs(m, actorId, RDF.TYPE);
		long x = propertyIRIs.stream().map(IRI::toString).filter(i->i.equals(vocabContainer.vocAs().Person())).count();
		assertTrue(x==0);
		IRI inbox = iri(actorId.stringValue() + "/col/inbox");
		
		m.forEach(t->System.out.println(t.getSubject() + " - " + t.getPredicate() + " - " + t.getObject()));
		System.out.println("actorid: " + actorId );
		
		IRI inboxIri = Values.iri(VocLDP.inbox);
		System.out.println("inboxIri: " + inboxIri );
		Optional<IRI> propertyIRI = Models.getPropertyIRI(m, actorId, inboxIri);
		
		
		assertEquals(Optional.of(inbox), propertyIRI);
		assertEquals(Optional.of(iri(actorId.stringValue() + "/col/outbox")), Models.getPropertyIRI(m, actorId, Values.iri(VocAs.outbox)));
		assertEquals(Optional.of(literal(name)), Models.getPropertyLiteral(m, actorId, Values.iri(VocAs.name)));
		
		// read as anonymous user
		mvcResult = testHelperV2.getMockMvc().perform(get(actorId.stringValue())
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		
		sr = new StringReader(mvcResult.getResponse().getContentAsString());
		m = Rio.parse(sr, RDFFormat.TURTLE);
		
		assertTrue(Models.getPropertyIRIs(m, actorId, RDF.TYPE).contains(Values.iri(VocAs.Person)));
		assertEquals(Optional.of(iri(actorId.stringValue() + "/col/inbox")), Models.getPropertyIRI(m, actorId, Values.iri(VocAs.inbox)));
		assertEquals(Optional.of(iri(actorId.stringValue() + "/col/outbox")), Models.getPropertyIRI(m, actorId, Values.iri(VocAs.outbox)));
		assertEquals(Optional.of(literal(name)), Models.getPropertyLiteral(m, actorId, Values.iri(VocAs.name)));
	}

	@Test	
	/**
	 * Parsing a sample of the activityPub Spec, to be sure, that we have an idea of the jsanpaths.
	 * @throws Exception
	 */
	void parse() throws Exception {
		String expected = vocabContainer.vocAs().Person().getIRIString().replace(VocAs.NS, "");
		assertEquals(expected, JsonPath.read(getActivityPubSample9Actor(), "$.type"));
		assertEquals("https://kenzoishii.example.com/", JsonPath.read(getActivityPubSample9Actor(), "$.id"));
		assertEquals("https://kenzoishii.example.com/following.json", JsonPath.read(getActivityPubSample9Actor(), "$.following"));
		assertEquals("https://kenzoishii.example.com/followers.json", JsonPath.read(getActivityPubSample9Actor(), "$.followers"));
		assertEquals("https://kenzoishii.example.com/liked.json", JsonPath.read(getActivityPubSample9Actor(), "$.liked"));
		assertEquals("https://kenzoishii.example.com/inbox.json", JsonPath.read(getActivityPubSample9Actor(), "$.inbox"));
		assertEquals("https://kenzoishii.example.com/feed.json", JsonPath.read(getActivityPubSample9Actor(), "$.outbox"));
		assertEquals("kenzoishii", JsonPath.read(getActivityPubSample9Actor(), "$.preferredUsername"));
		assertEquals("石井健蔵", JsonPath.read(getActivityPubSample9Actor(), "$.name"));
	}

	private String getActivityPubSample9Actor() {
		return "{\n"
				+ "  \"@context\": [\"https://www.w3.org/ns/activitystreams\",\n"
				+ "               {\"@language\": \"ja\"}],\n"
				+ "  \"type\": \"Person\",\n"
				+ "  \"id\": \"https://kenzoishii.example.com/\",\n"
				+ "  \"following\": \"https://kenzoishii.example.com/following.json\",\n"
				+ "  \"followers\": \"https://kenzoishii.example.com/followers.json\",\n"
				+ "  \"liked\": \"https://kenzoishii.example.com/liked.json\",\n"
				+ "  \"inbox\": \"https://kenzoishii.example.com/inbox.json\",\n"
				+ "  \"outbox\": \"https://kenzoishii.example.com/feed.json\",\n"
				+ "  \"preferredUsername\": \"kenzoishii\",\n"
				+ "  \"name\": \"石井健蔵\",\n"
				+ "  \"summary\": \"この方はただの例です\",\n"
				+ "  \"icon\": [\n"
				+ "    \"https://kenzoishii.example.com/image/165987aklre4\"\n"
				+ "  ]\n"
				+ "}";
	}
}
