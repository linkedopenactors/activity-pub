package org.linkedopenactors.rdfpub.uc;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.RdfPubClient4TestFactory;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestReadCollectionPublic")
public class TestReadCollectionPublic {

	@Autowired
	private RdfPubClient4TestFactory rdfPubClient4TestFactory;

	@Autowired
	private TestHelperV2 testHelperV2;

	@Value("${instance.domain}")
	private String domain;

	@BeforeEach
	public void setup() throws Exception {
	}
	
	@Test
	public void testAlmostEmpty() {
		RdfPubClient rdfPubClient = rdfPubClient4TestFactory.getRdfPubClient4Anonymous();
		Set<Activity> publicCollectionItems = rdfPubClient.readPublicActivityCollection();
		
		// create instance actor activity & instance actor is already there 
		assertEquals(1, publicCollectionItems.size());
	}
	
	@Test
	public void testWithActor() throws Exception {
		
		RdfPubClient rdfPubClient = rdfPubClient4TestFactory.getRdfPubClient4Anonymous();
		Set<Activity> initialPublicCollectionItems = rdfPubClient.readPublicActivityCollection();
		
		String maxUserId = UUID.randomUUID().toString();
		String maxPreferredUsername = RandomStringUtils.random(10, true, false);

		testHelperV2.createActor(maxUserId, maxPreferredUsername, getInstanceDomainString());
				
		Set<Activity> publicCollectionItems = rdfPubClient.readPublicActivityCollection();
		assertEquals(initialPublicCollectionItems.size()+1, publicCollectionItems.size());
	}

	private String getInstanceDomainString() {
		return domain.endsWith("/") ? domain.substring(0, domain.length()-1) : domain;
	}
}
