package org.linkedopenactors.rdfpub.uc;


import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.RdfPubClient4TestFactory;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdResource;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestSendCreateActivityAndUpdateIt")
class TestSendCreateActivityAndUpdateIt {

	@Autowired
	private VocabContainer vocabContainer;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private TestHelperV2 testHelperV2;

	@Autowired
	private RdfPubClient4TestFactory rdfPubClient4TestFactory;
	
	private String maxUserId;
	private String maxPreferredUsername;

	@Value("${instance.domain}")
	private String domain;

	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	@BeforeEach
	public void setup() throws Exception {
		maxUserId = UUID.randomUUID().toString();
		maxPreferredUsername = RandomStringUtils.random(10, true, false);

		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");
	
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);
	}

	
	@Test
	void testWithNewMethods() throws Exception {

		Actor actor = testHelperV2.createActor(maxUserId, maxPreferredUsername, getInstanceDomainString());
		JwtRequestPostProcessor maxJwt = testHelperV2.jwt(maxUserId, maxPreferredUsername, getInstanceDomainString());
		
		String url = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(get(url)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		String contentAsString = mvcResult.getResponse().getContentAsString();
		Actor actor2 = activityPubObjectFactory
				.create(
						actor.getSubject()
						.toString(), 
						contentAsString)
				.asConvertable()
				.asActor();
		
		IRI objectId = Values.iri(activityPubObjectIdBuilder.createResource(actor.getSubject()).getExternalSubject().getIRIString());
		IRI activityId = Values.iri(activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());

		String objectIdentifier = UUID.randomUUID().toString();
		Literal nameLiteral = literal("Max Mustermann");
				
		Model initialActivityModel = new ModelBuilder()
				.subject(activityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), objectId)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(objectId)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), objectIdentifier)
				.build();

		String inboxUurl = actor2.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		String locationOfInitialActivity = send(toJsonLdString(initialActivityModel), maxJwt, inboxUurl);
		
		mvcResult = testHelperV2.getMockMvc().perform(get(locationOfInitialActivity)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		contentAsString = mvcResult.getResponse().getContentAsString();
		Activity initialActivity = activityPubObjectFactory.create(contentAsString).asConvertable().asActivity();

		// read initial object
		String urlOfObject = initialActivity.getObject().stream().findFirst().orElseThrow().asResource().orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(urlOfObject)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		ActivityPubObject initialObject = activityPubObjectFactory.create(mvcResult.getResponse().getContentAsString());

		Optional<RdfPubBlankNodeOrIRI> wasAttributedTo = initialObject.getWasAttributedTo();
		assertTrue(wasAttributedTo.isPresent());
		Optional<RdfPubObjectIdActor> wasAttributedToActor =  wasAttributedTo.orElseThrow().asActor();
		String initialObjectWasAttributedTo = wasAttributedToActor.orElseThrow().getExternalSubject().getIRIString();
		String actorIri = actor2.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		assertTrue(initialObjectWasAttributedTo.startsWith(actorIri));
		
		RdfPubBlankNodeOrIRI wasGeneratedByUrl = initialObject.getWasGeneratedBy().orElseThrow().asRdfPubObjectId().orElseThrow().getExternalSubject();
		assertEquals(initialActivity.getSubject().asRdfPubObjectId().orElseThrow().getExternalSubject(), wasGeneratedByUrl);
		
				
		///////////////////		
		// Create Update Activity
		///////////////////
		IRI objectToChangeSubject = Values.iri(initialObject.getSubject().toString());
		IRI updateActivityId = Values.iri(activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());
		Literal nameLiteralUpdated = literal("Max Updated");
		Model updateActivityModel = new ModelBuilder()
				.subject(updateActivityId)
					.add(RDF.TYPE, Values.iri(VocAs.Update))
					.add(Values.iri(VocAs.object), objectToChangeSubject)
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
				.subject(objectToChangeSubject)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteralUpdated)
					.add(Values.iri(VocSchema.identifier), objectIdentifier)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					// ^ we update an existing version !
				.build();
			

		String outboxUrl = actor2.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		String locationOfUpdatedActivity = send(toJsonLdString(updateActivityModel), maxJwt, outboxUrl);

		mvcResult = testHelperV2.getMockMvc().perform(get(locationOfUpdatedActivity)
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		contentAsString = mvcResult.getResponse().getContentAsString();
		Activity updatedActivity = activityPubObjectFactory.create(contentAsString).asConvertable().asActivity();

		org.apache.commons.rdf.api.IRI updatedObjIri = updatedActivity.getObject().stream().findFirst().orElseThrow().asRdfPubObjectId().orElseThrow().getExternalSubject();
		
		mvcResult = testHelperV2.getMockMvc().perform(get(updatedObjIri.toString())
				.with(maxJwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		contentAsString = mvcResult.getResponse().getContentAsString();
		ActivityPubObject updatedOriginalObject = activityPubObjectFactory.create(contentAsString);

		assertTrue(updatedOriginalObject.getWasAttributedTo().orElseThrow().getIRIString().startsWith(actor2.getSubject().getIRIString()));
		RdfPubBlankNodeOrIRI iri = updatedOriginalObject.getWasGeneratedBy().orElseThrow().asRdfPubObjectId().orElseThrow().getExternalSubject();
		assertEquals(iri, updatedActivity.getSubject().asRdfPubObjectId().orElseThrow().getExternalSubject());
//		assertEquals(initialObject.getSameAs().orElse(null), updatedOriginalObject.getWasRevisionOf().orElse(null));
		// ^^ TODO das muss nochmal klar getestet werden, oder haben wir das schon ?
		// Wie können wir das darstellen? cmdLine INterface das Revision Collections liest ?
	}
	
	private String toJsonLdString(Model model) {
		StringWriter sw = new StringWriter();
		Rio.write(model, sw, RDFFormat.JSONLD);
		return sw.toString();
	}

	
	
	@Test
	void testAlreadyExists() throws Exception {
		
		String userId = UUID.randomUUID().toString();
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		RdfPubClient rdfPubClient = rdfPubClient4TestFactory.getRdfPubClient(userId, preferredUsername);
		
		RdfPubBlankNodeOrIRI objectIdRdfPubIRI = activityPubObjectIdBuilder.createResource(actor.getSubject()).getExternalSubject();
		RdfPubObjectIdActivity activityIdRdfPubIRI = activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow());

		String name = "Max Mustermann";
		
		ActivityPubObject obj = activityPubObjectFactory.create(objectIdRdfPubIRI);
		obj.addType(vocabContainer.vocAs().Object());
		obj.setName(name);
		obj.setSummary("That is the summary");
		
		Activity createActivity = activityPubObjectFactory.create(activityIdRdfPubIRI)
				.asConvertable()
				.asActivity();
		createActivity.addType(vocabContainer.vocAs().Create());
		createActivity.addObject(obj);
		createActivity.setSummary("That is the create activity");

		///////////////////
		// Initial save Activity & Object
		///////////////////
		RdfPubObjectIdActivity activityLocationRdfPubIRI = rdfPubClient.sendToOutbox(actor, createActivity);		
		String activityLocation = activityLocationRdfPubIRI.getExternalSubject().getIRIString();
		assertNotNull(activityLocation);
		
		// Read created Activity
		Activity reReadActivity = rdfPubClient.readActivity(activityLocationRdfPubIRI).orElseThrow();
		assertEquals(1, reReadActivity.getObject().size());
		RdfPubObjectIdResource objectRdfPubObjectId = reReadActivity.getObject().stream().findFirst().orElseThrow().asResource().orElseThrow();

		// Read created Object
		ActivityPubObject reReadObject = rdfPubClient.readObject(objectRdfPubObjectId).orElseThrow();

		///////////////////		
		// Create Update Activity
		///////////////////		
		String nameUpdated = "Max Updated";
		
		reReadObject.setName(nameUpdated);
		reReadObject.setSummary("That is the summary of the updated object");
		
		Activity updateActivity = activityPubObjectFactory.create(activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow()))
				.asConvertable()
				.asActivity();
		updateActivity.addType(vocabContainer.vocAs().Update());
		updateActivity.addObject(reReadObject);
		updateActivity.setSummary("That is the update activity");
		
		// POst update activity
		RdfPubObjectIdActivity updateActivityLocation = rdfPubClient.update(actor, updateActivity);
		activityLocation = updateActivityLocation.getExternalSubject().getIRIString();
		
		////////////
		// Reread
		////////////		
		assertNotNull(activityLocation);
		reReadActivity = rdfPubClient.readActivity(updateActivityLocation).orElseThrow();

		assertEquals(1, reReadActivity.getObject().size());
		objectRdfPubObjectId = reReadActivity.getObject().stream().findFirst().orElseThrow().asResource().orElseThrow();
	}

	@Test
	void test() throws Exception {
		String userId = UUID.randomUUID().toString();
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");
		IRI objectId = Values.iri(activityPubObjectIdBuilder.createResource(actor.getSubject()).getExternalSubject().getIRIString());
		IRI activityId = Values.iri(activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());
		
		String activityIdentifier = UUID.randomUUID().toString();
		String objectIdentifier = activityIdentifier.toString();
		Literal nameLiteral = literal("Max Mustermann");
		
		Model activity = new ModelBuilder()
				.subject(activityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), objectId)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), activityIdentifier)
				.subject(objectId)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocSchema.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), objectIdentifier)
				.build();
		
		
		StringWriter activitySw = new StringWriter();
		Rio.write(activity, activitySw, RDFFormat.JSONLD);
		
		// Save initial Activity & Object
		String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(url)
				.with(jwt)
				.content(activitySw.toString())
			    .contentType("application/ld+json")
				.header(HttpHeaders.ACCEPT, "application/ld+json"))				
				.andExpect(status().isCreated())
				.andReturn();

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);

		Model reReadActivity = read(activityLocation, jwt);
		
		assertEquals(Values.iri(VocAs.Create), Models.getPropertyIRI(reReadActivity, Values.iri(activityLocation), RDF.TYPE).orElseThrow());
		assertEquals(1L, Models.getPropertyLiteral(reReadActivity, Values.iri(activityLocation), Values.iri(VocSchema.version)).orElseThrow().longValue());
		assertEquals(activityIdentifier, Models.getPropertyLiteral(reReadActivity, Values.iri(activityLocation), Values.iri(VocSchema.identifier)).orElseThrow().stringValue());

		objectId = Models.getPropertyIRI(reReadActivity, Values.iri(activityLocation), Values.iri(VocAs.object)).orElseThrow();
		IRI initialObjectId = objectId;

		Model reReadObject = read(objectId, jwt);
		
		Set<String> types = Models.getPropertyIRIs(reReadObject, objectId, RDF.TYPE).stream()
				.map(IRI::stringValue)
				.collect(Collectors.toSet());

		assertTrue(types.contains(Values.iri(VocAs.Object).stringValue()));
		
		assertEquals(1L, Models.getPropertyLiteral(reReadObject, objectId, Values.iri(VocSchema.version)).orElseThrow().longValue());
		assertEquals(objectIdentifier, Models.getPropertyLiteral(reReadObject, objectId, Values.iri(VocSchema.identifier)).orElseThrow().stringValue());		
		assertEquals(nameLiteral, Models.getPropertyLiteral(reReadObject, objectId, Values.iri(VocSchema.name)).orElseThrow());
		assertEquals(1, Models.getPropertyIRIs(reReadObject, objectId, OWL.SAMEAS).size());
		assertTrue(Models.getPropertyIRI(reReadObject, objectId, OWL.SAMEAS).isPresent());
		
		String actorUrl = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		assertEquals(actorUrl, Models.getPropertyIRI(reReadObject, objectId, Values.iri(VocAs.attributedTo)).orElseThrow().stringValue());
		
		IRI sameAs = Models.getPropertyIRI(reReadObject, objectId, OWL.SAMEAS).orElseThrow();
		Model sameAsObject = read(sameAs, jwt);
		
		types = Models.getPropertyIRIs(sameAsObject, sameAs, RDF.TYPE).stream()
				.map(IRI::stringValue)
				.collect(Collectors.toSet());

		assertTrue(types.contains(Values.iri(VocAs.Object).stringValue()));
		
		assertEquals(1L, Models.getPropertyLiteral(sameAsObject, sameAs, Values.iri(VocSchema.version)).orElseThrow().longValue());
		assertEquals(objectIdentifier, Models.getPropertyLiteral(sameAsObject, sameAs, Values.iri(VocSchema.identifier)).orElseThrow().stringValue());		
		assertEquals(nameLiteral, Models.getPropertyLiteral(sameAsObject, sameAs, Values.iri(VocSchema.name)).orElseThrow());
		assertEquals(0, Models.getPropertyIRIs(sameAsObject, sameAs, OWL.SAMEAS).size());
		
		String attributedTo = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		assertEquals(attributedTo, Models.getPropertyIRI(sameAsObject, sameAs, Values.iri(VocAs.attributedTo)).orElseThrow().stringValue());
		
		IRI activityId2 = Values.iri(activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());
		
		Literal nameLiteralUpdated = literal("Max Updated");
		Model updateActivity = new ModelBuilder()
				.subject(activityId2)
					.add(RDF.TYPE, Values.iri(VocAs.Update))
					.add(Values.iri(VocAs.object), objectId)
					.add(Values.iri(VocSchema.identifier), activityIdentifier)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
				.subject(objectId)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocSchema.name), nameLiteralUpdated)
					.add(Values.iri(VocSchema.identifier), objectIdentifier)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(2L))
				.build();
		
		activitySw = new StringWriter();
		Rio.write(updateActivity, activitySw, RDFFormat.JSONLD);
		
		// Save update Activity & Object V2
		String urlOutbox = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(post(urlOutbox)
				.with(jwt)
				.content(activitySw.toString())
			    .contentType("application/ld+json")
				.header(HttpHeaders.ACCEPT, "application/ld+json"))				
				.andExpect(status().isCreated())
				.andReturn();

		String updateActivityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(updateActivityLocation);
		
		Model reReadUpdatedActivity = read(updateActivityLocation, jwt);

		Set<IRI> propertyIRIs = Models.getPropertyIRIs(reReadUpdatedActivity, Values.iri(updateActivityLocation), RDF.TYPE);
		assertTrue(propertyIRIs.contains(Values.iri(VocAs.Update)));
		assertEquals(1L, Models.getPropertyLiteral(reReadUpdatedActivity, Values.iri(updateActivityLocation), Values.iri(VocSchema.version)).orElseThrow().longValue());
		assertEquals(activityIdentifier, Models.getPropertyLiteral(reReadUpdatedActivity, Values.iri(updateActivityLocation), Values.iri(VocSchema.identifier)).orElseThrow().stringValue());

		IRI updatedObjectId = Models.getPropertyIRI(reReadUpdatedActivity, Values.iri(updateActivityLocation), Values.iri(VocAs.object)).orElseThrow();
		assertEquals(initialObjectId, updatedObjectId);
		Model updatedObject = read(updatedObjectId, jwt);
		
		assertTrue(Models.getPropertyIRIs(updatedObject, updatedObjectId, RDF.TYPE).contains(Values.iri(VocAs.Object)));
//		assertEquals(AS.Object, Models.getPropertyIRIs(updatedObject, updatedObjectId, RDF.TYPE).orElseThrow());
		assertEquals(1, Models.getPropertyLiterals(updatedObject, updatedObjectId, Values.iri(VocSchema.version)).size());
		assertEquals(2L, Models.getPropertyLiteral(updatedObject, updatedObjectId, Values.iri(VocSchema.version)).orElseThrow().longValue());
		assertEquals(objectIdentifier, Models.getPropertyLiteral(updatedObject, updatedObjectId, Values.iri(VocSchema.identifier)).orElseThrow().stringValue());
		assertEquals(nameLiteralUpdated, Models.getPropertyLiteral(updatedObject, updatedObjectId, Values.iri(VocSchema.name)).orElseThrow());
		assertTrue(Models.getPropertyIRI(updatedObject, updatedObjectId, OWL.SAMEAS).isPresent());
		
		String urlAttributedTo = actor.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		assertEquals(urlAttributedTo, Models.getPropertyIRI(updatedObject, updatedObjectId, Values.iri(VocAs.attributedTo)).orElseThrow().stringValue());
		
		sameAs = Models.getPropertyIRI(updatedObject, updatedObjectId, OWL.SAMEAS).orElseThrow();
		sameAsObject = read(sameAs, jwt);
		
		assertTrue(Models.getPropertyIRIs(sameAsObject, sameAs, RDF.TYPE).contains(Values.iri(VocAs.Object)));
		
		assertEquals(1, Models.getPropertyLiterals(sameAsObject, sameAs, Values.iri(VocSchema.version)).size());
		assertEquals(2L, Models.getPropertyLiteral(sameAsObject, sameAs, Values.iri(VocSchema.version)).orElseThrow().longValue());
		assertEquals(objectIdentifier, Models.getPropertyLiteral(sameAsObject, sameAs, Values.iri(VocSchema.identifier)).orElseThrow().stringValue());
		assertEquals(nameLiteralUpdated, Models.getPropertyLiteral(sameAsObject, sameAs, Values.iri(VocSchema.name)).orElseThrow());
		assertTrue(Models.getPropertyIRI(sameAsObject, sameAs, OWL.SAMEAS).isEmpty());
		assertEquals(urlAttributedTo, Models.getPropertyIRI(sameAsObject, sameAs, Values.iri(VocAs.attributedTo)).orElseThrow().stringValue());
	}
	
	private Model read(String subject, JwtRequestPostProcessor jwt) throws Exception {
		return read(Values.iri(subject), jwt);
	}
	
	private Model read(IRI subject, JwtRequestPostProcessor jwt) throws Exception {
		MvcResult mvcResult =  testHelperV2.getMockMvc().perform(get(subject.stringValue())
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
				.andReturn();

		
//		MvcResult mvcResult = testHelperV2.getMockMvc().perform(get(maxActorId.stringValue())
//				.header(HttpHeaders.ACCEPT, "application/ld+json"))
//				.andDo(print())
//				.andExpect(status().isOk())
//				.andReturn();
		StringReader sr = new StringReader(mvcResult.getResponse().getContentAsString());
		return Rio.parse(sr,  RDFFormat.JSONLD);
	}
	
	private String send(String body, JwtRequestPostProcessor jwt, String uri) throws Exception {
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(uri)
				.with(jwt)
				.content(body)
			    .contentType("application/ld+json")
				.header(HttpHeaders.ACCEPT, "application/ld+json"))				
				.andExpect(status().isCreated())
				.andReturn();		
		
		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		return activityLocation;
	}

	private String getInstanceDomainString() {
		return domain.endsWith("/") ? domain.substring(0, domain.length()-1) : domain;
	}
}
