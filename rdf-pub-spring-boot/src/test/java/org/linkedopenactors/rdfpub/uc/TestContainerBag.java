package org.linkedopenactors.rdfpub.uc;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFContainers;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * This test is not testing rdf-pub functionality, but it's showing how to save/read Lists. 
 */
@SpringBootTest(classes = TestingWebApplication.class)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestInboxDelivery")
@ContextConfiguration
@WebAppConfiguration
@Slf4j
class TestContainerBag {

	@Autowired
	private TestHelperV2 testHelperV2;
	
	private MockMvc mockMvc;
	
	@BeforeEach
	public void setup() {
		this.mockMvc = testHelperV2.getMockMvc();
	}

	@Test
	void test() throws Exception {
		IRI containerUrl = iri("http://example.com");
		
		// Create a List of IRIs
		IRI iri1 = iri("http://example.com/test1");
		IRI iri2 = iri("http://example.com/test2");
		IRI iri3 = iri("http://example.com/test3");
		List<IRI> iris = Arrays.asList(new IRI[] { iri1, iri2, iri3 });
		
		String userId = UUID.randomUUID().toString();
		String userName = "max_" + RandomStringUtils.random(10, true, false);
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, userName, "http://example.com");
		
		// Call Actors Profile as authenticated user, so the profile will be created!
		Actor actor = testHelperV2.createActor(userId, userName, "http://example.com");
		
		// Create an RDF List of the IRIs
		Model sampleSequenceModel = RDFContainers.toRDF(RDF.SEQ, iris, containerUrl, new TreeModel());
		sampleSequenceModel.add(containerUrl, RDF.TYPE, Values.iri(VocAs.Object));
		sampleSequenceModel.add(containerUrl, Values.iri(VocSchema.version), literal(BigInteger.valueOf(1L)));
		sampleSequenceModel.add(containerUrl, Values.iri(VocSchema.identifier), literal(UUID.randomUUID()));
		
		// Send the RDF List as activiy respectively object
		
		StringWriter sw = new StringWriter();
		Rio.write(sampleSequenceModel, sw, RDFFormat.TURTLE);
		String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = this.mockMvc.perform(post(url)
				.with(jwt)
				.content(sw.toString())
			    .contentType("text/turtle")
				.header(HttpHeaders.ACCEPT, "text/turtle"))				
				.andExpect(status().isCreated())
				.andReturn();
		
		// ReRead the Activity
		String location = Optional.ofNullable(mvcResult.getResponse().getHeader(HttpHeaders.LOCATION))
				.orElseThrow();
		assertTrue(StringUtils.hasText(location));
		log.debug("location: " + Optional.ofNullable(location).orElseThrow());
		mvcResult = this.mockMvc.perform(get(location)
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())				
				.andReturn();

		// Extract the object ID of the activity
		StringReader sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model activity = Rio.parse(sr, RDFFormat.TURTLE);
		IRI objectIRI = Models.getPropertyIRI(activity, iri(location), Values.iri(VocAs.object)).orElseThrow();
		
		// Read the object, which should be our RDF List
		mvcResult = this.mockMvc.perform(get(objectIRI.toString())
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
//				.andDo(print())
				.andExpect(status().isOk())				
				.andReturn();

		// Convert the reReded object into a List  
		sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model object = Rio.parse(sr, RDFFormat.TURTLE);
		
		List<Value> newList = RDFContainers.toValues(RDF.BAG, object, objectIRI, new ArrayList<>());
		
		// compare the List, that was saved as an activity respectively object and readed from rdf-pub with the initial IRIs.  
		assertEquals(iris.size(), newList.size());		
		assertTrue(newList.contains(iri1));
		assertTrue(newList.contains(iri2));
		assertTrue(newList.contains(iri3));
	}
}
