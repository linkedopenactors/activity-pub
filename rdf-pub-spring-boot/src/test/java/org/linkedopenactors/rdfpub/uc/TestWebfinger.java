package org.linkedopenactors.rdfpub.uc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

import org.hamcrest.core.StringStartsWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestWebfinger")
class TestWebfinger {

	@Autowired
	private TestHelperV2 testHelperV2;
	
	private String maxUserId;
	private String maxPreferredUsername;

	@Value("${instance.domain}")
	private String domain;

	private String serverPart;

	@BeforeEach
	public void setup() throws Exception {
		maxUserId = UUID.randomUUID().toString();
		maxPreferredUsername = "max_" + UUID.randomUUID().toString();
		int port = getInstanceDomain().getPort();
		serverPart = getInstanceDomain().getHost();
		if(port != -1) {
			serverPart = serverPart + ":" + port;
		}
		testHelperV2.createActor(maxUserId, maxPreferredUsername, domain);
	}
	
	@Test
	void testWebfingerFullQualified() throws Exception {
		testHelperV2.getMockMvc().perform(get("/.well-known/webfinger?resource=acct:" + maxPreferredUsername + "@" + serverPart))
//			.andDo(print())
			.andExpect(status().isOk()).andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.subject").value("acct:" + maxPreferredUsername + "@" + serverPart))
			.andExpect(MockMvcResultMatchers.jsonPath("$.links[0].href").value(new StringStartsWith(domain)));
	}

	@Test
	void testWebfingerPreferredUsername() throws Exception {
		testHelperV2.getMockMvc().perform(get("/.well-known/webfinger?resource=acct:" + maxPreferredUsername))
//			.andDo(print())
			.andExpect(status().isOk()).andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.subject").value("acct:" + maxPreferredUsername))
			.andExpect(MockMvcResultMatchers.jsonPath("$.links[0].href").value(new StringStartsWith(domain)));
	}

	@Test
	void testWebfingerMissingResourceParameter() throws Exception {
		testHelperV2.getMockMvc().perform(get("/.well-known/webfinger"))
//			.andDo(print())
			.andExpect(status().isBadRequest());
	}

	@Test
	void testWebfingerMalformedResourceParameterInsertingExtraEqualsCharacter() throws Exception {
		testHelperV2.getMockMvc().perform(get("/.well-known/webfinger?resource==acct:" + maxPreferredUsername + "@" + serverPart))
//			.andDo(print())
			.andExpect(status().isBadRequest());
	}

	@Test
	void testWebfingerWithoutAcct() throws Exception {
		testHelperV2.getMockMvc().perform(get("/.well-known/webfinger?resource=" + maxPreferredUsername + "@" + serverPart))
//			.andDo(print())
			.andExpect(status().isBadRequest());
	}

	private String getInstanceDomainString() {
		return domain.endsWith("/") ? domain.substring(0, domain.length()-1) : domain;
	}

	private URL getInstanceDomain() {
		try {
			return new URI(getInstanceDomainString()).toURL();
		} catch (MalformedURLException | URISyntaxException e) {
			throw new IllegalStateException(getInstanceDomainString() + " is not a valid URL.", e);
		}
	}
}
