package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.RdfPubClient4TestFactory;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestReceiveFollowActivityInbox")
class TestReceiveActivity {

	@Value("${app.rdfRepositoryHome}")
	private String rdfRepositoryHome;

	@Autowired
	private VocabContainer vocabContainer; 

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private RdfPubClient4TestFactory rdfPubClient4TestFactory;

	@Autowired
	private TestHelperV2 testHelperV2;

	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private PublicActorStore publicActorStore;
	
	@BeforeEach
	public void setup() throws Exception {
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");

		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);
	}

	@Test
	void test() throws Exception {
		
		String actorThatWantsToFollowUserId = UUID.randomUUID().toString();
		String actorThatWantsToFollowPreferredUsername = "actorThatWantsToFollow_" + RandomStringUtils.random(10, true, false);
		Actor actorThatWantsToFollow = testHelperV2.createActor(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername, "http://example.com");
				
		RdfPubClient rdfPubClientOfActorThatWantsToFollow = rdfPubClient4TestFactory.getRdfPubClient(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername);
		
		ActivityPubObject note = activityPubObjectFactory.create(activityPubObjectIdBuilder.createResource(actorThatWantsToFollow.getSubject()));
		note.addType(vocabContainer.vocAs().Note());
		note.addTo(vocabContainer.vocAs().Public());
		note.setContent("That is a test note!");
		
		// Send to Outbox
		RdfPubBlankNodeOrIRI activityLocation = rdfPubClientOfActorThatWantsToFollow.sendToOutbox(actorThatWantsToFollow, note);
		
		Set<Activity> outbox = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getOutbox().asCollection().orElseThrow());
		assertTrue(contains(outbox, activityLocation));
		
		OrderedCollectionPage publicCol = publicActorStore.getPublicCollection(0, 100);
		Set<String> items = publicCol.getItems().stream().map(RdfPubBlankNodeOrIRI::getIRIString).collect(Collectors.toSet());
		assertTrue(items.contains(activityLocation.getIRIString()));
	}
	
	private boolean contains(Set<? extends ActivityPubObject> pendingFollowers, RdfPubBlankNodeOrIRI expectedItem) {
		return pendingFollowers.stream()
			.map(ActivityPubObject::getSubject)
			.map(RdfPubBlankNodeOrIRI::getIRIString)
			.filter(x->x.equals(expectedItem.getIRIString()))
			.collect(Collectors.toSet())
			.size() == 1;
	}
}
