package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.RdfPubClient4TestFactory;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestReceiveFollowActivityInbox")
class TestReceiveFollowActivity {

	@Value("${app.rdfRepositoryHome}")
	private String rdfRepositoryHome;

	@Autowired
	private VocabContainer vocabContainer; 

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private RdfPubClient4TestFactory rdfPubClient4TestFactory;

	@Autowired
	private TestHelperV2 testHelperV2;

	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@BeforeEach
	public void setup() throws Exception {
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");

		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);
	}

	@Test
	void testWithOneItemAndUndo() throws Exception {
		
		String actorToBeFollowedUserId = UUID.randomUUID().toString();
		String actorToBeFollowedPreferredUsername = "actorToBeFollowed_" + RandomStringUtils.random(10, true, false);
		Actor actorToBeFollowed = testHelperV2.createActor(actorToBeFollowedUserId, actorToBeFollowedPreferredUsername, "http://example.com");
		
		String actorThatWantsToFollowUserId = UUID.randomUUID().toString();
		String actorThatWantsToFollowPreferredUsername = "actorThatWantsToFollow_" + RandomStringUtils.random(10, true, false);
		Actor actorThatWantsToFollow = testHelperV2.createActor(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername, "http://example.com");
				
		RdfPubClient rdfPubClientOfActorThatWantsToFollow = rdfPubClient4TestFactory.getRdfPubClient(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername);
		RdfPubClient rdfPubClientOfActorToBeFollowed = rdfPubClient4TestFactory.getRdfPubClient(actorToBeFollowedUserId, actorToBeFollowedPreferredUsername);
		
		// Create Activity: actorThatWantsToFollow follow actorToBeFollowed
		Activity followActivity = activityPubObjectFactory.createActivity(activityPubObjectIdBuilder.createActivity(actorThatWantsToFollow.getSubject()));
		followActivity.addType(vocabContainer.vocAs().Follow());
		followActivity.addTo(actorToBeFollowed.getSubject().asActor().orElseThrow().getExternalSubject());
		followActivity.addObject(actorToBeFollowed.getSubject().asActor().orElseThrow().getExternalSubject());
		
		// Send to actorThatWantsToFollow's Outbox
		RdfPubBlankNodeOrIRI followActivityLocation = rdfPubClientOfActorThatWantsToFollow.sendToOutbox(actorThatWantsToFollow, followActivity);
		
		
		// ActorThatWantsToFollow Perspective
		// ----------------------------------
		// Check if the followActivity is in the actorThatWantsToFollow's outbox collection
		Set<Activity> outbox = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getOutbox().asCollection().orElseThrow());
		assertTrue(contains(outbox, followActivityLocation));

		// Check if the followActivity is in the actorThatWantsToFollow's pendingFollowing collection 
		Set<Activity> pendingFollowing = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getPendingFollowing().flatMap(RdfPubBlankNodeOrIRI::asCollection).orElseThrow());
		assertTrue(contains(pendingFollowing, followActivityLocation));
		

		// ActorToBeFollowed Perspective
		// ----------------------------------
		// Check if the followActivity is in the actorToBeFollowed's inbox collection
		
		// TODO macht es Sinn dem rdfPubClient den actor zu übergeben, damit er nicht jedesmal mitgegeben werden muss ? Ist das immer derselbe ??
		Set<Activity> inbox = rdfPubClientOfActorToBeFollowed.readActivityCollection(actorToBeFollowed.getInbox().asCollection().orElseThrow());
		assertTrue(contains(inbox, followActivityLocation));
		
		// Check if the followActivity is in the actorToBeFollowed's pendingFollowers collection
		Set<Activity> pendingFollowers = rdfPubClientOfActorToBeFollowed.readActivityCollection(actorToBeFollowed.getPendingFollowers().asCollection().orElseThrow());
		assertTrue(contains(pendingFollowers, followActivityLocation));
		
		//////////////
		// undo follow
		//////////////
		Activity undoFollowActivity = activityPubObjectFactory.createActivity(activityPubObjectIdBuilder.createActivity(actorThatWantsToFollow.getSubject()));
		undoFollowActivity.addType(vocabContainer.vocAs().Undo());
		undoFollowActivity.addTo(actorToBeFollowed.getSubject().asActor().orElseThrow().getExternalSubject());
		undoFollowActivity.addObject(followActivityLocation);

		RdfPubBlankNodeOrIRI undoFollowActivityLocation = rdfPubClientOfActorThatWantsToFollow.sendToOutbox(actorThatWantsToFollow, undoFollowActivity);

		// Check if the activity was removed from the pendingFollowers collection 
		pendingFollowing = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getPendingFollowing().flatMap(RdfPubBlankNodeOrIRI::asCollection).orElseThrow());
		assertFalse(contains(pendingFollowing, followActivityLocation));

		// Check if the undoFollowActivity is in the outbox collection
		outbox = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getOutbox().asCollection().orElseThrow());
		assertTrue(contains(outbox, undoFollowActivityLocation));

		// Check if the followActivity is still in the outbox collection
		outbox = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getOutbox().asCollection().orElseThrow());
		assertTrue(contains(outbox, followActivityLocation));

		// Check if the undoFollowActivity is in the inbox collection
		pendingFollowers = rdfPubClientOfActorToBeFollowed.readActivityCollection(actorToBeFollowed.getPendingFollowers().asCollection().orElseThrow());
		assertFalse(contains(pendingFollowers, followActivityLocation));
	}

	@Test
	void testWithForeignActor() throws Exception {
		String actorThatWantsToFollowUserId = UUID.randomUUID().toString();
		String actorThatWantsToFollowPreferredUsername = "actorThatWantsToFollow_" + RandomStringUtils.random(10, true, false);
		Actor actorThatWantsToFollow = testHelperV2.createActor(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername, "http://example.com");
				
		RdfPubClient rdfPubClientOfActorThatWantsToFollow = rdfPubClient4TestFactory.getRdfPubClient(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername);
		RdfPubBlankNodeOrIRI actorToBeFollowedIri = activityPubObjectIdBuilder.createFromUrl("https://example.org/mike");
		
		// Create Activity: actorThatWantsToFollow follow actorToBeFollowed
		Activity followActivity = activityPubObjectFactory.createActivity(activityPubObjectIdBuilder.createActivity(actorThatWantsToFollow.getSubject()));
		followActivity.addType(vocabContainer.vocAs().Follow());
		followActivity.addTo(actorToBeFollowedIri);
		followActivity.addObject(actorToBeFollowedIri);
		
		// Send to actorThatWantsToFollow's Outbox
		/*RdfPubIRI followActivityLocation =*/ rdfPubClientOfActorThatWantsToFollow.sendToOutbox(actorThatWantsToFollow, followActivity);	
	}

	@Test
	void testWithOneItemAndIgnore() throws Exception {
		String actorThatWantsToFollowUserId = UUID.randomUUID().toString();
		String actorThatWantsToFollowPreferredUsername = "actorThatWantsToFollow_" + RandomStringUtils.random(10, true, false);
		Actor actorThatWantsToFollow = testHelperV2.createActor(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername, "http://example.com");
				
		RdfPubClient rdfPubClientOfActorThatWantsToFollow = rdfPubClient4TestFactory.getRdfPubClient(actorThatWantsToFollowUserId, actorThatWantsToFollowPreferredUsername);
		RdfPubBlankNodeOrIRI actorToBeFollowedIri = activityPubObjectIdBuilder.createFromUrl("https://example.org/mike");
		
		// Create Activity: actorThatWantsToFollow follow actorToBeFollowed
		Activity followActivity = activityPubObjectFactory.createActivity(activityPubObjectIdBuilder.createActivity(actorThatWantsToFollow.getSubject()));
		followActivity.addType(vocabContainer.vocAs().Follow());
		followActivity.addTo(actorToBeFollowedIri);
		followActivity.addObject(actorToBeFollowedIri);
		
		// Send to actorThatWantsToFollow's Outbox
		RdfPubBlankNodeOrIRI followActivityLocation = rdfPubClientOfActorThatWantsToFollow.sendToOutbox(actorThatWantsToFollow, followActivity);
		
		// Create Activity: actorThatWantsToFollow IGNORE follow actorToBeFollowed
		Activity ignoreActivity = activityPubObjectFactory.createActivity(activityPubObjectIdBuilder.createActivity(actorThatWantsToFollow.getSubject()));
		ignoreActivity.addType(vocabContainer.vocAs().Ignore());
		ignoreActivity.addObject(followActivityLocation);
		
		/*RdfPubIRI ignoreActivityLocation =*/ rdfPubClientOfActorThatWantsToFollow.sendToOutbox(actorThatWantsToFollow, ignoreActivity);
	
		// Check if the activity was removed from the pendingFollowers collection 
		Set<Activity> pendingFollower = rdfPubClientOfActorThatWantsToFollow.readActivityCollection(actorThatWantsToFollow.getPendingFollowers().asCollection().orElseThrow());
		assertFalse(contains(pendingFollower, followActivityLocation));
	}
	
	@Disabled
	@Test
	void testWithOneItemAndAccept() throws Exception {

	}

	@Disabled
	@Test
	void testWithTwoItems() throws Exception {

	}

	private boolean contains(Set<? extends ActivityPubObject> pendingFollowers, RdfPubBlankNodeOrIRI expectedItem) {
		return pendingFollowers.stream()
			.map(ActivityPubObject::getSubject)
			.map(RdfPubBlankNodeOrIRI::getIRIString)
			.filter(x->x.equals(expectedItem.getIRIString()))
			.collect(Collectors.toSet())
			.size() == 1;
	}
}
