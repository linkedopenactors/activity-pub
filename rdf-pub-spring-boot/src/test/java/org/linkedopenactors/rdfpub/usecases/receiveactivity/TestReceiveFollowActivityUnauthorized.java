package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = {TestingWebApplication.class}, 
	properties = "app.rdfRepositoryHome=target/store/unittest/TestReceiveActivityServiceJsonLdInbox")
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"test", "signatureTest"})
public class TestReceiveFollowActivityUnauthorized {	
	
	@Autowired
	private TestHelperV2 testHelperV2;
	
	@Test
	public void testWithoutSignature() throws Exception {
		String userId = UUID.randomUUID().toString();
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");

		StringReader sr = new StringReader(sampleMessage(UUID.randomUUID().toString(), actor.getSubject().toString()));
		Model activity = Rio.parse(sr, RDFFormat.JSONLD);
		
		StringWriter sw = new StringWriter();
		Rio.write(activity, sw, RDFFormat.JSONLD);

		String url = actor.getInbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		testHelperV2.getMockMvc().perform(post(url)
				.with(jwt)
				.content(sw.toString())
			    .contentType("application/activity+json")
			    )
//				.andDo(print())
				.andExpect(status().isUnauthorized())
				.andReturn();
	}

	private String sampleMessage(String identifier, String actorId) {
		return "{\n"
				+ "	\"@context\": \"https://www.w3.org/ns/activitystreams\",\n"
				+ "	\"id\": \"https://example.com/d6f1e16a-bea8-4616-b139-da1f9d6afa1e\",\n"
				+ "	\"type\": \"Follow\",\n"
				+ "	\"actor\": \"https://example.com/users/badalus_vortinsol\",\n"
				+ "	\"object\": \"https://dev.rdf-pub.org/resource/acto28598aa3-6677-415a-9d65-047d48857f3f\"\n"
				+ "}";
	}
}
