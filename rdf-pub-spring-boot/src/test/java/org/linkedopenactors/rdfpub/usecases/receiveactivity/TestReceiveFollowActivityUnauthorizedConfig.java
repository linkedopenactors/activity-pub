package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import org.linkedopenactors.rdfpub.adapter.http.SignatureVerifierFactory;
import org.linkedopenactors.rdfpub.adapter.http.SignatureVerifierFactoryDefault;
import org.linkedopenactors.rdfpub.app.external.DiscoverExternalActor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({"signatureTest", "test"})
public class TestReceiveFollowActivityUnauthorizedConfig {
	
    @Bean
    public SignatureVerifierFactory receiveActivityVerifier(DiscoverExternalActor discoverExternalActor) {
        return new SignatureVerifierFactoryDefault(discoverExternalActor);
    }
}
