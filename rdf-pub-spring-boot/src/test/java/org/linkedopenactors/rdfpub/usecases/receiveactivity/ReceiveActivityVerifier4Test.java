package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import org.linkedopenactors.rdfpub.adapter.http.HttpHeaders;
import org.linkedopenactors.rdfpub.adapter.http.SignatureVerifierFactory;
import org.linkedopenactors.rdfpub.adapter.http.UnauthorizedException;
import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;
import org.mockito.Mockito;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"!signatureTest & test"})
public class ReceiveActivityVerifier4Test implements SignatureVerifierFactory {

	@Override
	public SignatureVerifier build(String body, HttpHeaders httpHeaders) throws UnauthorizedException, BadRequestException {
		SignatureVerifier fv = Mockito.mock(SignatureVerifier.class);
		return fv;
	}
}
