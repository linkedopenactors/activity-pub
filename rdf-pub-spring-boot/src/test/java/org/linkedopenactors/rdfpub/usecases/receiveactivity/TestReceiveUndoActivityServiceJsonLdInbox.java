package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(classes = {TestingWebApplication.class}, properties = "app.rdfRepositoryHome=target/store/unittest/TestReceiveActivityServiceJsonLdInbox")
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles("test")
@Disabled // is redundant to org.linkedopenactors.rdfpub.usecases.receiveactivity.TestReceiveFollowActivity, isn't it? and it's buggy.

public class TestReceiveUndoActivityServiceJsonLdInbox {	
	
	private static final String ACTIVITY_IRI = "https://mastodon.social/users/naturzukunft#follows/38967958/undo";
	
	@Autowired
	private TestHelperV2 testHelperV2;

	@Test
	public void testSuccess() throws Exception {

		String userId = Integer.toString(new Random().ints(1111, 9999).findFirst().getAsInt());
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");

		StringReader sr = new StringReader(sampleMessage(UUID.randomUUID().toString(), actor.getSubject().toString()));
		Model activity = Rio.parse(sr, RDFFormat.JSONLD);
		
		StringWriter sw = new StringWriter();
		Rio.write(activity, sw, RDFFormat.JSONLD);
		
		String url = actor.getInbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(url)
				.with(jwt)
				.content(sw.toString())
			    .contentType("application/activity+json")
			    )
//				.andDo(print())
				.andExpect(status().isCreated())
				.andReturn();

//		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
//		assertNotNull(activityLocation);
		
		// load collection
		String inboxCollectionUrl = actor.getInbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(inboxCollectionUrl)
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();
		String inboxCollectionAsString = mvcResult.getResponse().getContentAsString();
		Model inboxCollectionModel = Rio.parse(new StringReader(inboxCollectionAsString), RDFFormat.TURTLE);

		// extract the statements of the initial activity in the collection
		Model filteredInbox = inboxCollectionModel.filter(iri(ACTIVITY_IRI), null, null);		
		
		// Test, that the activity with type is available
		Set<IRI> propertyIRIs = Models.getPropertyIRIs(filteredInbox, iri(ACTIVITY_IRI), RDF.TYPE);
		assertTrue(propertyIRIs.contains(Values.iri(VocAs.Undo)), "propertyIRIs: " + propertyIRIs);

		// TODO check that undo is executed e.g. follower removed, etc!
	}

	private String sampleMessage(String identifier, String actorId) {
		return "{\n"
				+ "	\"@context\": \"https://www.w3.org/ns/activitystreams\",\n"
				+ "	\"id\": \"" + ACTIVITY_IRI + "\",\n"
				+ "	\"type\": \"Undo\",\n"
				+ "	\"actor\": \"https://mastodon.social/users/naturzukunft\",\n"
				+ "	\"object\": {\n"
				+ "		\"id\": \"https://mastodon.social/9ed4c9c3-4e22-44c1-a376-83513b56f556\",\n"
				+ "		\"type\": \"Follow\",\n"
				+ "		\"actor\": \"https://mastodon.social/users/naturzukunft\",\n"
				+ "		\"object\": \"https://dev.rdf-pub.org/dab207d0-436f-4ffa-a1a2-27a2c5fbeef2\"\n"
				+ "	}\n"
				+ "}";
	}
}
