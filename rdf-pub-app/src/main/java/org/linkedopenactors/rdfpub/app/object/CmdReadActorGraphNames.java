package org.linkedopenactors.rdfpub.app.object;

import org.linkedopenactors.rdfpub.domain.Actor;

import lombok.Data;

@Data
public class CmdReadActorGraphNames {
	private Actor actor;
}
