package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxReceiveActivityService;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUseCaseContext;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUsecaseProcessor;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Helper Class for {@link OutboxReceiveActivityService} to create the
 * OutboxUsecaseProcessor.
 */
@Component
public class UsecaseProcessorFactoryC2S {
	
	@Autowired
	private InternalDelivery internalDelivery;
	
	@Autowired
	private ExternalDelivery externalDelivery;
	
	@Autowired
	private PublicActorStore publicActorStore;;
	
	
	/**
	 * Creates an {@link OutboxUsecaseProcessor} that is able to process the passed {@link OutboxUseCaseContext}.
	 * @param ucCtx {@link OutboxUseCaseContext} to process.
	 * @return The useCase processor that is able to process the passed {@link OutboxUseCaseContext}
	 */
	public OutboxUsecaseProcessor create(OutboxUseCaseContext ucCtx) {
		switch (ucCtx.getUseCase()) {
		case c2s_createObject: {
			return new UcC2SCreateObject(ucCtx, internalDelivery, externalDelivery);
		}
//		case s2s_deleteRequest: {
//			return new UcS2SDelete();
//		}
		case c2s_followRequest: {
			return new UcC2SFollow(ucCtx, internalDelivery, externalDelivery);
		}
		case c2s_undoFollow: {
			return new UcC2SUndoFollow(ucCtx, internalDelivery, externalDelivery);
		}
		case c2s_updateObject: {
			return new UcC2SUpdateObject(ucCtx, internalDelivery, externalDelivery, publicActorStore);
		}
		case c2s_ignoreFollowRequest: {
			return new UcC2SIgnoreFollowRequest(ucCtx, internalDelivery, externalDelivery, publicActorStore);
		}
		case c2s_rejectFollowRequest: {
			return new UcC2SRejectFollowRequest(ucCtx, internalDelivery, externalDelivery, publicActorStore);
		}
		case c2s_accept_follow: {
			return new UcC2SAcceptFollow(ucCtx, internalDelivery, externalDelivery);
		}
		case c2s_removeFollower: {
			return new UcC2SRemoveFollower(ucCtx, internalDelivery, externalDelivery);
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + ucCtx.getUseCase());
		}
	}	
}
