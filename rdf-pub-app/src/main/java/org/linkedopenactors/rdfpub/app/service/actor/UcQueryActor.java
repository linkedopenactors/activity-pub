package org.linkedopenactors.rdfpub.app.service.actor;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;

/**
 * Covers different use cases to find actors.
 */
public interface UcQueryActor {

	/**
	 * Searches a actor by it's id. where the id is part of the users URL!
	 * @param query
	 * @return Actor found by the passed id.
	 */
	Optional<Actor> perform(QueryById query);

	/**
	 * Searches a actor by it's {@link RdfPubObjectId}.
	 * @param query
	 * @return Actor found by the passed id.
	 */
	Optional<Actor> perform(QueryByRdfPubObjectId query);
	
	/**
	 * Searches a user by an url. This is for HTTP Controllers.
	 * @param queryByUrl
	 * @return The actor as String. Format is depending from the 'content-type'.
	 */
	Optional<String> perform(QueryByUrl queryByUrl);
	
	/**
	 * Searches a actor by it's Oauth2IssuerUserId.
	 * @param query
	 * @return Actor found by the passed issuerUserI.
	 */
	Optional<Actor> perform(QueryByOauth2IssuerUserId query);

	/**
	 * Finds the current authenticated Actor.
	 * @param query
	 * @return current authenticated Actor.
	 */
	Actor perform(QueryByAuthentication query);

	/**
	 * Finds the user by it's preferred username.
	 * @param queryByPreferredUsername
	 * @return User, found for the passed preferred username.
	 */
	Optional<Actor> perform(QueryByPreferredUsername queryByPreferredUsername);	
}
