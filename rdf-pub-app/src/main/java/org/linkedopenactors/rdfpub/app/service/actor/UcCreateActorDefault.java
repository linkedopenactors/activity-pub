package org.linkedopenactors.rdfpub.app.service.actor;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.security.PublicKey;
import java.time.Instant;
import java.util.Base64;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Endpoints;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocLDP;
import org.linkedopenactors.rdfpub.domain.iri.VocPending;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
class UcCreateActorDefault implements UcCreatePerson, UcCreateInstanceApplication {

	@Autowired
	private PublicActorStore publicActorsStore;

	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder; 
	
	@Autowired
	private org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private KeyPairGenerator keyPairGenerator;

	@Autowired
	private VocabContainer vocabContainer;
	
	/**
	 * Creates an actor of type person.
	 * @param cmd
	 */
	public void perform(CmdCreatePerson cmd) {
		MDC.put("uc", cmd.getClass().getSimpleName());
		try {
			performInternal(cmd);
		} finally {
			MDC.remove("uc");
		}
	}
	
	private void performInternal(CmdCreatePerson cmd) {		
		RdfPubObjectId actorId = cmd.getActorId();
		String issuerUserId = cmd.getIssuerUserId();
		String issuerPreferredUsername = cmd.getIssuerPreferredUsername();
		URI issuer = cmd.getIssuer();
		
		// create Actor
		Actor actor = createActor(Set.of(vocabContainer.vocAs().Person()), actorId, issuerUserId, issuerPreferredUsername, issuer);
		
		// create / add Endpoints
		Endpoints endpoints = createEndpoints(actorId);
		actor.setEndpoints(endpoints);
		
		// create / add PublicKey
		org.linkedopenactors.rdfpub.domain.PublicKey publicKey = createPublicKey(actorId, actor.getSubject().asActor().orElseThrow().getIdentifier());
		actor.setPublicKey(publicKey);
					
		storeActor(actorId, actor, actor);
	}

	public void perform(CmdCreateInstance cmd) {
		MDC.put("uc", cmd.getClass().getSimpleName());
		try {
			performInternal(cmd);
		} finally {
			MDC.remove("uc");
		}
	}
	
	private void performInternal(CmdCreateInstance cmd) {
		RdfPubObjectId instanceActorSubject = activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME);
		String issuerPreferredUsername = Instance.INSTANCE_ACTOR_NAME;
		Actor actorObject = createActor(Set.of(vocabContainer.vocAs().Application()), instanceActorSubject, null, issuerPreferredUsername, null);
		Instance instance = actorObject.asConvertable().asInstance();
		
		// create / add PublicKey
		org.linkedopenactors.rdfpub.domain.PublicKey publicKey = createPublicKey(instanceActorSubject, instance.getSubject().asActor().orElseThrow().getIdentifier());
		instance.setPublicKey(publicKey);
		
		String publicCollection = VocAs.Public.replace(VocAs.NS, "");
		instance.setPublicCollection(activityPubObjectIdBuilder.createCollection(instanceActorSubject, publicCollection));
		
		storeActor(instanceActorSubject, instance);
	}
	
	private void storeActor(RdfPubIRI actorId, Actor actor, ActivityPubObject... objects) {
		// createSurroundingActivity
		Activity activity = createSurroundingActivity(actorId, actor, objects);
		activity.addObject(actor);

		// creates the actor by sending the create activity to the publicActorsStore		
		publicActorsStore.addToInbox(activity, "adding actor ("+actor.getPreferredUsername()+")");

		// Add the actorId to the collection of persons
		publicActorsStore.addToPersons(actorId);
		
		// Reads the saved actor for validating and returning
		readActor(actorId);
	}
	
	/**
	 * Reads the saved actor for validating.
	 * @param actorId
	 * @return the actor with the passed actorId.
	 */
	private Actor readActor(RdfPubBlankNodeOrIRI actorId) {
		RdfPubBlankNodeOrIRI bagActorId = actorId;
		
		Actor bagActor = publicActorsStore.findBySubject(bagActorId, 1)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor)
				.orElseThrow(()->new IllegalStateException("actorsStore.find("+bagActorId+", 1) store: " + publicActorsStore));
		
		log.debug("created Actor("+bagActorId+"): " + bagActor);
		if(!bagActor.getSubject().equals(bagActorId)) {
			throw new RuntimeException("bagActorId mismatch");
		}
		return bagActor;
	}
	
	/**
	 * Creates a surrounding create activity for the actor
	 * @param actorId
	 * @param actor
	 * @param objects
	 * @return The surrounding activity
	 */
	private Activity createSurroundingActivity(RdfPubBlankNodeOrIRI actorId, Actor actor, ActivityPubObject... objects) {
		RdfPubObjectId id = activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow());
		Activity activity = activityPubObjectFactory.create(id).asConvertable().asActivity();
		activity.setPublished(Instant.now());
		activity.setName("create activity for " + actorId);
		activity.addType(vocabContainer.vocAs().Create());		
		activity.addActor(actor.getSubject());
		
		for (int i = 0; i < objects.length; i++) {
			activity.addObject(objects[i]);
		}
		
		return activity;
	}

	/**
	 * Creates the actor.
	 * @param actorId
	 * @param issuerUserId
	 * @param issuerPreferredUsername
	 * @param issuer
	 * @return the created 'not yet stired' actor.
	 */
	private Actor createActor(Set<RdfPubIRI> types, RdfPubObjectId actorId, String issuerUserId, String issuerPreferredUsername,
			URI issuer) {
		Actor actor = activityPubObjectFactory.create(actorId).asConvertable().asActor();		
		actor.setType(types);
		
		String liked = VocAs.liked.replace(VocAs.NS, "");
		actor.setLiked(activityPubObjectIdBuilder.createCollection(actorId, liked));
		
		String inbox = VocLDP.inbox.replace(VocLDP.NS, "");
		actor.setInbox(activityPubObjectIdBuilder.createCollection(actorId, inbox));
		
		String outbox = VocAs.outbox.replace(VocAs.NS, "");
		actor.setOutbox(activityPubObjectIdBuilder.createCollection(actorId, outbox));
		
		String pendingFollowers = VocPending.pendingFollowers.replace(VocPending.NS, "");
		actor.setPendingFollowers(activityPubObjectIdBuilder.createCollection(actorId, pendingFollowers));

		String followers = VocAs.followers.replace(VocAs.NS, "");
		actor.setFollowers(activityPubObjectIdBuilder.createCollection(actorId, followers));

		String pendingFollowing = VocPending.pendingFollowing.replace(VocPending.NS, "");
		actor.setPendingFollowing(activityPubObjectIdBuilder.createCollection(actorId, pendingFollowing));
		
		String following = VocAs.following.replace(VocAs.NS, "");
		actor.setFollowing(activityPubObjectIdBuilder.createCollection(actorId, following));

		Optional.ofNullable(issuerUserId).ifPresent(val->actor.setOauth2IssuerUserId(val));
		Optional.ofNullable(issuer).ifPresent(val->actor.setOauth2Issuer(val.getRawAuthority()));
		String userName = Optional.ofNullable(issuerPreferredUsername).orElseThrow();
		actor.setPreferredUsername(userName);
		actor.setName(userName);
		actor.setPublished(Instant.now());
		return actor;
	}

	/**
	 * Creates the endpoints object for the actor.
	 * @param actorId
	 * @return The endpoints object.
	 */
	private Endpoints createEndpoints(RdfPubBlankNodeOrIRI actorId) {
		RdfPubBlankNodeOrIRI endpointsIri = activityPubObjectIdBuilder.createResource(actorId);
		Endpoints endpoints = activityPubObjectFactory.create(endpointsIri).asConvertable().asEndpoints();
		endpoints.setName("endpoints of " + actorId);
		endpoints.setOauthAuthorizationEndpoint(activityPubObjectIdBuilder.create("/oauth/oauthAuthorizationEndpoint")); // TODO hardcoded url !!!
		endpoints.setOauthTokenEndpoint(activityPubObjectIdBuilder.create("/oauth/oauthTokenEndpoint")); // TODO hardcoded url !!!
		endpoints.addType(vocabContainer.vocAs().Object());
		return endpoints;
	}
	
	/**
	 * Creates the publicKey for the actor.
	 * @param actorId
	 * @param actorIdentifier
	 * @return The publicKey for the actor.
	 */
	private org.linkedopenactors.rdfpub.domain.PublicKey createPublicKey(RdfPubBlankNodeOrIRI actorId, String actorIdentifier) {
		org.linkedopenactors.rdfpub.domain.PublicKey publicKey = activityPubObjectFactory.create(activityPubObjectIdBuilder.createResource(actorId)).asConvertable().asPublicKey();
		publicKey.setName("public key of " + actorId);
		publicKey.setPublicKeyPem(createPublicKeyString(actorIdentifier));
		publicKey.setOwner(actorId);
		publicKey.addType(vocabContainer.vocAs().Object());
		return publicKey;
	}

	/**
	 * Create / find the Base64.Encoded publicKey as String.
	 * @param actorIdentifier
	 * @return The Base64.Encoded publicKey as String.
	 */
	private String createPublicKeyString(String actorIdentifier) {
		try {
			PublicKey publicKey = keyPairGenerator.generate(actorIdentifier);
			Base64.Encoder encoder = Base64.getEncoder();
			StringWriter out = new StringWriter();
			out.write("-----BEGIN RSA PUBLIC KEY-----\n");
			out.write(encoder.encodeToString(publicKey.getEncoded()));
			out.write("\n-----END RSA PUBLIC KEY-----\n");
			out.close();
			return out.toString();
		} catch (IOException e) {
			throw new IllegalStateException("unable to create publicKey String", e);
		}
	}
}
