package org.linkedopenactors.rdfpub.app.object;

import java.util.Set;

public interface AdminService {

	Set<String> perform(CmdReadActorGraphNames c);
}
