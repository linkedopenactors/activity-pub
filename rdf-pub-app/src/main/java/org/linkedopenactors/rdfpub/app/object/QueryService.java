package org.linkedopenactors.rdfpub.app.object;

import java.util.Optional;

public interface QueryService {

	org.linkedopenactors.rdfpub.domain.OrderedCollectionPage perform(CmdReadActors c);
	// ^TODO move to adminService ??
	
	Optional<org.linkedopenactors.rdfpub.domain.ActivityPubObject> perform(CmdFindObjectById cmd);
	
//	OutputStream perform(CmdSparqlPublic cmd) throws IOException;
//
//	OutputStream perform(CmdSparqlCollection cmd) throws IOException;
}
