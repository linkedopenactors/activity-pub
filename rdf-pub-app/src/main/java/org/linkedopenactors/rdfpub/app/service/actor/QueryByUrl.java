package org.linkedopenactors.rdfpub.app.service.actor;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Query object.
 */
@Getter
@Setter
@ToString
public class QueryByUrl extends CmdQueryBaseAbstract {
	private String contentType;
	private String requestedUrl;
	
	/**
	 * Constructor
	 * @param authenticatedActorHolder Information about a authenticated user. 
	 * @param contentType used for fomatting the result.
	 * @param requestedUrl thr url, that is requested. That is the id of the object, but also the HTTP Get url.
	 * @param userAgent Info about the caller of the service.
	 */
	public QueryByUrl(AuthenticatedActorHolder authenticatedActorHolder, String contentType, String requestedUrl, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.contentType = contentType;
		this.requestedUrl = requestedUrl;
	}
}
