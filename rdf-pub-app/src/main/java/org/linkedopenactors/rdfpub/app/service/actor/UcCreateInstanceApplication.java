package org.linkedopenactors.rdfpub.app.service.actor;

public interface UcCreateInstanceApplication {
	void perform(CmdCreateInstance cmd);
}
