package org.linkedopenactors.rdfpub.app.service.actor;

import lombok.Data;

/**
 * Query object.
 */
@Data
public class QueryByPreferredUsername {
	private final String preferredUsername;

	/**
	 * Constructor.
	 * @param preferredUsername The preferredUsername to search for. 
	 */
	public QueryByPreferredUsername(String preferredUsername) {
		super();
		this.preferredUsername = preferredUsername;
	}
}
