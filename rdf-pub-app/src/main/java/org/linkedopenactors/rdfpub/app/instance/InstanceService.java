package org.linkedopenactors.rdfpub.app.instance;
import org.linkedopenactors.rdfpub.domain.InstanceProperties;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

/**
 * Services round about the running rdf-pub instance.
 */
public interface InstanceService {
	
	/**
	 * @return {@link InstanceProperties} 
	 */
	org.linkedopenactors.rdfpub.domain.InstanceProperties getInstanceProperties();
	
	/**
	 * Creates a {@link RdfPubBlankNodeOrIRI} with the Domain as prefix followed by the passed extension.
	 * @param extension The path to add to the Domain. Can optionaly starts with a slash.
	 * @return A {@link RdfPubBlankNodeOrIRI} with the Domain as prefix followed by the passed extension.
	 */
	RdfPubBlankNodeOrIRI getInternalUrnExtendedWith(String extension);
}
