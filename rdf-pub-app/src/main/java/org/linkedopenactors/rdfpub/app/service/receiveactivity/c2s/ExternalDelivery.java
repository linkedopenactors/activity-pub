package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

/**
 * Is responsible for the delivery of activities to external Actors.
 */
public interface ExternalDelivery {

	/**
	 * Delivery of an activity
	 * @param authenticatedActorStore the store of the authenticated actor
	 * @param authenticatedActor The owner (store) which contains the activity to deliver.
	 * @param activitySubject The subject of the activity to deliver.
	 */
	void deliver(ActorsStore authenticatedActorStore, Actor authenticatedActor, RdfPubBlankNodeOrIRI activitySubject);
}
