package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUsecaseProcessor;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;
import org.linkedopenactors.rdfpub.domain.security.VerificationResult;

import lombok.extern.slf4j.Slf4j;

/**
 * Abstract UseCase Class, that provides the possibility of verifying a HTTP Signature.
 */
@Slf4j
abstract class UcS2SSignatureVerfified implements InboxUsecaseProcessor {

	private SignatureVerifier signatureVerifier;

	public UcS2SSignatureVerfified(SignatureVerifier signatureVerifier) {
		this.signatureVerifier = signatureVerifier;
	}

	/**
	 * Checks whether the HTTP signature of the incoming message is correct.
	 * @throws BadRequestException
	 */
	protected void verify() throws BadRequestException {
		VerificationResult verified = signatureVerifier.verify();
		log.debug("verified: " + verified);
		if( !verified.isSucceeded() ) {
			log.error(verified.getFailureMessage());
			throw new BadRequestException(verified.getFailureMessage());
		}
	}
}
