package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;

import lombok.Data;

/**
 * Command Object, that holds the data, that should be send to the actors inbox.
 * This is used for internal deivery (a user on the same server sends an activity).
 */
@Data
public class CmdReceiveActivityInboxTyped {
    /**
     * Fully qualified name of the address returned by getRemoteHost(). 
     * Or, if the remote engine cannot or chooses not to resolve, the hostname, 
     * this is the IP address.
     */
    public String remoteHost;

    /**
	 * The id of an object to that the activity was sent. Also known as inbox owner. 
	 */
	private final RdfPubObjectId receiverId;
	
	/**
	 * A loggable name of the reciver.
	 */
	private final String receiverLoggableName;
	
	/**
	 * The activity to receive from the inbox. 
	 */
	private final Activity activity;
	
	/**
	 * An instance of a {@link SignatureVerifier} that holds all data, that is needed for HTTPSignature validation.
	 * The validation is done in the use case, as it can only be determined in the useCase whether a signature needs to be verified.
	 */
	private SignatureVerifier signatureVerifier;
	
	/**
	 * Creates an instance.
	 * @param recipientActorId The id of an actor to whom the activity was sent. Also known as inbox owner.
	 * @param activity The activity to receive from the inbox.
	 */
	public CmdReceiveActivityInboxTyped(RdfPubObjectId recipientActorId, String receiverLoggableName, Activity activity) {
		this.receiverId = recipientActorId;
		this.receiverLoggableName = receiverLoggableName;
		this.activity = activity;		
	}
	
	/**
	 * Creates an instance.
	 * @param recipientActorId The id of an actor to whom the activity was sent. Also known as inbox owner.
	 * @param activity The activity to receive from the inbox.
	 * @param signatureVerifier see {@link #signatureVerifier}
	 */
	public CmdReceiveActivityInboxTyped(RdfPubObjectId recipientActorId, String receiverLoggableName, Activity activity, SignatureVerifier signatureVerifier) {
		this(recipientActorId, receiverLoggableName, activity);
		this.signatureVerifier = signatureVerifier;		
	}

	/**
	 * @return {@link Optional} of {@link SignatureVerifier}. 
	 */
	public Optional<SignatureVerifier> getSignatureVerifier() {
		return Optional.ofNullable(signatureVerifier);
	}
	
	public Optional<String> remoteHost() {
		return Optional.ofNullable(remoteHost);
	}

	public String getReceiverLoggableName() {
		return receiverLoggableName;
	}
}
