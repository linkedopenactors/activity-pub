package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Helper Class for {@link OutboxReceiveActivityServiceDefault} to determine the
 * useCase type and provide an {@link OutboxUseCaseContext}
 */
@Component
@Slf4j
class OutboxUseCasesDeterminator extends AbstractUseCasesDeterminator {
	
	public OutboxUseCasesDeterminator(VocabContainer vocabContainer, ExternalObjectRepository externalObjectRepository, PublicActorStore publicActorStore) {
		super(vocabContainer, publicActorStore, externalObjectRepository);
	}

	public OutboxUseCaseContext determinateOutbox(AuthenticatedActor authenticatedActor, Activity activity) {
		Set<UseCase> usecases = new HashSet<>();
		Set<ActivityPubObject> objects = getObjects(activity, activity.resolveObject(), authenticatedActor.getActorsStore());
		
		if(isDeleteRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.c2s_deleteRequest);
		}
		if(isFollowingRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.c2s_followRequest);
		}
		if(isIgnoreFollowingRequest(activity, activity.getTypes(), objects)) {
			// ^ TODO remove!! should alway be reject, with or without receiver !
			usecases.add(UseCase.c2s_ignoreFollowRequest);
		}		
		if(isRejectFollowingRequest(activity, activity.getTypes(), objects)) {
			usecases.add(UseCase.c2s_rejectFollowRequest);
		}
		if(isCreateObjectRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.c2s_createObject);
		}
		if(isUpdateObjectRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.c2s_updateObject);
		}
		if(isUndoFollowingRequest(activity, activity.getTypes(), objects)) {
			usecases.add(UseCase.c2s_undoFollow);
		}
		if(isAcceptFollower(authenticatedActor, activity, activity.getTypes())) {
			usecases.add(UseCase.c2s_accept_follow);
		}		
		if(isRemoveFollowerRequest(authenticatedActor.getActor(), activity, activity.getTypes(), objects)) {			
			usecases.add(UseCase.c2s_removeFollower);
		}
		
		if(usecases.size()!=1) {
			log.error("activity that makes trouble:\n" + activity);
			if(usecases.size()>1) {
				throw new IllegalStateException("currently we expect exactly one usecase. But is: " + usecases + "See activity above.");
			} else {
				throw new IllegalStateException("Could not find a UseCase! See activity above.");
			}
		}

		if(usecases.size()!=1) {
			// Puh, that can happen!! If we need to read something from a remote server and it's not accessable at the moment!!
			// Do we need a cache?? thats a difficult question
			// or should we introduce async processing with a queue ?
			log.error("activity that makes trouble:\n" + activity);
			throw new IllegalStateException("currently we expect exactly one usecase. But is: " + usecases);
		}
		
		String loggableName = authenticatedActor.getActor().getPreferredUsername()
				.orElse(authenticatedActor.getActor().getSubject().asRdfPubObjectId().orElseThrow().getBaseSubject().getIRIString());
		logUseCase(loggableName, usecases, activity);
		
		UseCase uc = usecases.stream().findFirst().orElseThrow();
		
		return new OutboxUseCaseContext(uc, authenticatedActor.getActorsStore(),
				authenticatedActor.getPendingFollowerCache(), authenticatedActor.getPendingFollowingCache(), activity,
				objects);
	}

	private boolean isIgnoreFollowingRequest(Activity activity, Set<RdfPubIRI> types, Set<ActivityPubObject> objects)  {
		if(!types.contains(vocAs().Ignore())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		Optional<RdfPubObjectId> objectIri = activity.getObject().stream().findFirst().orElseThrow().asRdfPubObjectId();
		if(objectIri.isPresent()) {
			if(!objectIri.orElseThrow().isActivity()) {
				return false;
			}
		}
		if(objects.size() != 1) {
			log.warn("not exactly one object!");
			return false;
		}
		
		Set<RdfPubIRI> objectTypes = objects.stream().findFirst().orElseThrow().getTypes();
		if( !objectTypes.contains(vocAs().Follow()) ) {
			return false;
		}
		
		return true;	
	}

	private boolean isRejectFollowingRequest(Activity activity, Set<RdfPubIRI> types, Set<ActivityPubObject> objects)  {
		if(!types.contains(vocAs().Reject())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		Optional<RdfPubObjectId> objectIri = activity.getObject().stream().findFirst().orElseThrow().asRdfPubObjectId();
		if(objectIri.isPresent()) {
			if(!objectIri.orElseThrow().isActivity()) {
				return false;
			}
		}
		if(objects.size() != 1) {
			log.warn("not exactly one object!");
			return false;
		}
		
		Set<RdfPubIRI> objectTypes = objects.stream().findFirst().orElseThrow().getTypes();
		if( !objectTypes.contains(vocAs().Follow()) ) {
			return false;
		}
		
		return true;	
	}
}
