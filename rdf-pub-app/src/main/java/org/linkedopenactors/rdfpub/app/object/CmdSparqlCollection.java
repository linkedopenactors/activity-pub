package org.linkedopenactors.rdfpub.app.object;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public abstract class CmdSparqlCollection {
	private final String query;
	private final String acceptHeader;
	private final org.linkedopenactors.rdfpub.domain.Actor authenticatedActor;
	
	public String getAcceptHeader() {
		return acceptHeader;
//		"application/sparql-results+json" use as default ??
	}
}
