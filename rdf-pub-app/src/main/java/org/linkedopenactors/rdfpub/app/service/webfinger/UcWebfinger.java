package org.linkedopenactors.rdfpub.app.service.webfinger;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.actor.QueryById;
import org.linkedopenactors.rdfpub.app.service.actor.QueryByOauth2IssuerUserId;
import org.linkedopenactors.rdfpub.app.service.actor.QueryByPreferredUsername;
import org.linkedopenactors.rdfpub.app.service.actor.UcQueryActor;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Responsible for providing the webfinger profile of a user.
 */
@Slf4j
@Component
public class UcWebfinger {

	@Autowired
	private UcQueryActor ucQueryActor;
	
	@Autowired
	private AuthenticatedActorHolder authenticatedActorHolder;
	
	/**
	 * Provides a webfinger profile for the actor, that is identifiable by the passed webfingerResource.
	 * @param query
	 * @return A webfinger profile for the actor, that is identifiable by the passed webfingerResource.
	 */
	public Optional<String> perform(QueryByWebfingerResource query) {
		RdfPubMDC.put(getClass().getSimpleName());
		try {
			return performInternal(query);
		} finally {
			RdfPubMDC.remove();
		}
	}
	
	private Optional<String> performInternal(QueryByWebfingerResource query) {
		logUseCase(query);
		Optional<String> result = Optional.empty();
		Optional<Actor> actor = findActorByWebfingerResource(query);
		if(actor.isPresent()) {
			RdfPubBlankNodeOrIRI externalActorSubject = actor.get().getSubject().asActor().orElseThrow().getExternalBaseSubject();
			String body = getWebFingerProfile(externalActorSubject.toString(), query.getWebfingerResource());
			result = Optional.ofNullable(body);
		}
		log.debug("<-UcWebfinger " + result);
		return result;
	}
	
	private Optional<Actor> findActorByWebfingerResource(QueryByWebfingerResource query) {
		String webfingerResource = query.getWebfingerResource();
		Optional<String> userOpt = extractUserName(webfingerResource);
		log.trace("-> webfinger requested: " + webfingerResource + " extractedUser: " + userOpt);
		Optional<Actor> actor = userOpt.flatMap(user-> findActor(query.getAuthenticatedActorHolder(), user, query.getUserAgent()));
		return actor;
	}

	private Optional<String> extractUserName(String resource) {
		Optional<String> resourceOpt = Optional.ofNullable(resource);
		Optional<String> userOpt = resourceOpt.map(this::removeAcct);
		return userOpt;
	}

	/**
	 * The requested resource can be prefixed with 'acct:' we ignore that.
	 * @param actorResource
	 * @return the 'cleaned' actorResource
	 */
	private String removeAcct(String actorResource) {
		if (actorResource.startsWith("acct:")) {
			actorResource = actorResource.substring(5);
		}
		return actorResource;
	}

	private Optional<? extends Actor> findActor(AuthenticatedActorHolder authenticatedActorHolder, String user, UserAgent userAgent) {
		Optional<Actor> actor;		
		if(isRequestForCurrentAuthenticatedUser(user)) {
			log.trace("RequestForCurrentAuthenticatedUser ("+user+")!");
			actor = authenticatedActorHolder.getAuthenticatedActor().map(AuthenticatedActor::getActor);
		} else {
			actor = ucQueryActor.perform(new QueryByOauth2IssuerUserId(user));
			log.trace("discoverActorsService.findByOauth2IssuerUserId("+user+") found: " + !actor.isEmpty());
			if(actor.isEmpty()) {
				actor = findByName(user);
			}
		}
		if(actor.isEmpty()) {
			String actorIdentifier = user.startsWith("@") ? user.substring(1) : user;
			if(actorIdentifier.contains("@")) {
				actorIdentifier = actorIdentifier.substring(0, actorIdentifier.indexOf("@"));
			}
			actor = ucQueryActor.perform(new QueryByPreferredUsername(actorIdentifier));
			if(actor.isEmpty()) {
				
				try {
					actor = ucQueryActor.perform(new QueryById(authenticatedActorHolder, actorIdentifier, userAgent));
				} catch (IllegalArgumentException e) {
					// ignore
				}
			}
		}
		return actor;
	}

	/**
	 * 
	 * @param principalName The principal name which is, by default, the Jwt's subject
	 * @return True, if the passed principalName is equal to the principalName in the JwtAuthenticationToken.
	 */
	private boolean isRequestForCurrentAuthenticatedUser(String principalName) {
		return authenticatedActorHolder.getAuthenticatedActor()
			.map(AuthenticatedActor::getActor)
			.map(Actor::getOauth2IssuerUserId)
			.map(principalName::equals)
			.orElse(false);
	}

	/**
	 * Tries to find the user by it's name.
	 * @param user The username with or without host. E.g. 'max' or 'may@myserver.org'.
	 * @return The Actor for the given name.
	 */
	private Optional<Actor> findByName(String user) {
		
		Optional<Actor> actor;
		String username = user;
		if(user.contains("@")) {
			username = removeNamespace(user);
		}					
		actor = ucQueryActor.perform(new QueryByPreferredUsername(username));
		return actor;
	}

	/**
	 * We accept ids with namespace e.g. @rdfpub.test.opensourceecology.de but ignoring it. 
	 * @param resource
	 * @return the 'cleaned' actorResource
	 */
	private String removeNamespace(String resource) {
		String manipulated = resource;
		if(resource.contains("@")) {
			String[] splitted = StringUtils.split(resource, "@");
			if(splitted!=null && splitted.length>1) {
				manipulated = resource.substring(0, resource.lastIndexOf("@"));
			}
			log.trace("removeNamespace("+resource+") -> " + manipulated);
		}
		return manipulated;		
	}

	/**
	 * Creates the passed profile url and the requested resource in a webfinger json response string.
	 * @param actorId The profile url
	 * @param userId the requested resource
	 * @return A webfinger json response string.
	 */
	private String getWebFingerProfile(String actorId, String userId)  {
		log.trace("->getWebFingerProfile( " + actorId + ", "+userId+")");
		String jsonTemplate = "{\n"
				+ "	\"subject\": \"${resource}\",\n"
				+ "\n"
				+ "	\"links\": [\n"
				+ "		{\n"
				+ "			\"rel\": \"self\",\n"
				+ "			\"type\": \"application/activity+json\",\n"
				+ "			\"href\": \"${iri}\"\n"
				+ "		}\n"
				+ "	]\n"
				+ "}\n";
		
		jsonTemplate = jsonTemplate.replace("${resource}", userId);
		String profile = jsonTemplate.replace("${iri}", actorId);
		log.trace("webfinger profile for: " + actorId.toString() + " ("+userId+")\n" + profile);
		log.trace("<-getWebFingerProfile()");
		return profile;		
	}
	
	protected void logUseCase(QueryByWebfingerResource query) {
		log.debug("\n######################################\n" 
				+ this.getClass().getSimpleName()
				+ "\nWebfingerResource: " + query.getWebfingerResource() + " (isAuthenticated: " + authenticatedActorHolder.getAuthenticatedActor().isPresent() + ")"
				+ "\n######################################\n");		
	}
}
