package org.linkedopenactors.rdfpub.app.adapter;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Service;

@Service
public class ObjectAdapterDefault implements ObjectAdapter {
	
	private AdapterMastodon adapterMastodon;
	
	private GraphToStringConverter graphToStringConverter;

	public ObjectAdapterDefault(VocabContainer vocabContainer, GraphToStringConverter graphToStringConverter) {
		this.graphToStringConverter = graphToStringConverter;
		adapterMastodon = new AdapterMastodon(vocabContainer, graphToStringConverter);
	}
	
	public String adjust(ActivityPubObject activityPubObject, RdfFormat rdfFormat, UserAgent userAgent) {
		String product = Optional.ofNullable(userAgent).map(UserAgent::getProduct).map(String::toLowerCase).orElse("unknown");
		return switch (product) {
		case "mastodon" -> {
			yield adapterMastodon.adjustMastodon(activityPubObject, rdfFormat);
		}
		case "postmanruntime" -> {
			yield adapterMastodon.adjustMastodon(activityPubObject, rdfFormat);
		}		
		case "friendica" -> { 
			yield adapterMastodon.adjustMastodon(activityPubObject, rdfFormat);
		}
		case "manyfold" -> { 
			yield adapterMastodon.adjustMastodon(activityPubObject, rdfFormat);
		}
		case "http.rb" -> { // activitypub.academy
			yield adapterMastodon.adjustMastodon(activityPubObject, rdfFormat);
		}
		default -> {
			yield graphToStringConverter.convert(rdfFormat, activityPubObject.asGraph(), true);
		}
		};
	}
}
