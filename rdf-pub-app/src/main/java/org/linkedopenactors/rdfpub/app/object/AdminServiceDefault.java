package org.linkedopenactors.rdfpub.app.object;

import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class AdminServiceDefault implements AdminService{

	@Autowired
	private ActorsStoreRepository actorsStoreRepository;
	
	@Override
	public Set<String> perform(CmdReadActorGraphNames c) {
		ActorsStore actorsstore = actorsStoreRepository.find(c.getActor()).orElseThrow();
		return actorsstore.getGraphNames().stream()
			.map(Object::toString)
			.collect(Collectors.toSet());
	}
}
