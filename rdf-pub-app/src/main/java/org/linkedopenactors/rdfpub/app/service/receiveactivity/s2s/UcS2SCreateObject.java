package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;

/**
 * An external actor created an object and an actor on this server is a receiver. 
 */
class UcS2SCreateObject extends UcS2SSignatureVerfified {
	private InboxUseCaseContext ucCtx;
	private boolean internalDelivery;

	public UcS2SCreateObject(InboxUseCaseContext ucCtx) {
		this(ucCtx, false);
	}

	public UcS2SCreateObject(InboxUseCaseContext ucCtx, boolean internalDelivery) {
		super(ucCtx.getSignatureVerifier());
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
	}

	/**
	 * Process the usecase.
	 * @throws BadRequestException
	 */
	@Override
	public void process() throws BadRequestException {
		if(!internalDelivery) {
			verify();
		}
		ucCtx.getReceiverStore().addToInbox(ucCtx.getActivity(), "todo");
	}
}
