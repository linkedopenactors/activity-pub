package org.linkedopenactors.rdfpub.app.object;

import lombok.Data;

@Data
public class CmdReadActors {
	private Integer pageSize;
	private Integer startIndex;
}
