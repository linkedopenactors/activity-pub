package org.linkedopenactors.rdfpub.app.service.webfinger;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;

import lombok.Getter;

/**
 * Input Data for the usecase {@link UcWebfinger}.
 */
@Getter
public class QueryByWebfingerResource extends CmdQueryBaseAbstract {
	private final String webfingerResource;

	/**
	 * Constructor.
	 * 
	 * @param authenticatedActorHolder
	 * @param webfingerResource        the value of the resource http parameter that
	 *                                 was send with the webfinger rectest. E.g.
	 *                                 acct: @tester@example.org.
	 */
	public QueryByWebfingerResource(AuthenticatedActorHolder authenticatedActorHolder, String webfingerResource, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.webfingerResource = webfingerResource;
	}
}
