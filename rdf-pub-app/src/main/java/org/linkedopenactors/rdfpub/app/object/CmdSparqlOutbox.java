package org.linkedopenactors.rdfpub.app.object;

public class CmdSparqlOutbox extends CmdSparqlCollection {
	public CmdSparqlOutbox(String query, String acceptHeader, org.linkedopenactors.rdfpub.domain.Actor authenticatedActor) {
		super(query, acceptHeader, authenticatedActor);
	}
}
