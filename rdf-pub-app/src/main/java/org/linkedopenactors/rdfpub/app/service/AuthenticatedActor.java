package org.linkedopenactors.rdfpub.app.service;

import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

public interface AuthenticatedActor {
	/**
	 * Return the actor who authenticated for this request.
	 * @return the actor who authenticated for this request.
	 */
	Actor getActor();
	
	/**
	 * Return the id of the actor who authenticated for this request.
	 * @return The id of the actor who authenticated for this request..
	 */
	RdfPubObjectIdActor getActorId();
	
	/**
	 * Return the {@link ActorsStore} of the actor who authenticated for this request.
	 * @return the {@link ActorsStore} of the actor who authenticated for this request.
	 */
	ActorsStore getActorsStore();
		
	/**
	 * Returns the PendingFollowerCache of the current authenticated actor.  
	 * @return The PendingFollowerCache of the current authenticated actor.
	 */
	PendingFollowerCache getPendingFollowerCache();
	
	/**
	 * @return E.g. the preferredUsername of the actor.
	 */
	String getActorsLoggableName();
}
