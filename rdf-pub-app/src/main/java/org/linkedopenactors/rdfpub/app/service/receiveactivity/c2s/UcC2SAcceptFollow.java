package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import java.util.Set;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;

import lombok.extern.slf4j.Slf4j;

/**
 * An actor on this server wants to accept a follower request. 
 */
@Slf4j
class UcC2SAcceptFollow extends AbstractUcC2SCreateObject {
	
	private OutboxUseCaseContext ucCtx;
	private InternalDelivery internalDelivery;
	private ExternalDelivery externalDelivery;

	public UcC2SAcceptFollow(OutboxUseCaseContext ucCtx, InternalDelivery internalDelivery,
			ExternalDelivery externalDelivery) {
		super();
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
		this.externalDelivery = externalDelivery;
	}

	/**
	 * Process the usecase.
	 * @return the subject / location of the created activity
	 */
	public RdfPubObjectIdActivity process(AuthenticatedActorHolder authenticatedActorHolder) {
		
		Activity c2sActivity = ucCtx.getActivity();
		String act = c2sActivity.toString(RdfFormat.JSONLD);
		log.debug("act: " + act);
		
		RdfPubBlankNodeOrIRI followActivityId = ucCtx.getActivity().getObject().stream().findFirst().orElseThrow();
		
		Activity followActivity = ucCtx.getPendingFollowerCache().getPendingFollowerActivities().stream()
			.filter(a->a.getSubject().equals(followActivityId)).findFirst().orElseThrow();
		
		RdfPubBlankNodeOrIRI followerActorId =  followActivity.getActor().stream().findFirst().orElseThrow();
		
		c2sActivity.setTo(Set.of(followerActorId));
		c2sActivity.setObjects(Set.of(followActivity));
		act = c2sActivity.toString(RdfFormat.JSONLD);
		log.debug("act: " + act); 
				
		RdfPubBlankNodeOrIRI activityIriToRemove = getActivityIriToRemove();// Do this before adjustActivityAddToOutboxAndDeliver,
																	// because graph externalisation!
		
		RdfPubObjectIdActivity activityIri = adjustActivityAddToOutboxAndDeliverRename(c2sActivity);
//		RdfPubObjectIdActivity activity = ucCtx.getRecipientActorStore().addToOutbox(c2sActivity, "add acceptFollow Activity to outbox");
		
		
		ucCtx.getPendingFollowerCache().removePendingFollowActivity(activityIriToRemove);
		
		ucCtx.getRecipientActorStore().addFollower(getFollower());
		
//		ucCtx.getRecipientActorStore().getFollowers().forEach(f->log.debug("followers item: " + f));
		
		// TODO we do not have any transactions, thats a problem ?!
		return activityIri;
	}
	
	private RdfPubIRI getFollower() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getObjects().stream().findFirst().orElseThrow().asConvertable().asActivity();
		
		RdfPubIRI followerIri = activity.getActor().stream().findFirst().orElseThrow();
		return followerIri;
	}

	private RdfPubBlankNodeOrIRI getActivityIriToRemove() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getActivity();
		Set<RdfPubBlankNodeOrIRI> object = activity.getObject();
		RdfPubBlankNodeOrIRI  ddd = object.stream().findFirst().orElseThrow();
		return ddd;
	}

	protected RdfPubObjectIdActivity adjustActivityAddToOutboxAndDeliverRename(Activity activity) {
		activity.replaceSubjects(ucCtx.getOutboxOwner().getSubject().asActor().orElseThrow(), false);
		
		RdfPubObjectIdActivity addedActivitySubject = ucCtx.getRecipientActorStore().addToOutbox(ucCtx.getActivity(), "ReceiveActivityOutboxServiceDefault#perform create");
		String outboxOwnerLoggableName = ucCtx.getOutboxOwner().getPreferredUsername().orElse(ucCtx.getOutboxOwner().getSubject().getIRIString());
		internalDelivery.deliver(ucCtx.getRecipientActorStore(), addedActivitySubject, outboxOwnerLoggableName);
		externalDelivery.deliver(ucCtx.getRecipientActorStore(), ucCtx.getOutboxOwner(), addedActivitySubject);
		return addedActivitySubject;
	}
}
