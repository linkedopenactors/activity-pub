package org.linkedopenactors.rdfpub.app.service.actor;

public interface UcCreatePerson {
	void perform(CmdCreatePerson cmd);
}
