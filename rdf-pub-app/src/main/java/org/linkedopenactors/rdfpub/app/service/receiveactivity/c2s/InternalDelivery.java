package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import java.util.HashSet;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.CmdReceiveActivityInboxTyped;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxReceiveActivityService;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.springframework.stereotype.Component;

import io.sentry.spring.jakarta.tracing.SentrySpan;

/**
 * Is responsible for the delivery of activities to internal Actors.
 */
@Component
class InternalDelivery {

	private InboxReceiveActivityService receiveActivityInboxService;

	public InternalDelivery(InboxReceiveActivityService receiveActivityInboxServicet) {
		this.receiveActivityInboxService = receiveActivityInboxServicet;
	}
		
	/**
	 * Delivery of an activity
 
	 * @param triggeringActor The actor who triggered the activity.
	 * @param activitySubject The subject of the activity to deliver.
	 */
	@SentrySpan
	public void deliver(ActorsStore authenticatedActorStore, RdfPubBlankNodeOrIRI activitySubject, String outboxOwnerLoggableName) {
		Activity activity = authenticatedActorStore.find(activitySubject, 1).orElseThrow().asConvertable().asActivity();
		
		Set<RdfPubObjectIdCollection> receiverCollections = activity.getInternalReceiverCollections();
		// TODO deliver to collections!!
		
		Set<RdfPubObjectIdActor> receiversActors = activity.getInternalReceivers();
		
		Set<RdfPubObjectId> receivers = new HashSet<>();
		receivers.addAll(receiverCollections);
		receivers.addAll(receiversActors);
		
		activity.hideBlindReceivers();
				
		receivers.stream().forEach(receiverId->{
				try {
					receiveActivityInboxService.perform(new CmdReceiveActivityInboxTyped(receiverId, outboxOwnerLoggableName, activity));
				} catch (BadRequestException e) {
					throw new IllegalStateException("never happpens ;-)", e);
				}
		});
	}
}
