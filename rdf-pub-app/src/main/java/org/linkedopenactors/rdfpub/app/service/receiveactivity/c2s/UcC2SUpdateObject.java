package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

import lombok.extern.slf4j.Slf4j;

/**
 * An actor on this server wants to update an object. 
 */
@Slf4j
class UcC2SUpdateObject extends AbstractUcC2SCreateObject {

	private OutboxUseCaseContext ucCtx;
	private PublicActorStore publicActorStore;

	public UcC2SUpdateObject(OutboxUseCaseContext ucCtx, InternalDelivery internalDelivery,
			ExternalDelivery externalDelivery, PublicActorStore publicActorStore) {
		super();
		this.ucCtx = ucCtx;
		this.publicActorStore = publicActorStore;
	}

	/**
	 * Process the usecase.
	 * @return the subject / location of the created activity
	 */
	@Override
	public RdfPubObjectIdActivity process(AuthenticatedActorHolder authenticatedActorHolder) {
		Optional<ActivityPubObject> objOpt = ucCtx.getActivity().resolveObject().stream().findFirst(); // TODO handle All !!!
		ActivityPubObject objectWithPredicatesToChangeXXX = objOpt.orElseThrow(()->new IllegalStateException("update activity without object is not supported."));
		RdfPubObjectIdActor	triggeringActorsBaseSubject = ucCtx.getOutboxOwner().getSubject().asActor().orElseThrow();
		return processUpdateActivityWithObject(ucCtx.getRecipientActorStore(), triggeringActorsBaseSubject, ucCtx.getActivity(), objectWithPredicatesToChangeXXX);
	}
	
	/**
	 * @param actorsStore
	 * @param triggeringActorsBaseSubject the actor that is doing the update
	 * @param activity The update activity
	 * @param objectWithPredicatesToChangeXYZ The object that contains the predicates to be updated! Note, is partial!! 
	 * @return IRI of the created update activity 
	 */
	private RdfPubObjectIdActivity processUpdateActivityWithObject(ActorsStore actorsStore,
			RdfPubObjectIdActor triggeringActorsBaseSubject, org.linkedopenactors.rdfpub.domain.Activity activity,
			org.linkedopenactors.rdfpub.domain.ActivityPubObject objectWithPredicatesToChangeXYZ) {
		
		////////////////////
		// TODO, wenn eine update activity gesendet wird, wird dann in allen ehemaligen empfängern verändert oder nur diejenigen, die den update empfangen ??
		////////////////////
		if(activity.getObject().size()!=1) {
			throw new IllegalStateException("exact one object excpecet, but was: " + activity.getObject().size());
		}
		
		activity.replaceSubjects(triggeringActorsBaseSubject, false);
		activity.replaceSubjectsWithInternals();
		activity.addAttributedTo(triggeringActorsBaseSubject);
		
		
		Set<RdfPubBlankNodeOrIRI> objects = activity.getObject();
		Optional<ActivityPubObject> existingOpt = objects.stream()
				.findFirst()
				.flatMap(actorsStore::findBySubject);
		
		if(existingOpt.isEmpty()) {
			// TODO is the current asking actor allowed to change public objects
			// is the object an actor ?? 
			// is the current asking actor allowed to change actors ?? 
//			PublicActorStore pas = actorsStoreRepository.findPublic();
			existingOpt = activity.getObject().stream()
				.findFirst()
				.flatMap(publicActorStore::findBySubject);
		}
		
		ActivityPubObject existing = existingOpt.orElseThrow(); 
		
		org.linkedopenactors.rdfpub.domain.ActivityPubObject newOne = activity.resolveObject().stream().findFirst().orElseThrow();
		
		log.debug("existing before: " + existing);
		
		existing.partialUpdate(newOne);
		
		log.debug("existing after: " + existing);
		
		activity.replaceObject(existing);
		
		actorsStore.addToOutbox(activity, "ReceiveActivityOutboxServiceDefault.processUpdateActivityWithObject");
		
		log.debug(""+activity.getSubject());
		return activity.getSubject().asActivity().orElseThrow();
	}

}
