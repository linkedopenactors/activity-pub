package org.linkedopenactors.rdfpub.app.service;

import java.util.Optional;

public interface AuthenticatedActorHolder {
	Optional<AuthenticatedActor> getAuthenticatedActor();
}
