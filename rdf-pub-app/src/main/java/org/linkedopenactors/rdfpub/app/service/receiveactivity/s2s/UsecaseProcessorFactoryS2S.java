package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxReceiveActivityService;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUsecaseProcessor;
import org.springframework.stereotype.Service;

/**
 * Helper Class for {@link InboxReceiveActivityService} to create the
 * InboxUsecaseProcessor.
 */
@Service
public class UsecaseProcessorFactoryS2S {

	/**
	 * Creates an {@link InboxUsecaseProcessor} that is able to process the passed {@link InboxUseCaseContext}.
	 * @param ucCtx {@link InboxUseCaseContext} to process.
	 * @return The {@link InboxUsecaseProcessor} that is able to process the passed {@link InboxUseCaseContext}
	 */
	public InboxUsecaseProcessor create(InboxUseCaseContext ucCtx) {
		switch (ucCtx.getUseCase()) {
		case s2s_createObject: {
			return new UcS2SCreateObject(ucCtx);
		}
		case s2s_createObjectInternal: {
			return new UcS2SCreateObject(ucCtx, true);
		}
		case s2s_deleteRequest: {
			return new UcS2SDelete();
		}
		case s2s_followRequest: {
			return new UcS2SFollow(ucCtx);
		}
		case s2s_followInternal: {
			return new UcS2SFollow(ucCtx, true);
		}
		case s2s_undoFollow: {
			return new UcS2SUndoFollow(ucCtx);
		}
		case s2s_undoFollowInternal: {
			return new UcS2SUndoFollow(ucCtx, true);
		}
		case s2s_updateObject: {
			return new UcS2SUpdateObject(ucCtx);
		}
		case s2s_like: {
			return new UcS2SLikeObject(ucCtx);
		}
		case s2s_accept_follower: { // TODO dead code ? See UcC2SAcceptFollow 
			return new UcS2SAcceptFollow(ucCtx);
		}
		case s2s_accept_following: {
			return new UcS2SAcceptFollowing(ucCtx);
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + ucCtx.getUseCase());
		}
	}	
}
