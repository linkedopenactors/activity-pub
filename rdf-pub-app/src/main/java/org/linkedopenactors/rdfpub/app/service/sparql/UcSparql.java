package org.linkedopenactors.rdfpub.app.service.sparql;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.springframework.stereotype.Component;

@Component
public class UcSparql {

	public Optional<String> perform(QuerySparqlTuple query) {
		return query.getAuthenticatedActorHolder().getAuthenticatedActor()			
			.map(authenticatedActor->{
				ActorsStore actorsStore = authenticatedActor.getActorsStore();
				String result = actorsStore.queryTuple(query.getQuery());
				return result;
		});
	}
}
