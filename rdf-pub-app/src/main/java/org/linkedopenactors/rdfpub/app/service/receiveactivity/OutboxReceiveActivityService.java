package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;

/**
 * The service that handles C2S interactions.
 */
public interface OutboxReceiveActivityService {
	
	/**
	 * 
	 * @param cmd
	 * @return The id / location of the created activity.
	 */
	RdfPubObjectIdActivity perform(CmdReceiveActivityOutbox cmd);
	
	/**
	 * 
	 * @param cmd
	 * @return The id / location of the created activity.
	 */
	RdfPubObjectIdActivity perform(CmdReceiveActivityOutboxTyped cmd);	
}
	
