package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;

import lombok.Data;

/**
 * Container for the attributes that an OutboxUsecaseProcessor needs to execute the UseCase.
 */
@Data
public class OutboxUseCaseContext {
	private final UseCase useCase;
	private final ActorsStore recipientActorStore;
	private final PendingFollowerCache pendingFollowerCache;
	private final PendingFollowerCache pendingFollowingCache;
	private final Activity activity;
	private Set<ActivityPubObject> objects;
	private Actor outboxOwner;	
	
	/**
	 * Constructs a {@link OutboxUseCaseContext} 
	 * @param useCase The type of the useCase.
	 * @param recipientActorStore The {@link ActorsStore} of the recipient {@link Actor}.
	 * @param pendingFollowerCache 
	 * @param pendingFollowingCache
	 * @param activity The activity to process 
	 * @param objects The objects of the activity to process
	 */
	public OutboxUseCaseContext(UseCase useCase, ActorsStore recipientActorStore,
			PendingFollowerCache pendingFollowerCache, PendingFollowerCache pendingFollowingCache, Activity activity,
			Set<ActivityPubObject> objects) {
		this.useCase = useCase;
		this.recipientActorStore = recipientActorStore;
		this.pendingFollowerCache = pendingFollowerCache;
		this.pendingFollowingCache = pendingFollowingCache;
		this.activity = activity;
		this.objects = objects;
	}
}
