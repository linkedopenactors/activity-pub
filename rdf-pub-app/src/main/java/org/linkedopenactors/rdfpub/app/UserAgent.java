package org.linkedopenactors.rdfpub.app;

import lombok.Data;

@Data
public class UserAgent {
	private final String product;
	private final String productVersion;
	private final String comment;
}
