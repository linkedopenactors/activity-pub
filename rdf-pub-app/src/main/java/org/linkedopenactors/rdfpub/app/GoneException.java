package org.linkedopenactors.rdfpub.app;

public class GoneException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public GoneException(String message) {
		super(message);
	}
}
