package org.linkedopenactors.rdfpub.app.external;

import java.security.PublicKey;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.Actor;

public interface DiscoverExternalActor {

	/**
	 * Try to find an external Actor by the passed identifier.
	 * @param identifier The url or the webfinger 'token'.
	 * @return The found external Actor or {@link Optional#empty()}.
	 */
	Optional<Actor> discover(String identifier);
	
	/**
	 * The keyIdOfSignature regarding a HTTPSignature can reference different
	 * things, see:
	 * https://swicg.github.io/activitypub-http-signature/#how-to-obtain-a-signature-s-public-key
	 * This method helps you to solve this mess and try to get the public key.
	 * 
	 * @param keyIdOfSignature
	 * @return The {@link PublicKey}
	 */
	Optional<PublicKey> discoverPublicKey(String keyIdOfSignature);
}
