package org.linkedopenactors.rdfpub.app.object;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

import lombok.Data;

@Data
public class CmdFindObjectById {
	private org.linkedopenactors.rdfpub.domain.Actor authenticatedActor;
	private RdfPubBlankNodeOrIRI id;
	private RdfPubObjectIdActor ownerId;
	private int deep;
}
