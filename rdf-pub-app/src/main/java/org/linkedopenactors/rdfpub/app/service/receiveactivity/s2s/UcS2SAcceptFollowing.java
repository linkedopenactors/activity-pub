package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

/**
 * An external actor accepts a follow request from an actor on this server. 
 */
class UcS2SAcceptFollowing extends UcS2SSignatureVerfified {
	
	private InboxUseCaseContext ucCtx;

	public UcS2SAcceptFollowing(InboxUseCaseContext ucCtx) {
		super(ucCtx.getSignatureVerifier());
		this.ucCtx = ucCtx;
	}

	/**
	 * Process the usecase.
	 * @throws BadRequestException
	 */
	@Override
	public void process() throws BadRequestException {
		verify();
		
		if(ucCtx.getReceiverStore() instanceof ActorsStore) {
			ActorsStore as = (ActorsStore)ucCtx.getReceiverStore();
			as.addToInbox(ucCtx.getActivity(), "todo");
			
			ucCtx.getPendingFollowingCache().removePendingFollowActivity(getActivityIriToRemove());
			
			as.addFollowing(getFollower());
		}
	}
	
	private RdfPubIRI getFollower() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getObjects().stream().findFirst().orElseThrow().asConvertable().asActivity();
		RdfPubIRI followerIri = (RdfPubIRI)activity.getObject().stream().findFirst().orElseThrow();
		return followerIri;
	}

	private RdfPubBlankNodeOrIRI getActivityIriToRemove() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getObjects().stream().findFirst().orElseThrow().asConvertable().asActivity();
		RdfPubBlankNodeOrIRI activityIriToRemove = activity.getSubject();
		return activityIriToRemove;
	}
}
