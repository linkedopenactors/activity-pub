package org.linkedopenactors.rdfpub.app.auth;

import java.util.Optional;

public interface AuthenticatedActorHolder {
	Optional<AuthenticatedActor> getAuthenticatedActor();
	Authentication getAuthentication();
}
