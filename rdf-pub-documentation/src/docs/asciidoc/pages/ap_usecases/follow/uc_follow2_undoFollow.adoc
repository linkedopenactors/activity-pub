== Max sends undo follow request to jon

This UseCase shows the flow of undoing the follow activity of UseCase (link:uc_follow1_follow.html[uc_follow1_follow])
 
[NOTE]
====
The UseCase describes the flow of a Activity-Pub Server that implements https://codeberg.org/fediverse/fep/src/branch/main/fep/4ccd/fep-4ccd.md[fep-4ccd]
====

As a reminder, here is the follow activity again:

include::pages/ap_usecases/follow/uc_follow1_follow-follow_activity.adoc[]

the undo activty:

include::pages/ap_usecases/follow/uc_follow2_undoFollow-undo_activity.adoc[]

[IMPORTANT]
====
- The undo activity must have the target actor as a recipient. The target actor can be contained in one of the following predicates: `as:to, as:bto, as:cc, as:bcc, as:audience`
- The activity pub server (in our case rdfpub.org) is responsible for delivering the activities to the recipients of the activity. Even if the target server is currently unavailable. The activity should then be stored and sent at a later time.
====


[IMPORTANT]
====
TODO:
The undo activity and the activity being undone MUST both have the same actor. 
====

.uc_follow2_undoFollow: Max sends undo/follow request to jon 
[plantuml, uc_follow2_undoFollow_diagram1, png]
....
autonumber
actor @max@rdfpub.org
collections SomeApp
database "rdfpub.org" as rdfpub
database "max\noutbox" as max_out
database "mast.org" as mastodon

@max@rdfpub.org -> SomeApp: undo/follow @jon@mast.org
SomeApp -> max_out: as:Undo
rdfpub -> mastodon: as:Undo https://mast.org/max/inbox
....

1. The Actor @max@rdfpub.org tells SomeApp that he wants to undo following @jon@mast.org.
2. SomeApp sends a <<uc_follow2_undoFollow-undo_activity, C2S as:Undo Activity>> to the outbox of Actor @max@rdfpub.org.
3. The Activity-Pub Server 'rdfpub.org' sends a <<uc_follow2_undoFollow-undo_activity, S2S as:Undo Activity>> to the inbox of @jon@mast.org
4. The Activity-Pub Server 'rdfpub.org' removes the <<uc_follow2_undoFollow-undo_activity, as:Undo Activity>> from the pendingFollowing collection of Actor @max@rdfpub.org.