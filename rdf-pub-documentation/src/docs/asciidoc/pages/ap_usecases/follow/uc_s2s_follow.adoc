== @max@rdf-pub.org receives a follow request

This UseCase shows the flow of receiving a follow activity.
 
[NOTE]
====
The UseCase describes the flow of a Activity-Pub Server that implements https://codeberg.org/fediverse/fep/src/branch/main/fep/4ccd/fep-4ccd.md[fep-4ccd]
====

.uc_s2s_follow_json-ld
[source, json]
----
{
   "@context":"https://www.w3.org/ns/activitystreams",
   "id":"https://activitypub.academy/4d4070a9",
   "type":"Follow",
   "actor":"https://activitypub.academy/users/john",
   "object":"https://dev.rdf-pub.org/resource/max"
}
----

.uc_s2s_follow 
[plantuml, uc_s2s_follow_diagram1, png]
....
autonumber
database "activitypub.academy" as mast_server
control     UcS2SFollow
database "max\npending\nFollowers" as max_pending_followers


mast_server -> UcS2SFollow: as:Follow
UcS2SFollow -> max_pending_followers: as:Follow
....

1. Some Activity-Pub Server sends a as:follow request to the inbox of max.
2. rdf-pub delegates the incoming activity to UcS2SFollow 
3. UcS2SFollow adds the activity to the inbox collection. (??)
2. UcS2SFollow adds the activity to the PendingFollowers collection


== @max@rdf-pub.org accepts a follow request
.uc_c2s_accept_follow_json-ld
[source, json]
----
{
   "@context":"https://www.w3.org/ns/activitystreams",
   "id":"https://resource/accept_follow_1",
   "type":"Accept",   
   "object":"https://activitypub.academy/4d4070a9"
}
----

.uc_c2s_s2s_accept_follow 
[plantuml, uc_c2s_s2s_accept_follow_diagram1, png]
....
autonumber
actor max
participant "Client-App" as client_app

control     UcC2SAcceptFollow
database "max\npending\nFollowers" as max_pending_followers
database "max\nFollowers" as max_followers

max -> client_app : accept/follow
client_app -> UcC2SAcceptFollow : accept/follow
UcC2SAcceptFollow -> max_pending_followers : remove (https://activitypub.academy/4d4070a9)
UcC2SAcceptFollow -> max_followers : add (https://activitypub.academy/users/john)


....

----