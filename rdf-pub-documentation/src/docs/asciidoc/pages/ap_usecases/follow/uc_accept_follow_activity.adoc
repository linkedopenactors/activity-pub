.uc_accept_follow_activity
[source, json]
----
{
  "id" : "https://rdfpub.org/0818",
  "type" : [ "Accept" ],
  "actor" : "https://rdfpub.org/max",
  "attributedTo" : "https://rdfpub.org/max",
  "object" : {
    "id" : "https://rdfpub.org/0717",
    "type" : [ "Follow"],
    "actor" : "https://rdfpub.org/max",
    "attributedTo" : "https://rdfpub.org/max",
    "object" : "https://rdfpub.org/0815",
    "to" : "https://mast.org/jon"
  },
  "to" : "https://mast.org/jon",
  "@context" : [ "https://www.w3.org/ns/activitystreams" ]
}
----
