package org.linkedopenactors.rdfpub.adapter.http;

public class UnauthorizedException extends Exception {

	public UnauthorizedException() {
		super();
	}

	public UnauthorizedException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
