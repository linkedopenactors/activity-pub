package org.linkedopenactors.rdfpub.adapter.http;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;

public interface SignatureVerifierFactory {
	SignatureVerifier build(String body, HttpHeaders httpHeaders) throws UnauthorizedException, BadRequestException;
}
