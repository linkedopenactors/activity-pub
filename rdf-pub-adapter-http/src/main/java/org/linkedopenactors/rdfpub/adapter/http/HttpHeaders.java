package org.linkedopenactors.rdfpub.adapter.http;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import lombok.Data;

@Data
/**
 * These is a container, holding all values of the Activity Pub related HTTPSignature HTTP request.  
 */
public class HttpHeaders {
	/**
	 * The This is not only the signature! It's a csv with parameters, among others the signature itself.
	 */
	public static final String SIGNATURE = "signature";

	/**
	 * The Digest of the payload.
	 */
	public static final String DIGEST = "digest";

	/**
	 * The date.
	 */
	public static final String DATE = "date";

	/**
	 * The host that do/did the request.
	 */
	public static final String HOST = "host";

	public static final String USER_AGENT = "user-agent";
	
	/**
	 * The host that do/did the request.
	 */
	public static final String CONTENT_TYPE = "content-type";
	
	public static final String CONTENT_LENGTH = "content-length";
	
	public static final String REQUEST_TARGET = "(request-target)";

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z")
			.withLocale(Locale.forLanguageTag( "en-EN" ))
			.withZone(ZoneId.of("GMT")); // TODO can that be UTC ?? Because we need a ZoneOffset and there is only a ZoneOffset.UTC. that is a bit confusing

	private String host;
    private String date;
    private String digest;
    private String signature;
    private String contentType;
    private String requestTarget;
	private String contentLength;
	private String userAgent;
    
    public Optional<SignatureContainer> getSignatureContainer() {
    	return Optional.ofNullable(signature)
    		.map(SignatureContainer::new);  
    }
    
    /**
     * @return The parameters of this container as {@link Map}.
     */
    public Map<String, String> asMap() {
    	Map<String, String> map = new HashMap<>();
    	Optional.ofNullable(signature).ifPresent(val -> map.put(SIGNATURE, val));
    	Optional.ofNullable(digest).ifPresent(val -> map.put(DIGEST, val));
    	Optional.ofNullable(date).ifPresent(val -> map.put(DATE, val));
    	Optional.ofNullable(host).ifPresent(val -> map.put(HOST, val));
    	Optional.ofNullable(contentType).ifPresent(val -> map.put(CONTENT_TYPE, val));
    	Optional.ofNullable(contentLength).ifPresent(val -> map.put(CONTENT_LENGTH, val));
    	Optional.ofNullable(userAgent).ifPresent(val -> map.put(USER_AGENT, val));
    	return map;
    }

	public void initDateWithCurrentDateTime() {
    	date = LocalDateTime.now(ZoneId.of("GMT")).format(DATE_TIME_FORMATTER);
	}

	public LocalDateTime getDateAsLocalDateTime() {
		// TODO nullsave ???
		return LocalDateTime.parse(date, DATE_TIME_FORMATTER);
	}

	public Long getDateInMilliseconds() {
		// TODO nullsave ???
		return getDateAsLocalDateTime().toEpochSecond(ZoneOffset.UTC);
	}

	public void setRequestTarget(HttpMethod httpMethod, URL destination) {
		setRequestTarget(httpMethod, destination.getPath());
	}

	public void setRequestTarget(HttpMethod httpMethod, String path) {
		this.requestTarget = httpMethod.toString().toLowerCase() + " " + path;
	}
	
	public String toString() {
		StringBuilder sb  = new StringBuilder();
		Optional.ofNullable(host).ifPresent(x->sb.append("host : " + x).append("\n"));
		Optional.ofNullable(date).ifPresent(x->sb.append("date : " + x).append("\n"));
		Optional.ofNullable(digest).ifPresent(x->sb.append("digest : " + x).append("\n"));
		Optional.ofNullable(signature).ifPresent(x->sb.append("signature : " + x).append("\n"));
		Optional.ofNullable(contentType).ifPresent(x->sb.append("contentType : " + x).append("\n"));
		Optional.ofNullable(userAgent).ifPresent(x->sb.append(USER_AGENT + ": " + x).append("\n"));
		Optional.ofNullable(requestTarget).ifPresent(x->sb.append(REQUEST_TARGET +": " + x).append("\n"));
		return sb.toString();
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;		
	}
}
