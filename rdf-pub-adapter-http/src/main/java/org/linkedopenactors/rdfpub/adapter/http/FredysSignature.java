package org.linkedopenactors.rdfpub.adapter.http;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.util.List;

import org.tomitribe.auth.signatures.Base64;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FredysSignature extends AbstractFredysHttpSignature {

	public HttpHeaders getSignatureHeadersGet(final URL requestUri, final String publicKeyUrl, final PrivateKey privateKey) {
		HttpHeaders headers = getHttpHeadersGet(requestUri);
		List<String> required = List.of("(request-target)", "host", "date");
		String requiredHeadersString = "(request-target) host date"; // TODO get from avove list!!!!
		
		String signingString = createSigningString(required, headers);
		
		String sigedSignature = sign(privateKey, signingString);
		SignatureContainer sc = new SignatureContainer(publicKeyUrl, requiredHeadersString, sigedSignature);
		
		headers.setSignature(sc.getSignatureHeaderAttributeValue());
		return headers;
	}

	public HttpHeaders getSignatureHeadersPost(final String publicKeyUrl, final PrivateKey privateKey, final URL requestUri, String payload) {
		
		List<String> required = List.of("(request-target)", "host", "date", "digest");
		String requiredHeadersString = "(request-target) host date digest"; // TODO get from avove list!!!!
		log.debug("requiredHeadersString: " + requiredHeadersString);
		HttpHeaders headers = getHttpHeadersPost(requestUri, payload);
		
		log.debug("REQUEST: \n" + toRequestString(requestUri, payload, headers));
		
		String signingString = createSigningString(required, headers);
		
		log.debug("Signature Base: " + signingString);

		
		String sigedSignature = sign(privateKey, signingString);
		SignatureContainer sc = new SignatureContainer(publicKeyUrl, requiredHeadersString, sigedSignature);
		
		headers.setSignature(sc.getSignatureHeaderAttributeValue());
		
		log.debug("REQUEST: \n" + toRequestString(requestUri, payload, headers));
		
		return headers;
	}

	private String toRequestString(final URL requestUri, String payload, HttpHeaders headers) {
		StringBuilder sb = new StringBuilder();
		sb.append("POST " + requestUri.getPath() + " HTTP/1.1");
		headers.asMap().forEach((k,v)->sb.append("\n" + k + ": " + v));
		sb.append("\n\n");
		sb.append(payload);
		return sb.toString();
	}

	/**
	 * Signs the passed stringToSign with the passed privateKey and return it as Base64 encoded String.
	 * @param privateKey
	 * @param stringToSign
	 * @return signed 'stringToSign' as Base64 encoded String.
	 */
	private String sign(PrivateKey privateKey, String stringToSign) {
	    try {
			final java.security.Signature javaSignatureInstance = java.security.Signature.getInstance(SignatureContainer.DEFAULT_ALGORITHM.getJvmName());

			javaSignatureInstance.initSign(privateKey);
			javaSignatureInstance.update(stringToSign.getBytes(StandardCharsets.UTF_8));

			byte[] binarySignature = javaSignatureInstance.sign();
			final byte[] encoded = Base64.encodeBase64(binarySignature);
	        return new String(encoded, "UTF-8");
		} catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException | UnsupportedEncodingException e) {
			throw new IllegalStateException("error signing string", e);
		}
	}
	
	public HttpHeaders getHttpHeadersGet(URL url) {
		HttpHeaders h = new HttpHeaders();
		h.setRequestTarget(HttpMethod.GET, url);
		h.setHost(url.getAuthority());
		h.initDateWithCurrentDateTime();
		return h;
	}
	
	private HttpHeaders getHttpHeadersPost(URL url, String payload) {
		HttpHeaders h = new HttpHeaders();
		h.setRequestTarget(HttpMethod.POST, url);
		h.setHost(url.getAuthority());
		h.initDateWithCurrentDateTime();
		h.setDigest(getBase64EncoderMessageDigest(payload));
		return h;
	}
}
