package org.linkedopenactors.rdfpub.adapter.http;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;
import org.linkedopenactors.rdfpub.domain.security.VerificationResult;
import org.tomitribe.auth.signatures.Algorithm;
import org.tomitribe.auth.signatures.Base64;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class FredysVerifier extends AbstractFredysHttpSignature implements SignatureVerifier {

    /**
     * The maximum time skew between the client and the server.
     * This is used to validate the (created) and (expires) fields in the HTTP signature.
     */
    public static long maxAgeInMinutes;
    
    private List<String> required; // <- todo
    
    private Algorithm algorithm; // Algorithm.get(String - algorithm);
    
    private PublicKeyProvider publicKeyProvider;
    
    /**
      * The base-64 encoded value of the signature.
      */
    private String signature;

	private HttpHeaders httpHeaders;

	private String payload;

	public FredysVerifier(HttpHeaders httpHeaders, String payload, PublicKeyProvider publicKeyProvider) {
		this.httpHeaders = httpHeaders;
		this.payload = payload;
		this.publicKeyProvider = publicKeyProvider;
		SignatureContainer signatureContainer = httpHeaders.getSignatureContainer().orElseThrow(()->new IllegalStateException("missing 'signatureContainer' in httpHeaders!"));
		this.signature = signatureContainer.getSignedSignature();

		maxAgeInMinutes = 90;

		required = signatureContainer.getHeadersAsList();
		String algorithmString = Optional.ofNullable(signatureContainer.getAlgorithm()).orElseThrow(()->new IllegalStateException("missing 'algorithm' in signatureContainer!"));
		this.algorithm = Algorithm.get(algorithmString);
	}   

    public VerificationResult verify() 
    {
    	VerificationResult result = verifySignatureValidityDates();
    	
    	result = verifyDigest();
    	
    	if(result.succeeded()) {
	        final String signingString = createSigningString(required, httpHeaders);
	        log.debug("signingString: \n" + signingString);
	        result = verify(signingString.getBytes());
    	}
    	return result;
    }

	private VerificationResult verifyDigest() {
		VerificationResult result = VerificationResult.ok();
		String digest = httpHeaders.getDigest();
		if(digest!=null) {
			String myDigest = getBase64EncoderMessageDigest(payload).substring("SHA-256=".length());
			String thereDigest = digest.substring("SHA-256=".length());
			log.trace("thereDigest: " + thereDigest);
			log.trace("myDigest:	" + myDigest);
			if(!myDigest.equals(thereDigest)) {
				result = VerificationResult.failed("Digest mismatch");
			}
		}
		return result;
	}
    
    private VerificationResult verify(final byte[] signingStringBytes) {
        try {

            final java.security.Signature javaSignatureInstance = java.security.Signature.getInstance(algorithm.getJvmName());

            javaSignatureInstance.initVerify(publicKeyProvider.provide());
            javaSignatureInstance.update(signingStringBytes);
            boolean sidVerified = javaSignatureInstance.verify(Base64.decodeBase64(signature.getBytes()));
            if(sidVerified) {
            	return VerificationResult.ok();
            } else {
            	return VerificationResult.failed("signature string is not valid.");
            }
        } catch (final NoSuchAlgorithmException e) {
        	log.error("unable to verify signature. UnsupportedAlgorithm " + algorithm.getJvmName(), e);
        	return VerificationResult.failed("unsupportedAlgorithm " + algorithm.getJvmName());

        } catch (final Exception e) {
        	log.error("unable to verify signature", e);
        	return VerificationResult.failed("signature string is not valid.");
        }
    }
    
    /**
     * Verify the signature is valid with regards to the (created) and (expires) fields.
     *
     * When the '(created)' field is present in the HTTP signature, the '(created)' field
     * represents the date when the signature has been created.
     * When the '(expires)' field is present in the HTTP signature, the '(expires)' field
     * represents the date when the signature expires.
     */
    private VerificationResult verifySignatureValidityDates() {
    	LocalDateTime now = httpHeaders.getDateAsLocalDateTime();
		if(httpHeaders.getDateAsLocalDateTime().plusMinutes(maxAgeInMinutes).isBefore(now)) {
        	return VerificationResult.failed("Signature has expired");
        }
        return VerificationResult.ok();
    }
}

