package org.linkedopenactors.rdfpub.adapter.http;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.PublicKey;
import java.util.Optional;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.external.DiscoverExternalActor;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Profile("!test")
@Service
@Slf4j
public class SignatureVerifierFactoryDefault implements SignatureVerifierFactory {

	private DiscoverExternalActor discoverExternalActor;

	public SignatureVerifierFactoryDefault(DiscoverExternalActor discoverExternalActor) {
		this.discoverExternalActor = discoverExternalActor;
	}
	
	public SignatureVerifier build(String body, HttpHeaders httpHeaders) throws UnauthorizedException, BadRequestException{
		Optional<SignatureContainer> signatureContainerOpt = httpHeaders.getSignatureContainer();
		if(signatureContainerOpt.isEmpty()) {
			throw new UnauthorizedException("no signature header.");
		}
		SignatureContainer signatureContainer = signatureContainerOpt.orElseThrow();
		
		log.trace("httpSignatureHeaders: " + httpHeaders);
		log.trace("signatureHeader: " + signatureContainer);
		
		String keyId = signatureContainer.getKeyId();
		URI keyIdUrl;
		try {
			keyIdUrl = new URI(keyId);
			keyIdUrl.toURL(); // check if it'S a valid url
			keyIdUrl = removeFragment(keyIdUrl);
		} catch (MalformedURLException | URISyntaxException e) {
			String msg = "keyId("+keyId+") is no valid url.";
			log.error(msg, e);			
			throw new BadRequestException(msg);
		} 
		SignatureVerifier fredysVerifier = new FredysVerifier(httpHeaders, body, createPublicKeyProvider(keyIdUrl.toString()));
		return fredysVerifier;
	}

	private URI removeFragment(URI uri) throws URISyntaxException {
		return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), null);
	}
	
	private PublicKeyProvider createPublicKeyProvider(String keyIdUrlAsString) {
		return new PublicKeyProvider() {
			
			@Override
			public PublicKey provide() throws BadRequestException {
				Optional<PublicKey> pk = discoverExternalActor.discoverPublicKey(keyIdUrlAsString);
				if(pk.isEmpty()) {
					throw new BadRequestException("we cannot find an publicKey in the request");
				}
				return pk.orElseThrow();
			}
		};
	}
}
