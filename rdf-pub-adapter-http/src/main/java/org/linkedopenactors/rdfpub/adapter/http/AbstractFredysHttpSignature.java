package org.linkedopenactors.rdfpub.adapter.http;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tomitribe.auth.signatures.Join;
import org.tomitribe.auth.signatures.MissingRequiredHeaderException;

public class AbstractFredysHttpSignature {

	/**
	 * Create a string that has to be added to the signature in a signed way.
	 * @param required The list of headers that should be included in the HTTP signature.
	 * @param headersParam values of the required header params to use.
	 * @return Sting to sign
	 */
    protected String createSigningString(final List<String> required, HttpHeaders headersParam) {

    	Map<String, String> headers = lowercase(headersParam.asMap());

        final List<String> list = new ArrayList<String>(required.size());

        for (final String key : required) {
            if ("(request-target)".equals(key)) {
                list.add("(request-target): " + headersParam.getRequestTarget());
            } else {
	            final String value = headers.get(key);
	            if (value == null) throw new MissingRequiredHeaderException(key);
	            list.add(key + ": " + value);
            }
        }
        return Join.join("\n", list);
    }

    private static Map<String, String> lowercase(final Map<String, String> headers) {
        final Map<String, String> map = new HashMap<String, String>();
        for (final Map.Entry<String, String> entry : headers.entrySet()) {
            map.put(entry.getKey().toLowerCase(), entry.getValue());
        }

        return map;
    }

	protected String getBase64EncoderMessageDigest(String payload) {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA256");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("", e);
		}

		String digest;
    	try {
	        final byte[] hash = messageDigest.digest(payload.getBytes("UTF-8"));
	        String base64Sha256 = Base64.getEncoder().encodeToString(hash);
			digest = "sha-256=" + base64Sha256;
		} catch (Exception e) {
			throw new IllegalStateException("unable to generate digest.", e);
		}
		return digest;
	}
}
