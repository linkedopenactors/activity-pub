package org.linkedopenactors.rdfpub.adapter.http;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.tomitribe.auth.signatures.Algorithm;

import lombok.Data;

/**
 * This is a container that containingt values of the signature HTTP Header.
 */
@Data
public class SignatureContainer {
	public static final Algorithm DEFAULT_ALGORITHM = Algorithm.RSA_SHA256;
	private static final String SIGNATURE = "signature";
	private static final String KEY_ID = "keyId";
	private static final String HEADERS = "headers";
	private static final String ALGORYTHM = "algorithm";
	
	private String keyId;
	private String headers;
	private String signedSignature;
	private String algorithmString;

	/**
	 * @param signatureHeaderAttribute The attribute named "signature" from the HTTP Header.
	 */
	public SignatureContainer(String signatureHeaderAttribute) {
		Map<String,String> map = toMap(signatureHeaderAttribute);
		this.keyId = Optional.ofNullable(map.get(KEY_ID)).map(this::removeApostrophe).orElse(null);
		this.headers = Optional.ofNullable(map.get(HEADERS)).map(this::removeApostrophe).orElse(null);
		this.signedSignature = Optional.ofNullable(map.get(SIGNATURE)).map(this::removeApostrophe).orElse(null);
		this.algorithmString = Optional.ofNullable(map.get(ALGORYTHM)).map(this::removeApostrophe).orElse(null);
	}
	
	public SignatureContainer(String keyId, String requiredHeaders, String signedSignature) {
		this(keyId, requiredHeaders, signedSignature, DEFAULT_ALGORITHM);
	}
	
	public SignatureContainer(String keyId, String requiredHeaders, String signedSignature, Algorithm algorithm) {
		this.keyId = keyId;
		this.headers = requiredHeaders;
		this.signedSignature = signedSignature;
		this.algorithmString = algorithm.getJvmName();
	}

	private String removeApostrophe(String text) {
		String result = text;
		if(text.startsWith("\"")) {
			result = text.substring(1, text.length()-1);
		}
		return result;
	}
	
    public String getEncryptionAlgorithm() {
    	String algorithm = getAlgorithm();
    	if("hs2019".equals(algorithm)) {
    		algorithm = "sha256"; 
    	}
		return algorithm;
    }

	private Map<String,String> toMap(String signature) {
		Map<String,String> map = new HashMap<>();
		String str[] = signature.split(",");
        for(int i=0;i<str.length;i++){
        	int seperator = str[i].indexOf("=");
        	String p1 = str[i].substring(0,  seperator);
        	String p2 = str[i].substring(seperator+1);
            map.put(p1, p2);
        }
        return map;
	}

	public List<String> getHeadersAsList() {
		List<String> result = Arrays.asList(headers.split(" ", -1));
		return result;
	}

	public String getAlgorithm() {
		return Optional.ofNullable(algorithmString)
				.orElse(DEFAULT_ALGORITHM.getJvmName());	
	}
	
	public String getSignatureHeaderAttributeValue() {
		return "keyId=\"" + keyId + "\",headers=\""+headers+"\",signature=\""+signedSignature + "\"";
	}
}
