package org.linkedopenactors.rdfpub.adapter.http;

import java.security.PublicKey;

import org.linkedopenactors.rdfpub.app.BadRequestException;

/**
 * We have to architectonally seperate the determination of the publicKey from the verification of data.
 */
public interface PublicKeyProvider {
	public PublicKey provide() throws BadRequestException;
}
