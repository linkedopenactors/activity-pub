package org.linkedopenactors.rdfpub.adapter.http;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.actor.KeyPairGenerator;
import org.linkedopenactors.rdfpub.domain.security.VerificationResult;

import lombok.extern.slf4j.Slf4j;


@Slf4j
class TestHttpSignature {

	String actorsPublicKeyId = "https://example.or/max/publicKey";
	String sendersIdentifier = "0815";
	URL receiversInbox;
	String jsonLdBody = "{\n"
			+ "  \"@context\": \"https://www.w3.org/ns/activitystreams\",\n"
			+ "  \"type\": \"Note\",\n"
			+ "  \"content\": \"This is a note\",\n"
			+ "  \"published\": \"2015-02-10T15:04:55Z\",\n"
			+ "  \"to\": [\"https://example.org/~john/\"],\n"
			+ "  \"cc\": [\"https://example.com/~erik/followers\",\n"
			+ "         \"https://www.w3.org/ns/activitystreams#Public\"]\n"
			+ "}";

	private KeyPairGenerator keyPairGenerator;

	@BeforeEach
	public void setup() {
		keyPairGenerator = new KeyPairGeneratorDefault("target");
		keyPairGenerator.generate(sendersIdentifier);
		try {
			receiversInbox = new URI("https://example.org/max/inbox").toURL();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void test() throws IOException {
		PrivateKey privateKey = keyPairGenerator.getPrivateKey(sendersIdentifier);
		String publicKeyUrl = "http://example.org/somePk";
		
		FredysSignature fredysSignature = new FredysSignature();
		org.linkedopenactors.rdfpub.adapter.http.HttpHeaders headers = fredysSignature.getSignatureHeadersPost(publicKeyUrl, privateKey, receiversInbox, jsonLdBody);
		
		log.info("headers: " + headers);

		log.info("===================== verify =====================");
		
		FredysVerifier fredysVerifier = new FredysVerifier(headers, jsonLdBody, getPublicKeyProvider());
		VerificationResult verified = fredysVerifier.verify();
		log.info("verified: " + verified);
	}
	private PublicKeyProvider getPublicKeyProvider() {
		return new PublicKeyProvider() {
			
			@Override
			public PublicKey provide() throws BadRequestException {
				return keyPairGenerator.getPublicKey(sendersIdentifier);
			}
		};
	}
}
