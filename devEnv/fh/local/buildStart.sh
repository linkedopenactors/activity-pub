#!/bin/sh
set echo on

sudo docker stop $(sudo docker ps -aq)
docker rmi $(docker images -f "dangling=true" -q) --force

export SLR_BUILD_HOME=`pwd`
export WORKSPACE=/home/fredy/ide/ws/loa
 

#. ../buildKeycloakSetup.sh
. ../buildApp.sh

#docker-compose pull keycloak
#docker-compose pull loa-admin-server
docker-compose up 
